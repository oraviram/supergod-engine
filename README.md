## OVERVIEW:
This is a WIP game engine.     
Currently it only allows for basic graphics, input and maths.    
Can only be very useful for tests, so unless you don't feel like giving feedback...don't bother with it. If you want to make actual games then use something like Unity or Unreal. :P

## SETUP:

To set up the the project, simply open the solution. You will see 3 projects:

#### Supergod Sandbox   
      This originally was supposed to be called "Supergod's Sandbox", but assembly names can't have ' in them... :/ Anyways, this is a sandbox game used to test the engine.     
      Feel free to mess with this project and make your own basic game there using the SupergodEngine dll.
	
#### Supergod Engine
      This is the core engine. This is where you can look at the files and see implementations, and maybe even mess with it.     
      It outputs a dll you should include in the games made with the engine.

#### Supergod Engine Testing
      This is a project containing all the unit tests for the engine. This is pretty useless for most users, so don't mind it unless you are REALLY bored.

Now you should set up the dependinces. This is simple: SupergodSandbox and SupergodEngineTesting both depend on SupergodEngine.   
For the start up project, use SupergodSandbox.

## Roadmap
There is no real roadmap, but I have a Trello board you can follow to see what will come in the future. https://trello.com/b/TuTkhr5N/supergod-engine

## CONTACT
If you need any help or have any questions, feel free to contact me at preferably Discord (oraviram#0071), or Email (or2.aviram@gmail.com).