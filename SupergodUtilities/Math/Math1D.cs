﻿using System;

namespace SupergodUtilities.Math
{
    using Math = System.Math;
    
    /// <summary>
    /// A 1D float math library. For more complex and general maths, see <seealso cref="SMath"/>.
    /// </summary>
    public static class Math1D
    {
        /// <summary>The ratio between a circle's diameter and its circumference.</summary>
        public const float PI = 3.1415926535897932f;
        
        /// <summary>The E constant (base for the natural logarithm).</summary>
        public const float E = 2.71828182846f;

        /// <summary>A tiny floating point number that is bigger than zero. Default value used for the threshold in CloseEnough methods.</summary>
        public const float Epsilon = 0.0001f;

        public const float Infinity = 1f / 0f;
        public const float NegativeInfinity = -Infinity;
        public const float NaN = 0f / 0f;
        
        /// <summary>I found myself calculating this way too many times, so I decided to make it a constant... sqrt(2)/2.</summary>
        public const float Sqrt2Over2 = 0.70710678118f;
        public const float Sqrt2Over3 = Sqrt2 / 3;
        public const float Sqrt2Over10 = Sqrt2 / 10;
        public const float Sqrt2 = Sqrt2Over2 * 2;

        #region Supergod Mathematics.
        struct SMathStruct : IAbsolutable<float>
        {
            private float value;

            public SMathStruct(float value)
            {
                this.value = value;
            }

            public float Abs
            {
                get
                {
                    if (value < 0)
                        return -value;

                    return value;
                }
            }
        }

        /// <summary>
        /// Gets the absolute value of value.
        /// </summary>
        public static float Abs(this float value)
        {
            return new SMathStruct(value).Abs;
        }
        #endregion

        /// <summary>
        /// Returns the sign of value (+ or -).
        /// </summary>
        /// <returns>1 if value is positive or 0, -1 if value is negative.</returns>
        public static float Sign(this float value)
        {
            return value < 0 ? -1 : 1;
        }

        /// <summary>
        /// Gets the factorial value of n.
        /// </summary>
        /// <exception cref="ArgumentException">Argument n is negative.</exception>
        public static int Factorial(this int n)
        {
            if (n == 0 || n == 1)
                return 1;
            
            if (n < 0)
                throw new ArgumentException("Parameter n in method SMath.Factorial cannot be negative.", "n");

            return Factorial(n - 1) * n;
        }

        public static float Average(params float[] numbers)
        {
            float result = 0;
            for (int i = 0; i < numbers.Length; i++)
                result += numbers[i];

            result /= numbers.Length;
            return result;
        }

        /// <summary>
        /// Is value NaN?
        /// </summary>
        /// <returns>True if value is NOT equal to value.</returns>
        public static bool IsNaN(this float value)
        {
            #pragma warning disable CS1718 // Comparison made to same variable
            return value != value;
        }

        #region Rounding methods.
        /// <summary>
        /// Rounds value so it only has decimals numebers after the decimal point.
        /// </summary>
        public static float Round(this float value, int decimals)
        {
            return (float)Math.Round(value, decimals);
        }

        /// <summary>
        /// Rounds value to 0 decimal places.
        /// </summary>
        public static float Round(this float value)
        {
            return value.Round(0);
        }
        #endregion

        #region Wrap methods.
        /// <summary>
        /// Wraps value so when it gets smaller than min by n it wraps back to max-n again and if it gets bigger than max by n it wraps back to min+n.
        /// </summary>
        /// <param name="value">The value to wrap.</param>
        /// <param name="min">The minimum value in the range.</param>
        /// <param name="max">The maximum value in the range.</param>
        /// <returns>Value wrapped between min and max.</returns>
        public static float Wrap(this float value, float min, float max)
        {
            float minToMaxRange = Abs(max - min);
            if (value > max)
            {
                while (value > max)
                    value -= minToMaxRange;
            }
            else
            {
                while (value < min)
                    value += minToMaxRange;
            }
            return value;
        }

        /// <summary>
        /// Wraps value between 0 to length. See <seealso cref="Wrap(float, float, float)"/>.
        /// </summary>
        public static float Wrap(this float value, float length)
        {
            return value.Wrap(0, length);
        }
        #endregion

        #region Powers, roots, exponentionals and logarithms.
        /// <summary>
        /// Gets the square root of x.
        /// </summary>
        public static float Sqrt(this float x)
        {
            return Root(x, 2);
        }
        
        /// <summary>
        /// Gets the n'th root of x.
        /// </summary>
        public static float Root(this float x, float n)
        {
            return Pow(x, 1 / n);
        }

        /// <summary>
        /// Resturns e raised to x.
        /// </summary>
        public static float Exp(this float x)
        {
            return E.Pow(x);
        }

        /// <summary>
        /// Returns x to the power of y. (Doesn't support decimal exponents yet.)
        /// </summary>
        public static float Pow(this float baseValue, float exponentValue)
        {
            return (float)Math.Pow(baseValue, exponentValue);

            // This is old implementation I tried to do myself, but sadly it didn't work with floats. Anyways, I will make my own implementation later.
            //float result = 1;

            //if (exponentValue > 0)
            //    for (int i = 0; i < exponentValue; i++)
            //        result *= baseValue;
            //else
            //    for (int i = 0; i < Abs(exponentValue); i++)
            //        result /= baseValue;

            //return result;
        }

        /// <summary>
        /// Gets value to the power of 2 (faster than Pow(value, 2)). Same as multiplying it by itself.
        /// </summary>
        public static float Squared(this float value)
        {
            return value * value;
        }

        /// <summary>
        /// Gets value to the power of 3 (faster than Pow(value, 3)). Same as multiplying it by itself 3 times.
        /// </summary>
        public static float Cubed(this float value)
        {
            return value * value * value;
        }
        
        /// <summary>
        /// What to the power of logBase equals x?
        /// </summary>
        /// <param name="x">Number to try to get to.</param>
        /// <param name="logBase">Base of the logarithm.</param>
        public static float Log(float x, float logBase)
        {
            return (float)Math.Log(x, logBase);
        }

        /// <summary>
        /// 10 to the power of what equals x?
        /// </summary>
        public static float Log10(this float x)
        {
            return Log(x, 10);
        }

        /// <summary>
        /// E to the power of what equals x?
        /// </summary>
        public static float Ln(this float x)
        {
            return Log(x, E);
        }
        #endregion

        #region Min/Max.
        /// <summary>
        /// Returns the bigger number between the two.
        /// </summary>
        public static float Max(float a, float b)
        {
            if (a >= b)
                return a;

            return b;
        }

        /// <summary>
        /// Returns the biggest number in the array.
        /// </summary>
        public static float Max(params float[] values)
        {
            if (values.Length <= 0)
            {
                throw new ArgumentException("The array values in method SMath.Max must have at least one value!", "values");
            }

            float biggest = NegativeInfinity;
            for (int i = 0; i < values.Length; i++)
            {
                if (biggest < values[i])
                    biggest = values[i];
            }
            return biggest;
        }

        /// <summary>
        /// Returns the smaller number between the two.
        /// </summary>
        public static float Min(float a, float b)
        {
            if (a <= b)
                return a;

            return b;
        }

        /// <summary>
        /// Returns the smallest number in the array.
        /// </summary>
        public static float Min(params float[] values)
        {
            if (values.Length <= 0)
            {
                throw new ArgumentException("The array values in method SMath.Min must have at least one value!", "values");
            }

            float smallest = Infinity;
            for (int i = 0; i < values.Length; i++)
            {
                if (smallest > values[i])
                    smallest = values[i];
            }
            return smallest;
        }
        #endregion

        #region Trig Functions.
        /// <summary>
        /// Gets the sine of angle angle.
        /// </summary>
        /// <param name="theta">The angle in radians to get the sine of.</param>
        public static float Sin(float theta)
        {
            return (float)Math.Sin(theta);
        }

        /// <summary>
        /// Gets the cosine of angle angle.
        /// </summary>
        /// <param name="theta">The angle in radians to get the cosine of.</param>
        public static float Cos(float theta)
        {
            return Sin(theta + PI / 2);
        }

        /// <summary>
        /// Gets the tangent of angle angle.
        /// </summary>
        /// <param name="theta">The angle in radians to get the tangent of.</param>
        public static float Tan(float theta)
        {
            return Sin(theta) / Cos(theta);
        }

        /// <summary>
        /// Finds the angle in radians whos cosine is cos.
        /// </summary>
        public static float Acos(float cos)
        {
            return (float)Math.Acos(cos);
        }

        /// <summary>
        /// Finds the angle in radians whos sine is sin.
        /// </summary>
        public static float Asin(float cos)
        {
            return (float)Math.Asin(cos);
        }

        /// <summary>
        /// Finds the angle in radians whos tangent is tan.
        /// </summary>
        public static float Atan(float tan)
        {
            return (float)Math.Atan(tan);
        }

        /// <summary>
        /// Finds the angle in radians whos tangent is y/x.
        /// </summary>
        public static float Atan2(float y, float x)
        {
            return (float)Math.Atan2(y, x);
        }
        #endregion
    }
}