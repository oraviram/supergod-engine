﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// Represents a 4D (4 by 4) matrix. Visual representation looks like this:
    /// <para/>
    /// [r0c0, r0c1, r0c2, r0c3]
    /// <para/>
    /// [r1c0, r1c1, r1c2, r1c3]
    /// <para/>
    /// [r2c0, r2c1, r2c2, r2c3]
    /// <para/>
    /// [r3c0, r3c1, r3c2, r3c3]
    /// </summary>
    public struct Matrix4x4 : IMatrix<Matrix4x4, Vector4D>, IRotator<Matrix4x4>, ITransformer<Matrix4x4>
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Matrix4x4));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        #region Presets for common matrices.
        /// <summary>
        /// The identity matrix:
        /// <para>[1 0 0 0]</para>
        /// <para>[0 1 0 0]</para>
        /// <para>[0 0 1 0]</para>
        /// <para>[0 0 0 1]</para>
        /// </summary>
        public static readonly Matrix4x4 identity = new Matrix4x4(1);

        /// <summary>
        /// A 4 by 4 matrix with all of its components set to 0:
        /// <para>[0 0 0 0]</para>
        /// <para>[0 0 0 0]</para>
        /// <para>[0 0 0 0]</para>
        /// <para>[0 0 0 0]</para>
        /// </summary>
        public static readonly Matrix4x4 zero = new Matrix4x4();
        #endregion

        public float r0c0, r0c1, r0c2, r0c3;
        public float r1c0, r1c1, r1c2, r1c3;
        public float r2c0, r2c1, r2c2, r2c3;
        public float r3c0, r3c1, r3c2, r3c3;

        /// <summary>
        /// Makes a new 4 by 4 matrix with specified values for each element of it.
        /// </summary>
        public Matrix4x4(
            float r0c0 = 1, float r0c1 = 0, float r0c2 = 0, float r0c3 = 0,
            float r1c0 = 0, float r1c1 = 1, float r1c2 = 0, float r1c3 = 0,
            float r2c0 = 0, float r2c1 = 0, float r2c2 = 1, float r2c3 = 0,
            float r3c0 = 0, float r3c1 = 0, float r3c2 = 0, float r3c3 = 1)
        {
            this.r0c0 = r0c0; this.r0c1 = r0c1; this.r0c2 = r0c2; this.r0c3 = r0c3;
            this.r1c0 = r1c0; this.r1c1 = r1c1; this.r1c2 = r1c2; this.r1c3 = r1c3;
            this.r2c0 = r2c0; this.r2c1 = r2c1; this.r2c2 = r2c2; this.r2c3 = r2c3;
            this.r3c0 = r3c0; this.r3c1 = r3c1; this.r3c2 = r3c2; this.r3c3 = r3c3;
        }

        /// <summary>
        /// Creates a new matrix with vectors for the rows.
        /// </summary>
        public static Matrix4x4 FromRows(Vector4D firstRow, Vector4D secondRow, Vector4D thirdRow, Vector4D fourthRow)
        {
            return new Matrix4x4(
                firstRow.x, firstRow.y, firstRow.z, firstRow.w,
                secondRow.x, secondRow.y, secondRow.z, secondRow.w,
                thirdRow.x, thirdRow.y, thirdRow.z, thirdRow.w,
                fourthRow.x, fourthRow.y, fourthRow.z, fourthRow.w
                );
        }

        /// <summary>
        /// Creates a new matrix with vectors for the columns.
        /// </summary>
        public static Matrix4x4 FromColumns(Vector4D firstColumn, Vector4D secondColumn, Vector4D thirdColumn, Vector4D fourthColumn)
        {
            return new Matrix4x4(
                firstColumn.x, secondColumn.x, thirdColumn.x, fourthColumn.x,
                firstColumn.y, secondColumn.y, thirdColumn.y, fourthColumn.y,
                firstColumn.z, secondColumn.z, thirdColumn.z, fourthColumn.z,
                firstColumn.w, secondColumn.w, thirdColumn.w, fourthColumn.w
                );
        }

        #region As arrays.
        /// <summary>
        /// Gets the elements of the matrix as a 2D float array.
        /// </summary>
        public float[,] AsFloatArray => new float[,]
        {
            { r0c0, r0c1, r0c2, r0c3 },
            { r1c0, r1c1, r1c2, r1c3 },
            { r2c0, r2c1, r2c2, r2c3 },
            { r3c0, r3c1, r3c2, r3c3 },
        };

        /// <summary>
        /// Gets the rows of the matrix as a vector array.
        /// </summary>
        public Vector4D[] Rows => new Vector4D[]
        {
            new Vector4D(r0c0, r0c1, r0c2, r0c3),
            new Vector4D(r1c0, r1c1, r1c2, r1c3),
            new Vector4D(r2c0, r2c1, r2c2, r2c3),
            new Vector4D(r3c0, r3c1, r3c2, r3c3),
        };

        /// <summary>
        /// Gets the columns of the matrix as a vector array.
        /// </summary>
        public Vector4D[] Columns => new Vector4D[]
        {
            new Vector4D(r0c0, r1c0, r2c0, r3c0),
            new Vector4D(r0c1, r1c1, r2c1, r3c1),
            new Vector4D(r0c2, r1c2, r2c2, r3c2),
            new Vector4D(r0c3, r1c3, r2c3, r3c3),
        };
        #endregion

        #region Getters and setters.
        private const string INDEX_OUT_OF_RANGE_MESSAGE = "The column/row of the get/set methods in Matrix4x4 must be either 0, 1, 2 or 3!";

        /// <summary>
        /// Gets/sets an element at the row of row and column of column.
        /// </summary>
        public float this[int row, int column]
        {
            get => GetRow(row)[column];
            set
            {
                Vector4D newRow = GetRow(row);
                newRow[column] = value;
                SetRow(row, newRow);
            }
        }

        /// <summary>
        /// Gets a column at the index of index.
        /// </summary>
        /// <param name="index">The index of the column (from left to right, must be either 0, 1, 2 or 3).</param>
        /// <exception cref="IndexOutOfRangeException">Index was something other than 0, 1, 2 and 3.</exception>
        public Vector4D GetColumn(int index)
        {
            switch (index)
            {
                case 0:
                    return new Vector4D(r0c0, r1c0, r2c0, r3c0);

                case 1:
                    return new Vector4D(r0c1, r1c1, r2c1, r3c1);

                case 2:
                    return new Vector4D(r0c2, r1c2, r2c2, r3c2);

                case 3:
                    return new Vector4D(r0c3, r1c3, r2c3, r3c3);
            }
            throw new IndexOutOfRangeException(INDEX_OUT_OF_RANGE_MESSAGE);
        }

        /// <summary>
        /// Gets a row at the index of index.
        /// </summary>
        /// <param name="index">The index of the column (from top to bottom, must be either 0, 1, 2 or 3).</param>
        /// <exception cref="IndexOutOfRangeException">Index was something other than 0, 1, 2 and 3.</exception>
        public Vector4D GetRow(int index)
        {
            switch (index)
            {
                case 0:
                    return new Vector4D(r0c0, r0c1, r0c2, r0c3);

                case 1:
                    return new Vector4D(r1c0, r1c1, r1c2, r1c3);

                case 2:
                    return new Vector4D(r2c0, r2c1, r2c2, r2c3);

                case 3:
                    return new Vector4D(r3c0, r3c1, r3c2, r3c3);
            }
            throw new IndexOutOfRangeException(INDEX_OUT_OF_RANGE_MESSAGE);
        }

        /// <summary>
        /// Sets a column at the index of index to value.
        /// </summary>
        /// <param name="index">The index of the column (from left to right, must be either 0, 1, 2 or 3).</param>
        /// <exception cref="IndexOutOfRangeException">Index was something other than 0, 1, 2 and 3.</exception>
        public void SetColumn(int index, Vector4D value)
        {
            switch (index)
            {
                case 0:
                    r0c0 = value.x;
                    r1c0 = value.y;
                    r2c0 = value.z;
                    r3c0 = value.w;
                    break;

                case 1:
                    r0c1 = value.x;
                    r1c1 = value.y;
                    r2c1 = value.z;
                    r3c1 = value.w;
                    break;

                case 2:
                    r0c2 = value.x;
                    r1c2 = value.y;
                    r2c2 = value.z;
                    r3c2 = value.w;
                    break;

                case 3:
                    r0c3 = value.x;
                    r1c3 = value.y;
                    r2c3 = value.z;
                    r3c3 = value.w;
                    break;

                default:
                    throw new IndexOutOfRangeException(INDEX_OUT_OF_RANGE_MESSAGE);
            }
        }

        /// <summary>
        /// Sets a row at the index of index to value.
        /// </summary>
        /// <param name="index">The index of the column (from top to bottom, must be either 0, 1, 2 or 3).</param>
        /// <exception cref="IndexOutOfRangeException">Index was something other than 0, 1, 2 and 3.</exception>
        public void SetRow(int index, Vector4D value)
        {
            switch (index)
            {
                case 0:
                    r0c0 = value.x;
                    r0c1 = value.y;
                    r0c2 = value.z;
                    r0c3 = value.w;
                    break;

                case 1:
                    r1c0 = value.x;
                    r1c1 = value.y;
                    r1c2 = value.z;
                    r1c3 = value.w;
                    break;

                case 2:
                    r2c0 = value.x;
                    r2c1 = value.y;
                    r2c2 = value.z;
                    r2c3 = value.w;
                    break;

                case 3:
                    r3c0 = value.x;
                    r3c1 = value.y;
                    r3c2 = value.z;
                    r3c3 = value.w;
                    break;

                default:
                    throw new IndexOutOfRangeException(INDEX_OUT_OF_RANGE_MESSAGE);
            }
        }
        #endregion

        #region Basic transformations and conversions from other types of rotations + conversion from quaternion.
        /// <summary>
        /// Creates a rotation matrix from a rotation (unit length) quaternion.
        /// The quaternion MUST BE a rotation (unit length) quaternion.
        /// </summary>
        /// <param name="rotationQuaternion">Rotation quaternion to construct the rotation from..</param>
        public static Matrix4x4 FromRotationQuaternion(Quaternion rotationQuaternion)
        {
            if (!rotationQuaternion.IsUnitLength)
                throw new ArgumentException("rotationQuaternion must be a unit length quaternion to create a 4x4 rotation matrix from it! Consider normalize it.", "rotationQuaternion");

            float xx = rotationQuaternion.x.Squared();
            float yy = rotationQuaternion.y.Squared();
            float zz = rotationQuaternion.z.Squared();

            float xy = rotationQuaternion.x * rotationQuaternion.y;
            float wz = rotationQuaternion.w * rotationQuaternion.z;
            float xz = rotationQuaternion.x * rotationQuaternion.z;
            float wy = rotationQuaternion.w * rotationQuaternion.y;
            float yz = rotationQuaternion.y * rotationQuaternion.z;
            float wx = rotationQuaternion.w * rotationQuaternion.x;

            return new Matrix4x4(
                1 - 2 * (yy + zz), 2 * (xy - wz), 2 * (xz + wy), 0,
                2 * (xy + wz), 1 - 2 * (zz + xx), 2 * (yz - wx), 0,
                2 * (xz - wy), 2 * (yz +  wx), 1 - 2 * (yy + xx), 0,
                0, 0, 0, 1
                );

            //float w = quaternion.w;
            //float x = quaternion.x;
            //float y = quaternion.y;
            //float z = quaternion.z;
            //return new Matrix4x4(
            //    w, -x, -y, -z,
            //    x,  w, -z,  y,
            //    y,  z,  w, -x,
            //    z, -y,  x,  w);
        }

        /// <summary>
        /// Creates a scale matrix in 4D space.
        /// This is NOT RECOMMENDED to use. If a 4D scale is needed...wait, why do you need to scale something in 4D?
        /// </summary>
        /// <param name="x">The scale on the x axis.</param>
        /// <param name="y">The scale on the y axis.</param>
        /// <param name="z">The scale on the z axis.</param>
        /// <param name="w">The scale on the w axis.</param>
        public static Matrix4x4 Scale4D(float x, float y, float z, float w)
        {
            return new Matrix4x4(
                x, 0, 0, 0,
                0, y, 0, 0,
                0, 0, z, 0,
                0, 0, 0, w);
        }

        /// <summary>
        /// Creates a scale matrix in 4D space.
        /// This is NOT RECOMMENDED to use. If a 4D scale is needed...wait, why do you need to scale something in 4D?
        /// </summary>
        /// <param name="scale">The scale to make on the x, y, z and w axes.</param>
        public static Matrix4x4 Scale4D(Vector4D scale) => Scale4D(scale.x, scale.y, scale.z, scale.w);

        /// <summary>
        /// Creates a scale matrix.
        /// </summary>
        /// <param name="x">The scale on the x axis.</param>
        /// <param name="y">The scale on the y axis.</param>
        /// <param name="z">The scale on the z axis.</param>
        public static Matrix4x4 Scale(float x, float y, float z) => Scale4D(x, y, z, 1);

        /// <summary>
        /// Creates a scale matrix.
        /// </summary>
        /// <param name="scale">The scale to make on the x, y and z axes.</param>
        public static Matrix4x4 Scale(Vector3D scale) => Scale(scale.x, scale.y, scale.z);

        /// <summary>
        /// Creates a translation matrix.
        /// </summary>
        /// <param name="x">The translation on the x axis.</param>
        /// <param name="y">The translation on the y axis.</param>
        /// <param name="z">The translation on the z axis.</param>
        public static Matrix4x4 Translate(float x, float y, float z)
        {
            return new Matrix4x4(
                1, 0, 0, x,
                0, 1, 0, y,
                0, 0, 1, z,
                0, 0, 0, 1);
        }

        /// <summary>
        /// Creates a translation matrix.
        /// </summary>
        /// <param name="translation">The translation on the x, y and z axes.</param>
        public static Matrix4x4 Translate(Vector3D translation) => Translate(translation.x, translation.y, translation.z);
        
        /// <summary>
        /// Creates a rotation matrix that will rotate by angle around the x axis.
        /// </summary>
        public static Matrix4x4 RotateX(Angle angle)
        {
            float cos = Math1D.Cos(angle);
            float sin = Math1D.Sin(angle);

            return new Matrix4x4(
                1, 0, 0, 0,
                0, cos, -sin, 0,
                0, sin, cos, 0,
                0, 0, 0, 1
                );
        }

        /// <summary>
        /// Creates a rotation matrix that will rotate by angle around the y axis.
        /// </summary>
        public static Matrix4x4 RotateY(Angle angle)
        {
            float cos = Math1D.Cos(angle);
            float sin = Math1D.Sin(angle);

            return new Matrix4x4(
                cos, 0, sin, 0,
                0, 1, 0, 0,
                -sin, 0, cos, 0,
                0, 0, 0, 1
                );
        }

        /// <summary>
        /// Creates a rotation matrix that will rotate by angle around the z axis.
        /// </summary>
        public static Matrix4x4 RotateZ(Angle angle)
        {
            float cos = Math1D.Cos(angle);
            float sin = Math1D.Sin(angle);

            return new Matrix4x4(
                cos, -sin, 0, 0,
                sin, cos, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1
                );
        }

        /// <summary>
        /// Creates a rotation matrix from euler angles.
        /// </summary>
        /// <param name="pitch">Rotation angle around the x axis.</param>
        /// <param name="yaw">Rotation angle around the y axis.</param>
        /// <param name="roll">Rotation angle around the z axis.</param>
        public static Matrix4x4 FromEulerAngles(Angle pitch, Angle yaw, Angle roll)
        {
            return RotateZ(roll) * RotateY(yaw) * RotateX(pitch);
        }

        /// <summary>
        /// Creates a rotation matrix from euler angles.
        /// </summary>
        /// <param name="angles">The rotation around the x, y and z axes.</param>
        public static Matrix4x4 FromEulerAngles(EulerAngles angles) => FromEulerAngles(angles.x, angles.y, angles.z);
        
        /// <summary>
        /// Creates a rotation matrix from an axis and an angle.
        /// </summary>
        /// <param name="axis">The axis to rotate around.</param>
        /// <param name="angle">The angle to rotate by.</param>
        public static Matrix4x4 FromAxisAngle(Vector3D axis, Angle angle)
        {
            return FromEulerAngles(EulerAngles.FromAxisAngle(axis, angle));
        }

        /// <summary>
        /// Creates a rotation matrix from an axis and an angle.
        /// </summary>
        /// <param name="axisAngle">The axis to rotate around and angle to rotate by.</param>
        public static Matrix4x4 FromAxisAngle(AxisAngle axisAngle) => FromAxisAngle(axisAngle.axis, axisAngle.angle);
        #endregion

        #region Special transformations (Look, LookAt and projection matrices).
        /// <summary>
        /// Creates an orthogonal projection matrix.
        /// </summary>
        /// <param name="width">The width of the projection box.</param>
        /// <param name="height">The height of the projection box.</param>
        /// <param name="zNear">The position on z of the near clipping plane.</param>
        /// <param name="zFar">The position on z of the far clipping plane.</param>
        public static Matrix4x4 CreateOrthographicProjection(float width, float height, float zNear, float zFar)
        {
            float widthOver2 = width / 2;
            float heightOver2 = height / 2;
            return CreateOrthographicProjection(-widthOver2, widthOver2, -heightOver2, heightOver2, zNear, zFar);
        }
        
        /// <summary>
        /// Creates an orthogonal projection matrix.
        /// </summary>
        /// <param name="left">The position on x of the left clipping plane.</param>
        /// <param name="right">The position on x of the right clipping plane.</param>
        /// <param name="bottom">The position on y of the bottom clipping plane.</param>
        /// <param name="top">The position on y of the top clipping plane.</param>
        /// <param name="zNear">The position on z of the near clipping plane.</param>
        /// <param name="zFar">The position on z of the far clipping plane.</param>
        public static Matrix4x4 CreateOrthographicProjection(float left, float right, float bottom, float top, float zNear, float zFar)
        {
            float invRL = 1 / (right - left);
            float invTB = 1 / (top - bottom);
            float invFN = 1 / (zFar - zNear);

            return new Matrix4x4(
                2 * invRL, 0, 0, -(right + left) * invRL,
                0, 2 * invTB, 0, -(top + bottom) * invTB,
                0, 0, -2 * invFN, -(zFar + zNear) * invFN,
                0, 0, 0, 1
                );
        }
        
        /// <summary>
        /// Creates a perspective projection matrix, where objects further on z are smaller.
        /// </summary>
        /// <param name="fieldOfView">The field of view of the view frustum.</param>
        /// <param name="aspectRatio">The aspect ratio of the view frustum.</param>
        /// <param name="zNear">The position on z of the near clipping plane.</param>
        /// <param name="zFar">The position on z of the far clipping plane.</param>
        public static Matrix4x4 CreatePerspectiveProjection(Angle fieldOfView, float aspectRatio, float zNear, float zFar)
        {
            float tan = Math1D.Tan(fieldOfView / 2);
            float fMinusN = zFar - zNear;
            return new Matrix4x4(
                1 / (aspectRatio * tan), 0, 0, 0,
                0, 1 / tan, 0, 0,
                0, 0, -(zFar + zNear) / (fMinusN), -2 * zNear * zFar / fMinusN,
                0, 0, -1, 0
                );
        }

        /// <summary>
        /// Creates a view matrix looking in the direction of forward with the up axis up from origin.
        /// </summary>
        /// <param name="origin">The location to look from.</param>
        /// <param name="forward">The forward direction (ASSUMED NORMALIZED).</param>
        /// <param name="up">The up direction (ASSUMED NORMALIZED).</param>
        public static Matrix4x4 Look(Vector3D origin, Vector3D forward, Vector3D up)
        {
            Vector3D right = up.Cross(forward);
            up = forward.Cross(right);

            return new Matrix4x4(
                right.x, right.y, right.z, -right.Dot(origin),
                up.x, up.y, up.z, -up.Dot(origin),
                forward.x, forward.y, forward.z, -forward.Dot(origin),
                0, 0, 0, 1
                );
        }

        /// <summary>
        /// Creates a view matrix looking in the direction of forward with the up axis (0, 1, 0) from origin;
        /// </summary>
        /// <param name="origin">The location to look from.</param>
        /// <param name="forward">The forward direction (ASSUMED NORMALIZED).</param>
        public static Matrix4x4 Look(Vector3D origin, Vector3D forward) => Look(origin, forward, Vector3D.up);

        /// <summary>
        /// Creates a view matrix looking from origin to target with the up axis up.
        /// </summary>
        /// <param name="origin">The location to look from.</param>
        /// <param name="target">The location to look at.</param>
        /// <param name="up">The up vector (ASSUMED NORMALIZED).</param>
        public static Matrix4x4 LookAt(Vector3D origin, Vector3D target, Vector3D up) => Look(origin, target.PointAt(origin), up);

        /// <summary>
        /// Creates a view matrix looking from origin to target with the up axis (0, 1, 0).
        /// </summary>
        /// <param name="origin">The location to look from.</param>
        /// <param name="target">The location to look at.</param>
        public static Matrix4x4 LookAt(Vector3D origin, Vector3D target) => LookAt(origin, target, Vector3D.up);
        #endregion

        #region Conversion properties from/to different types of rotations and properties for transformations of this matrix.
        /// <summary>
        /// Uh...
        /// This should ONLY be used if this matrix does NOT have translation in it. Use <seealso cref="Matrix4x4"/> if 3D rotation and translation are needed together.
        /// </summary>
        Matrix3x3 IRotator.RotationMatrix3x3 { get => (Matrix3x3)this; set => this = value; }

        /// <summary>
        /// Uh...
        /// This should ONLY be used if this matrix does NOT have translation in it. Use <seealso cref="Matrix4x4"/> if 3D rotation and translation are needed together.
        /// </summary>
        Matrix4x4 IRotator.RotationMatrix { get => this; set => this = value; }

        /// <summary>
        /// Gets/sets the rotation of this matrix as a quaternion.
        /// </summary>
        public Quaternion Quaternion { get => Quaternion.FromRotationMatrix((Matrix3x3)this); set => this = FromRotationQuaternion(value); }

        /// <summary>
        /// Gets/sets the rotation of this matrix as euler angles.
        /// </summary>
        public EulerAngles EulerAngles { get => EulerAngles.FromRotationMatrix((Matrix3x3)this); set => this = FromEulerAngles(value); }

        /// <summary>
        /// Gets/sets the rotation of this matrix as an axis to rotate around and an angle to rotate by.
        /// </summary>
        public AxisAngle AxisAngle { get => AxisAngle.FromRotationMatrix((Matrix3x3)this); set => this = FromAxisAngle(value); }

        /// <summary>
        /// Gets/sets the translation of this matrix.
        /// </summary>
        public Vector3D Translation
        {
            get => new Vector3D(r0c3, r1c3, r2c3);
            set
            {
                r0c3 = value.x;
                r1c3 = value.y;
                r2c3 = value.z;
            }
        }

        /// <summary>
        /// Gets/sets the scale of this matrix. It's called "Size" instead of "Scale" so it won't conflict with <seealso cref="Scale(Vector3D)"/>
        /// </summary>
        public Vector3D Size
        {
            get => new Vector3D(r0c0, r1c1, r2c2);
            set
            {
                r0c0 = value.x;
                r1c1 = value.y;
                r2c2 = value.z;
            }
        }

        /// <summary>
        /// Gets/sets the 4D scale of this matrix. It's called "Size4D" instead of "Scale4D" so it won't conflict with <seealso cref="Scale4D(Vector4D)"/>.<para/>
        /// This should ONLY be used if this matrix does NOT have translation in it. If you need a 4D scale matrix with translation then...uh...I don't know...what do you even need that for?
        /// </summary>
        public Vector4D Size4D
        {
            get => new Vector4D(r0c0, r1c1, r2c2, r3c3);
            set
            {
                r0c0 = value.x;
                r1c1 = value.y;
                r2c2 = value.z;
                r3c3 = value.w;
            }
        }
        #endregion

        #region Combinations and ways to transform vectors.
        /// <summary>
        /// Rotates vector by this matrix. Applies all transformations in this matrix to vector excpet for translation.
        /// </summary>
        public Vector3D RotateVector(Vector3D vector)
        {
            return (Vector3D)(this * new Vector4D(vector, 0));
        }

        /// <summary>
        /// Transforms a vector by this matrix.
        /// </summary>
        public Vector3D TransformVector(Vector3D vector)
        {
            return (Vector3D)(this * new Vector4D(vector, 1));
        }

        /// <summary>
        /// Combines two rotation matrices: First this and then other.
        /// This is just going to combine the transformations (<seealso cref="CombineTransformations(Matrix4x4)"/>), so it doesn't really combine only rotations...but if those are pure rotation matrices, it will combine their rotations.
        /// </summary>
        public Matrix4x4 CombineRotations(Matrix4x4 other) => CombineTransformations(other);
        
        /// <summary>
        /// Combines this with second, first applying this and then second.
        /// </summary>
        /// <param name="other">Transformation to combine this with.</param>
        public Matrix4x4 CombineTransformations(Matrix4x4 other)
        {
            return other * this;
        }

        public Matrix4x4 DiscombineTransformations(Matrix4x4 secondTransformation) => secondTransformation.Inverted * this;

        /// <summary>
        /// Combines three matrices as a model matrix, a view matrix, and a projection matrix.
        /// </summary>
        /// <param name="model">Local to world transformation matrix.</param>
        /// <param name="view">World to view transformation matrix.</param>
        /// <param name="projection">View to projected space transformation matrix.</param>
        /// <returns>A model view projection matrix with the specified matrices.</returns>
        public static Matrix4x4 MVP(Matrix4x4 model, Matrix4x4 view, Matrix4x4 projection)
        {
            return projection * view * model;
        }

        /// <summary>
        /// Creates a transformation matrix from translation, rotation and scale.
        /// </summary>
        /// <param name="translation">Amount of translation to make.</param>
        /// <param name="rotation">Rotation quaternion of the transformation.</param>
        /// <param name="scale">Amount of scale to make.</param>
        public static Matrix4x4 TRS(Vector3D translation, Quaternion rotation, Vector3D scale)
        {
            return Scale(scale) * FromRotationQuaternion(rotation) * Translate(translation);
        }

        /// <summary>
        /// Creates a transformation matrix for a transform.
        /// </summary>
        /// <param name="transform">Translation, rotation and scale of the matrix.</param>
        public static Matrix4x4 TRS(Transform transform) => TRS(transform.position, transform.rotation, transform.scale);
        #endregion

        #region Multiplication.
        /// <summary>Multiplies this by other.</summary>
        public Matrix4x4 Multiply(Matrix4x4 other) => this * other;

        /// <summary>Multiplies a by b.</summary>
        public static Matrix4x4 operator *(Matrix4x4 a, Matrix4x4 b)
        {
            return new Matrix4x4(
                a.GetRow(0).Dot(b.GetColumn(0)), a.GetRow(0).Dot(b.GetColumn(1)), a.GetRow(0).Dot(b.GetColumn(2)), a.GetRow(0).Dot(b.GetColumn(3)),
                a.GetRow(1).Dot(b.GetColumn(0)), a.GetRow(1).Dot(b.GetColumn(1)), a.GetRow(1).Dot(b.GetColumn(2)), a.GetRow(1).Dot(b.GetColumn(3)),
                a.GetRow(2).Dot(b.GetColumn(0)), a.GetRow(2).Dot(b.GetColumn(1)), a.GetRow(2).Dot(b.GetColumn(2)), a.GetRow(2).Dot(b.GetColumn(3)),
                a.GetRow(3).Dot(b.GetColumn(0)), a.GetRow(3).Dot(b.GetColumn(1)), a.GetRow(3).Dot(b.GetColumn(2)), a.GetRow(3).Dot(b.GetColumn(3)));
        }

        /// <summary>Multiplies each element of this by scalar.</summary>
        public Matrix4x4 Multiply(float scalar) => this * scalar;

        /// <summary>Multiplies each element of matrix by scalar.</summary>
        public static Matrix4x4 operator *(Matrix4x4 matrix, float scalar)
        {
            return new Matrix4x4(
                matrix.r0c0 * scalar, matrix.r0c1 * scalar, matrix.r0c2 * scalar, matrix.r0c3 * scalar,
                matrix.r1c0 * scalar, matrix.r1c1 * scalar, matrix.r1c2 * scalar, matrix.r1c3 * scalar,
                matrix.r2c0 * scalar, matrix.r2c1 * scalar, matrix.r2c2 * scalar, matrix.r2c3 * scalar,
                matrix.r3c0 * scalar, matrix.r3c1 * scalar, matrix.r3c2 * scalar, matrix.r3c3 * scalar);
        }

        /// <summary>Multiplies each element of matrix by scalar.</summary>
        public static Matrix4x4 operator *(float scalar, Matrix4x4 matrix) => matrix * scalar;

        /// <summary>Multiplies this by vector (as a column vector).</summary>
        public Vector4D Multiply(Vector4D vector) => this * vector;
        
        /// <summary>Multiplies matrix by vector (as a column vector).</summary>
        public static Vector4D operator *(Matrix4x4 matrix, Vector4D vector) => vector.x * matrix.GetColumn(0) + vector.y * matrix.GetColumn(1) + vector.z * matrix.GetColumn(2) + vector.w * matrix.GetColumn(3);

        /// <summary>Multiplies vector (as a row vector) by matrix.</summary>
        public static Vector4D operator *(Vector4D vector, Matrix4x4 matrix) => vector.x * matrix.GetRow(0) + vector.y * matrix.GetRow(1) + vector.z * matrix.GetRow(2) + vector.w * matrix.GetRow(3);
        #endregion

        #region Addition and subtraction.
        /// <summary>Adds every element of this to the corresponding element in other.</summary>
        public Matrix4x4 Add(Matrix4x4 other) => this + other;

        /// <summary>Adds every element of a to the corresponding element in b.</summary>
        public static Matrix4x4 operator +(Matrix4x4 a, Matrix4x4 b)
        {
            return new Matrix4x4(
                a.r0c0 + b.r0c0, a.r0c1 + b.r0c1, a.r0c2 + b.r0c2, a.r0c3 + b.r0c3,
                a.r1c0 + b.r1c0, a.r1c1 + b.r1c1, a.r1c2 + b.r1c2, a.r1c3 + b.r1c3,
                a.r2c0 + b.r2c0, a.r2c1 + b.r2c1, a.r2c2 + b.r2c2, a.r2c3 + b.r2c3,
                a.r3c0 + b.r3c0, a.r3c1 + b.r3c1, a.r3c2 + b.r3c2, a.r3c3 + b.r3c3);
        }

        /// <summary>Subtracts every element of this by the corresponding element in other.</summary>
        public Matrix4x4 Subtract(Matrix4x4 other) => this - other;

        /// <summary>Subtracts every element of a by the corresponding element in other.</summary>
        public static Matrix4x4 operator -(Matrix4x4 a, Matrix4x4 b)
        {
            return new Matrix4x4(
                a.r0c0 - b.r0c0, a.r0c1 - b.r0c1, a.r0c2 - b.r0c2, a.r0c3 - b.r0c3,
                a.r1c0 - b.r1c0, a.r1c1 - b.r1c1, a.r1c2 - b.r1c2, a.r1c3 - b.r1c3,
                a.r2c0 - b.r2c0, a.r2c1 - b.r2c1, a.r2c2 - b.r2c2, a.r2c3 - b.r2c3,
                a.r3c0 - b.r3c0, a.r3c1 - b.r3c1, a.r3c2 - b.r3c2, a.r3c3 - b.r3c3);
        }
        #endregion
        
        #region Negating, transposing, cofactor and absolute value.
        /// <summary>The matrix with all of its elements negated (multiplied by -1).</summary>
        public Matrix4x4 Negated => -this;

        /// <summary>Negates (multiplied by -1) all the elements of matrix.</summary>
        public static Matrix4x4 operator -(Matrix4x4 matrix)
        {
            return new Matrix4x4(
                -matrix.r0c0, -matrix.r0c1, -matrix.r0c2, -matrix.r0c3,
                -matrix.r1c0, -matrix.r1c1, -matrix.r1c2, -matrix.r1c3,
                -matrix.r2c0, -matrix.r2c1, -matrix.r2c2, -matrix.r2c3,
                -matrix.r3c0, -matrix.r3c1, -matrix.r3c2, -matrix.r3c3);
        }

        /// <summary>Gets the transpose (replaced rows with columns and columns with rows) of this matrix.</summary>
        public Matrix4x4 Transposed
        {
            get
            {
                return new Matrix4x4(
                    r0c0, r1c0, r2c0, r3c0,
                    r0c1, r1c1, r2c1, r3c1,
                    r0c2, r1c2, r2c2, r3c2,
                    r0c3, r1c3, r2c3, r3c3);
            }
        }

        /// <summary>Gets the cofactor of this matrix.</summary>
        public Matrix4x4 Cofactor
        {
            get
            {
                return new Matrix4x4(
                    // First row:
                    new Matrix3x3(r1c1, r1c2, r1c3, r2c1, r2c2, r2c3, r3c1, r3c2, r3c3).Determinant,
                    -new Matrix3x3(r1c0, r1c2, r1c3, r2c0, r2c2, r2c3, r3c0, r3c2, r3c3).Determinant,
                    new Matrix3x3(r1c0, r1c1, r1c3, r2c0, r2c1, r2c3, r3c0, r3c1, r3c3).Determinant,
                    -new Matrix3x3(r1c0, r1c1, r1c2, r2c0, r2c1, r2c2, r3c0, r3c1, r3c2).Determinant,

                    // Second row:
                    -new Matrix3x3(r0c1, r0c2, r0c3, r2c1, r2c2, r2c3, r3c1, r3c2, r3c3).Determinant,
                    new Matrix3x3(r0c0, r0c2, r0c3, r2c0, r2c2, r2c3, r3c0, r3c2, r3c3).Determinant,
                    -new Matrix3x3(r0c0, r0c1, r0c3, r2c0, r2c1, r2c3, r3c0, r3c1, r3c3).Determinant,
                    new Matrix3x3(r0c0, r0c1, r0c2, r2c0, r2c1, r2c2, r3c0, r3c1, r3c2).Determinant,

                    // Third row:
                    new Matrix3x3(r0c1, r0c2, r0c3, r1c1, r1c2, r1c3, r3c1, r3c2, r3c3).Determinant,
                    -new Matrix3x3(r0c0, r0c2, r0c3, r1c0, r1c2, r1c3, r3c0, r3c2, r3c3).Determinant,
                    new Matrix3x3(r0c0, r0c1, r0c3, r1c0, r1c1, r1c3, r3c0, r3c1, r3c3).Determinant,
                    -new Matrix3x3(r0c0, r0c1, r0c2, r1c0, r1c1, r1c2, r3c0, r3c1, r3c2).Determinant,

                    // Fourth row:
                    -new Matrix3x3(r0c1, r0c2, r0c3, r1c1, r1c2, r1c3, r2c1, r2c2, r2c3).Determinant,
                    new Matrix3x3(r0c0, r0c2, r0c3, r1c0, r1c2, r1c3, r2c0, r2c2, r2c3).Determinant,
                    -new Matrix3x3(r0c0, r0c1, r0c3, r1c0, r1c1, r1c3, r2c0, r2c1, r2c3).Determinant,
                    new Matrix3x3(r0c0, r0c1, r0c2, r1c0, r1c1, r1c2, r2c0, r2c1, r2c2).Determinant
                    );
            }
        }

        /// <summary>Gets a new matrix where each element has the absolute value of the corresponding element in this.</summary>
        public Matrix4x4 Abs
        {
            get
            {
                return new Matrix4x4(
                    r0c0.Abs(), r0c1.Abs(), r0c2.Abs(), r0c3.Abs(),
                    r1c0.Abs(), r1c1.Abs(), r1c2.Abs(), r1c3.Abs(),
                    r2c0.Abs(), r2c1.Abs(), r2c2.Abs(), r2c3.Abs(),
                    r3c0.Abs(), r3c1.Abs(), r3c2.Abs(), r3c3.Abs());
            }
        }
        #endregion

        #region Inverting and adjugate.
        /// <summary>Gets the inverse of this matrix.</summary>
        public Matrix4x4 Inverted => Matrix.Invert(this);

        /// <summary>Gets the adjugate (transpose of cofactor) of this matrix.</summary>
        public Matrix4x4 Adjugate => Matrix.Adjugate(this);
        #endregion
        
        #region Determinant and Trace.
        /// <summary>The determinant (how much the area of a 1x1 square will be multiplied by after it's transformed with this matrix) of this matrix.</summary>
        public float Determinant
        {
            get
            {
                return
                    r0c0 * new Matrix3x3(r1c1, r1c2, r1c3, r2c1, r2c2, r2c3, r3c1, r3c2, r3c3).Determinant -
                    r0c1 * new Matrix3x3(r1c0, r1c2, r1c3, r2c0, r2c2, r2c3, r3c0, r3c2, r3c3).Determinant +
                    r0c2 * new Matrix3x3(r1c0, r1c1, r1c3, r2c0, r2c1, r2c3, r3c0, r3c1, r3c3).Determinant -
                    r0c3 * new Matrix3x3(r1c0, r1c1, r1c2, r2c0, r2c1, r2c2, r3c0, r3c1, r3c2).Determinant;
            }
        }

        /// <summary>Gest the trace (sum of diagonal elements) of this matrix.</summary>
        public float Trace => r0c0 + r1c1 + r2c2 + r3c3;
        #endregion

        #region Clamp methods (Clamp, ClampElements, ClampRows, ClampColumns).
        /// <summary>Clamps each element of this so it's never smaller than the corresponding element in min and never bigger than the corresponding element in max.</summary>
        public Matrix4x4 Clamp(Matrix4x4 min, Matrix4x4 max)
        {
            return new Matrix4x4(
                r0c0.Clamp(min.r0c0, max.r0c0), r0c1.Clamp(min.r0c1, max.r0c1), r0c2.Clamp(min.r0c2, max.r0c2), r0c3.Clamp(min.r0c3, max.r0c3),
                r1c0.Clamp(min.r1c0, max.r1c0), r1c1.Clamp(min.r1c1, max.r1c1), r1c2.Clamp(min.r1c2, max.r1c2), r1c3.Clamp(min.r1c3, max.r1c3),
                r2c0.Clamp(min.r2c0, max.r2c0), r2c1.Clamp(min.r2c1, max.r2c1), r2c2.Clamp(min.r2c2, max.r2c2), r2c3.Clamp(min.r2c3, max.r2c3),
                r3c0.Clamp(min.r3c0, max.r3c0), r3c1.Clamp(min.r3c1, max.r3c1), r3c2.Clamp(min.r3c2, max.r3c2), r3c3.Clamp(min.r3c3, max.r3c3));
        }

        /// <summary>Clamps each element of this so it's never smaller than min and never bigger than max.</summary>
        public Matrix4x4 ClampElements(float min, float max)
        {
            return new Matrix4x4(
                r0c0.Clamp(min, max), r0c1.Clamp(min, max), r0c2.Clamp(min, max), r0c3.Clamp(min, max),
                r1c0.Clamp(min, max), r1c1.Clamp(min, max), r1c2.Clamp(min, max), r1c3.Clamp(min, max),
                r2c0.Clamp(min, max), r2c1.Clamp(min, max), r2c2.Clamp(min, max), r2c3.Clamp(min, max),
                r3c0.Clamp(min, max), r3c1.Clamp(min, max), r3c2.Clamp(min, max), r3c3.Clamp(min, max));
        }

        /// <summary>Clamps each element of each row in this so that it's never smaller than the corresponding element in min and never bigger than the corresponding element in max.</summary>
        public Matrix4x4 ClampRows(Vector4D min, Vector4D max) => FromRows(GetRow(0).Clamp(min, max), GetRow(1).Clamp(min, max), GetRow(2).Clamp(min, max), GetRow(3).Clamp(min, max));

        /// <summary>Clamps each element of each column in this so that it's never smaller than the corresponding element in min and never bigger than the corresponding element in max.</summary>
        public Matrix4x4 ClampColumns(Vector4D min, Vector4D max) => FromColumns(GetColumn(0).Clamp(min, max), GetColumn(1).Clamp(min, max), GetColumn(2).Clamp(min, max), GetColumn(3).Clamp(min, max));
        #endregion

        #region Comparison methods (Equals, Operators, GetHashCode, CloseEnough).
        /// <summary>
        /// Are all of the elements of other <seealso cref="SMath.CloseEnough(float, float, float)"/> to the elements of this?
        /// </summary>
        public bool CloseEnough(Matrix4x4 other, float threshold = Math1D.Epsilon)
        {
            return
                r0c0.CloseEnough(other.r0c0, threshold) && r0c1.CloseEnough(other.r0c1, threshold) && r0c2.CloseEnough(other.r0c2, threshold) && r0c3.CloseEnough(other.r0c3, threshold) &&
                r1c0.CloseEnough(other.r1c0, threshold) && r1c1.CloseEnough(other.r1c1, threshold) && r1c2.CloseEnough(other.r1c2, threshold) && r1c3.CloseEnough(other.r1c3, threshold) &&
                r2c0.CloseEnough(other.r2c0, threshold) && r2c1.CloseEnough(other.r2c1, threshold) && r2c2.CloseEnough(other.r2c2, threshold) && r2c3.CloseEnough(other.r2c3, threshold) &&
                r3c0.CloseEnough(other.r3c0, threshold) && r3c1.CloseEnough(other.r3c1, threshold) && r3c2.CloseEnough(other.r3c2, threshold) && r3c3.CloseEnough(other.r3c3, threshold);
        }

        /// <summary>Is obj a Matrix4x4? If so, are all of the components equal?</summary>
        public override bool Equals(object obj)
        {
            if (!(obj is Matrix4x4))
                return false;

            return Equals((Matrix4x4)obj);
        }

        public override int GetHashCode()
        {
            return
                r0c0.GetHashCode() ^ r0c1.GetHashCode() ^ r0c2.GetHashCode() ^ r0c3.GetHashCode() ^
                r1c0.GetHashCode() ^ r1c1.GetHashCode() ^ r1c2.GetHashCode() ^ r1c3.GetHashCode() ^
                r2c0.GetHashCode() ^ r2c1.GetHashCode() ^ r2c2.GetHashCode() ^ r2c3.GetHashCode() ^
                r3c0.GetHashCode() ^ r3c1.GetHashCode() ^ r3c2.GetHashCode() ^ r3c3.GetHashCode();
        }

        /// <summary>Are all of the components of the matrices equal?</summary>
        public bool Equals(Matrix4x4 other)
        {
            return this == other;
        }

        /// <summary>Are all of the components of the matrices equal?</summary>
        public static bool operator ==(Matrix4x4 left, Matrix4x4 right)
        {
            return
                left.r0c0 == right.r0c0 && left.r0c1 == right.r0c1 && left.r0c2 == right.r0c2 && left.r0c3 == right.r0c3 &&
                left.r1c0 == right.r1c0 && left.r1c1 == right.r1c1 && left.r1c2 == right.r1c2 && left.r1c3 == right.r1c3 &&
                left.r2c0 == right.r2c0 && left.r2c1 == right.r2c1 && left.r2c2 == right.r2c2 && left.r2c3 == right.r2c3 &&
                left.r3c0 == right.r3c0 && left.r3c1 == right.r3c1 && left.r3c2 == right.r3c2 && left.r3c3 == right.r3c3;
        }

        /// <summary>Are any of the components of the matrices NOT equal?</summary>
        public static bool operator !=(Matrix4x4 left, Matrix4x4 right)
        {
            return !(left == right);
        }
        #endregion

        #region Conversions.
        /// <summary>
        /// Uses the top left elements of matrix as the elements for the new matrix.
        /// </summary>
        public static explicit operator Matrix2x2(Matrix4x4 matrix)
        {
            return new Matrix2x2(
                matrix.r0c0, matrix.r0c1,
                matrix.r1c0, matrix.r1c1);
        }

        /// <summary>
        /// Uses the top left elements of matrix as the elements for the new matrix.
        /// </summary>
        public static explicit operator Matrix3x3(Matrix4x4 matrix)
        {
            return new Matrix3x3(
                matrix.r0c0, matrix.r0c1, matrix.r0c2,
                matrix.r1c0, matrix.r1c1, matrix.r1c2,
                matrix.r2c0, matrix.r2c1, matrix.r2c2);
        }

        public override string ToString()
        {
            return $@"
[{r0c0}, {r0c1}, {r0c2}, {r0c3}]
[{r1c0}, {r1c1}, {r1c2}, {r1c3}]
[{r2c0}, {r2c1}, {r2c2}, {r2c3}]
[{r3c0}, {r3c1}, {r3c2}, {r3c3}]
";
        }
        #endregion
    }
}