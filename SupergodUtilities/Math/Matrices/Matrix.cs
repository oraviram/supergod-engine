﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// Class with all the shared functionality between all <seealso cref="IMatrix{TMat, TVec}"/>s.
    /// </summary>
    public static class Matrix
    {
        #region Static wrappers for methods implemented by individual IMatrix<TMat, TVec> structs.
        /// <summary>
        /// Calls the ClampRows method on matrix and passes min as the minimum and max as the maximum.
        /// </summary>
        public static TMat ClampRows<TMat, TVec>(TMat matrix, TVec min, TVec max)
            where TMat : struct, IMatrix<TMat, TVec>
            where TVec : struct, IVector<TVec>
        {
            return matrix.ClampRows(min, max);
        }

        /// <summary>
        /// Calls the ClampColumns method on matrix and passes min as the minimum and max as the maximum.
        /// </summary>
        public static TMat ClampColumns<TMat, TVec>(TMat matrix, TVec min, TVec max)
            where TMat : struct, IMatrix<TMat, TVec>
            where TVec : struct, IVector<TVec>
        {
            return matrix.ClampColumns(min, max);
        }
        #endregion

        #region Static wrappers for methods implemented by individual IMatrix<T> structs.
        /// <summary>
        /// Calls the Add method on a and b.
        /// </summary>
        public static T Add<T>(T a, T b)
            where T : struct, IMatrix<T>
        {
            return a.Add(b);
        }
        
        /// <summary>
        /// Calls the Subtract method on a and passes b.
        /// </summary>
        public static T Subtract<T>(T a, T b)
            where T : struct, IMatrix<T>
        {
            return a.Subtract(b);
        }

        /// <summary>
        /// Calls the Multiply method on vector and passes scalar.
        /// </summary>
        public static T Multiply<T>(T matrix, float scalar)
            where T : struct, IMatrix<T>
        {
            return matrix.Multiply(scalar);
        }

        /// <summary>
        /// Calls the Multiply method on vector and passes scalar.
        /// </summary>
        public static T Multiply<T>(float scalar, T matrix)
            where T : struct, IMatrix<T>
        {
            return matrix.Multiply(scalar);
        }

        /// <summary>
        /// Calls the Multiply method on a and b.
        /// </summary>
        public static T Multiply<T>(T a, T b)
            where T : struct, IMatrix<T>
        {
            return a.Multiply(b);
        }

        /// <summary>
        /// Gest the Negated property of matrix.
        /// </summary>
        public static T Negate<T>(T matrix)
            where T : struct, IMatrix<T>
        {
            return matrix.Negated;
        }

        /// <summary>
        /// Gest the Transposed property of matrix.
        /// </summary>
        public static T Transpose<T>(T matrix)
            where T : struct, IMatrix<T>
        {
            return matrix.Transposed;
        }

        /// <summary>
        /// Gest the Cofactor property of matrix.
        /// </summary>
        public static T Cofactor<T>(T matrix)
            where T : struct, IMatrix<T>
        {
            return matrix.Cofactor;
        }

        /// <summary>
        /// Gets the determinant of matrix.
        /// </summary>
        public static float Det<T>(T matrix)
            where T : struct, IMatrix<T>
        {
            return matrix.Determinant;
        }

        /// <summary>
        /// Gets the trace of matrix.
        /// </summary>
        public static float Tr<T>(T matrix)
            where T : struct, IMatrix<T>
        {
            return matrix.Trace;
        }
        
        /// <summary>
        /// Calls teh ClampAxes method of vector and passes min as the minimum and max as the maximum.
        /// </summary>
        public static T ClampElements<T>(T matrix, float min, float max)
            where T : struct, IMatrix<T>
        {
            return matrix.ClampElements(min, max);
        }

        /// <summary>
        /// Calls the Abs method of matrix.
        /// </summary>
        public static T Abs<T>(T matrix)
            where T : struct, IMatrix<T>
        {
            return matrix.Abs;
        }
        #endregion

        #region Implementations for shared functions.
        /// <summary>
        /// Gets the adjugate matrix of matrix.
        /// Same as the transpose of the cofactor matrix.
        /// </summary>
        public static T Adjugate<T>(T matrix)
            where T : struct, IMatrix<T>
        {
            return matrix.Cofactor.Transposed;
        }

        /// <summary>
        /// Gets the matrix that when multiplies matrix will result in the identity matrix.
        /// Same case when multiplying matrix by the resulting matrix from this method.
        /// </summary>
        /// <exception cref="ArgumentException">det(matrix) = 0. That means it's not invertible.</exception>
        public static T Invert<T>(T matrix)
            where T : struct, IMatrix<T>
        {
            float det = matrix.Determinant;
            if (det == 0)
                throw new ArgumentException("Cannot invert matrix! Make sure the determinant of the matrix is not 0 before taking its inverse.");

            return matrix.Adjugate.Multiply(1 / det);
        }
        #endregion
    }
}