﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// Represents a 3D (3 by 3) matrix. Visual representation should look something like this:
    /// <para/>
    /// [r0c0, r0c1, r0c2]
    /// <para/>
    /// [r1c0, r1c1, r1c2]
    /// <para/>
    /// [r2c0, r2c1, r2c2]
    /// </summary>
    public struct Matrix3x3 : IMatrix<Matrix3x3, Vector3D>, IRotator<Matrix3x3>, ITransformer<Matrix3x3>
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Matrix3x3));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        #region Presets for common matrices.
        /// <summary>
        /// The multiplicative identity of matrices:<para/>
        /// [1 0 0]<para/>
        /// [0 1 0]<para/>
        /// [0 0 1]<para/>
        /// </summary>
        public static readonly Matrix3x3 identity = new Matrix3x3(
            1, 0, 0,
            0, 1, 0,
            0, 0, 1);

        /// <summary>
        /// A matrix with all of its elements set to 0:<para/>
        /// [0 0 0]<para/>
        /// [0 0 0]<para/>
        /// [0 0 )]<para/>
        /// </summary>
        public static readonly Matrix3x3 zero = new Matrix3x3(
            0, 0, 0,
            0, 0, 0,
            0, 0, 0);
        #endregion

        public float r0c0, r0c1, r0c2;
        public float r1c0, r1c1, r1c2;
        public float r2c0, r2c1, r2c2;

        public Matrix3x3(float r0c0 = 1, float r0c1 = 0, float r0c2 = 0,
                         float r1c0 = 0, float r1c1 = 1, float r1c2 = 0,
                         float r2c0 = 0, float r2c1 = 0, float r2c2 = 1)
        {
            this.r0c0 = r0c0; this.r0c1 = r0c1; this.r0c2 = r0c2;
            this.r1c0 = r1c0; this.r1c1 = r1c1; this.r1c2 = r1c2;
            this.r2c0 = r2c0; this.r2c1 = r2c1; this.r2c2 = r2c2;
        }

        /// <summary>
        /// Creates a new matrix with vectors for the rows.
        /// </summary>
        public static Matrix3x3 FromRows(Vector3D firstRow, Vector3D secondRow, Vector3D thirdRow)
        {
            return new Matrix3x3(
                firstRow.x, firstRow.y, firstRow.z,
                secondRow.x, secondRow.y, secondRow.z,
                thirdRow.x, thirdRow.y, thirdRow.z
                );
        }

        /// <summary>
        /// Creates a new matrix with vectors for the columns.
        /// </summary>
        public static Matrix3x3 FromColumns(Vector3D firstColumn, Vector3D secondColumn, Vector3D thirdColumn)
        {
            return new Matrix3x3(
                firstColumn.x, secondColumn.x, thirdColumn.x,
                firstColumn.y, secondColumn.y, thirdColumn.y,
                firstColumn.z, secondColumn.z, thirdColumn.z
                );
        }

        #region As arrays.
        /// <summary>
        /// Gets the elements of the matrix as a 2D float array.
        /// </summary>
        public float[,] AsFloatArray => new float[,]
        {
            { r0c0, r0c1, r0c2 },
            { r1c0, r1c1, r1c2 },
            { r2c0, r2c1, r2c2 },
        };

        /// <summary>
        /// Gets the rows of the matrix as a vector array.
        /// </summary>
        public Vector3D[] Rows => new Vector3D[]
        {
            new Vector3D(r0c0, r0c1, r0c2),
            new Vector3D(r1c0, r1c1, r1c2),
            new Vector3D(r2c0, r2c1, r2c2),
        };

        /// <summary>
        /// Gets the columns of the matrix as a vector array.
        /// </summary>
        public Vector3D[] Columns => new Vector3D[]
        {
            new Vector3D(r0c0, r1c0, r2c0),
            new Vector3D(r0c1, r1c1, r2c1),
            new Vector3D(r0c2, r1c2, r2c2),
        };
        #endregion

        #region Getters and setters.
        private const string INDEX_OUT_OF_RANGE_MESSAGE = "The column/row of the get/set methods in Matrix3x3 must be either 0, 1 or 2!";

        /// <summary>
        /// Gets/sets an element at the row of row and column of column.
        /// </summary>
        public float this[int row, int column]
        {
            get => GetRow(row)[column];
            set
            {
                Vector3D newRow = GetRow(row);
                newRow[column] = value;
                SetRow(row, newRow);
            }
        }

        /// <summary>
        /// Gets a column at the index of index.
        /// </summary>
        /// <param name="index">The index of the column (from left to right, must be either 0, 1 or 2).</param>
        /// <exception cref="IndexOutOfRangeException">Index was something other than 0, 1 and 2.</exception>
        public Vector3D GetColumn(int index)
        {
            switch (index)
            {
                case 0:
                    return new Vector3D(r0c0, r1c0, r2c0);

                case 1:
                    return new Vector3D(r0c1, r1c1, r2c1);

                case 2:
                    return new Vector3D(r0c2, r1c2, r2c2);
            }
            throw new IndexOutOfRangeException(INDEX_OUT_OF_RANGE_MESSAGE);
        }

        /// <summary>
        /// Gets a row at the index of index.
        /// </summary>
        /// <param name="index">The index of the column (from top to bottom, must be either 0, 1 or 2).</param>
        /// <exception cref="IndexOutOfRangeException">Index was something other than 0, 1 and 2.</exception>
        public Vector3D GetRow(int index)
        {
            switch (index)
            {
                case 0:
                    return new Vector3D(r0c0, r0c1, r0c2);

                case 1:
                    return new Vector3D(r1c0, r1c1, r1c2);

                case 2:
                    return new Vector3D(r2c0, r2c1, r2c2);
            }
            throw new IndexOutOfRangeException(INDEX_OUT_OF_RANGE_MESSAGE);
        }

        /// <summary>
        /// Sets a column at the index of index to value.
        /// </summary>
        /// <param name="index">The index of the column (from left to right, must be either 0, 1 or 2).</param>
        /// <exception cref="IndexOutOfRangeException">Index was something other than 0, 1 and 2.</exception>
        public void SetColumn(int index, Vector3D value)
        {
            switch (index)
            {
                case 0:
                    r0c0 = value.x;
                    r1c0 = value.y;
                    r2c0 = value.z;
                    break;

                case 1:
                    r0c1 = value.x;
                    r1c1 = value.y;
                    r2c1 = value.z;
                    break;

                case 2:
                    r0c2 = value.x;
                    r1c2 = value.y;
                    r2c2 = value.z;
                    break;

                default:
                    throw new IndexOutOfRangeException(INDEX_OUT_OF_RANGE_MESSAGE);
            }
        }

        /// <summary>
        /// Sets a row at the index of index to value.
        /// </summary>
        /// <param name="index">The index of the column (from top to bottom, must be either 0, 1 or 2).</param>
        /// <exception cref="IndexOutOfRangeException">Index was something other than 0, 1 and 2.</exception>
        public void SetRow(int index, Vector3D value)
        {
            switch (index)
            {
                case 0:
                    r0c0 = value.x;
                    r0c1 = value.y;
                    r0c2 = value.z;
                    break;

                case 1:
                    r1c0 = value.x;
                    r1c1 = value.y;
                    r1c2 = value.z;
                    break;

                case 2:
                    r2c0 = value.x;
                    r2c1 = value.y;
                    r2c2 = value.z;
                    break;

                default:
                    throw new IndexOutOfRangeException(INDEX_OUT_OF_RANGE_MESSAGE);
            }
        }
        #endregion

        #region Transformations and conversions from other types of rotations.
        /// <summary>
        /// Creates a scale matrix.
        /// </summary>
        /// <param name="x">The amount of scale on the x axis.</param>
        /// <param name="y">The amount of scale on the y axis.</param>
        public static Matrix3x3 Scale(float x, float y)
        {
            return new Matrix3x3(
                x, 0, 0,
                0, y, 0,
                0, 0, 1);
        }

        /// <summary>
        /// Creates a scale matrix.
        /// </summary>
        /// <param name="scale">Scale to make.</param>
        public static Matrix3x3 Scale(Vector2D scale) => Scale(scale.x, scale.y);

        /// <summary>
        /// Creates a scale matrix in 3D space.
        /// This is NOT RECOMMENDED to use. If a 3D scale is needed, use a 4x4 matrix.
        /// </summary>
        /// <param name="x">The scale on the x axis.</param>
        /// <param name="y">The scale on the y axis.</param>
        /// <param name="z">The scale on the z axis.</param>
        public static Matrix3x3 Scale3D(float x, float y, float z)
        {
            return new Matrix3x3(
                x, 0, 0,
                0, y, 0,
                0, 0, z);
        }

        /// <summary>
        /// Creates a scale matrix in 3D space.
        /// This is NOT RECOMMENDED to use. If a 3D scale is needed, use a 4x4 matrix.
        /// </summary>
        /// <param name="scale">The scale to apply.</param>
        public static Matrix3x3 Scale3D(Vector3D scale) => Scale3D(scale.x, scale.y, scale.z);

        /// <summary>
        /// Creates a 2D rotation matrix. Same as <seealso cref="RotateZ(Angle)"/>.
        /// </summary>
        public static Matrix3x3 Rotate(Angle angle) => RotateZ(angle);

        /// <summary>
        /// Rotates by angle around the x axis.
        /// </summary>
        public static Matrix3x3 RotateX(Angle angle)
        {
            float cos = Math1D.Cos(angle);
            float sin = Math1D.Sin(angle);

            return new Matrix3x3(
                1, 0, 0,
                0, cos, -sin,
                0, sin, cos
                );
        }

        /// <summary>
        /// Rotates by angle around the y axis.
        /// </summary>
        public static Matrix3x3 RotateY(Angle angle)
        {
            float cos = Math1D.Cos(angle);
            float sin = Math1D.Sin(angle);

            return new Matrix3x3(
                cos, 0, sin,
                0, 1, 0,
                -sin, 0, cos
                );
        }

        /// <summary>
        /// Rotates by angle around the z axis.
        /// </summary>
        public static Matrix3x3 RotateZ(Angle angle)
        {
            float cos = Math1D.Cos(angle);
            float sin = Math1D.Sin(angle);

            return new Matrix3x3(
                cos, -sin, 0,
                sin, cos, 0,
                0, 0, 1);
        }

        /// <summary>
        /// Creates a rotation matrix from the specified euler angles.
        /// </summary>
        /// <param name="pitch">Angle of rotation around the x axis.</param>
        /// <param name="yaw">Angle of rotation around the y axis.</param>
        /// <param name="roll">Angle of rotation around the z axis.</param>
        public static Matrix3x3 FromEulerAngles(Angle pitch, Angle yaw, Angle roll)
        {
            return RotateZ(roll) * RotateY(yaw) * RotateX(pitch);
        }

        /// <summary>
        /// Creates a rotation matrix from the specified euler angles.
        /// </summary>
        /// <param name="angles">Yaw pitch and roll of the rotation.</param>
        public static Matrix3x3 FromEulerAngles(EulerAngles angles) => FromEulerAngles(angles.x, angles.y, angles.z);

        /// <summary>
        /// Creates a rotation matrix from the specified rotation (unit length) quaternion.
        /// </summary>
        /// <param name="rotationQuaternion">Rotation (unit length) to create the rotation from.</param>
        public static Matrix3x3 FromRotationQuaternion(Quaternion rotationQuaternion)
        {
            if (!rotationQuaternion.IsUnitLength)
                throw new ArgumentException("rotationQuaternion must be a unit length quaternion to create a 3x3 rotation matrix from it! Consider normalize it.", "rotationQuaternion");

            float xx = rotationQuaternion.x.Squared();
            float yy = rotationQuaternion.y.Squared();
            float zz = rotationQuaternion.z.Squared();

            float xy = rotationQuaternion.x * rotationQuaternion.y;
            float wz = rotationQuaternion.w * rotationQuaternion.z;
            float xz = rotationQuaternion.x * rotationQuaternion.z;
            float wy = rotationQuaternion.w * rotationQuaternion.y;
            float yz = rotationQuaternion.y * rotationQuaternion.z;
            float wx = rotationQuaternion.w * rotationQuaternion.x;

            return new Matrix3x3(
                1 - 2 * (yy + zz), 2 * (xy - wz), 2 * (xz + wy),
                2 * (xy + wz), 1 - 2 * (zz + xx), 2 * (yz - wx),
                2 * (xz - wy), 2 * (yz + wx), 1 - 2 * (yy + xx)
                );
        }

        /// <summary>
        /// Creates a rotation matrix from an axis and an angle.
        /// </summary>
        /// <param name="axis">The axis to rotate around.</param>
        /// <param name="angle">The angle to rotate by.</param>
        public static Matrix3x3 FromAxisAngle(Vector3D axis, Angle angle)
        {
            return FromEulerAngles(EulerAngles.FromAxisAngle(axis, angle));
        }

        /// <summary>
        /// Creates a rotation matrix from an axis and an angle.
        /// </summary>
        /// <param name="axisAngle">The axis to rotate around and angle to rotate by.</param>
        public static Matrix3x3 FromAxisAngle(AxisAngle axisAngle) => FromAxisAngle(axisAngle.axis, axisAngle.angle);

        /// <summary>
        /// Creates a 2D translation matrix. If a 3D translation is needed, use a 4x4 matrix.
        /// </summary>
        /// <param name="x">The translation on the x, axis.</param>
        /// <param name="y">The translation on the y axis.</param>
        public static Matrix3x3 Translate(float x, float y)
        {
            return new Matrix3x3(
                1, 0, x,
                0, 1, y,
                0, 0, 1);
        }

        /// <summary>
        /// Creates a 2D translation matrix. If a 3D translation is needed, use a 4x4 matrix.
        /// </summary>
        /// <param name="translation">The translation on x and y.</param>
        public static Matrix3x3 Translate(Vector2D translation) => Translate(translation.x, translation.y);
        #endregion

        #region Conversion properties from/to different types of rotations and properties for transformations of this matrix.
        /// <summary>
        /// Uh...
        /// This should ONLY be used if this matrix does NOT have translation in it. Use <seealso cref="Matrix4x4"/> if 3D rotation and translation are needed together.
        /// </summary>
        Matrix3x3 IRotator.RotationMatrix3x3 { get => this; set => this = value; }

        /// <summary>
        /// Uh...
        /// This should ONLY be used if this matrix does NOT have translation in it. Use <seealso cref="Matrix4x4"/> if 3D rotation and translation are needed together.
        /// </summary>
        Matrix4x4 IRotator.RotationMatrix { get => this; set => this = (Matrix3x3)value; }

        /// <summary>
        /// Gets/sets the rotation of this matrix as a quaternion.
        /// This should ONLY be used if this matrix does NOT have translation in it. Use <seealso cref="Matrix4x4"/> if 3D rotation and translation are needed together.
        /// </summary>
        public Quaternion Quaternion { get => Quaternion.FromRotationMatrix(this); set => this = FromRotationQuaternion(value); }

        /// <summary>
        /// Gets/sets the rotation of this matrix as euler angles.
        /// This should ONLY be used if this matrix does NOT have translation in it. Use <seealso cref="Matrix4x4"/> if 3D rotation and translation are needed together.
        /// </summary>
        public EulerAngles EulerAngles { get => EulerAngles.FromRotationMatrix(this); set => this = FromEulerAngles(value); }

        /// <summary>
        /// Gets/sets the rotation of this matrix as an axis to rotate around and an angle to rotate by.
        /// This should ONLY be used if this matrix does NOT have translation in it. Use <seealso cref="Matrix4x4"/> if 3D rotation and translation are needed together.
        /// </summary>
        public AxisAngle AxisAngle { get => AxisAngle.FromRotationMatrix(this); set => this = FromAxisAngle(value); }

        /// <summary>
        /// Gets/sets the translation of this matrix.
        /// </summary>
        public Vector2D Translation
        {
            get => new Vector2D(r0c2, r1c2);
            set
            {
                r0c2 = value.x;
                r1c2 = value.y;
            }
        }

        /// <summary>
        /// The 2D rotation (rotation around Z) of this matrix.
        /// The value is the same as <seealso cref="EulerAngles"/>.Roll, but this should be faster (it won't calculate the other euler angles).
        /// </summary>
        public Angle Rotation { get => Math1D.Atan2(r1c0, r0c0); set => this = Rotate(value) * this; }

        /// <summary>
        /// Gets/sets the scale of this matrix. It's called "Size" instead of "Scale" so it won't conflict with <seealso cref="Scale(Vector2D)"/>
        /// </summary>
        public Vector2D Size
        {
            get => new Vector2D(r0c0, r1c1);
            set
            {
                r0c0 = value.x;
                r1c1 = value.y;
            }
        }

        /// <summary>
        /// Gets/sets the 3D scale of this matrix. It's called "Size3D" instead of "Scale3D" so it won't conflict with <seealso cref="Scale3D(Vector3D)"/>.<para/>
        /// This should ONLY be used if this matrix does NOT have translation in it. Use <seealso cref="Matrix4x4"/> if 3D scale and translation are needed together.
        /// </summary>
        public Vector3D Size3D
        {
            get => new Vector3D(r0c0, r1c1, r2c2);
            set
            {
                r0c0 = value.x;
                r1c1 = value.y;
                r2c2 = value.z;
            }
        }
        #endregion

        #region Ways to transform vectors and combine matrices.
        /// <summary>
        /// Rotates a 3D vector by this matrix. It's recommended to use Matrix4x4 if translation is also needed.
        /// If this matrix is not a pure rotation matrix, it will apply the other transformations as well.
        /// </summary>
        public Vector3D RotateVector(Vector3D vector)
        {
            return this * vector;
        }

        /// <summary>
        /// Rotates a 2D vector by this matrix.
        /// </summary>
        public Vector2D RotateVector(Vector2D vector)
        {
            return (Vector2D)(this * new Vector3D(vector, 0));
        }

        /// <summary>
        /// Transforms a 3D vector by this matrix. If translation is used, use <seealso cref="Matrix4x4"/> instead.
        /// </summary>
        public Vector3D TransformVector(Vector3D vector)
        {
            return this * vector;
        }

        /// <summary>
        /// Transforms a 2D vector by this matrix.
        /// It's recommended to use a matrix that only rotates around z (<seealso cref="RotateZ(Angle)"/>), scales in 2D (<seealso cref="Scale(Vector2D)"/>), and/or translaltes.
        /// </summary>
        public Vector2D TransformVector(Vector2D vector)
        {
            return (Vector2D)(this * new Vector3D(vector, 1));
        }

        /// <summary>
        /// Creates a transformation matrix that will first perform this and then other.<para/>
        /// Exactly the same as <seealso cref="CombineTransformations(Matrix3x3)"/>.
        /// </summary>
        public Matrix3x3 CombineRotations(Matrix3x3 other)
        {
            return other * this;
        }

        /// <summary>
        /// Creates a transformation matrix that will first perform this and then other.<para/>
        /// Exactly the same as <seealso cref="CombineRotations(Matrix3x3)"/>.
        /// </summary>
        public Matrix3x3 CombineTransformations(Matrix3x3 other) => CombineRotations(other);

        /// <summary>
        /// Returns the transformation that needs to be combined with secondTransformation to get this.
        /// </summary>
        /// <param name="secondTransformation">The transformation that when applied to the return value of this method gives this.</param>
        public Matrix3x3 DiscombineTransformations(Matrix3x3 secondTransformation) => secondTransformation.Inverted * this;
        #endregion

        #region Multiplication.
        /// <summary>Multiplies this by other.</summary>
        public Matrix3x3 Multiply(Matrix3x3 other) => this * other;

        /// <summary>Multiplies a by b.</summary>
        public static Matrix3x3 operator *(Matrix3x3 a, Matrix3x3 b)
        {
            return new Matrix3x3(
                a.GetRow(0).Dot(b.GetColumn(0)), a.GetRow(0).Dot(b.GetColumn(1)), a.GetRow(0).Dot(b.GetColumn(2)),
                a.GetRow(1).Dot(b.GetColumn(0)), a.GetRow(1).Dot(b.GetColumn(1)), a.GetRow(1).Dot(b.GetColumn(2)),
                a.GetRow(2).Dot(b.GetColumn(0)), a.GetRow(2).Dot(b.GetColumn(1)), a.GetRow(2).Dot(b.GetColumn(2)));
        }

        /// <summary>Multiplies each element of this by scalar.</summary>
        public Matrix3x3 Multiply(float scalar) => this * scalar;

        /// <summary>Multiplies each element of matrix by scalar.</summary>
        public static Matrix3x3 operator *(Matrix3x3 matrix, float scalar)
        {
            return new Matrix3x3(
                matrix.r0c0 * scalar, matrix.r0c1 * scalar, matrix.r0c2 * scalar,
                matrix.r1c0 * scalar, matrix.r1c1 * scalar, matrix.r1c2 * scalar,
                matrix.r2c0 * scalar, matrix.r2c1 * scalar, matrix.r2c2 * scalar);
        }

        /// <summary>Multiplies each element of matrix by scalar.</summary>
        public static Matrix3x3 operator *(float scalar, Matrix3x3 matrix) => matrix * scalar;

        /// <summary>Multiplies this by vector (as a column vector).</summary>
        public Vector3D Multiply(Vector3D vector) => this * vector;

        /// <summary>Multiplies matrix by vector (as a column vector).</summary>
        public static Vector3D operator *(Matrix3x3 matrix, Vector3D vector) => vector.x * matrix.GetColumn(0) + vector.y * matrix.GetColumn(1) + vector.z * matrix.GetColumn(2);

        /// <summary>Multiplies vector (as a row vector) by matrix.</summary>
        public static Vector3D operator *(Vector3D vector, Matrix3x3 matrix) => vector.x * matrix.GetRow(0) + vector.y * matrix.GetRow(1) + vector.z * matrix.GetRow(2);
        #endregion

        #region Addition and subtraction.
        /// <summary>Adds every element of this to the corresponding element in other.</summary>
        public Matrix3x3 Add(Matrix3x3 other) => this + other;

        /// <summary>Adds every element of a to the corresponding element in b.</summary>
        public static Matrix3x3 operator +(Matrix3x3 a, Matrix3x3 b)
        {
            return new Matrix3x3(
                a.r0c0 + b.r0c0, a.r0c1 + b.r0c1, a.r0c2 + b.r0c2,
                a.r1c0 + b.r1c0, a.r1c1 + b.r1c1, a.r1c2 + b.r1c2,
                a.r2c0 + b.r2c0, a.r2c1 + b.r2c1, a.r2c2 + b.r2c2);
        }

        /// <summary>Subtracts every element of this by the corresponding element in other.</summary>
        public Matrix3x3 Subtract(Matrix3x3 other) => this - other;

        /// <summary>Subtracts every element of a by the corresponding element in other.</summary>
        public static Matrix3x3 operator -(Matrix3x3 a, Matrix3x3 b)
        {
            return new Matrix3x3(
                a.r0c0 - b.r0c0, a.r0c1 - b.r0c1, a.r0c2 - b.r0c2,
                a.r1c0 - b.r1c0, a.r1c1 - b.r1c1, a.r1c2 - b.r1c2,
                a.r2c0 - b.r2c0, a.r2c1 - b.r2c1, a.r2c2 - b.r2c2);
        }
        #endregion

        #region Negating, transposing, cofactor and absolute value.
        /// <summary>The matrix with all of its elements negated (multiplied by -1).</summary>
        public Matrix3x3 Negated => -this;

        /// <summary>Negates (multiplied by -1) all the elements of matrix.</summary>
        public static Matrix3x3 operator -(Matrix3x3 matrix)
        {
            return new Matrix3x3(
                -matrix.r0c0, -matrix.r0c1, -matrix.r0c2,
                -matrix.r1c0, -matrix.r1c1, -matrix.r1c2,
                -matrix.r2c0, -matrix.r2c1, -matrix.r2c2);
        }

        /// <summary>Gets the transpose (replaced rows with columns and columns with rows) of this matrix.</summary>
        public Matrix3x3 Transposed
        {
            get
            {
                return new Matrix3x3(
                    r0c0, r1c0, r2c0,
                    r0c1, r1c1, r2c1,
                    r0c2, r1c2, r2c2);
            }
        }

        /// <summary>Gets the cofactor of this matrix.</summary>
        public Matrix3x3 Cofactor
        {
            get
            {
                return new Matrix3x3(
                    new Matrix2x2(r1c1, r1c2, r2c1, r2c2).Determinant,
                    -new Matrix2x2(r1c0, r1c2, r2c0, r2c2).Determinant,
                    new Matrix2x2(r1c0, r1c1, r2c0, r2c1).Determinant,

                    -new Matrix2x2(r0c1, r0c2, r2c1, r2c2).Determinant,
                    new Matrix2x2(r0c0, r0c2, r2c0, r2c2).Determinant,
                    -new Matrix2x2(r0c0, r0c1, r2c0, r2c1).Determinant,

                    new Matrix2x2(r0c1, r0c2, r1c1, r1c2).Determinant,
                    -new Matrix2x2(r0c0, r0c2, r1c0, r1c2).Determinant,
                    new Matrix2x2(r0c0, r0c1, r1c0, r1c1).Determinant);
            }
        }

        /// <summary>Gets a new matrix where each element has the absolute value of the corresponding element in this.</summary>
        public Matrix3x3 Abs
        {
            get
            {
                return new Matrix3x3(
                    r0c0.Abs(), r0c1.Abs(), r0c2.Abs(),
                    r1c0.Abs(), r1c1.Abs(), r1c2.Abs(),
                    r2c0.Abs(), r2c1.Abs(), r2c2.Abs());
            }
        }
        #endregion

        #region Inverting and adjugate.
        /// <summary>Gets the inverse of this matrix.</summary>
        public Matrix3x3 Inverted => Matrix.Invert(this);

        /// <summary>Gets the adjugate (transpose of cofactor) of this matrix.</summary>
        public Matrix3x3 Adjugate => Matrix.Adjugate(this);
        #endregion

        #region Determinant and Trace.
        /// <summary>The determinant (how much the area of a 1x1 square will be multiplied by after it's transformed with this matrix) of this matrix.</summary>
        public float Determinant
        {
            get
            {
                return
                    r0c0 * new Matrix2x2(r1c1, r1c2, r2c1, r2c2).Determinant -
                    r0c1 * new Matrix2x2(r1c0, r1c2, r2c0, r2c2).Determinant +
                    r0c2 * new Matrix2x2(r1c0, r1c1, r2c0, r2c1).Determinant;
            }
        }

        /// <summary>Gest the trace (sum of diagonal elements) of this matrix.</summary>
        public float Trace => r0c0 + r1c1 + r2c2;
        #endregion

        #region Clamp methods (Clamp, ClampElements, ClampRows, ClampColumns).
        /// <summary>Clamps each element of this so it's never smaller than the corresponding element in min and never bigger than the corresponding element in max.</summary>
        public Matrix3x3 Clamp(Matrix3x3 min, Matrix3x3 max)
        {
            return new Matrix3x3(
                r0c0.Clamp(min.r0c0, max.r0c0), r0c1.Clamp(min.r0c1, max.r0c1), r0c2.Clamp(min.r0c2, max.r0c2),
                r1c0.Clamp(min.r1c0, max.r1c0), r1c1.Clamp(min.r1c1, max.r1c1), r1c2.Clamp(min.r1c2, max.r1c2),
                r2c0.Clamp(min.r2c0, max.r2c0), r2c1.Clamp(min.r2c1, max.r2c1), r2c2.Clamp(min.r2c2, max.r2c2));
        }

        /// <summary>Clamps each element of this so it's never smaller than min and never bigger than max.</summary>
        public Matrix3x3 ClampElements(float min, float max)
        {
            return new Matrix3x3(
                r0c0.Clamp(min, max), r0c1.Clamp(min, max), r0c2.Clamp(min, max),
                r1c0.Clamp(min, max), r1c1.Clamp(min, max), r1c2.Clamp(min, max),
                r2c0.Clamp(min, max), r2c1.Clamp(min, max), r2c2.Clamp(min, max));
        }

        /// <summary>Clamps each element of each row in this so that it's never smaller than the corresponding element in min and never bigger than the corresponding element in max.</summary>
        public Matrix3x3 ClampRows(Vector3D min, Vector3D max) => FromRows(GetRow(0).Clamp(min, max), GetRow(1).Clamp(min, max), GetRow(2).Clamp(min, max));

        /// <summary>Clamps each element of each column in this so that it's never smaller than the corresponding element in min and never bigger than the corresponding element in max.</summary>
        public Matrix3x3 ClampColumns(Vector3D min, Vector3D max) => FromColumns(GetColumn(0).Clamp(min, max), GetColumn(1).Clamp(min, max), GetColumn(2).Clamp(min, max));
        #endregion

        #region Comparison methods (Equals, Operators, GetHashCode, CloseEnough).
        /// <summary>
        /// Are all of the elements of other <seealso cref="SMath.CloseEnough(float, float, float)"/> to the elements of this?
        /// </summary>
        public bool CloseEnough(Matrix3x3 other, float threshold = Math1D.Epsilon)
        {
            return
                r0c0.CloseEnough(other.r0c0, threshold) && r0c1.CloseEnough(other.r0c1, threshold) && r0c2.CloseEnough(other.r0c2, threshold) &&
                r1c0.CloseEnough(other.r1c0, threshold) && r1c1.CloseEnough(other.r1c1, threshold) && r1c2.CloseEnough(other.r1c2, threshold) &&
                r2c0.CloseEnough(other.r2c0, threshold) && r2c1.CloseEnough(other.r2c1, threshold) && r2c2.CloseEnough(other.r2c2, threshold);
        }

        /// <summary>Is obj a Matrix3x3? If so, are all of the components equal?</summary>
        public override bool Equals(object obj)
        {
            if (!(obj is Matrix3x3))
                return false;

            return Equals((Matrix3x3)obj);
        }

        public override int GetHashCode()
        {
            return
                r0c0.GetHashCode() ^ r0c1.GetHashCode() ^ r0c2.GetHashCode() ^
                r1c0.GetHashCode() ^ r1c1.GetHashCode() ^ r1c2.GetHashCode() ^
                r2c0.GetHashCode() ^ r2c1.GetHashCode() ^ r2c2.GetHashCode();
        }

        /// <summary>Are all of the components of the matrices equal?</summary>
        public bool Equals(Matrix3x3 other)
        {
            return this == other;
        }

        /// <summary>Are all of the components of the matrices equal?</summary>
        public static bool operator ==(Matrix3x3 left, Matrix3x3 right)
        {
            return
                left.r0c0 == right.r0c0 && left.r0c1 == right.r0c1 && left.r0c2 == right.r0c2 &&
                left.r1c0 == right.r1c0 && left.r1c1 == right.r1c1 && left.r1c2 == right.r1c2 &&
                left.r2c0 == right.r2c0 && left.r2c1 == right.r2c1 && left.r2c2 == right.r2c2;
        }

        /// <summary>Are any of the components of the matrices NOT equal?</summary>
        public static bool operator !=(Matrix3x3 left, Matrix3x3 right)
        {
            return !(left == right);
        }
        #endregion

        #region Conversions.
        /// <summary>
        /// Uses the top left elements of matrix as the elements for the new matrix.
        /// </summary>
        public static explicit operator Matrix2x2(Matrix3x3 matrix)
        {
            return new Matrix2x2(
                matrix.r0c0, matrix.r0c1,
                matrix.r1c0, matrix.r1c1);
        }

        /// <summary>
        /// Uses the elements of matrix for the top left corner of the new matrix and sets the bottom right element of the new matrix to 1.
        /// </summary>
        public static implicit operator Matrix4x4(Matrix3x3 matrix)
        {
            return new Matrix4x4(
                matrix.r0c0, matrix.r0c1, matrix.r0c2, 0,
                matrix.r1c0, matrix.r1c1, matrix.r1c2, 0,
                matrix.r2c0, matrix.r2c1, matrix.r2c2, 0,
                0, 0, 0, 1);
        }

        public override string ToString()
        {
            return $@"
[{r0c0}, {r0c1}, {r0c2}]
[{r1c0}, {r1c1}, {r1c2}]
[{r2c0}, {r2c1}, {r2c2}]
";
        }
        #endregion
    }
}