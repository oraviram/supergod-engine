﻿using System;

namespace SupergodUtilities.Math
{
    public struct Matrix2x2 : IMatrix<Matrix2x2, Vector2D>
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Matrix2x2));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        #region Presets for common matrices.
        /// <summary>
        /// [0, 0]<para/>
        /// [0, 0]
        /// </summary>
        public static readonly Matrix2x2 zero = new Matrix2x2(
            0, 0,
            0, 0);

        /// <summary>
        /// [1, 1]<para/>
        /// [1, 1]
        /// </summary>
        public static readonly Matrix2x2 one = new Matrix2x2(
            1, 1,
            1, 1);

        /// <summary>
        /// [1/0, 1/0]<para/>
        /// [1/0, 1/0]
        /// </summary>
        public static readonly Matrix2x2 infinity = new Matrix2x2(
            Math1D.Infinity, Math1D.Infinity,
            Math1D.Infinity, Math1D.Infinity);

        /// <summary>
        /// [0/0, 0/0]<para/>
        /// [0/0, 0/0]
        /// </summary>
        public static readonly Matrix2x2 NaN = new Matrix2x2(
            Math1D.NaN, Math1D.NaN,
            Math1D.NaN, Math1D.NaN);

        /// <summary>
        /// [1, 0]<para/>
        /// [0, 1]
        /// </summary>
        public static readonly Matrix2x2 identity = new Matrix2x2(
            1, 0,
            0, 1);
        #endregion

        public float r0c0, r0c1;
        public float r1c0, r1c1;

        public Matrix2x2(float r0c0 = 1, float r0c1 = 0,
                         float r1c0 = 0, float r1c1 = 1)
        {
            this.r0c0 = r0c0; this.r0c1 = r0c1;
            this.r1c0 = r1c0; this.r1c1 = r1c1;
        }

        /// <summary>
        /// Creates a new matrix with vectors for the rows.
        /// </summary>
        public static Matrix2x2 FromRows(Vector2D firstRow, Vector2D secondRow)
        {
            return new Matrix2x2(
                firstRow.x, firstRow.y,
                secondRow.x, secondRow.y);
        }

        /// <summary>
        /// Creates a new matrix with vectors for the columns.
        /// </summary>
        public static Matrix2x2 FromColumns(Vector2D firstColumn, Vector2D secondColumn)
        {
            return new Matrix2x2(
                firstColumn.x, secondColumn.x,
                firstColumn.y, secondColumn.y);
        }

        #region As arrays.
        /// <summary>
        /// Gets the elements of the matrix as a 2D float array.
        /// </summary>
        public float[,] AsFloatArray => new float[,] 
        {
            { r0c0, r0c1 },
            { r1c0, r1c1 }
        };

        /// <summary>
        /// Gets the rows of the matrix as a vector array.
        /// </summary>
        public Vector2D[] Rows => new Vector2D[] 
        {
            new Vector2D(r0c0, r0c1),
            new Vector2D(r1c0, r1c1)
        };

        /// <summary>
        /// Gets the columns of the matrix as a vector array.
        /// </summary>
        public Vector2D[] Columns => new Vector2D[]
        {
            new Vector2D(r0c0, r1c0),
            new Vector2D(r0c1, r1c1)
        };
        #endregion

        #region Getters and setters.
        private const string INDEX_OUT_OF_RANGE_MESSAGE = "The column/row of the get/set methods in Matrix2x2 must be either 0 or 1!";

        /// <summary>
        /// Gets/sets an element at the row of row and column of column.
        /// </summary>
        public float this[int row, int column]
        {
            get => GetRow(row)[column];
            set
            {
                Vector2D newRow = GetRow(row);
                newRow[column] = value;
                SetRow(row, newRow);
            }
        }
        
        /// <summary>
        /// Gets a column at the index of index.
        /// </summary>
        /// <param name="index">The index of the column (from left to right, must be either 0 or 1).</param>
        /// <exception cref="IndexOutOfRangeException">Index was something other than 0 and 1.</exception>
        public Vector2D GetColumn(int index)
        {
            switch (index)
            {
                case 0:
                    return new Vector2D(r0c0, r1c0);

                case 1:
                    return new Vector2D(r0c1, r1c1);
            }
            throw new IndexOutOfRangeException(INDEX_OUT_OF_RANGE_MESSAGE);
        }

        /// <summary>
        /// Gets a row at the index of index.
        /// </summary>
        /// <param name="index">The index of the column (from top to bottom, must be either 0 or 1).</param>
        /// <exception cref="IndexOutOfRangeException">Index was something other than 0 and 1.</exception>
        public Vector2D GetRow(int index)
        {
            switch (index)
            {
                case 0:
                    return new Vector2D(r0c0, r0c1);

                case 1:
                    return new Vector2D(r1c0, r1c1);
            }
            throw new IndexOutOfRangeException(INDEX_OUT_OF_RANGE_MESSAGE);
        }

        /// <summary>
        /// Sets a column at the index of index to value.
        /// </summary>
        /// <param name="index">The index of the column (from left to right, must be either 0 or 1).</param>
        /// <exception cref="IndexOutOfRangeException">Index was something other than 0 and 1.</exception>
        public void SetColumn(int index, Vector2D value)
        {
            switch (index)
            {
                case 0:
                    r0c0 = value.x;
                    r1c0 = value.y;
                    break;

                case 1:
                    r0c1 = value.x;
                    r1c1 = value.y;
                    break;

                default:
                    throw new IndexOutOfRangeException(INDEX_OUT_OF_RANGE_MESSAGE);
            }
        }

        /// <summary>
        /// Sets a row at the index of index to value.
        /// </summary>
        /// <param name="index">The index of the column (from top to bottom, must be either 0 or 1).</param>
        /// <exception cref="IndexOutOfRangeException">Index was something other than 0 and 1.</exception>
        public void SetRow(int index, Vector2D value)
        {
            switch (index)
            {
                case 0:
                    r0c0 = value.x;
                    r0c1 = value.y;
                    break;

                case 1:
                    r1c0 = value.x;
                    r1c1 = value.y;
                    break;

                default:
                    throw new IndexOutOfRangeException(INDEX_OUT_OF_RANGE_MESSAGE);
            }
        }
        #endregion

        #region Transformations.
        /// <summary>
        /// Creates a shear transformation matrix.
        /// </summary>
        /// <param name="shear">The shear to make.</param>
        public static Matrix2x2 Shear(Vector2D shear)
        {
            return Shear(shear.x, shear.y);
        }

        /// <summary>
        /// Creates a shear transformation matrix.
        /// </summary>
        /// <param name="x">The shear on x to make.</param>
        /// <param name="y">The shear on y to make.</param>
        public static Matrix2x2 Shear(float x, float y)
        {
            return new Matrix2x2(
                1, x,
                y, 1);
        }

        /// <summary>
        /// Creates a scale transformation matrix.
        /// </summary>
        /// <param name="scale">The amount to scale.</param>
        public static Matrix2x2 Scale(Vector2D scale)
        {
            return Scale(scale.x, scale.y);
        }

        /// <summary>
        /// Creates a scale transformation matrix.
        /// </summary>
        /// <param name="x">The amount to scale on x.</param>
        /// <param name="y">The amount to scale on y.</param>
        public static Matrix2x2 Scale(float x, float y)
        {
            return new Matrix2x2(
                x, 0,
                0, y);
        }

        /// <summary>
        /// Creates a rotation matrix around the z axis with the angle of angle.
        /// </summary>
        /// <param name="angle">Angle to create the rotation with.</param>
        public static Matrix2x2 Rotate(Angle angle)
        {
            float cos = Math1D.Cos(angle);
            float sin = Math1D.Sin(angle);
            return new Matrix2x2(
                cos, -sin,
                sin, cos);
        }
        #endregion

        #region Multiplication.
        /// <summary>Multiplies this by other.</summary>
        public Matrix2x2 Multiply(Matrix2x2 other) => this * other;
        
        /// <summary>Multiplies a by b.</summary>
        public static Matrix2x2 operator *(Matrix2x2 a, Matrix2x2 b)
        {
            return new Matrix2x2(
                a.GetRow(0).Dot(b.GetColumn(0)), a.GetRow(0).Dot(b.GetColumn(1)),
                a.GetRow(1).Dot(b.GetColumn(0)), a.GetRow(1).Dot(b.GetColumn(1)));
        }

        /// <summary>Multiplies each element of this by scalar.</summary>
        public Matrix2x2 Multiply(float scalar) => this * scalar;
        
        /// <summary>Multiplies each element of matrix by scalar.</summary>
        public static Matrix2x2 operator *(Matrix2x2 matrix, float scalar) => new Matrix2x2(matrix.r0c0 * scalar, matrix.r0c1 * scalar, matrix.r1c0 * scalar, matrix.r1c1 * scalar);

        /// <summary>Multiplies each element of matrix by scalar.</summary>
        public static Matrix2x2 operator *(float scalar, Matrix2x2 matrix) => matrix * scalar;

        /// <summary>Multiplies this by vector (as a column vector).</summary>
        public Vector2D Multiply(Vector2D vector) => this * vector;

        /// <summary>Multiplies matrix by vector (as a column vector).</summary>
        public static Vector2D operator *(Matrix2x2 matrix, Vector2D vector) => vector.x * matrix.GetColumn(0) + vector.y * matrix.GetColumn(1);

        /// <summary>Multiplies vector (as a row vector) by matrix.</summary>
        public static Vector2D operator *(Vector2D vector, Matrix2x2 matrix) => vector.x * matrix.GetRow(0) + vector.y * matrix.GetRow(1);
        #endregion

        #region Addition and subtraction.
        /// <summary>Adds every element of this to the corresponding element in other.</summary>
        public Matrix2x2 Add(Matrix2x2 other) => this + other;

        /// <summary>Adds every element of a to the corresponding element in b.</summary>
        public static Matrix2x2 operator +(Matrix2x2 a, Matrix2x2 b) => new Matrix2x2(a.r0c0 + b.r0c0, a.r0c1 + b.r0c1, a.r1c0 + b.r1c0, a.r1c1 + b.r1c1);

        /// <summary>Subtracts every element of this by the corresponding element in other.</summary>
        public Matrix2x2 Subtract(Matrix2x2 other) => this - other;

        /// <summary>Subtracts every element of a by the corresponding element in other.</summary>
        public static Matrix2x2 operator -(Matrix2x2 a, Matrix2x2 b) => new Matrix2x2(a.r0c0 - b.r0c0, a.r0c1 - b.r0c1, a.r1c0 - b.r1c0, a.r1c1 - b.r1c1);
        #endregion

        #region Negating, transposing, cofactor and absolute value.
        /// <summary>The matrix with all of its elements negated (multiplied by -1).</summary>
        public Matrix2x2 Negated => -this;

        /// <summary>Negates (multiplied by -1) all the elements of matrix.</summary>
        public static Matrix2x2 operator -(Matrix2x2 matrix) => new Matrix2x2(-matrix.r0c0, -matrix.r0c1, -matrix.r1c0, -matrix.r1c1);

        /// <summary>Gets the transpose (replaced rows with columns and columns with rows) of this matrix.</summary>
        public Matrix2x2 Transposed => new Matrix2x2(r0c0, r1c0, r0c1, r1c1);

        /// <summary>Gets the cofactor of this matrix.</summary>
        public Matrix2x2 Cofactor => new Matrix2x2(r1c1, -r1c0, -r0c1, r0c0);

        /// <summary>Gets a new matrix where each element has the absolute value of the corresponding element in this.</summary>
        public Matrix2x2 Abs => new Matrix2x2(r0c0.Abs(), r0c1.Abs(), r1c0.Abs(), r1c1.Abs());
        #endregion

        #region Inverting and adjugate.
        /// <summary>Gets the inverse of this matrix.</summary>
        public Matrix2x2 Inverted => Matrix.Invert(this);

        /// <summary>Gets the adjugate (transpose of cofactor) of this matrix.</summary>
        public Matrix2x2 Adjugate => Matrix.Adjugate(this);
        #endregion

        #region Determinant and Trace.
        /// <summary>The determinant (how much the area of a 1x1 square will be multiplied by after it's transformed with this matrix) of this matrix.</summary>
        public float Determinant => r0c0 * r1c1 - r0c1 * r1c0;

        /// <summary>Gest the trace (sum of diagonal elements) of this matrix.</summary>
        public float Trace => r0c0 + r1c1;
        #endregion

        #region Clamp methods (Clamp, ClampElements, ClampRows, ClampColumns).
        /// <summary>Clamps each element of this so it's never smaller than the corresponding element in min and never bigger than the corresponding element in max.</summary>
        public Matrix2x2 Clamp(Matrix2x2 min, Matrix2x2 max) => new Matrix2x2(r0c0.Clamp(min.r0c0, max.r0c0), r0c1.Clamp(min.r0c1, max.r0c1), r1c0.Clamp(min.r1c0, max.r1c0), r1c1.Clamp(min.r1c1, max.r1c1));

        /// <summary>Clamps each element of this so it's never smaller than min and never bigger than max.</summary>
        public Matrix2x2 ClampElements(float min, float max) => new Matrix2x2(r0c0.Clamp(min, max), r0c1.Clamp(min, max), r1c0.Clamp(min, max), r1c1.Clamp(min, max));

        /// <summary>Clamps each element of each row in this so that it's never smaller than the corresponding element in min and never bigger than the corresponding element in max.</summary>
        public Matrix2x2 ClampRows(Vector2D min, Vector2D max) => FromRows(GetRow(0).Clamp(min, max), GetRow(1).Clamp(min, max));

        /// <summary>Clamps each element of each column in this so that it's never smaller than the corresponding element in min and never bigger than the corresponding element in max.</summary>
        public Matrix2x2 ClampColumns(Vector2D min, Vector2D max) => FromColumns(GetColumn(0).Clamp(min, max), GetColumn(1).Clamp(min, max));
        #endregion

        #region Comparison methods (Equals, Operators, GetHashCode, CloseEnough).
        /// <summary>
        /// Are all of the elements of other <seealso cref="SMath.CloseEnough(float, float, float)"/> to the elements of this?
        /// </summary>
        public bool CloseEnough(Matrix2x2 other, float threshold = Math1D.Epsilon)
        {
            return r0c0.CloseEnough(other.r0c0, threshold) && r0c1.CloseEnough(other.r0c1, threshold) &&
                r1c0.CloseEnough(other.r1c0, threshold) && r1c1.CloseEnough(other.r1c1, threshold);
        }

        /// <summary>Is obj a Matrix2x2? If so, are all of the components equal?</summary>
        public override bool Equals(object obj)
        {
            if (!(obj is Matrix2x2))
                return false;

            return Equals((Matrix2x2)obj);
        }

        public override int GetHashCode()
        {
            return r0c0.GetHashCode() ^ r0c1.GetHashCode() ^ r1c0.GetHashCode() ^ r1c1.GetHashCode();
        }

        /// <summary>Are all of the components of the matrices equal?</summary>
        public bool Equals(Matrix2x2 other)
        {
            return this == other;
        }

        /// <summary>Are all of the components of the matrices equal?</summary>
        public static bool operator ==(Matrix2x2 left, Matrix2x2 right)
        {
            return left.r0c0 == right.r0c0 && left.r0c1 == right.r0c1 && left.r1c0 == right.r1c0 && left.r1c1 == right.r1c1;
        }

        /// <summary>Are any of the components of the matrices NOT equal?</summary>
        public static bool operator !=(Matrix2x2 left, Matrix2x2 right)
        {
            return !(left == right);
        }
        #endregion

        #region Conversions.
        /// <summary>
        /// Uses matrix as the top left matrix and initializes the bottom right element to 1.
        /// </summary>
        public static implicit operator Matrix3x3(Matrix2x2 matrix)
        {
            return new Matrix3x3(
                matrix.r0c0, matrix.r0c1, 0,
                matrix.r1c0, matrix.r1c1, 0,
                0, 0, 1);
        }

        /// <summary>
        /// Uses matrix as the top left matrix and initializes the other elements in the diagonal to 1.
        /// </summary>
        public static implicit operator Matrix4x4(Matrix2x2 matrix)
        {
            return new Matrix4x4(
                matrix.r0c0, matrix.r0c1, 0, 0,
                matrix.r1c0, matrix.r1c1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1);
        }

        public override string ToString()
        {
            return $@"
[{r0c0}, {r0c1}]
[{r1c0}, {r1c1}]
";
        }
        #endregion
    }
}