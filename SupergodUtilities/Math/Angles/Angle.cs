﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// Struct wraping angles into radians, degrees and revolutions. It also handles wraping the value in a valid range for angles.
    /// </summary>
    public struct Angle : IClampable<Angle>, IEquatable<Angle>, ILerpable<Angle>, ISupergodEquatable<Angle>
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Angle));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        #region Common angle presets.
        /// <summary>A right angle. 90 degrees, pi / 2 radians, 1/4 revolutions.</summary>
        public static readonly Angle right = Math1D.PI / 2;

        /// <summary>Half of a right angle. pi / 4 radians, 45 degrees, 0.125 revolution.</summary>
        public static readonly Angle halfRight = Math1D.PI / 4;

        /// <summary>A straight angle. 180 degrees, pi radians, 1/2 revolutions.</summary>
        public static readonly Angle straight = Math1D.PI;

        /// <summary>A zero angle.</summary>
        public static readonly Angle zero = 0;

        /// <summary>An angle that is a full revolution. 2pi radians, 360 degrees, 1 revolution.</summary>
        public static readonly Angle fullRotation = Math1D.PI * 2;

        /// <summary>An angle that is a right angle before a full rotation. 1.5pi radians, 270 degrees, 0.75 revolution.</summary>
        public static readonly Angle straightAndHalf = Math1D.PI * 1.5f;
        #endregion

        #region Angle conversions constants.
        /// <summary>Multiply a radian by this to get the equivelant angle in degrees.</summary>
        public const float Rad2Deg = 360 / (2 * Math1D.PI);

        /// <summary>Multiply a radian by this to get the equivelant angle in revolutions.</summary>
        public const float Rad2Rev = 1 / (2 * Math1D.PI);

        /// <summary>Multiply degrees by this to get the equivelant angle in radians.</summary>
        public const float Deg2Rad = Math1D.PI * 2 / 360;

        /// <summary>Multiply degrees by this to get the equivelant angle in revolutions.</summary>
        public const float Deg2Rev = 1 / 360f;

        /// <summary>Multiply revolutions by this to get the equivelant angle in degrees.</summary>
        public const float Rev2Deg = 360;

        /// <summary>Multiply revolutions by this to get the equivelant angle in radians.</summary>
        public const float Rev2Rad = Math1D.PI * 2;
        #endregion

        #region Wrapping for floats.
        /// <summary>
        /// Wraps value between 0 to 2pi using <seealso cref="Math1D.Wrap(float, float)"/>.
        /// </summary>
        public static float WrapRadians(float value)
        {
            return Math1D.Wrap(value, Math1D.PI * 2);
        }

        /// <summary>
        /// Wraps value between 0 to 2pi using <seealso cref="Math1D.Wrap(float, float)"/>.
        /// </summary>
        public static float WrapDegrees(float value)
        {
            return Math1D.Wrap(value, 360);
        }

        /// <summary>
        /// Wraps value between 0 to 2pi using <seealso cref="Math1D.Wrap(float, float)"/>.
        /// </summary>
        public static float WrapRevolutions(float value)
        {
            return Math1D.Wrap(value, 1);
        }
        #endregion

        private float _radians;

        /// <summary>
        /// The angle represented as radians in range [0, 2pi].
        /// </summary>
        public float Radians
        {
            get { return _radians; }
            set { _radians = WrapRadians(value); }
        }

        /// <summary>
        /// The angle represented as radians in range [-pi, pi].
        /// </summary>
        public float NegativeRadians
        {
            get { return Radians - Math1D.PI; }
            set { Radians = value + Math1D.PI; }
        }

        /// <summary>
        /// The angle represented as degrees in range [0, 360].
        /// </summary>
        public float Degrees
        {
            get { return Radians * Rad2Deg; }
            set { Radians = value * Deg2Rad; }
        }

        /// <summary>
        /// The angle represented as degrees in range [-180, 180].
        /// </summary>
        public float NegativeDegrees
        {
            get { return Degrees - 180; }
            set { Degrees = value + 180; }
        }

        /// <summary>
        /// The angle represented as revolutions in range [0, 1].
        /// </summary>
        public float Revolutions
        {
            get { return Radians * Rad2Rev; }
            set { Radians = value * Rev2Rad; }
        }

        /// <summary>
        /// The angle represented as revolutions in range [-0.5, 0.5].
        /// </summary>
        public float NegativeRevolutions
        {
            get { return Revolutions - .5f; }
            set { Revolutions = value + .5f; }
        }

        /// <summary>
        /// Creates a new angle with a value and wraps it to a valid range for angles.
        /// </summary>
        /// <param name="angle">The value of the angle.</param>
        /// <param name="measurement">The type of measurement to measure the angles with.</param>
        public Angle(float angle, AngleMeasurement measurement = AngleMeasurement.Radians) : this()
        {
            switch (measurement)
            {
                case AngleMeasurement.Degrees:
                    Degrees = angle;
                    break;

                case AngleMeasurement.Radians:
                    Radians = angle;
                    break;

                case AngleMeasurement.Revolutions:
                    Revolutions = angle;
                    break;

                default:
                    Radians = 0;
                    break;
            }
        }

        #region Comparison methods.
        /// <summary>Are the radians of this and obj the same? Note that if obj is a number, it will be compared as radians.</summary>
        public override bool Equals(object obj)
        {
            if (obj is Angle)
                return Equals((Angle)obj);

            return false;
        }

        /// <summary>Are the radians of this and other the same?</summary>
        public bool Equals(Angle other) { return this == other; }

        /// <summary>Are the radians of left and right the same?</summary>
        public static bool operator ==(Angle left, Angle right) { return left.Radians == right.Radians; }

        /// <summary>Are the radians of left and right not the same?</summary>
        public static bool operator !=(Angle left, Angle right) { return !(left == right); }

        /// <summary>
        /// Are the radians of this and other <seealso cref="SMath.CloseEnough(float, float, float)"/>?
        /// </summary>
        public bool CloseEnough(Angle other, float threshold = Math1D.Epsilon)
        {
            return Radians.CloseEnough(other.Radians, threshold);
        }

        /// <summary>Are bigger's radians bigger than smaller's radians?</summary>
        public static bool BiggerThan(Angle bigger, Angle smaller) { return bigger > smaller; }

        /// <summary>Are the radians of this bigger than the radians of other?</summary>
        public bool BiggerThan(Angle other) { return BiggerThan(this, other); }

        /// <summary>Are bigger's radians bigger than smaller's radians?</summary>
        public static bool operator >(Angle bigger, Angle smaller)
        {
            return bigger.Radians > smaller.Radians;
        }

        /// <summary>Are smaller's radians smaller than bigger's radians?</summary>
        public static bool SmallerThan(Angle smaller, Angle bigger) { return smaller < bigger; }

        /// <summary>Are the radians of this smaller than the radians of other?</summary>
        public bool SmallerThan(Angle other) { return SmallerThan(this, other); }

        /// <summary>Are smaller's radians smaller than bigger's radians?</summary>
        public static bool operator <(Angle smaller, Angle bigger)
        {
            return smaller.Radians < bigger.Radians;
        }

        public override int GetHashCode()
        {
            return Radians.GetHashCode();
        }
        #endregion

        #region Scaling (scalar multiplication and division).
        /// <summary>Multiplies scalar by the radians of angle.</summary>
        public static Angle Multiply(float scalar, Angle angle) { return scalar * angle; }

        /// <summary>Multiplies the radians of this by scalar.</summary>
        public Angle Multiply(float scalar) { return Multiply(this, scalar); }

        /// <summary>Multiplies the radians of angle by scalar.</summary>
        public static Angle Multiply(Angle angle, float scalar) { return angle * scalar; }

        /// <summary>Multiplies scalar by the radians of angle.</summary>
        public static Angle operator *(float scalar, Angle angle) { return new Angle(angle.Radians * scalar, AngleMeasurement.Radians); }

        /// <summary>Multiplies the radians of angle by scalar.</summary>
        public static Angle operator *(Angle angle, float scalar) { return scalar * angle; }

        /// <summary>Divides the radians of angle by scalar.</summary>
        public static Angle Divide(Angle angle, float scalar) { return angle / scalar; }

        /// <summary>Divides the radians of this by scalar.</summary>
        public Angle Divide(float scalar) { return Divide(this, scalar); }

        /// <summary>Divides the radians of angle by scalar.</summary>
        public static Angle operator /(Angle angle, float scalar) { return new Angle(angle.Radians / scalar, AngleMeasurement.Radians); }
        #endregion

        #region Addition and subtraction.
        /// <summary>Adds the radians of a and b.</summary>
        public static Angle Add(Angle a, Angle b) { return a + b; }

        /// <summary>Adds the radians of this and other.</summary>
        public Angle Add(Angle other) { return Add(this, other); }

        /// <summary>Adds the radians of a and b.</summary>
        public static Angle operator +(Angle a, Angle b) { return new Angle(a.Radians + b.Radians, AngleMeasurement.Radians); }

        /// <summary>Subtracts the radians of b from the radians of a.</summary>
        public static Angle Subtract(Angle a, Angle b) { return a - b; }

        /// <summary>Subtracts the radians of other from the radians of this.</summary>
        public Angle Subtract(Angle other) { return Subtract(this, other); }

        /// <summary>Subtracts the radians of b from the radians of a.</summary>
        public static Angle operator -(Angle a, Angle b) { return new Angle(a.Radians - b.Radians, AngleMeasurement.Radians); }
        #endregion

        #region Flipping and reflecting.
        /// <summary>
        /// Flips angle, so it's pointing to the opposite angle (subtracts 90 degrees from the angle).
        /// </summary>
        public static Angle Flip(Angle angle)
        {
            return angle - straight;
        }

        /// <summary>
        /// Flips this angle, so it's pointing to the opposite angle (subtracts 90 degrees from the angle).
        /// </summary>
        public Angle Flipped => Flip(this);

        /// <summary>
        /// <seealso cref="Reflect(Angle)"/>s angle. It will take the rotation and instead of rotating counter clockwise, it will rotate clockwise.
        /// </summary>
        public static Angle operator -(Angle angle)
        {
            return fullRotation - angle;
        }

        /// <summary>
        /// Takes this rotation and puts it on the other side of the circle (subtract it from 360 degrees).
        /// </summary>
        /// <param name="angle">The angle to reflect.</param>
        public static Angle Reflect(Angle angle)
        {
            return -angle;
        }

        /// <summary>
        /// <seealso cref="Reflect(Angle)"/>s this angle.
        /// </summary>
        public Angle Reflection => Reflect(this);
        #endregion

        #region Clamping.
        /// <summary>
        /// Clamps this so it's never smaller than min and never bigger than max.
        /// </summary>
        public Angle Clamp(Angle min, Angle max)
        {
            return Radians.Clamp(min.Radians, max.Radians);
        }
        #endregion

        #region Conversions.
        /// <summary>
        /// Returns the radians as a string.
        /// </summary>
        public override string ToString()
        {
            return Radians.ToString();
        }

        /// <summary>
        /// Gets the radians of the angle.
        /// </summary>
        public static implicit operator float(Angle angle)
        {
            return angle.Radians;
        }

        /// <summary>
        /// Converts radians to an angle.
        /// </summary>
        public static implicit operator Angle(float radians)
        {
            return new Angle(radians, AngleMeasurement.Radians);
        }
        #endregion
    }
}