﻿namespace SupergodUtilities.Math
{
    /// <summary>
    /// Types of angle measurments.
    /// </summary>
    public enum AngleMeasurement
    {
        /// <summary>
        /// Angle measurment of degrees (full circle = 360)
        /// </summary>
        Degrees = 1,

        /// <summary>
        /// Angle measurment of radians (full circle = 2pi).
        /// </summary>
        Radians = 2,

        /// <summary>
        /// Angle measurment of revolutions (full circle = 1).
        /// </summary>
        Revolutions = 3,
    }
}