﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// A 4-dimentional complex number (w + x*i + y*j + z*k, where i² = j² = k² = ijk = -1).<para/>
    /// Used to represent rotations in 3D space. I really don't know what other uses you have for it, but you can use it for whatever you need a 4D complex number for.
    /// </summary>
    public struct Quaternion : IRotator<Quaternion>, IVector<Quaternion>
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Quaternion));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        #region Quaternion presets.
        /// <summary>The identity (no rotation) quaternion. w = 1, and all the other components are 0.</summary>
        public static readonly Quaternion identity = new Quaternion(1, 0, 0, 0);

        /// <summary>A quaternion with its x set to 0.</summary>
        public static readonly Quaternion i = new Quaternion(0, 1, 0, 0);

        /// <summary>A quaternion with its y set to 0.</summary>
        public static readonly Quaternion j = new Quaternion(0, 0, 1, 0);

        /// <summary>A quaternion with its z set to 0.</summary>
        public static readonly Quaternion k = new Quaternion(0, 0, 0, 1);

        /// <summary>A quaternion with all of its components set to NaN.</summary>
        public static readonly Quaternion NaN = new Quaternion(Math1D.NaN, Math1D.NaN, Math1D.NaN, Math1D.NaN);
        #endregion

        public float w, x, y, z;

        private const string INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE = "Component index in a Quaternion indexer must be either 0, 1, 2 or 3.";

        /// <summary>
        /// Gets or sets a component at the index of index.
        /// </summary>
        /// <param name="index">0 = w, 1 = x, 2 = y, 3 = z. Anything else will through an index out of range exception.</param>
        /// <exception cref="IndexOutOfRangeException">The given index wasn't 0, 1, 2 or 3.</exception>
        public float this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return w;

                    case 1:
                        return x;

                    case 2:
                        return y;

                    case 3:
                        return z;
                }
                throw new IndexOutOfRangeException(INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE);
            }
            set
            {
                switch (index)
                {
                    case 0:
                        w = value;
                        return;

                    case 1:
                        x = value;
                        return;

                    case 2:
                        y = value;
                        return;

                    case 3:
                        z = value;
                        return;
                }
                throw new IndexOutOfRangeException(INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE);
            }
        }

        /// <summary>
        /// Gets this quaternion as a float array with the order "w, x, y, z".
        /// </summary>
        public float[] AsFloatArray => new float[] { w, x, y, z };

        /// <summary>
        /// Initializes a new quaternion with an x, a y, a z, and w values.
        /// Note that it will not be normalized automatically, so make sure to normalize it if you call this construct (it's recommended to use the helper methods).
        /// </summary>
        public Quaternion(float w = 1, float x = 0, float y = 0, float z = 0)
        {
            this.w = w;
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /// <summary>
        /// Initializes a new quaternion with a vector from x y and z, and a float for w.
        /// Note that it will not be normalized automatically, so make sure to normalize it if you call this construct (it's recommended to use the helper methods).
        /// </summary>
        public Quaternion(float w, Vector3D xyz) : this(w, xyz.x, xyz.y, xyz.z)
        {
        }

        /// <summary>The x y and z components of the quaternion.</summary>
        public Vector3D XYZ => new Vector3D(x, y, z);

        /// <summary>
        /// Combines (other by this and normalizes the result) this quaternion and the other rotator as a quaternion.
        /// </summary>
        /// <param name="other">Rotation to apply to this (will be applied second).</param>
        public Quaternion CombineRotations(Quaternion other)
        {
            return Normalize(other * this);
        }

        /// <summary>
        /// Rotates vector by this quaternion.
        /// </summary>
        public Vector3D RotateVector(Vector3D vector)
        {
            return (this * new Quaternion(0, vector) * Conjugate).XYZ;
        }

        #region Construction methods from other types of rotations.
        /// <summary>
        /// Creates a quaternion from an axis to rotate around and an angle to rotate by.
        /// </summary>
        /// <param name="axis">Axis to rotate around.</param>
        /// <param name="angle">Angle to rotate by.</param>
        public static Quaternion FromAxisAngle(Vector3D axis, Angle angle)
        {
            float halfAngle = angle / 2;
            return new Quaternion(Math1D.Cos(halfAngle), axis * Math1D.Sin(halfAngle));
        }

        /// <summary>
        /// Creates a quaternion from an axis to rotate around and an angle to rotate by.
        /// </summary>
        /// <param name="axisAngle">The axis and the angle.</param>
        public static Quaternion FromAxisAngle(AxisAngle axisAngle)
        {
            return FromAxisAngle(axisAngle.axis, axisAngle.angle);
        }

        /// <summary>
        /// Rotates around the x axis (pitch) by angle.
        /// </summary>
        public static Quaternion RotateX(Angle angle)
        {
            float halfAngle = angle / 2;
            return new Quaternion(Math1D.Cos(halfAngle), Math1D.Sin(halfAngle), 0, 0);
        }

        /// <summary>
        /// Rotates around the y axis (yaw) by angle.
        /// </summary>
        public static Quaternion RotateY(Angle angle)
        {
            float halfAngle = angle / 2;
            return new Quaternion(Math1D.Cos(halfAngle), 0, Math1D.Sin(halfAngle), 0);
        }

        /// <summary>
        /// Rotates around the z axis (roll) by angle.
        /// </summary>
        public static Quaternion RotateZ(Angle angle)
        {
            float halfAngle = angle / 2;
            return new Quaternion(Math1D.Cos(halfAngle), 0, 0, Math1D.Sin(halfAngle));
        }

        /// <summary>
        /// Creates a quaternion from euler angles. Rotation order is first pitch, then yaw, then roll.
        /// </summary>
        /// <param name="pitch">The angle of rotation around x.</param>
        /// <param name="yaw">The angle of rotation around y.</param>
        /// <param name="roll">The angle of rotation around z.</param>
        public static Quaternion FromEulerAngles(Angle pitch, Angle yaw, Angle roll)
        {
            return RotateZ(roll) * RotateY(yaw) * RotateX(pitch);
        }

        /// <summary>
        /// Creates a quaternion from euler angles. Rotation order is first pitch, then yaw, then roll.
        /// </summary>
        /// <param name="angles">The angles of rotation.</param>
        public static Quaternion FromEulerAngles(EulerAngles angles)
        {
            return FromEulerAngles(angles.Pitch, angles.Yaw, angles.Roll);
        }

        /// <summary>
        /// Creates a quaternion from a rotation matrix. Note that this only works if rotationMatrix is a pure rotation matrix.
        /// </summary>
        /// <param name="rotationMatrix">The pure rotation matrix to create the quaternion from.</param>
        /// <returns>A quaternion representing the same rotation as rotationMatrix.</returns>
        public static Quaternion FromRotationMatrix(Matrix3x3 rotationMatrix)
        {
            float trace = rotationMatrix.r0c0 + rotationMatrix.r1c1 + rotationMatrix.r2c2;
            if (trace > 0)
            {
                float s = 0.5f / Math1D.Sqrt(trace + 1.0f);
                return new Quaternion(
                    0.25f / s,
                    (rotationMatrix.r2c1 - rotationMatrix.r1c2) * s,
                    (rotationMatrix.r0c2 - rotationMatrix.r2c0) * s,
                    (rotationMatrix.r1c0 - rotationMatrix.r0c1) * s);
            }
            else
            {
                if (rotationMatrix.r0c0 > rotationMatrix.r1c1 && rotationMatrix.r0c0 > rotationMatrix.r2c2)
                {
                    float s = 2.0f * Math1D.Sqrt(1.0f + rotationMatrix.r0c0 - rotationMatrix.r1c1 - rotationMatrix.r2c2);
                    return new Quaternion(
                        (rotationMatrix.r2c1 - rotationMatrix.r1c2) / s,
                        0.25f * s,
                        (rotationMatrix.r0c1 + rotationMatrix.r1c0) / s,
                        (rotationMatrix.r0c2 + rotationMatrix.r2c0) / s);
                }
                else if (rotationMatrix.r1c1 > rotationMatrix.r2c2)
                {
                    float s = 2.0f * Math1D.Sqrt(1.0f + rotationMatrix.r1c1 - rotationMatrix.r0c0 - rotationMatrix.r2c2);
                    return new Quaternion(
                        (rotationMatrix.r0c2 - rotationMatrix.r2c0) / s,
                        (rotationMatrix.r0c1 + rotationMatrix.r1c0) / s,
                        0.25f * s,
                        (rotationMatrix.r1c2 + rotationMatrix.r2c1) / s);
                }
                else
                {
                    float s = 2.0f * Math1D.Sqrt(1.0f + rotationMatrix.r2c2 - rotationMatrix.r0c0 - rotationMatrix.r1c1);
                    return new Quaternion(
                        (rotationMatrix.r1c0 - rotationMatrix.r0c1) / s,
                        (rotationMatrix.r0c2 + rotationMatrix.r2c0) / s,
                        (rotationMatrix.r1c2 + rotationMatrix.r2c1) / s,
                        0.25f * s);
                }
            }

            // This is an old and simple solution I like, but I think the above one hanles all cases, so it's better.
            // Don't worry, I will evantually learn why those formulas work, and then delete the useless things like this part.

            //float w = SMath.Sqrt((1 + rotationMatrix.r0c0 + rotationMatrix.r1c1 + rotationMatrix.r2c2)) / 2;
            //float fourTimesW = 4 * w;
            //return new Quaternion(
            //    w,
            //    (rotationMatrix.r2c1 - rotationMatrix.r1c2) / fourTimesW,
            //    (rotationMatrix.r0c2 - rotationMatrix.r2c0) / fourTimesW,
            //    (rotationMatrix.r1c0 - rotationMatrix.r0c1) / fourTimesW
            //    );
        }
        #endregion

        #region Conversion properties to/from other types of rotations.
        /// <summary>
        /// Gets/sets this quaternion as a 3x3 rotation matrix.
        /// </summary>
        public Matrix3x3 RotationMatrix3x3 { get => Matrix3x3.FromRotationQuaternion(this); set => this = FromRotationMatrix(value); }

        /// <summary>
        /// Gets/sets this quaternion as a 4x4 rotation matrix.
        /// </summary>
        public Matrix4x4 RotationMatrix { get => Matrix4x4.FromRotationQuaternion(this); set => this = FromRotationMatrix((Matrix3x3)value); }

        /// <summary>
        /// Gets/sets this quaternion as an axis to rotate around and an angle to rotate by.
        /// </summary>
        public AxisAngle AxisAngle { get => AxisAngle.FromRotationQuaternion(this); set => this = FromAxisAngle(value); }

        /// <summary>
        /// Gets/sets this quaternion as euler angles.
        /// </summary>
        public EulerAngles EulerAngles { get => EulerAngles.FromRotationQuaternion(this); set => this = FromEulerAngles(value); }

        /// <summary>
        /// Uh...
        /// </summary>
        Quaternion IRotator.Quaternion { get => this; set => this = value; }
        #endregion

        #region Helper construction methods.
        /// <summary>
        /// Constructs a new rotation that will take the shortest path from direction from to direction to.
        /// If the angle between from and to is 180 degrees the rotation axis will be around y.
        /// </summary>
        /// <param name="from">The direction to rotate from.</param>
        /// <param name="to">The direction to rotate to (through the shortest path).</param>
        public static Quaternion FromToRotation(Vector3D from, Vector3D to)
        {
            if (from == to)
                return identity;

            Angle angle = from.SmallestAngle(to);
            if (angle == Angle.straight)
                return FromAxisAngle(new Vector3D(-from.y + from.z, from.x, -from.x).Normalized, Angle.straight);

            return FromAxisAngle(from.Cross(to).Normalized, angle);
        }

        /// <summary>
        /// Creates a quaternion rotation looking at the direction of forward with the up direction up.
        /// </summary>
        public static Quaternion Look(Vector3D forward, Vector3D up)
        {
            if (up != forward)
            {
                Vector3D v = forward + up * -up.Dot(forward);
                Quaternion q = FromToRotation(Vector3D.forward, v);
                return FromToRotation(v, forward) * q;
            }
            else
                return FromToRotation(Vector3D.forward, forward);
        }

        /// <summary>
        /// Creates a quaternion rotation looking at the direction of forward with the up direction (0, 1, 0).
        /// </summary>
        public static Quaternion Look(Vector3D forward)
        {
            return Look(forward, Vector3D.up);
        }

        /// <summary>
        /// Creates a rotation that will look at target from origin.
        /// </summary>
        /// <param name="origin">The point the quaternion should look from.</param>
        /// <param name="target">The point the quaternion should look at.</param>
        /// <param name="up">The local up direction of origin.</param>
        public static Quaternion LookAt(Vector3D origin, Vector3D target, Vector3D up)
        {
            return Look(origin.PointAt(target), up);
        }

        public static Quaternion LookAt(Vector3D origin, Vector3D target)
        {
            return LookAt(origin, target, Vector3D.up);
        }
        #endregion

        #region Addition and subtraction.
        /// <summary>
        /// Adds every component of a to its corresponding component in b.
        /// </summary>
        public static Quaternion operator +(Quaternion a, Quaternion b)
        {
            return new Quaternion(a.w + b.w, a.XYZ + b.XYZ);
        }

        /// <summary>
        /// Adds every component of a to its corresponding component in b.
        /// </summary>
        public static Quaternion Add(Quaternion a, Quaternion b)
        {
            return a + b;
        }

        /// <summary>
        /// Adds every component of this to its corresponding component in other.
        /// </summary>
        public Quaternion Add(Quaternion other)
        {
            return Add(this, other);
        }

        /// <summary>
        /// Subtracts every component of a by its corresponding component in b.
        /// </summary>
        public static Quaternion operator -(Quaternion a, Quaternion b)
        {
            return new Quaternion(a.w - b.w, a.XYZ - b.XYZ);
        }

        /// <summary>
        /// Subtracts every component of a by its corresponding component in b.
        /// </summary>
        public static Quaternion Subtract(Quaternion a, Quaternion b)
        {
            return a - b;
        }

        /// <summary>
        /// Subtracts every component of this by its corresponding component in other.
        /// </summary>
        public Quaternion Subtract(Quaternion other)
        {
            return Subtract(this, other);
        }
        #endregion

        #region Dot product.
        /// <summary>
        /// Gets the dot product of a and b.
        /// </summary>
        public static float Dot(Quaternion a, Quaternion b)
        {
            return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
        }

        /// <summary>
        /// Gets the dot product of this and other.
        /// </summary>
        public float Dot(Quaternion other)
        {
            return Dot(this, other);
        }
        #endregion

        #region Angle.
        /// <summary>
        /// Returns the smallest angle between left and right.
        /// </summary>
        /// <param name="normalizeLeft">Finding the angle requires normalizing the vectors, which is an expensive operation. So if left is already normalized, it's useless to normalize it again, so you can make this false. Else, it should be true, or the angle won't be correct.</param>
        /// <param name="normalizeRight">Finding the angle requires normalizing the vectors, which is an expensive operation. So if right is already normalized, it's useless to normalize it again, so you can make this false. Else, it should be true, or the angle won't be correct.</param>
        public static Angle SmallestAngle(Quaternion left, Quaternion right, bool normalizeLeft = true, bool normalizeRight = true)
        {
            if (normalizeLeft)
                left = left.Normalized;

            if (normalizeRight)
                right = right.Normalized;

            float dot = left.Dot(right);
            if (dot.Abs() > 1)
                dot = dot.Sign();

            return Math1D.Acos(dot);
        }

        /// <summary>
        /// Returns the smallest angle between this and other.
        /// </summary>
        /// <param name="normalizeLeft">Finding the angle requires normalizing the vectors, which is an expensive operation. So if left is already normalized, it's useless to normalize it again, so you can make this false. Else, it should be true, or the angle won't be correct.</param>
        /// <param name="normalizeRight">Finding the angle requires normalizing the vectors, which is an expensive operation. So if right is already normalized, it's useless to normalize it again, so you can make this false. Else, it should be true, or the angle won't be correct.</param>
        public Angle SmallestAngle(Quaternion other, bool normalizeLeft = true, bool normalizeRight = true)
        {
            return SmallestAngle(this, other, normalizeLeft, normalizeRight);
        }

        /// <summary>
        /// Returns the biggest angle between left and right.
        /// </summary>
        /// <param name="normalizeLeft">Finding the angle requires normalizing the vectors, which is an expensive operation. So if left is already normalized, it's useless to normalize it again, so you can make this false. Else, it should be true, or the angle won't be correct.</param>
        /// <param name="normalizeRight">Finding the angle requires normalizing the vectors, which is an expensive operation. So if right is already normalized, it's useless to normalize it again, so you can make this false. Else, it should be true, or the angle won't be correct.</param>
        public static Angle BiggestAngle(Quaternion left, Quaternion right, bool normalizeLeft = true, bool normalizeRight = true)
        {
            return Angle.fullRotation - SmallestAngle(left, right, normalizeLeft, normalizeRight);
        }

        /// <summary>
        /// Returns the smallest angle between this and other.
        /// </summary>
        /// <param name="normalizeLeft">Finding the angle requires normalizing the vectors, which is an expensive operation. So if left is already normalized, it's useless to normalize it again, so you can make this false. Else, it should be true, or the angle won't be correct.</param>
        /// <param name="normalizeRight">Finding the angle requires normalizing the vectors, which is an expensive operation. So if right is already normalized, it's useless to normalize it again, so you can make this false. Else, it should be true, or the angle won't be correct.</param>
        public Angle BiggestAngle(Quaternion other, bool normalizeLeft = true, bool normalizeRight = true)
        {
            return BiggestAngle(this, other, normalizeLeft, normalizeRight);
        }
        #endregion

        #region Quaternion multiplication.
        /// <summary>
        /// Multiplies a by b in the form "(a.w, a.x*i, a.y*j, a.z*k) * (b.w, b.x*i, b.y*j, b.z*k)".
        /// If a and b are unit length, it will combine the rotations.
        /// </summary>
        public static Quaternion Multiply(Quaternion a, Quaternion b)
        {
            return a * b;
        }

        /// <summary>
        /// Multiplies this by other in the form "(this.w, this.x*i, this.y*j, this.z*k) * (other.w, other.x*i, other.y*j, other.z*k)".
        /// If this and other are unit length, it will combine the rotations.
        /// </summary>
        public Quaternion Multiply(Quaternion other)
        {
            return Multiply(this, other);
        }

        /// <summary>
        /// Multiplies a by b in the form "(a.w, a.x*i, a.y*j, a.z*k) * (b.w, b.x*i, b.y*j, b.z*k)".
        /// If a and b are unit length, it will combine the rotations.
        /// </summary>
        public static Quaternion operator *(Quaternion a, Quaternion b)
        {
            return new Quaternion(
                -a.XYZ.Dot(b.XYZ) + a.w * b.w,
                a.w * b.XYZ + b.w * a.XYZ + a.XYZ.Cross(b.XYZ)
                );
        }
        #endregion

        #region Scalar division and multiplication.
        /// <summary>
        /// Divides every component of quaternion by scalar.
        /// </summary>
        public static Quaternion Divide(Quaternion quaternion, float scalar)
        {
            return quaternion / scalar;
        }

        /// <summary>
        /// Divides every component of this by scalar.
        /// </summary>
        public Quaternion Divide(float scalar)
        {
            return Divide(this, scalar);
        }

        /// <summary>
        /// Divides every component of quaternion by scalar.
        /// </summary>
        public static Quaternion operator /(Quaternion quaternion, float scalar)
        {
            return new Quaternion(quaternion.w / scalar, quaternion.x / scalar, quaternion.y / scalar, quaternion.z / scalar);
        }

        /// <summary>
        /// Multiplies every component of quaternion by scalar.
        /// </summary>
        public static Quaternion Multiply(Quaternion quaternion, float scalar)
        {
            return quaternion * scalar;
        }

        /// <summary>
        /// Multiplies every component of this by scalar.
        /// </summary>
        public Quaternion Multiply(float scalar)
        {
            return Multiply(this, scalar);
        }

        /// <summary>
        /// Multiplies every component of quaternion by scalar.
        /// </summary>
        public static Quaternion operator *(Quaternion quaternion, float scalar)
        {
            return new Quaternion(quaternion.w * scalar, quaternion.x * scalar, quaternion.y * scalar, quaternion.z * scalar);
        }

        /// <summary>
        /// Multiplies every component of quaternion by scalar.
        /// </summary>
        public static Quaternion Multiply(float scalar, Quaternion quaternion)
        {
            return scalar * quaternion;
        }

        /// <summary>
        /// Multiplies every component of quaternion by scalar.
        /// </summary>
        public static Quaternion operator *(float scalar, Quaternion quaternion)
        {
            return quaternion * scalar;
        }
        #endregion

        #region Quaternion/Quaternion and float/Quaternion.
        /// <summary>
        /// Divides divided by divisor (by multiplying both the divided and the divisor by the conjugate of the divisor and then performing the division).
        /// </summary>
        public static Quaternion Divide(Quaternion divided, Quaternion divisor)
        {
            return divided / divisor;
        }

        /// <summary>
        /// Divides this by the divisor (by multiplying both the divided and the divisor by the conjugate of the divisor and then performing the division).
        /// </summary>
        public Quaternion Divide(Quaternion divisor)
        {
            return Divide(this, divisor);
        }

        /// <summary>
        /// Divides divided by divisor (by multiplying both the divided and the divisor by the conjugate of the divisor and then performing the division).
        /// </summary>
        public static Quaternion operator /(Quaternion divided, Quaternion divisor)
        {
            return divided * divisor.Conjugate / divisor.SqrMagnitude;
        }

        /// <summary>
        /// Divides scalar by quaternion (by multiplying both the scalar and the quaternion by the conjugate of quaternion and dividing the results).
        /// </summary>
        public static Quaternion Divide(float scalar, Quaternion quaternion)
        {
            return scalar / quaternion;
        }

        /// <summary>
        /// Divides scalar by quaternion (by multiplying both the scalar and the quaternion by the conjugate of quaternion and dividing the results).
        /// </summary>
        public static Quaternion operator /(float scalar, Quaternion quaternion)
        {
            return scalar * quaternion.Conjugate / quaternion.SqrMagnitude;
        }
        #endregion

        #region Inversion, conjugate, negation and absolute value.
        /// <summary>
        /// Gets the conjugate (negates the vector part) of quaternion. 
        /// </summary>
        public static Quaternion GetConjugate(Quaternion quaternion)
        {
            return new Quaternion(quaternion.w, -quaternion.XYZ);
        }

        /// <summary>
        /// Gets the conjugate (negates the vector part) of this quaternion. 
        /// </summary>
        public Quaternion Conjugate => GetConjugate(this);

        /// <summary>
        /// Gets the inverse of quaternion.
        /// </summary>
        public static Quaternion Invert(Quaternion quaternion)
        {
            return 1 / quaternion;
        }

        /// <summary>
        /// Gets the inverse of this quaternion.
        /// </summary>
        public Quaternion Inverted => Invert(this);

        /// <summary>
        /// Reverses every component of quaternion.
        /// </summary>
        public static Quaternion operator -(Quaternion quaternion)
        {
            return new Quaternion(-quaternion.w, -quaternion.XYZ);
        }

        /// <summary>
        /// Reverses every component of this quaternion.
        /// </summary>
        public Quaternion Negated => -this;

        /// <summary>
        /// Gets a quaternion with where every component is the absolute value of the corresponding component in this.<para/>
        /// Note that this is NOT the same as <seealso cref="Magnitude"/>.
        /// </summary>
        public Quaternion Abs => new Quaternion(w.Abs(), XYZ.Abs);
        #endregion

        #region Magnitude and normalization.
        /// <summary>Gets the squared magnitude of quaternion.</summary>
        public static float GetSqrMagnitude(Quaternion quaternion)
        {
            return quaternion.Dot(quaternion);
        }

        /// <summary>Gets the squared magnitude of this quaternion.</summary>
        public float SqrMagnitude => GetSqrMagnitude(this);

        /// <summary>Gets the magnitude of quaternion.</summary>
        public static float GetMagnitude(Quaternion quaternion)
        {
            float sqrMagnitude = quaternion.SqrMagnitude;
            if (sqrMagnitude == 0 || sqrMagnitude == 1)
                return sqrMagnitude;

            return Math1D.Sqrt(sqrMagnitude);
        }

        /// <summary>Gets the magnitude of this quaternion.</summary>
        public float Magnitude => GetMagnitude(this);

        public static Quaternion Normalize(Quaternion quaternion)
        {
            return quaternion / quaternion.Magnitude;
        }

        public Quaternion Normalized => Normalize(this);
        #endregion

        #region Exponentionals, powers and logarithms.
        /// <summary>
        /// Returns e raised to quaternion using the quaternion extension for Euler's formula.
        /// </summary>
        public static Quaternion Exp(Quaternion quaternion)
        {
            Vector3D v = quaternion.XYZ;
            float vMagnitude = v.Magnitude;
            return new Quaternion(Math1D.Cos(vMagnitude), Math1D.Sin(vMagnitude) / vMagnitude * v) * quaternion.w.Exp();
        }

        /// <summary>
        /// Returns e raised to this using the quaternion extension for Euler's formula.
        /// </summary>
        public Quaternion Exp()
        {
            return Exp(this);
        }

        /// <summary>
        /// Takes the natural logarithm of q.
        /// </summary>
        /// <exception cref="ArgumentException">q was not unit length.</exception>
        public static Quaternion Ln(Quaternion q)
        {
            if (!q.IsUnitLength)
                throw new ArgumentException("Cannot take the natural log of a non unit length quatenrion!" +
                    "Consider normalizing the quaternion before taking its natural log.", "q");

            float angle = Math1D.Acos(q.w);
            return new Quaternion(0, angle / Math1D.Sin(angle) * q.XYZ) * q.Magnitude;
        }

        /// <summary>
        /// Takes the natural logarithm of this.
        /// </summary>
        /// <exception cref="ArgumentException">q was not unit length.</exception>
        public Quaternion Ln()
        {
            return Ln(this);
        }

        /// <summary>
        /// Raises baseQuaternion to the power of exponent.
        /// </summary>
        /// <param name="baseQuaternion">The quaternion to raise to the power.</param>
        /// <param name="exponent">The power baseQuaternion should be raised to.</param>
        /// <exception cref="ArgumentException">q was not unit length.</exception>
        public static Quaternion Pow(Quaternion baseQuaternion, float exponent)
        {
            return baseQuaternion ^ exponent;
        }

        /// <summary>
        /// Raises this to the power of exponent.
        /// </summary>
        /// <param name="exponent">The power this should be raised to.</param>
        /// <exception cref="ArgumentException">this was not unit length.</exception>
        public Quaternion Pow(float exponent)
        {
            return Pow(this, exponent);
        }

        /// <summary>
        /// Raises baseQuaternion to the power of exponent.
        /// </summary>
        /// <param name="baseQuaternion">The quaternion to raise to the power.</param>
        /// <param name="exponent">The power baseQuaternion should be raised to.</param>
        /// <exception cref="ArgumentException">q was not unit length.</exception>
        public static Quaternion operator ^(Quaternion baseQuaternion, float exponent)
        {
            if (!baseQuaternion.IsUnitLength)
                throw new ArgumentException("Cannot raise a quaternion that is not unit length to a power!" +
                "Consider normalizing the quaternion before taking its natural log.", "baseQuaternion");

            // I am tired of this method, so instead of thinking of a good implementation I will just leave this in. :/

            //Angle angle = SMath.Acos(baseQuaternion.w);
            //float sin = SMath.Sin(angle);
            //Vector3D axis = baseQuaternion.XYZ / sin;
            //return new Quaternion(SMath.Cos(angle * exponent), axis * SMath.Sin(angle * exponent));

            return baseQuaternion.Ln().Multiply(exponent).Exp();
        }
        #endregion

        #region Interpolations.
        /// <summary>
        /// Spherically interpolates from a to b.
        /// </summary>
        /// <param name="alpha">A value between 0 to 1 representing the interpolation factor between a and b.</param>
        /// <param name="clampAlpha">Should the alpha be clamped between 0 to 1?</param>
        /// <exception cref="ArgumentException">a and b weren't both unit length.</exception>
        public static Quaternion Slerp(Quaternion a, Quaternion b, float alpha, bool clampAlpha = true)
        {
            if (!a.IsUnitLength || !b.IsUnitLength)
                throw new ArgumentException("Cannot spherecally interpolate between quaternions that are not unit length!");

            if (clampAlpha)
                alpha = alpha.Clamp(0, 1);

            return ((b / a) ^ alpha) * a;
        }

        /// <summary>
        /// Spherically interpolates from this to target.
        /// </summary>
        /// <param name="alpha">A value between 0 to 1 representing the interpolation factor between this and target.</param>
        /// <param name="clampAlpha">Should the alpha be clamped between 0 to 1?</param>
        /// <exception cref="ArgumentException">this and target weren't both unit length.</exception>
        public Quaternion Slerp(Quaternion target, float alpha, bool clampAlpha = true)
        {
            return Slerp(this, target, alpha, clampAlpha);
        }
        #endregion

        #region BiggestComponent/SmallestComponent.
        /// <summary>
        /// Gets the biggest component in this quaternion.
        /// </summary>
        public float BiggestComponent => Math1D.Max(w, x, y, z);

        /// <summary>
        /// Gets the smallest component in this quaternion.
        /// </summary>
        public float SmallestComponent => Math1D.Min(w, x, y, z);
        #endregion

        #region Clamping methods.
        /// <summary>
        /// Clamps every element of this so it's never smaller than the corresponding element in min and never bigger than the corresponding element in max.
        /// </summary>
        public Quaternion Clamp(Quaternion min, Quaternion max)
        {
            return new Quaternion(w.Clamp(min.w, max.w), x.Clamp(min.x, max.x), y.Clamp(min.y, max.y), z.Clamp(min.z, max.z));
        }

        /// <summary>
        /// Clamps every element of this so it's never biffer than min and never bigger than max.
        /// </summary>
        public Quaternion ClampComponents(float min, float max)
        {
            return new Quaternion(w.Clamp(min, max), XYZ.ClampComponents(min, max));
        }
        #endregion

        #region NaN Checking.
        /// <summary>
        /// Does quaternion have a component that is NaN?
        /// </summary>
        public static bool ContainsNaN(Quaternion quaternion)
        {
            return quaternion.w.IsNaN() || quaternion.x.IsNaN() || quaternion.y.IsNaN() || quaternion.z.IsNaN();
        }

        /// <summary>
        /// Does this have a component that is NaN?
        /// </summary>
        public bool HasNaN => ContainsNaN(this);

        /// <summary>
        /// Are all of the components of quaternion NaN?
        /// </summary>
        public static bool EqualsNaN(Quaternion quaternion)
        {
            return quaternion.w.IsNaN() && quaternion.x.IsNaN() && quaternion.y.IsNaN() && quaternion.z.IsNaN();
        }

        /// <summary>
        /// Are all of the components of this NaN?
        /// </summary>
        public bool IsNaN => EqualsNaN(this);
        #endregion

        #region IsSomething utility methods.
        /// <summary>
        /// Is the squared magnitude of this quaternion close enough to 1?
        /// </summary>
        public bool IsUnitLength => SqrMagnitude.CloseEnough(1);
        #endregion

        #region Comparison methods (Equals, CloseEnough, GetHashCode, ContainsComponent, operators)
        public override int GetHashCode()
        {
            return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Quaternion)
                return Equals((Quaternion)obj);

            return false;
        }

        public bool Equals(Quaternion other)
        {
            return this == other;
        }

        public static bool operator ==(Quaternion a, Quaternion b)
        {
            return a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w;
        }

        public static bool operator !=(Quaternion a, Quaternion b) { return !(a == b); }

        public bool CloseEnough(Quaternion other, float threshold = 0.0001F)
        {
            return XYZ.CloseEnough(other.XYZ, threshold) && w.CloseEnough(other.w, threshold);
        }
        
        /// <summary>
        /// Is any of the elements of this equal to componentValue?
        /// </summary>
        public bool ContainsComponent(float componentValue) => w == componentValue | XYZ.ContainsComponent(componentValue);
        #endregion

        #region Conversions (ToString, implicit Vector4D(Quaternion), implicit Quaternion(float)).
        /// <summary>
        /// Converts the quaternion to a string using <seealso cref="ToString(ToStringFormat)"/> and <seealso cref="ToStringFormat.Flexible_w_xi_yj_zk"/>.
        /// </summary>
        public override string ToString()
        {
            return ToString(ToStringFormat.Flexible_w_xi_yj_zk);
        }

        /// <summary>
        /// Converts a quaternion to a nicely formatted string.
        /// </summary>
        /// <param name="format">The format to use.</param>
        public string ToString(ToStringFormat format)
        {
            switch (format)
            {
                case ToStringFormat.Flexible_w_xi_yj_zk:
                    string result = "";

                    void AddValue(float value, string imaginaryUnitLetter)
                    {
                        if (value != 0)
                        {
                            if (result != string.Empty)
                                result += " + ";

                            if (value == 1)
                                result += imaginaryUnitLetter;
                            else
                                result += value + imaginaryUnitLetter;
                        }
                    }

                    if (w != 0)
                        result += w;

                    AddValue(x, "i");
                    AddValue(y, "j");
                    AddValue(z, "k");
                    return result;

                case ToStringFormat.w_xi_yj_zk:
                    return $"{w} + {x}i + {y}j + {z}k";

                case ToStringFormat.WXYZ:
                    return $"({w}, {x}, {y}, {z})";

                case ToStringFormat.XYZW:
                    return $"({x}, {y}, {z}, {w})";
            }
            return "Invalid Quaternion ToString Format";
        }

        /// <summary>
        /// Ways to format a quaternion as a string.
        /// </summary>
        public enum ToStringFormat
        {
            /// <summary>
            /// (x, y, z, w)
            /// </summary>
            XYZW,

            /// <summary>
            /// (w, x, y, z)
            /// </summary>
            WXYZ,

            /// <summary>
            /// w + xi + yj + zk
            /// </summary>
            w_xi_yj_zk,

            /// <summary>
            /// w + xi + yj + zk, but if a value is 1 it just shows the imaginary unit and if it's zero it deletes the whole part of the addition. (Note that this might cause a performance hit.)
            /// </summary>
            Flexible_w_xi_yj_zk
        }

        /// <summary>
        /// Makes a Vector4D with the x y z and w components of quaternion.
        /// </summary>
        public static implicit operator Vector4D(Quaternion quaternion)
        {
            return new Vector4D(quaternion.XYZ, quaternion.w);
        }

        /// <summary>
        /// Creates a new quaternion that will use w for its w component.
        /// </summary>
        public static implicit operator Quaternion(float w)
        {
            return new Quaternion(w, 0, 0, 0);
        }
        #endregion
    }
}