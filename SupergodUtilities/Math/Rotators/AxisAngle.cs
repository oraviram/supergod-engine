﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// Represents an axis-angle rotation.
    /// </summary>
    public struct AxisAngle : IRotator<AxisAngle>
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(AxisAngle));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        /// <summary>
        /// An axis angle rotaiton rotating around the y axis by an angle of zero.
        /// Can be thought of as a rotation of zero.
        /// </summary>
        public static readonly AxisAngle zeroUp = new AxisAngle(Vector3D.up, Angle.zero);

        /// <summary>The axis to rotate around.</summary>
        public Vector3D axis;

        /// <summary>The angle to rotate by.</summary>
        public Angle angle;

        /// <summary>
        /// Creates a new axis angle rotation with an axis and an angle.
        /// </summary>
        /// <param name="axis">The axis to rotate around.</param>
        /// <param name="angle">The angle to rotate by.</param>
        public AxisAngle(Vector3D axis, Angle angle)
        {
            this.axis = axis;
            this.angle = angle;
        }

        /// <summary>
        /// Creates a new axis angle rotation with an angle and an axis.
        /// </summary>
        /// <param name="angle">The angle to rotate by.</param>
        /// <param name="axis">The axis to rotate around.</param>
        public AxisAngle(Angle angle, Vector3D axis) : this(axis, angle)
        {
        }

        /// <summary>
        /// Creates a rotation that will first perform this and then other (by converting both to a quaternion, combining them, and converting back to an axis and an angle).
        /// </summary>
        /// <param name="other">Rotation to perform after this.</param>
        public AxisAngle CombineRotations(AxisAngle other)
        {
            // While using the implementation I am using, I found out that because of many sines and cosines there were many mistakes,
            // so I decided to see if I can move the things in the methods out and using fancy algebra remove some sines and cosines.
            // Of course I failed, so it's commented out...
            
            // I will do it at some point, but for now it's close enough for me to be happy.

            //Angle a = 2 * SMath.Acos(SMath.Cos(angle) * SMath.Cos(other.angle) - Vector.Dot(axis * SMath.Sin(angle), other.axis * SMath.Sin(other.angle)));
            //Vector3D n = (SMath.Cos(angle) * other.axis * SMath.Sin(other.angle) + SMath.Cos(other.angle) * axis * SMath.Sin(angle) + axis * Vector3D.Cross(axis * SMath.Sin(angle), other.axis * SMath.Sin(other.angle))) / SMath.Sqrt(1 - (SMath.Cos(angle) * SMath.Cos(other.angle) - Vector.Dot(axis * SMath.Sin(angle), other.axis * SMath.Sin(other.angle))).Squared());
            //return new AxisAngle(n, a);

            return Quaternion.CombineRotations(other.Quaternion).AxisAngle;
        }

        /// <summary>
        /// Rotates vector around axis by angle using Rodrigues' rotation formula.
        /// </summary>
        /// <param name="vector">The vector to rotate.</param>
        /// <returns>The rotated vector.</returns>
        public Vector3D RotateVector(Vector3D vector)
        {
            float cosAngle = Math1D.Cos(angle);
            return vector * cosAngle + vector.ProjectOnto(axis) * (1 - cosAngle) + axis.Cross(vector) * Math1D.Sin(angle);
        }

        #region Construction methods from other types of rotations.
        /// <summary>
        /// Creates a new axis angle rotation from a pure rotation (unit length) quaternion.
        /// Note that if the angle of rotation is a straight angle or 0, the rotation axis will just be defaulted to (0, 1, 0).
        /// </summary>
        /// <param name="rotationQuaternion">The unit length quaternion to construct the rotation from.</param>
        /// <exception cref="ArgumentException">rotationQuaternion was not unit length. Consdier normalizing it.</exception>
        public static AxisAngle FromRotationQuaternion(Quaternion rotationQuaternion)
        {
            if (!rotationQuaternion.IsUnitLength)
                throw new ArgumentException("Cannot convert an axis angle rotation from a quaternion that is not unit length. Consider normalizing it.", "rotationQuaternion");

            Angle angle = Math1D.Acos(rotationQuaternion.w) * 2;
            float sinAbs = Math1D.Sqrt(1 - rotationQuaternion.w * rotationQuaternion.w);
            if (sinAbs.CloseEnough(0))
                return new AxisAngle(Vector3D.up, angle);

            return new AxisAngle(rotationQuaternion.XYZ / sinAbs, angle);
        }

        /// <summary>
        /// Converts a PURE 3x3 ROTATION matrix to an axis angle rotation.
        /// </summary>
        /// <param name="rotationMatrix">The PURE ROTATION matrix to construct the rotation from.</param>
        public static AxisAngle FromRotationMatrix(Matrix3x3 rotationMatrix)
        {
            return FromRotationQuaternion(Quaternion.FromRotationMatrix(rotationMatrix));
        }

        /// <summary>
        /// Creates a new axis angle rotation from euler angles.
        /// </summary>
        /// <param name="pitch">Angle of rotation around the x axis.</param>
        /// <param name="yaw">Angle of rotation around the y axis.</param>
        /// <param name="roll">Angle of rotation around the z axis.</param>
        public static AxisAngle FromEulerAngles(Angle pitch, Angle yaw, Angle roll)
        {
            return FromRotationQuaternion(Quaternion.FromEulerAngles(pitch, yaw, roll));
        }

        /// <summary>
        /// Creates a new axis angle rotation from euler angles.
        /// </summary>
        /// <param name="angles">The angles of rotation around the x, y and z axes.</param>
        public static AxisAngle FromEulerAngles(EulerAngles angles)
        {
            return FromEulerAngles(angles.Pitch, angles.Yaw, angles.Roll);
        }
        #endregion

        #region Conversion properties to/from other types of rotations.
        /// <summary>
        /// Gets/sets this axis angle rotation as a quaternion.
        /// </summary>
        public Quaternion Quaternion { get => Quaternion.FromAxisAngle(this); set => this = FromRotationQuaternion(value); }

        /// <summary>
        /// Gets/sets this axis angle rotation as a 3x3 rotation matrix.
        /// </summary>
        public Matrix3x3 RotationMatrix3x3 { get => Matrix3x3.FromAxisAngle(this); set => this = FromRotationMatrix(value); }

        /// <summary>
        /// Gets/sets this axis angle rotation as a 4x4 rotation matrix.
        /// </summary>
        public Matrix4x4 RotationMatrix { get => Matrix4x4.FromAxisAngle(this); set => this = FromRotationMatrix((Matrix3x3)value); }

        /// <summary>
        /// Gets/sets this axis angle rotation as euler angles.
        /// </summary>
        public EulerAngles EulerAngles { get => EulerAngles.FromAxisAngle(this); set => this = FromEulerAngles(value); }

        /// <summary>
        /// Uh...
        /// </summary>
        AxisAngle IRotator.AxisAngle { get => this; set => this = value; }
        #endregion

        #region Clamping methods.
        /// <summary>
        /// Clamps the axis of this between the axis of min and the axis of max (with <seealso cref="Vector3D.Clamp(Vector3D, Vector3D)"/>) and the angle of this between the angle of min and the angle of max.
        /// </summary>
        public AxisAngle Clamp(AxisAngle min, AxisAngle max)
        {
            return new AxisAngle(axis.Clamp(min.axis, max.axis), angle.Clamp(min.angle, max.angle));
        }
        #endregion

        #region Equality operators and methods (and GetHashCode).
        public override bool Equals(object obj)
        {
            if (obj is AxisAngle)
                return Equals((AxisAngle)obj);

            return false;
        }

        public bool Equals(AxisAngle other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return axis.GetHashCode() ^ angle.GetHashCode();
        }

        public static bool operator ==(AxisAngle a, AxisAngle b)
        {
            return a.axis == b.axis && a.angle == b.angle;
        }

        public static bool operator !=(AxisAngle a, AxisAngle b)
        {
            return !(a == b);
        }
        
        /// <summary>
        /// Are the angle and axis of this and other <seealso cref="SMath.CloseEnough(float, float, float)"/> to each other?
        /// </summary>
        public bool CloseEnough(AxisAngle other, float threshold = Math1D.Epsilon)
        {
            return axis.CloseEnough(other.axis, threshold) && angle.CloseEnough(other.angle, threshold);
        }
        #endregion

        /// <summary>
        /// Returns a string in the form "Axis: {axis}, Angle: {angle}".
        /// </summary>
        public override string ToString()
        {
            return $"Axis: {axis}, Angle: {angle}";
        }
    }
}