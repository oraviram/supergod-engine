﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// Represents a 3D rotation in euler angles.
    /// </summary>
    public struct EulerAngles : IRotator<EulerAngles>
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(EulerAngles));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        #region Presets.
        /// <summary>A rotation with all of the angles set to zero.</summary>
        public static readonly EulerAngles zero = new EulerAngles();
        #endregion

        /// <summary>The rotation angle around the x axis (same as <seealso cref="Pitch"/>).</summary>
        public Angle x;

        /// <summary>The rotation angle around the y axis (same as <seealso cref="Yaw"/>).</summary>
        public Angle y;

        /// <summary>The rotation angle around the z axis (same as <seealso cref="Roll"/>).</summary>
        public Angle z;

        private const string INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE = "Angle index in an EulerAngles indexer must be either 0, 1 or 2.";

        /// <summary>
        /// Gets or sets an angle at the index of index.
        /// </summary>
        /// <param name="index">0 = x/pitch, 1 = y/yaw, 2 = z/roll. Anything else will through an index out of range exception.</param>
        /// <exception cref="IndexOutOfRangeException">The given index wasn't 0, 1 or 2.</exception>
        public Angle this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return x;

                    case 1:
                        return y;

                    case 2:
                        return z;
                }
                throw new IndexOutOfRangeException(INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE);
            }
            set
            {
                switch (index)
                {
                    case 0:
                        x = value;
                        return;

                    case 1:
                        y = value;
                        return;

                    case 2:
                        z = value;
                        return;
                }
                throw new IndexOutOfRangeException(INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE);
            }
        }

        /// <summary>
        /// Creates a new euler angles rotation with three angles.
        /// </summary>
        /// <param name="pitch">The rotation angle around x.</param>
        /// <param name="yaw">The rotation angle around y.</param>
        /// <param name="roll">The rotation angle around z.</param>
        public EulerAngles(Angle pitch, Angle yaw, Angle roll)
        {
            x = pitch;
            y = yaw;
            z = roll;
        }

        /// <summary>
        /// Creates a new euler angles rotation with floats for the angles and a measurement.
        /// </summary>
        /// <param name="pitch">The value for the pitch.</param>
        /// <param name="yaw">The value for the yaw.</param>
        /// <param name="roll">The value for the roll.</param>
        /// <param name="measurement">The measurement if the angles.</param>
        public EulerAngles(float pitch, float yaw, float roll, AngleMeasurement measurement)
            : this(new Angle(pitch, measurement), new Angle(yaw, measurement), new Angle(roll, measurement))
        {
        }

        /// <summary>
        /// The rotation angle around the x axis (same as <seealso cref="x"/>).
        /// </summary>
        public Angle Pitch
        {
            get { return x; }
            set { x = value; }
        }

        /// <summary>
        /// The rotation angle around the y axis (same as <seealso cref="y"/>).
        /// </summary>
        public Angle Yaw
        {
            get { return y; }
            set { y = value; }
        }

        /// <summary>
        /// The rotation angle around the z axis (same as <seealso cref="z"/>).
        /// </summary>
        public Angle Roll
        {
            get { return z; }
            set { z = value; }
        }
        
        /// <summary>
        /// Combines this rotation with other (by converting both to a quaternion, combining the quaternions, and converting back to euler angles).
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public EulerAngles CombineRotations(EulerAngles other)
        {
            return Quaternion.CombineRotations(other.Quaternion).EulerAngles;
        }

        /// <summary>
        /// Rotates vector by this rotation.
        /// Since euler angles can't really rotate vectors,
        /// this rotation will be converted to a quaternion and the quaternion will rotate the vector.
        /// </summary>
        /// <param name="vector">Vector to rotate.</param>
        /// <returns>The rotated vector.</returns>
        public Vector3D RotateVector(Vector3D vector)
        {
            return vector.RotateVector(Quaternion.FromEulerAngles(this));
        }

        #region Conversions from other types of rotations.
        /// <summary>
        /// Creates euler angles from a pure rotation (unit) quaternion.
        /// </summary>
        /// <param name="rotationQuaternion">The unit quaternion to take the rotation from.</param>
        /// <returns>A rotation in euler angles that represents the same rotation is rotationQuaternion.</returns>
        public static EulerAngles FromRotationQuaternion(Quaternion rotationQuaternion)
        {
            float ySqr = rotationQuaternion.y * rotationQuaternion.y;

            float t0 = 2 * (rotationQuaternion.w * rotationQuaternion.x + rotationQuaternion.y * rotationQuaternion.z);
            float t1 = 1 - 2 * (rotationQuaternion.x * rotationQuaternion.x + ySqr);

            float t2 = 2 * (rotationQuaternion.w * rotationQuaternion.y - rotationQuaternion.z * rotationQuaternion.x);
            t2 = ((t2 > 1) ? 1 : t2);
            t2 = ((t2 < -1) ? -1 : t2);

            float t3 = 2 * (rotationQuaternion.w * rotationQuaternion.z + rotationQuaternion.x * rotationQuaternion.y);
            float t4 = 1 - 2 * (ySqr + rotationQuaternion.z * rotationQuaternion.z);
            
            return new EulerAngles(
                Math1D.Atan2(t0, t1),
                Math1D.Asin(t2),
                Math1D.Atan2(t3, t4)
                );
        }

        /// <summary>
        /// Creates a euler angles rotation from a PURE ROTATION matrix.
        /// </summary>
        /// <param name="rotationMatrix">The PURE ROTATION matrix to construct the rotation form.</param>
        public static EulerAngles FromRotationMatrix(Matrix3x3 rotationMatrix)
        {
            return FromRotationQuaternion(Quaternion.FromRotationMatrix(rotationMatrix));
        }
        
        /// <summary>
        /// Creates a new euler angles rotation that will rotate by angle around axis.
        /// </summary>
        /// <param name="axis">The axis to rotate around.</param>
        /// <param name="angle">The angle rotate by.</param>
        public static EulerAngles FromAxisAngle(Vector3D axis, Angle angle)
        {
            return FromRotationQuaternion(Quaternion.FromAxisAngle(axis, angle));
        }

        /// <summary>
        /// Creates a rotation represented in euler angles that corresponds to the rotation of axisAngle.
        /// </summary>
        /// <param name="axisAngle">The axis to rotate around and ange to rotate by.</param>
        public static EulerAngles FromAxisAngle(AxisAngle axisAngle)
        {
            return FromAxisAngle(axisAngle.axis, axisAngle.angle);
        }
        #endregion

        #region Conversion properties to/from other types of rotations.
        /// <summary>
        /// Gets/sets this as a quaternion.
        /// </summary>
        public Quaternion Quaternion { get => Quaternion.FromEulerAngles(this); set => this = FromRotationQuaternion(value); }

        /// <summary>
        /// Gets/sets this as a 3x3 rotation matrix.
        /// </summary>
        public Matrix3x3 RotationMatrix3x3 { get => Matrix3x3.FromEulerAngles(this); set => this = FromRotationMatrix(value); }

        /// <summary>
        /// Gets/sets this as a 4x4 rotation matrix.
        /// </summary>
        public Matrix4x4 RotationMatrix { get => Matrix4x4.FromEulerAngles(this); set => this = FromRotationMatrix((Matrix3x3)value); }

        /// <summary>
        /// Gets/sets this as an axis to rotate around an an angle to rotate by.
        /// </summary>
        public AxisAngle AxisAngle { get => AxisAngle.FromEulerAngles(this); set => this = FromAxisAngle(value); }

        /// <summary>
        /// Uh...
        /// </summary>
        EulerAngles IRotator.EulerAngles { get => this; set => this = value; }
        #endregion

        #region Equality operators and methods (and GetHashCode).
        public override bool Equals(object obj)
        {
            if (obj is EulerAngles)
                return Equals((EulerAngles)obj);

            return false;
        }

        public bool Equals(EulerAngles other)
        {
            return this == other;
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
        }

        public static bool operator ==(EulerAngles a, EulerAngles b)
        {
            return a.x == b.x && a.y == b.y && a.z == b.z;
        }

        public static bool operator !=(EulerAngles a, EulerAngles b)
        {
            return !(a == b);
        }
        
        /// <summary>
        /// Are all of the angles in this and other <seealso cref="SMath.CloseEnough(float, float, float)"/> to each other?
        /// </summary>
        public bool CloseEnough(EulerAngles other, float threshold = Math1D.Epsilon)
        {
            return x.CloseEnough(other.x, threshold) && y.CloseEnough(other.y, threshold) && z.CloseEnough(other.z, threshold);
        }
        #endregion

        #region Addition.
        /// <summary>
        /// Adds each angle in this to its corresponding angle in other.
        /// </summary>
        public EulerAngles Add(EulerAngles other)
        {
            return Add(this, other);
        }

        /// <summary>
        /// Adds each angle in a to its corresponding angle in b.
        /// </summary>
        public static EulerAngles Add(EulerAngles a, EulerAngles b)
        {
            return a + b;
        }

        /// <summary>
        /// Adds each angle in a to its corresponding angle in b.
        /// </summary>
        public static EulerAngles operator +(EulerAngles a, EulerAngles b)
        {
            return new EulerAngles(a.x + b.x, a.y + b.y, a.z + b.z);
        }
        #endregion

        #region Subtraction.
        /// <summary>
        /// Subtracts each angle of other from its corresponding angle in this.
        /// </summary>
        /// <param name="other">The angles to subtract this by..</param>
        public EulerAngles Subtract(EulerAngles other)
        {
            return Subtract(this, other);
        }

        /// <summary>
        /// Subtracts each angle of b from its corresponding angle in a.
        /// </summary>
        /// <param name="a">The angles to subtract from.</param>
        /// <param name="b">The angles to subtract by.</param>
        public static EulerAngles Subtract(EulerAngles a, EulerAngles b)
        {
            return a - b;
        }

        /// <summary>
        /// Subtracts each angle of b from its corresponding angle in a.
        /// </summary>
        /// <param name="a">The angles to subtract from.</param>
        /// <param name="b">The angles to subtract by.</param>
        public static EulerAngles operator -(EulerAngles a, EulerAngles b)
        {
            return new EulerAngles(a.x - b.x, a.y - b.y, a.z - b.z);
        }
        #endregion

        #region Multiplication.
        /// <summary>
        /// Multiplies every angle in this by scalar.
        /// </summary>
        public EulerAngles Multiply(float scalar)
        {
            return Multiply(this, scalar);
        }

        /// <summary>
        /// Multiplies every angle in angles by scalar.
        /// </summary>
        public static EulerAngles operator *(EulerAngles angles, float scalar)
        {
            return new EulerAngles(angles.x * scalar, angles.y * scalar, angles.z * scalar);
        }

        /// <summary>
        /// Multiplies every angle in angles by scalar.
        /// </summary>
        public static EulerAngles Multiply(EulerAngles angles, float scalar)
        {
            return angles * scalar;
        }

        /// <summary>
        /// Multiplies every angle in angles by scalar.
        /// </summary>
        public static EulerAngles operator *(float scalar, EulerAngles angles)
        {
            return angles * scalar;
        }

        /// <summary>
        /// Multiplies every angle in angles by scalar.
        /// </summary>
        public static EulerAngles Multiply(float scalar, EulerAngles angles)
        {
            return scalar * angles;
        }
        #endregion

        #region Division.
        /// <summary>
        /// Divides each angle of this by scalar.
        /// </summary>
        /// <param name="scalar">The scalar to divide this by.</param>
        public EulerAngles Divide(float scalar)
        {
            return Divide(this, scalar);
        }

        /// <summary>
        /// Divides each angle of angles by scalar.
        /// </summary>
        /// <param name="angles">The euler angles to divide.</param>
        /// <param name="scalar">The scalar to divide angles by.</param>
        public static EulerAngles Divide(EulerAngles angles, float scalar)
        {
            return angles / scalar;
        }

        /// <summary>
        /// Divides each angle of angles by scalar.
        /// </summary>
        /// <param name="angles">The euler angles to divide.</param>
        /// <param name="scalar">The scalar to divide angles by.</param>
        public static EulerAngles operator /(EulerAngles angles, float scalar)
        {
            return new EulerAngles(angles.x / scalar, angles.y / scalar, angles.z / scalar);
        }
        #endregion

        #region Clamping methods.
        /// <summary>
        /// Clamps every angle of this so it's never smaller than the corresponding angle in min and never bigger than the corresponding angle in max.
        /// </summary>
        public EulerAngles Clamp(EulerAngles min, EulerAngles max)
        {
            return new EulerAngles(x.Clamp(min.x, max.x), y.Clamp(min.y, max.y), z.Clamp(min.z, max.z));
        }
        #endregion

        #region Conversions.
        /// <summary>
        /// Returns a string that formats the angles in the form (pitch, yaw, roll).
        /// </summary>
        public override string ToString()
        {
            return $"({x}, {y}, {z})";
        }

        /// <summary>
        /// Creates a new Vector3D with the radians of the angles in angles.
        /// </summary>
        public static implicit operator Vector3D(EulerAngles angles)
        {
            return new Vector3D(angles.x, angles.y, angles.z);
        }
        #endregion
    }
}