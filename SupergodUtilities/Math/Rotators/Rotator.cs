﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// Class containing static rotation methods.
    /// </summary>
    public static class Rotator
    {
        /// <summary>
        /// Rotates vector by rotator.
        /// </summary>
        /// <param name="vector">Vector to rotate.</param>
        /// <param name="rotator">Rotation to apply.</param>
        /// <returns>The rotated vector</returns>
        public static Vector3D RotateVector<T>(this Vector3D vector, T rotator)
            where T : struct, IRotator<T>
        {
            return rotator.RotateVector(vector);
        }

        /// <summary>
        /// Rotates vector by rotator.
        /// </summary>
        /// <param name="rotator">Rotation to apply.</param>
        /// <param name="vector">Vector to rotate.</param>
        /// <returns>The rotated vector</returns>
        public static Vector3D RotateVector<T>(T rotator, Vector3D vector)
            where T : struct, IRotator<T>
        {
            return RotateVector(vector, rotator);
        }

        /// <summary>
        /// Combines first and second so the first rotation performed is first and the second is second.
        /// </summary>
        /// <param name="first">The first rotation to apply.</param>
        /// <param name="second">The rotation to combine first with.</param>
        /// <returns>A new rotation that will first perform first and then second.</returns>
        public static T Combine<T>(T first, T second)
            where T : struct, IRotator<T>
        {
            return first.CombineRotations(second);
        }
    }
}