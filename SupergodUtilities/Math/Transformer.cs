﻿namespace SupergodUtilities.Math
{
    /// <summary>
    /// A class with static methods to call things from the <seealso cref="ITransformer{T}"/> interface.
    /// </summary>
    public static class Transformer
    {
        /// <summary>
        /// Transforms vector by the transformer.
        /// </summary>
        /// <typeparam name="T">The type of transformer.</typeparam>
        /// <param name="vector">The vector to transform.</param>
        /// <param name="transformer">The transformer to transform vector by.</param>
        /// <returns>The transformed vector.</returns>
        public static Vector3D TransformVector<T>(this Vector3D vector, T transformer)
            where T : struct, ITransformer<T>
        {
            return transformer.TransformVector(vector);
        }

        /// <summary>
        /// Transforms vector by the transformer.
        /// </summary>
        /// <typeparam name="T">The type of transformer.</typeparam>
        /// <param name="transformer">The transformer to transform vector by.</param>
        /// <param name="vector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        public static Vector3D TransformVector<T>(T transformer, Vector3D vector)
            where T : struct, ITransformer<T> => TransformVector(vector, transformer);

        /// <summary>
        /// Combines original with transformer.
        /// </summary>
        /// <typeparam name="T">The type of transformers.</typeparam>
        /// <param name="original">The first transform.</param>
        /// <param name="transformer">The transform to combine original with.</param>
        /// <returns>A combination of original and transformer.</returns>
        public static T CombineTransformations<T>(T original, T transformer)
            where T : struct, ITransformer<T>
        {
            return original.CombineTransformations(transformer);
        }

        /// <summary>
        /// Returns the transformation that when combined with secondTransformation gives combination.
        /// </summary>
        /// <typeparam name="T">The type of transformations to discombine.</typeparam>
        /// <param name="combination">The two transformations combined.</param>
        /// <param name="secondTransformation">The transformation that when combined with the return value gives combination.</param>
        public static T DiscombineTransformations<T>(T combination, T secondTransformation)
            where T : struct, ITransformer<T>
        {
            return combination.DiscombineTransformations(secondTransformation);
        }
    }
}