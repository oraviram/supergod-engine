﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// An interface for types that can be clamped between a minimum and a maximum.
    /// </summary>
    public interface IClampable<T>
        where T : struct
    {
        T Clamp(T min, T max);
    }
}