﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// An interface all vector structs can implement.
    /// </summary>
    /// <typeparam name="T">The type of the vector.</typeparam>
    public interface IVector<T> : IEquatable<T>, IAbsolutable<T>, ILerpable<T>, ISupergodEquatable<T>, IClampable<T>
        where T : struct
    {
        float this[int index] { get; set; }
        float[] AsFloatArray { get; }

        T Subtract(T other);
        T Negated { get; }

        T Multiply(T other);
        T Divide(T other);
        T Divide(float scalar);

        float Magnitude { get; }
        float SqrMagnitude { get; }
        T Normalized { get; }

        float BiggestComponent { get; }
        float SmallestComponent { get; }
        float Dot(T other);

        bool ContainsComponent(float componentValue);
        T ClampComponents(float min, float max);

        string ToString();
    }
}