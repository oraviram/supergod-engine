﻿namespace SupergodUtilities.Math
{
    /// <summary>
    /// The base interface for <seealso cref="IRotator{T}"/>. When implementing, use the generic version. This exists just for referencing a rotator.
    /// </summary>
    public interface IRotator
    {
        Quaternion Quaternion { get; set; }
        Matrix3x3 RotationMatrix3x3 { get; set; }
        Matrix4x4 RotationMatrix { get; set; }
        AxisAngle AxisAngle { get; set; }
        EulerAngles EulerAngles { get; set; }
    }

    /// <summary>
    /// An interface rotators can implement.
    /// </summary>
    /// <typeparam name="T">The type of the rotator.</typeparam>
    public interface IRotator<T> : IClampable<T>, ISupergodEquatable<T>, IRotator
        where T : struct
    {
        T CombineRotations(T other);
        Vector3D RotateVector(Vector3D vector);
    }
}