﻿namespace SupergodUtilities.Math
{
    /// <summary>
    /// An interface for things that can transform vectors and be combined.
    /// </summary>
    public interface ITransformer<T>
        where T : struct
    {
        Vector3D TransformVector(Vector3D vector);
        T CombineTransformations(T other);
        T DiscombineTransformations(T secondTransformation);
    }
}