﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// The base interface for <seealso cref="IMatrix{TMat, TVec}"/>. This is only used when trying to reference a matrix of a generic type.
    /// When implementing, use <seealso cref="IMatrix{TMat, TVec}"/>.
    /// </summary>
    /// <typeparam name="T">The type of the matrix.</typeparam>
    public interface IMatrix<T> : IEquatable<T>, IAbsolutable<T>, ILerpable<T>, ISupergodEquatable<T>, IClampable<T>
        where T : struct
    {
        float this[int row, int column] { get; set; }
        float[,] AsFloatArray { get; }

        T Subtract(T other);
        T Multiply(T other);

        T Negated { get; }
        T Transposed { get; }
        T Cofactor { get; }

        T Inverted { get; }
        T Adjugate { get; }

        float Determinant { get; }
        float Trace { get; }

        T ClampElements(float min, float max);
    }

    /// <summary>
    /// An interface all matrices can implement.
    /// </summary>
    /// <typeparam name="TMat">The type of the matrix.</typeparam>
    /// <typeparam name="TVec">The type of the vector the matrix uses.</typeparam>
    public interface IMatrix<TMat, TVec> : IMatrix<TMat>
        where TMat : struct
        where TVec : struct, IVector<TVec>
    {
        TVec[] Rows { get; }
        TVec[] Columns { get; }

        TVec GetRow(int index);
        TVec GetColumn(int index);
        void SetRow(int index, TVec value);
        void SetColumn(int index, TVec value);

        TVec Multiply(TVec scalar);
        TMat ClampRows(TVec min, TVec max);
        TMat ClampColumns(TVec min, TVec max);
    }
}