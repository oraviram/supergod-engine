﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// An interface that adds all the comparison methods needed to compare it like a real supergod object!
    /// </summary>
    /// <typeparam name="T">The types being compared.</typeparam>
    public interface ISupergodEquatable<T> : IEquatable<T>
    {
        bool CloseEnough(T other, float threshold = Math1D.Epsilon);
        bool Equals(object obj);
        int GetHashCode();
    }
}