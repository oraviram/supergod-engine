﻿namespace SupergodUtilities.Math
{
    /// <summary>
    /// Not sure if that's a word, but I had an interface in my previous system that only had the Abs method in it, so I decided to keep it...
    /// </summary>
    public interface IAbsolutable<T>
        where T : struct
    {
        T Abs { get; }
    }
}