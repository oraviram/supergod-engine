﻿namespace SupergodUtilities.Math
{
    /// <summary>
    /// When implementing this struct, it will be possible to linearly interpolate between two objects using <seealso cref="SMath.Lerp"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ILerpable<T>
        where T : struct
    {
        T Multiply(float scalar);
        T Add(T other);
    }
}