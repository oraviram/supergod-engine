﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// A lot of supergod mathematic helper functions.
    /// </summary>
    public static class SMath
    {
        public static T Clamp<T>(T value, T min, T max)
            where T : struct, IClampable<T>
        {
            return value.Clamp(min, max);
        }

        /// <summary>
        /// Clamps value so it's never smaller than min and never bigger than max.
        /// </summary>
        public static float Clamp(this float value, float min, float max)
        {
            if (value <= min)
                return min;

            if (value >= max)
                return max;

            return value;
        }

        /// <summary>
        /// Clamps value so it's never smaller than min and never bigger than max.
        /// </summary>
        public static int Clamp(this int value, int min, int max)
        {
            return (int)Clamp((float)value, min, max);
        }

        /// <summary>
        /// Checks if a is close enough to b with a threshold of <seealso cref="Math1D.Epsilon"/>.
        /// </summary>
        /// <typeparam name="T">The type of the equatable objects.</typeparam>
        /// <param name="a">The first object to compare.</param>
        /// <param name="b">The second object to compare.</param>
        public static bool CloseEnough<T>(T a, T b, float threshold = Math1D.Epsilon)
            where T : ISupergodEquatable<T>
        {
            return a.CloseEnough(b, threshold);
        }

        /// <summary>
        /// Is a close enough to b?
        /// </summary>
        /// <param name="a">The first value.</param>
        /// <param name="b">The second value.</param>
        /// <param name="threshold">How close should a and b be to each other?</param>
        /// <returns>True if the absolute value of a - b is smaller than the threshold.</returns>
        public static bool CloseEnough(this float a, float b, float threshold = Math1D.Epsilon)
        {
            return (a - b).Abs() <= threshold;
        }

        /// <summary>
        /// Is a close enough to b?
        /// </summary>
        /// <param name="a">The first value.</param>
        /// <param name="b">The second value.</param>
        /// <param name="threshold">How close should a and b be to each other?</param>
        /// <returns>True if the absolute value of a - b is smaller than the threshold.</returns>
        public static bool CloseEnough(this int a, int b, float threshold = Math1D.Epsilon)
        {
            return CloseEnough((float)a, b, threshold);
        }

        /// <summary>
        /// Linearly interpolates between source and target by alpha.
        /// </summary>
        /// <param name="alpha">The interpolation value. Will be clamped between 0 and 1 if clampAlpha is true.
        /// When 0, returns source, when 1, returns target, when 0.5, returns the midpoint between the two values.</param>
        /// <param name="clampAlpha">Should alpha be clamped between 0 and 1?</param>
        /// <returns>The interpolated result.</returns>
        public static T Lerp<T>(this T source, T target, float alpha, bool clampAlpha = true)
            where T : struct, ILerpable<T>
        {
            if (clampAlpha)
                alpha = alpha.Clamp(0, 1);

            return source.Multiply(1 - alpha).Add(target.Multiply(alpha));
        }

        /// <summary>
        /// Linearly interpolates between source and target by alpha.
        /// </summary>
        /// <param name="alpha">The interpolation value. Will be clamped between 0 and 1 if clampAlpha is true.
        /// When 0, returns source, when 1, returns target, when 0.5, returns the midpoint between the two numbers.</param>
        /// <param name="clampAlpha">Should alpha be clamped between 0 and 1?</param>
        /// <returns>The interpolated result.</returns>
        public static float Lerp(this float source, float target, float alpha, bool clampAlpha = true)
        {
            if (clampAlpha)
                alpha = alpha.Clamp(0, 1);

            return source * (1 - alpha) + target * alpha;
        }

        /// <summary>
        /// Linearly interpolates between source and target by alpha.
        /// </summary>
        /// <param name="alpha">The interpolation value. Will be clamped between 0 and 1 if clampAlpha is true.
        /// When 0, returns source, when 1, returns target, when 0.5, returns the midpoint between the two numbers.</param>
        /// <param name="clampAlpha">Should alpha be clamped between 0 and 1?</param>
        /// <returns>The interpolated result.</returns>
        public static int Lerp(this int source, int target, float alpha, bool clampAlpha = true)
        {
            return (int)Lerp((float)source, target, alpha, clampAlpha);
        }
    }
}