﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// Stores transformation information (position, rotation and scale).
    /// </summary>
    public struct Transform : ISupergodEquatable<Transform>, ITransformer<Transform>
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Transform));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        #region Presets for common transforms.
        /// <summary>
        /// A transform with a position of (0, 0, 0), no rotation and scale of (1, 1, 1).
        /// </summary>
        public static readonly Transform identity = new Transform(Vector3D.zero, Quaternion.identity, Vector3D.one);
        #endregion

        public Vector3D position;
        public Quaternion rotation;
        public Vector3D scale;

        /// <summary>
        /// Creates a new transform with a specified position, rotation and scale.
        /// </summary>
        /// <param name="position">Position of the transform.</param>
        /// <param name="rotation">Rotation of the transform.</param>
        /// <param name="scale">Scale of the transform.</param>
        public Transform(Vector3D position, Quaternion rotation, Vector3D scale)
        {
            this.position = position;
            this.rotation = rotation;
            this.scale = scale;
        }

        /// <summary>
        /// Creates a new transform with a specified position, rotation and scale.<para/>
        /// This will convert the rotation to a quaternion, which can be slow.
        /// </summary>
        /// <param name="position">Position of the transform.</param>
        /// <param name="rotation">Rotation of the transform represented in any type of rotation.</param>
        /// <param name="scale">Scale of the transform.</param>
        public Transform(Vector3D position, IRotator rotation, Vector3D scale) : this(position, rotation.Quaternion, scale)
        {
        }

        /// <summary>
        /// Initializes a new transform with position, rotation, and <seealso cref="Vector3D.one"/>.
        /// </summary>
        /// <param name="position">Position of the transform.</param>
        /// <param name="rotation">Rotation of the transform.</param>
        public Transform(Vector3D position, Quaternion rotation) : this(position, rotation, Vector3D.one)
        {
        }

        /// <summary>
        /// Initializes a new transform with a position, <seealso cref="Quaternion.identity"/>, and a scale.
        /// </summary>
        /// <param name="position">Position of the transform.</param>
        /// <param name="scale">Scale of the transform.</param>
        public Transform(Vector3D position, Vector3D scale) : this(position, Quaternion.identity, scale)
        {
        }

        /// <summary>
        /// Initializes a new transform with a position, <seealso cref="Quaternion.identity"/>, and <seealso cref="Vector3D.one"/>.
        /// </summary>
        /// <param name="position"></param>
        public Transform(Vector3D position) : this(position, Quaternion.identity, Vector3D.one)
        {
        }

        /// <summary>
        /// Initializes a new transform with <seealso cref="Vector3D.zero"/>, rotation, and <seealso cref="Vector3D.one"/>.
        /// </summary>
        /// <param name="rotation">Rotation of the transform.</param>
        public Transform(Quaternion rotation) : this(Vector3D.zero, rotation, Vector3D.one)
        {
        }

        #region Transformation matrices.
        /// <summary>
        /// Gets/sets the position of this transform as a matrix.
        /// </summary>
        public Matrix4x4 TranslationMatrix
        {
            get { return Matrix4x4.Translate(position); }
            set { position = value.Translation; }
        }

        /// <summary>
        /// Gets/sets the rotation of this transform as a matrix.
        /// </summary>
        public Matrix4x4 RotationMatrix
        {
            get { return Matrix4x4.FromRotationQuaternion(rotation); }
            set { rotation = value.Quaternion; }
        }

        /// <summary>
        /// Gets/sets the rotation of this transform as a matrix.
        /// </summary>
        public Matrix4x4 ScaleMatrix
        {
            get { return Matrix4x4.Scale(scale); }
            set { scale = value.Size; }
        }

        /// <summary>
        /// The model matrix of this matrix.
        /// </summary>
        public Matrix4x4 ModelMatrix
        {
            get { return Matrix4x4.TRS(this); }
            set
            {
                position = value.Translation;
                rotation = value.Quaternion;
                scale = value.Size;
            }
        }
        #endregion

        #region Comparison methods.
        /// <summary>
        /// Is obj a Transform? If so, are the position, rotation and scale of this and obj the same?
        /// </summary>
        public override bool Equals(object obj)
        {
            if (obj is Transform)
                return Equals((Transform)obj);

            return false;
        }

        public override int GetHashCode()
        {
            return position.GetHashCode() ^ rotation.GetHashCode() ^ scale.GetHashCode();
        }

        /// <summary>
        /// Are the position, rotation and scale of this and other the same?
        /// </summary>
        public bool Equals(Transform other)
        {
            return this == other;
        }

        /// <summary>
        /// Are the position, rotation and scale of left and right the same?
        /// </summary>
        public static bool operator ==(Transform left, Transform right) => left.position == right.position && left.rotation == right.rotation && left.scale == right.scale;

        /// <summary>
        /// Are the position, rotation or scale of left and right the different?
        /// </summary>
        public static bool operator !=(Transform left, Transform right) => !(left == right);

        public bool CloseEnough(Transform other, float threshold = Math1D.Epsilon) => position.CloseEnough(other.position, threshold) && rotation.CloseEnough(other.rotation, threshold) && scale.CloseEnough(other.scale, threshold);
        #endregion

        #region Transform directions.
        /// <summary>
        /// Gets/sets the local forward direction of the transform.
        /// </summary>
        public Vector3D Forward
        {
            get => rotation.RotateVector(Vector3D.forward);
            set => rotation = Quaternion.FromToRotation(Vector3D.forward, value);
        }

        /// <summary>
        /// Gets/sets the local back direction of the transform.
        /// </summary>
        public Vector3D Back { get => -Forward; set => Forward = -value; }

        /// <summary>
        /// Gets/sets the local up direction of the transform.
        /// </summary>
        public Vector3D Up
        {
            get => rotation.RotateVector(Vector3D.up);
            set => rotation = Quaternion.FromToRotation(Vector3D.up, value);
        }

        /// <summary>
        /// Gets/sets the local down direction of the transform.
        /// </summary>
        public Vector3D Down { get => -Up; set => Up = -value; }

        /// <summary>
        /// Gets/sets the local right direction of the transform.
        /// </summary>
        public Vector3D Right
        {
            get => rotation.RotateVector(Vector3D.right);
            set => rotation = Quaternion.FromToRotation(Vector3D.right, value);
        }

        /// <summary>
        /// Gets/sets the local left direction of the transform.
        /// </summary>
        public Vector3D Left { get => -Right; set => Right = -value; }
        #endregion

        #region Ways to transform a vector and combine transforms.
        /// <summary>
        /// Transforms a vector by this position, rotation and scale.
        /// </summary>
        /// <param name="vector">The vector to transform.</param>
        /// <returns>The transformed vector.</returns>
        public Vector3D TransformVector(Vector3D vector)
        {
            return rotation.RotateVector(vector + position) * scale;
        }

        /// <summary>
        /// Combines the position, rotation and scale of this with other.
        /// </summary>
        /// <param name="other">The transform to combine this transform with.</param>
        /// <returns>this and other combined into one transformation.</returns>
        public Transform CombineTransformations(Transform other) => CombineTransformations(other, CombinationRules.Default);

        /// <summary>
        /// Combines the position, rotation and scale of this based on the combination rules.
        /// </summary>
        /// <param name="other">The transform to combine this transform with.</param>
        /// <param name="combinationRules">The rules of how to combine the transformations.</param>
        /// <returns>this and other combined into one transformation based on the specified rules.</returns>
        public Transform CombineTransformations(Transform other, CombinationRules combinationRules)
        {
            Transform result = this;
            if (combinationRules.HasFlag(CombinationRules.Translate))
                result.position += other.position;

            if (combinationRules.HasFlag(CombinationRules.RotatePosition))
                result.position = other.rotation.RotateVector(result.position);

            if (combinationRules.HasFlag(CombinationRules.ScalePosition))
                result.position *= other.scale;

            if (combinationRules.HasFlag(CombinationRules.Rotate))
                result.rotation = result.rotation.CombineRotations(other.rotation);

            if (combinationRules.HasFlag(CombinationRules.Scale))
                result.scale *= other.scale;

            return result;
        }

        /// <summary>
        /// Returns the transformation that when combined with secondTransformation gives this.
        /// </summary>
        /// <param name="secondTransformation">The transformation that when combined with the return value gives this.</param>
        public Transform DiscombineTransformations(Transform secondTransformation) => DiscombineTransformations(secondTransformation, CombinationRules.Default);

        /// <summary>
        /// Returns the transformation that when combined with secondTransformation gives this.
        /// </summary>
        /// <param name="secondTransformation">The transformation that when combined with the return value gives this.</param>
        /// <param name="discombinationRules">The rules which the return value and secondTransformation were combined with to result in this.</param>
        public Transform DiscombineTransformations(Transform secondTransformation, CombinationRules discombinationRules)
        {
            Transform result = this;
            if (discombinationRules.HasFlag(CombinationRules.ScalePosition))
                result.position /= secondTransformation.scale;

            if (discombinationRules.HasFlag(CombinationRules.RotatePosition))
                result.position = result.position.RotateVector(secondTransformation.rotation.Inverted);

            if (discombinationRules.HasFlag(CombinationRules.Translate))
                result.position -= secondTransformation.position;

            if (discombinationRules.HasFlag(CombinationRules.Rotate))
                result.rotation = result.rotation.CombineRotations(secondTransformation.rotation.Inverted);

            if (discombinationRules.HasFlag(CombinationRules.Scale))
                result.scale /= secondTransformation.scale;

            return result;
        }

        /// <summary>
        /// Rules of transform combinations.
        /// </summary>
        [Flags]
        public enum CombinationRules : byte
        {
            /// <summary>
            /// Should the positions of the two transforms be added?
            /// </summary>
            Translate = 2,

            /// <summary>
            /// Should the rotations of the two transforms be combined?
            /// </summary>
            Rotate = 4,

            /// <summary>
            /// Should the scales of the two transforms be multiplied component-wise?
            /// </summary>
            Scale = 8,

            /// <summary>
            /// Should the position be rotated by the rotation of other?
            /// </summary>
            RotatePosition = 16,

            /// <summary>
            /// Should the position of be scaled by the scale of other?
            /// </summary>
            ScalePosition = 32,

            /// <summary>
            /// Translates (ONLY adds the positions), rotates and scales.
            /// </summary>
            TRS = Translate | Rotate | Scale,

            /// <summary>
            /// The default combination rules (translate, rotate and scale).
            /// </summary>
            Default = CombineAll,

            /// <summary>
            /// All the position related transformations (translation, rotating the position, scaling the position).
            /// </summary>
            FullPosition = Translate | RotatePosition | ScalePosition,

            /// <summary>
            /// Combines the positions, rotations and scales.
            /// </summary>
            CombineAll = Translate | Rotate | Scale | RotatePosition | ScalePosition,

            /// <summary>
            /// Doesn't combine anything.
            /// </summary>
            DontCombine = 0,
        }
        #endregion

        public override string ToString()
        {
            return $"{{ Position: {position}, Rotation: {rotation}, Scale: {scale} }}";
        }
    }
}