﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// Represents a color as 4 floats from 0 to 1.
    /// </summary>
    public struct FColor : ISupergodEquatable<FColor>, ILerpable<FColor>, IClampable<FColor>
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(FColor));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        #region Constant common colors.

        #region Simple RGB and clear.
        /// <summary>Clear color. RGBA is (0, 0, 0, 0).</summary>
        public static readonly FColor clear = new FColor(0, 0, 0, 0);

        /// <summary>Red color. RGBA is (1, 0, 0, 1).</summary>
        public static readonly FColor red = new FColor(1, 0, 0, 1);

        /// <summary>Green color. RGBA is (0, 1, 0, 1).</summary>
        public static readonly FColor green = new FColor(0, 1, 0, 1);

        /// <summary>Blue color. RGBA is (0, 0, 1, 1).</summary>
        public static readonly FColor blue = new FColor(0, 0, 1, 1);
        #endregion

        #region Basic combinations (yellow, pink, cyan).
        /// <summary>Blue color. RGBA is (1, 1, 0, 1).</summary>
        public static readonly FColor yellow = new FColor(1, 1, 0, 1);

        /// <summary>Pink color. RGBA is (1, 0, 1, 1).</summary>
        public static readonly FColor pink = new FColor(1, 0, 1, 1);

        /// <summary>Cyan color. RGBA is (0, 1, 1, 1).</summary>
        public static readonly FColor cyan = new FColor(0, 1, 1, 1);
        #endregion

        #region Complex combinations (purple, teal, orange).
        /// <summary>Purple color. RGBA is (0.3, 0, 1, 1).</summary>
        public static readonly FColor purple = new FColor(.3f, 0, 1, 1);

        /// <summary>Teal color. RGBA is (0, 1, 0.6, 1).</summary>
        public static readonly FColor teal = new FColor(0, 1, .6f, 1);

        /// <summary>Orange color. RGBA is (1, 0.2, 0, 1).</summary>
        public static readonly FColor orange = new FColor(1, .2f, 0, 1);
        #endregion

        #region No hue.
        /// <summary>White color. RGBA is (1, 1, 1, 1).</summary>
        public static readonly FColor white = new FColor(1, 1, 1, 1);

        /// <summary>Black color. RGBA is (0, 0, 0, 1).</summary>
        public static readonly FColor black = new FColor(0, 0, 0, 1);

        /// <summary>Very dark gray color. RGBA is (0.02, 0.02, 0.02, 1).</summary>
        public static readonly FColor veryDarkGray = new FColor(.02f, .02f, .02f, 1);

        /// <summary>Dark gray color. RGBA is (0.1, 0.1, 0.1, 1).</summary>
        public static readonly FColor darkGray = new FColor(.1f, .1f, .1f, 1);

        /// <summary>Medium gray color. RGBA is (0.5, 0.5, 0.5, 1).</summary>
        public static readonly FColor gray = new FColor(.5f, .5f, .5f, 1);

        /// <summary>Light gray color. RGBA is (0.75, 0.75, 0.75, 1).</summary>
        public static readonly FColor lightGray = new FColor(.75f, .75f, .75f, 1);
        #endregion

        #endregion
        
        /// <summary>Red component of the color (range 0-1).</summary>
        public float r;

        /// <summary>Green component of the color (range 0-1).</summary>
        public float g;

        /// <summary>Blue component of the color (range 0-1).</summary>
        public float b;

        /// <summary>Alpha component of the color (range 0-1).</summary>
        public float a;

        /// <summary>
        /// The red, green and blue components of the color (in that order).
        /// </summary>
        public Vector3D RGB
        {
            get => new Vector3D(r, g, b);
            set
            {
                r = value.x;
                g = value.y;
                b = value.z;
            }
        }

        /// <summary>
        /// Creates a new color with default red, green, blue and alpha values.
        /// </summary>
        public FColor(float r = 1, float g = 1, float b = 1, float a = 1)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        #region Comparison methods.
        /// <summary>
        /// Is obj of type Color? If so, is every component of this the same as the corresponding component in b?
        /// </summary>
        public override bool Equals(object obj)
        {
            if (obj is FColor)
                return Equals((FColor)obj);

            return false;
        }

        public override int GetHashCode()
        {
            return r.GetHashCode() ^ g.GetHashCode() ^ b.GetHashCode() ^ a.GetHashCode();
        }

        /// <summary>
        /// Are all of the components of this <seealso cref="SMath.CloseEnough(float, float, float)"/> to the corresponding component in other?
        /// </summary>
        public bool CloseEnough(FColor other, float threshold = Math1D.Epsilon)
        {
            return r.CloseEnough(other.r, threshold) && g.CloseEnough(other.g, threshold) && b.CloseEnough(other.b, threshold) && a.CloseEnough(other.a, threshold);
        }

        /// <summary>
        /// Is every component of this the same as the corresponding component in b?
        /// </summary>
        public bool Equals(FColor other) => this == other;

        /// <summary>
        /// Is every component of a the same as the corresponding component in b?
        /// </summary>
        public static bool operator ==(FColor a, FColor b) => a.r == b.r && a.g == b.g && a.b == b.b && a.a == b.a;

        /// <summary>
        /// Are any of the components of a and b different?
        /// </summary>
        public static bool operator !=(FColor a, FColor b) => !(a == b);
        #endregion

        #region Multiplication.
        /// <summary>
        /// Multiplies every component of color by scalar.
        /// </summary>
        public static FColor operator *(FColor color, float scalar) => new FColor(color.r * scalar, color.g * scalar, color.b * scalar, color.a * scalar);

        /// <summary>
        /// Multiplies every component of color by scalar.
        /// </summary>
        public static FColor operator *(float scalar, FColor color) => color * scalar;

        /// <summary>
        /// Multiplies every component of a by its corresponding component in b.
        /// </summary>
        public static FColor operator *(FColor a, FColor b) => new FColor(a.r * b.r, a.g * b.g, a.b * b.b, a.a * b.a);
        #endregion

        #region Division.
        /// <summary>
        /// Divides every component of color by scalar.
        /// </summary>
        public static FColor operator /(FColor color, float scalar) => new FColor(color.r / scalar, color.g / scalar, color.b / scalar, color.a / scalar);

        /// <summary>
        /// Divides every compojnent of a by its corresponding component in b.
        /// </summary>
        public static FColor operator /(FColor a, FColor b) => new FColor(a.r / b.r, a.g / b.g, a.b / b.b, a.a / b.a);
        #endregion

        #region Addition and subtraction.
        /// <summary>
        /// Adds every component of a with its corresponding component in b.
        /// </summary>
        public static FColor operator +(FColor a, FColor b) => new FColor(a.r + b.r, a.g + b.g, a.b + b.b, a.a + b.a);

        /// <summary>
        /// Subtracts every component of b from its corresponding component in a.
        /// </summary>
        public static FColor operator -(FColor a, FColor b) => new FColor(a.r - b.r, a.g - b.g, a.b - b.b, a.a - b.a);
        #endregion

        #region Inversion.
        /// <summary>
        /// Inverts color (every compnent will be 1 - the corresponding component in color).
        /// </summary>
        public static FColor Invert(FColor color) => new FColor(1 - color.r, 1 - color.g, 1 - color.b, 1 - color.a);

        /// <summary>
        /// <seealso cref="Invert(FColor)"/>s this.
        /// </summary>
        public FColor Inverted => Invert(this);
        #endregion

        #region Clamping and normalizing.
        /// <summary>
        /// Clamps every component of this so it's never smaller than the corresponding component in min and never bigger than the corresponding component in max.
        /// </summary>
        public FColor Clamp(FColor min, FColor max) => new FColor(r.Clamp(min.r, max.r), g.Clamp(min.g, max.g), b.Clamp(min.b, max.b), a.Clamp(min.a, max.a));

        /// <summary>
        /// Clamps every component of color between 0 and 1.
        /// </summary>
        public static FColor Normalize(FColor color) => color.Clamp(black, white);

        /// <summary>
        /// Clamps every component of this between 0 and 1.
        /// </summary>
        public FColor Normalized => Normalize(this);
        #endregion

        #region ILerpable implementations.
        FColor ILerpable<FColor>.Multiply(float scalar) => this * scalar;
        FColor ILerpable<FColor>.Add(FColor other) => this + other;
        #endregion

        public override string ToString()
        {
            return $"(R: {r}, G: {g}, B: {b}, A: {a})";
        }

        /// <summary>
        /// Creates a new Vector4D with the red, green, blue and alpha components of color in that order.
        /// </summary>
        public static implicit operator Vector4D(FColor color) => new Vector4D(color.RGB, color.a);

        /// <summary>
        /// Converts a color from floats to a color from bytes. Explicit because accuracy might be lost.
        /// </summary>
        public static explicit operator BColor(FColor color)
        {
            color = color.Normalized;
            byte FloatToByte(float f)
            {
                return (byte)(f * byte.MaxValue);
            }
            return new BColor(FloatToByte(color.r), FloatToByte(color.g), FloatToByte(color.b), FloatToByte(color.a));
        }
    }
}