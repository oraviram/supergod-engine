﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// Represents a 32 bit color with 4 bytes from 0 to the maximum value of a byte.
    /// </summary>
    public struct BColor
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(BColor));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        #region Constant common colors.

        #region Simple RGB and clear.
        /// <summary>Clear color. RGBA is (0, 0, 0, 0).</summary>
        public static readonly BColor clear = new BColor(0, 0, 0, 0);

        /// <summary>Red color. RGBA is (1, 0, 0, 1).</summary>
        public static readonly BColor red = new BColor(255, 0, 0, 255);

        /// <summary>Green color. RGBA is (0, 1, 0, 1).</summary>
        public static readonly BColor green = new BColor(0, 255, 0, 255);

        /// <summary>Blue color. RGBA is (0, 0, 1, 1).</summary>
        public static readonly BColor blue = new BColor(0, 0, 255, 255);
        #endregion

        #region Basic combinations (yellow, pink, cyan).
        /// <summary>Blue color. RGBA is (1, 1, 0, 1).</summary>
        public static readonly BColor yellow = new BColor(255, 255, 0, 255);

        /// <summary>Pink color. RGBA is (1, 0, 1, 1).</summary>
        public static readonly BColor pink = new BColor(255, 0, 255, 255);

        /// <summary>Cyan color. RGBA is (0, 1, 1, 1).</summary>
        public static readonly BColor cyan = new BColor(0, 255, 255, 255);
        #endregion

        #region Complex combinations (purple, teal, orange).
        /// <summary>Purple color. RGBA is (0.3, 0, 1, 1).</summary>
        public static readonly BColor purple = new BColor(77, 0, 255, 255);

        /// <summary>Teal color. RGBA is (0, 1, 0.6, 1).</summary>
        public static readonly BColor teal = new BColor(0, 255, 153, 255);

        /// <summary>Orange color. RGBA is (1, 0.2, 0, 1).</summary>
        public static readonly BColor orange = new BColor(255, 51, 0, 255);
        #endregion

        #region No hue.
        /// <summary>White color. RGBA is (1, 1, 1, 1).</summary>
        public static readonly BColor white = new BColor(255, 255, 255, 255);

        /// <summary>Black color. RGBA is (0, 0, 0, 1).</summary>
        public static readonly BColor black = new BColor(0, 0, 0, 255);

        /// <summary>Very dark gray color. RGBA is (0.02, 0.02, 0.02, 1).</summary>
        public static readonly BColor veryDarkGray = new BColor(5, 5, 5, 255);

        /// <summary>Dark gray color. RGBA is (0.1, 0.1, 0.1, 1).</summary>
        public static readonly BColor darkGray = new BColor(26, 26, 26, 255);

        /// <summary>Medium gray color. RGBA is (0.5, 0.5, 0.5, 1).</summary>
        public static readonly BColor gray = new BColor(128, 128, 128, 255);

        /// <summary>Light gray color. RGBA is (0.75, 0.75, 0.75, 1).</summary>
        public static readonly BColor lightGray = new BColor(191, 191, 191, 1);
        #endregion

        #endregion

        /// <summary>Red component of the color (range 0-1).</summary>
        public byte r;

        /// <summary>Green component of the color (range 0-1).</summary>
        public byte g;

        /// <summary>Blue component of the color (range 0-1).</summary>
        public byte b;

        /// <summary>Alpha component of the color (range 0-1).</summary>
        public byte a;

        /// <summary>
        /// Gets/sets the value of the color as an unsigned 32 bit integer.
        /// </summary>
        public unsafe uint UIntValue
        {
            get
            {
                fixed (BColor* value = &this)
                {
                    return *(uint*)value;
                }
            }

            set
            {
                byte* pointer = (byte*)&value;
                r = pointer[0];
                g = pointer[1];
                b = pointer[2];
                a = pointer[3];
            }
        }

        /// <summary>
        /// Creates a new color with default red, green, blue and alpha values.
        /// </summary>
        public BColor(byte r = byte.MaxValue, byte g = byte.MaxValue, byte b = byte.MaxValue, byte a = byte.MaxValue)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        /// <summary>
        /// Initializes a new color with an unsigned integer as its value.
        /// </summary>
        public BColor(uint value) : this()
        {
            UIntValue = value;
        }

        /// <summary>Gets the value of color as an unsigned 32 bit integer.</summary>
        public static implicit operator uint(BColor color) => color.UIntValue;

        /// <summary>Creates a color from an unsigned 32 bit integer.</summary>
        public static implicit operator BColor(uint color) => new BColor(color);

        /// <summary>Converts a 32-bit color to a 4-float color.</summary>
        public static implicit operator FColor(BColor color) => new FColor(color.r / byte.MaxValue, color.g / byte.MaxValue, color.b / byte.MaxValue, color.a / byte.MaxValue);
    }
}