﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// A 3D vector with an x, y and z components.
    /// </summary>
    public struct Vector3D : IVector<Vector3D>
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Vector3D));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        #region Static readonly presets for common vectors.
        /// <summary>Same as doing new Vector3D(0, 0, 0).</summary>
        public static readonly Vector3D zero = new Vector3D(0, 0, 0);

        /// <summary>Same as doing new Vector3D(1, 1, 1).</summary>
        public static readonly Vector3D one = new Vector3D(1, 1, 1);

        /// <summary>Same as doing new Vector3D(0, 1, 0).</summary>
        public static readonly Vector3D up = new Vector3D(0, 1, 0);

        /// <summary>Same as doing new Vector3D(0, -1, 0).</summary>
        public static readonly Vector3D down = new Vector3D(0, -1, 0);

        /// <summary>Same as doing new Vector3D(1, 0, 0).</summary>
        public static readonly Vector3D right = new Vector3D(1, 0, 0);

        /// <summary>Same as doing new Vector3D(-1, 0, 0).</summary>
        public static readonly Vector3D left = new Vector3D(-1, 0, 0);

        /// <summary>Same as doing new Vector3D(0, 0, 1).</summary>
        public static readonly Vector3D forward = new Vector3D(0, 0, 1);

        /// <summary>Same as doing new Vector3D(0, 0, -1).</summary>
        public static readonly Vector3D back = new Vector3D(0, 0, -1);
        #endregion

        public float x, y, z;
        
        private const string INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE = "Component index in a Vector3D indexer must be either 0, 1 or 2.";

        /// <summary>
        /// Gets or sets a component at the index of index.
        /// </summary>
        /// <param name="index">0 = x, 1 = y, 2 = z. Anything else will through an index out of range exception.</param>
        /// <exception cref="IndexOutOfRangeException">The given index wasn't 0, 1, or 2.</exception>
        public float this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return x;

                    case 1:
                        return y;

                    case 2:
                        return z;
                }
                throw new IndexOutOfRangeException(INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE);
            }
            set
            {
                switch (index)
                {
                    case 0:
                        x = value;
                        return;

                    case 1:
                        y = value;
                        return;

                    case 2:
                        z = value;
                        return;
                }
                throw new IndexOutOfRangeException(INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE);
            }
        }

        /// <summary>
        /// Converts the vector to a float array.
        /// </summary>
        public float[] AsFloatArray
        {
            get { return new float[] { x, y, z }; }
        }

        /// <summary>
        /// Creates a new 3D vector with default x, y and z values.
        /// </summary>
        public Vector3D(float x = 0, float y = 0, float z = 0)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /// <summary>
        /// Creates a new Vector3D with a default x and a default vector for y and z.
        /// </summary>
        public Vector3D(float x, Vector2D yz) : this(x, yz.x, yz.y)
        {
        }

        /// <summary>
        /// Creates a new Vector3D with a vector for x and y and a default value for z.
        /// </summary>
        public Vector3D(Vector2D xy, float z) : this(xy.x, xy.y, z)
        {
        }

        #region Axis Combinations.
        /// <summary>A Vector2D with the x and y components of this vector.</summary>
        public Vector2D XY
        {
            get { return new Vector2D(x, y); }
        }

        /// <summary>A Vector2D with the y and x components of this vector.</summary>
        public Vector2D YX
        {
            get { return new Vector2D(y, x); }
        }

        /// <summary>A Vector2D with the x and z components of this vector.</summary>
        public Vector2D XZ
        {
            get { return new Vector2D(x, z); }
        }

        /// <summary>A Vector2D with the z and x components of this vector.</summary>
        public Vector2D ZX
        {
            get { return new Vector2D(z, x); }
        }

        /// <summary>A Vector2D with the y and z components of this vector.</summary>
        public Vector2D YZ
        {
            get { return new Vector2D(y, z); }
        }

        /// <summary>A Vector2D with the z and y components of this vector.</summary>
        public Vector2D ZY
        {
            get { return new Vector2D(z, y); }
        }
        #endregion

        #region Magnitude, squared magnitude amd normalization.
        /// <summary>
        /// Gets the magnitude of this vector. Note that if the exact magnitude is not needed <seealso cref="SqrMagnitude"/> should be more performant.
        /// </summary>
        public float Magnitude => Vector.Magnitude(this);

        /// <summary>
        /// Gets the square of the magnitude of this vector. This action is less expensive than <seealso cref="Magnitude"/>.
        /// </summary>
        public float SqrMagnitude => Vector.SqrMagnitude(this);

        /// <summary>
        /// Gets the normalized version of this vector.
        /// </summary>
        public Vector3D Normalized => Vector.Normalize(this);
        #endregion

        #region Multiplication (Dot, Cross, Multiply, and the operators).
        /// <summary>
        /// Returns the <seealso cref="Dot(Vector3D, Vector3D)"/> product of this and other.
        /// </summary>
        public float Dot(Vector3D other) => XY.Dot(other.XY) + z * other.z;

        /// <summary>Gets the cross product of left and right.</summary>
        public static Vector3D Cross(Vector3D left, Vector3D right)
        {
            return new Vector3D(
                left.y * right.z - left.z * right.y,
                left.z * right.x - left.x * right.z,
                left.x * right.y - left.y * right.x);
        }

        /// <summary>Gets the <see cref="Cross(Vector3D, Vector3D)"/> product of this and other.</summary>
        public Vector3D Cross(Vector3D other) => Cross(this, other);

        /// <summary>Multiplies this and other component-wise.</summary>
        public Vector3D Multiply(Vector3D other) => this * other;

        /// <summary>Multiplies each element of this by scalar.</summary>
        public Vector3D Multiply(float scalar) => this * scalar;

        /// <summary>Multiplies each element of vector by scalar.</summary>
        public static Vector3D operator *(Vector3D vector, float scalar) => new Vector3D(vector.XY * scalar, vector.z * scalar);

        /// <summary>Multiplies each element of vector by scalar.</summary>
        public static Vector3D operator *(float scalar, Vector3D vector) => vector * scalar;

        /// <summary>Multiplies left and right component-wise.</summary>
        public static Vector3D operator *(Vector3D left, Vector3D right) => new Vector3D(left.XY * right.XY, left.z * right.z);
        #endregion

        #region Division.
        /// <summary>Divides each component of vectors this and other.</summary>
        public Vector3D Divide(Vector3D other) => this / other;

        /// <summary>Divides each component of this by scalar.</summary>
        public Vector3D Divide(float scalar) => this / scalar;

        /// <summary>Divides each component of vector by scalar.</summary>
        public static Vector3D operator /(Vector3D vector, float scalar) => new Vector3D(vector.XY / scalar, vector.z / scalar);

        /// <summary>Divides each component of vector by scalar.</summary>
        public static Vector3D operator /(Vector3D left, Vector3D right) => new Vector3D(left.XY / right.XY, left.z / right.z);

        #endregion

        #region Addition and subtraction.
        /// <summary>Adds each component of the vectors.</summary>
        public Vector3D Add(Vector3D other) => this + other;

        /// <summary>Subtracts each component of the vectors.</summary>
        public Vector3D Subtract(Vector3D other) => this - other;

        /// <summary>Adds each component of the vectors.</summary>
        public static Vector3D operator +(Vector3D left, Vector3D right) => new Vector3D(left.x + right.x, left.YZ + right.YZ);

        /// <summary>Subtracts each component of the vectors.</summary>
        public static Vector3D operator -(Vector3D left, Vector3D right) => new Vector3D(left.x - right.x, left.YZ - right.YZ);

        #endregion

        #region Negating (Negate, oprators).
        /// <summary>Flips (multiplies by -1) every component of this.</summary>
        public Vector3D Negated => -this;

        /// <summary>Flips (multiplies by -1) every component of vector.</summary>
        public static Vector3D operator -(Vector3D vector) => new Vector3D(-vector.XY, -vector.z);
        #endregion

        #region BiggestAxis/SmallestAxis.
        /// <summary>Returns the value of the biggest axis.</summary>
        public float BiggestComponent => Math1D.Max(x, y, z);

        /// <summary>Returns the value of the smallest axis.</summary>
        public float SmallestComponent => Math1D.Min(x, y, z);
        #endregion

        #region Comparison methods (Equals, operators, ContainsAxis, CloseEnough, GetHashCode).
        /// <summary>Does this contain an axis with the value of axisValue?</summary>
        public bool ContainsComponent (float componentValue) => XY.ContainsComponent(componentValue) || z == componentValue;

        /// <summary>Are all of the components of this and other the same?</summary>
        public bool Equals(Vector3D other) => this == other;

        /// <summary>Is this <seealso cref="SMath.CloseEnough(float, float, float)"/> to other?</summary>
        public bool CloseEnough(Vector3D other, float threshold = Math1D.Epsilon) => XY.CloseEnough(other.XY, threshold) && z.CloseEnough(other.z, threshold);

        /// <summary>Are all of the components of left and right the same?</summary>
        public static bool operator ==(Vector3D left, Vector3D right) => left.XY == right.XY && left.z == right.z;

        /// <summary>Are any of the components of left and right different?</summary>
        public static bool operator !=(Vector3D left, Vector3D right) { return !(left == right); }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector3D))
                return false;

            return Equals((Vector3D)obj);
        }

        public override int GetHashCode()
        {
            return XY.GetHashCode() ^ z.GetHashCode();
        }
        #endregion

        #region Clamping (Clamp, ClampAxes, Abs).
        /// <summary>Clamps all of the axes of this so they are never smaller than the corresponding component in min and never bigger than the corresponding component in max.</summary>
        public Vector3D Clamp(Vector3D min, Vector3D max) => new Vector3D(XY.Clamp(min.XY, max.XY), z.Clamp(min.z, max.z));

        /// <summary>Clamps all of the axes of this so they are never smaller than min and never bigger than max.</summary>
        public Vector3D ClampComponents(float min, float max) => new Vector3D(XY.ClampComponents(min, max), z.Clamp(min, max));

        /// <summary>Gets the absolute value of all of the components in the vector.</summary>
        public Vector3D Abs => new Vector3D(XY.Abs, z.Abs());
        #endregion

        #region Conversions.
        /// <summary>
        /// Returns a new Vector2D with the x and y components of vector.
        /// </summary>
        public static explicit operator Vector2D(Vector3D vector)
        {
            return new Vector2D(vector.x, vector.y);
        }
        
        /// <summary>
        /// Returns a new Vector4D with the x, y and z components of vector and defaults w to zero.
        /// </summary>
        public static implicit operator Vector4D(Vector3D vector)
        {
            return new Vector4D(vector.x, vector.y, vector.z, 0);
        }

        /// <summary>
        /// Uses the components for the vector as the radians in the new euler angles.
        /// </summary>
        public static implicit operator EulerAngles(Vector3D vector)
        {
            return new EulerAngles(vector.x, vector.y, vector.z);
        }

        /// <summary>
        /// Returns a string formatted as "(x, y, z)".
        /// </summary>
        public override string ToString()
        {
            return $"({x}, {y}, {z})";
        }
        #endregion
    }
}