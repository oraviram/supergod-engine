﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// Represents a 4D vector with x y z and w components.
    /// </summary>
    public struct Vector4D : IVector<Vector4D>
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Vector4D));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        #region Static readonly presets for common vectors.
        /// <summary>(0, 0, 0, 0)</summary>
        public static readonly Vector4D zero = new Vector4D(0, 0, 0, 0);

        /// <summary>(1, 1, 1, 1)</summary>
        public static readonly Vector4D one = new Vector4D(1, 1, 1, 1);

        /// <summary>(1, 0, 0, 0)</summary>
        public static readonly Vector4D unitX = new Vector4D(1, 0, 0, 0);

        /// <summary>(0, 1, 0, 0)</summary>
        public static readonly Vector4D unitY = new Vector4D(0, 1, 0, 0);

        /// <summary>(0, 0, 1, 0)</summary>
        public static readonly Vector4D unitZ = new Vector4D(0, 0, 1, 0);

        /// <summary>(0, 0, 0, 1)</summary>
        public static readonly Vector4D unitW = new Vector4D(0, 0, 0, 1);
        #endregion

        public float x, y, z, w;

        private const string INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE = "Component index in a Vector4D indexer must be either 0, 1, 2 or 3.";

        /// <summary>
        /// Gets or sets a component at the index of index.
        /// </summary>
        /// <param name="index">0 = x, 1 = y, 2 = z, 3 = w. Anything else will through an index out of range exception.</param>
        /// <exception cref="IndexOutOfRangeException">The given index wasn't 0, 1, 2 or 3.</exception>
        public float this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return x;

                    case 1:
                        return y;

                    case 2:
                        return z;

                    case 3:
                        return w;
                }
                throw new IndexOutOfRangeException(INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE);
            }
            set
            {
                switch (index)
                {
                    case 0:
                        x = value;
                        return;

                    case 1:
                        y = value;
                        return;

                    case 2:
                        z = value;
                        return;

                    case 3:
                        w = value;
                        return;
                }
                throw new IndexOutOfRangeException(INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE);
            }
        }

        /// <summary>Gets the vector as an array with x, y, z and w.</summary>
        public float[] AsFloatArray => new float[] { x, y, z, w };

        #region Specific Vector3D Axes.
        // X y and z combinations:
        public Vector3D YXZ => new Vector3D(y, x, z);
        public Vector3D YZX => new Vector3D(y, z, x);

        public Vector3D ZXY => new Vector3D(z, x, y);
        public Vector3D XZY => new Vector3D(x, z, y);

        public Vector3D ZYX => new Vector3D(z, y, x);
        public Vector3D XYZ => new Vector3D(x, y, z);

        // Y z and w combinations:
        public Vector3D YZW => new Vector3D(y, z, w);
        public Vector3D YWZ => new Vector3D(y, w, z);

        public Vector3D ZYW => new Vector3D(z, y, w);
        public Vector3D WYZ => new Vector3D(w, y, z);

        public Vector3D ZWY => new Vector3D(z, w, y);
        public Vector3D WZY => new Vector3D(w, z, y);

        // X z and w combinations:
        public Vector3D XZW => new Vector3D(x, z, w);
        public Vector3D XWZ => new Vector3D(x, w, z);

        public Vector3D ZXW => new Vector3D(z, x, w);
        public Vector3D WXZ => new Vector3D(w, x, z);

        public Vector3D ZWX => new Vector3D(z, w, x);
        public Vector3D WZX => new Vector3D(w, z, x);

        // Y w and x combinations:
        public Vector3D YWX => new Vector3D(y, w, x);
        public Vector3D YXW => new Vector3D(y, x, w);

        public Vector3D XYW => new Vector3D(x, y, w);
        public Vector3D WYX => new Vector3D(w, y, x);

        public Vector3D XWY => new Vector3D(x, w, y);
        public Vector3D WXY => new Vector3D(w, x, y);
        #endregion

        #region Specific Vector2D Axes.
        public Vector2D XY => new Vector2D(x, y);
        public Vector2D YX => new Vector2D(y, x);

        public Vector2D XZ => new Vector2D(x, z);
        public Vector2D ZX => new Vector2D(z, x);

        public Vector2D XW => new Vector2D(x, w);
        public Vector2D WX => new Vector2D(w, x);

        public Vector2D YZ => new Vector2D(y, z);
        public Vector2D ZY => new Vector2D(z, y);

        public Vector2D YW => new Vector2D(y, w);
        public Vector2D WY => new Vector2D(w, y);

        public Vector2D ZW => new Vector2D(z, w);
        public Vector2D WZ => new Vector2D(w, z);
        #endregion

        #region Constructors.
        /// <summary>Initializes a new Vector4D for with an x, a y, a z, and a w.</summary>
        public Vector4D(float x = 0, float y = 0, float z = 0, float w = 0) { this.x = x; this.y = y; this.z = z; this.w = w; }

        /// <summary>Initializes a new Vector4D with a vector for x and y and a vector for z and w.</summary>
        public Vector4D(Vector2D xy, Vector2D zw) : this(xy.x, xy.y, zw.x, zw.y) { }

        /// <summary>Initializes a new Vector4D with a vector for x, y and z and a float for w.</summary>
        public Vector4D(Vector3D xyz, float w) : this(xyz.x, xyz.y, xyz.z, w) { }

        /// <summary>Initializes a new Vector4D with a float for x and a vector for y, z and w.</summary>
        public Vector4D(float x, Vector3D yzw) : this(x, yzw.x, yzw.y, yzw.z) { }

        /// <summary>Initializes a new Vector4D with a float for x, y, and a vector for z and w.</summary>
        public Vector4D(float x, float y, Vector2D zw) : this(x, y, zw.x, zw.y) { }

        /// <summary>Initializes a new Vector4D with a float for x, a vector for y and z, and a float for w.</summary>
        public Vector4D(float x, Vector2D yz, float w) : this(x, yz.x, yz.y, w) { }

        /// <summary>Initializes a new Vector4D with a vector for x and y and floats for z and w.</summary>
        public Vector4D(Vector2D xy, float z, float w) : this(xy.x, xy.y, z, w) { }
        #endregion

        #region Addition and subtraction.
        /// <summary>
        /// Adds corresponding components.
        /// </summary>
        public Vector4D Add(Vector4D other) => this + other;

        /// <summary>
        /// Adds corresponding components.
        /// </summary>
        public static Vector4D operator +(Vector4D a, Vector4D b) => new Vector4D(a.XYZ + b.XYZ, a.w + b.w);

        /// <summary>
        /// Subtracts every component of this by its corresponding component in other.
        /// </summary>
        public Vector4D Subtract(Vector4D other) => this - other;

        /// <summary>
        /// Subtracts every component of a by its corresponding component in b.
        /// </summary>
        public static Vector4D operator -(Vector4D a, Vector4D b) => new Vector4D(a.XYZ - b.XYZ, a.w - b.w);
        #endregion

        #region Negating.
        /// <summary>
        /// Negates (multiplies by -1) every component of this.
        /// </summary>
        public Vector4D Negated => -this;
        
        /// <summary>
        /// Negates (multiplies by -1) every component of vector.
        /// </summary>
        public static Vector4D operator -(Vector4D vector) => new Vector4D(-vector.XYZ, -vector.w);
        #endregion

        #region Multiplication (Dot, Multiply and the operators).
        /// <summary>
        /// Multiplies this and other component-wise.
        /// </summary>
        public Vector4D Multiply(Vector4D other) => this * other;

        /// <summary>
        /// Multiplies a and b component-wise.
        /// </summary>
        public static Vector4D operator *(Vector4D a, Vector4D b) => new Vector4D(a.XYZ * b.XYZ, a.w * b.w);

        /// <summary>
        /// Multiplies every component of this by scalar.
        /// </summary>
        public Vector4D Multiply(float scalar) => this * scalar;

        /// <summary>
        /// Multiplies every component of vector by scalar.
        /// </summary>
        public static Vector4D operator *(Vector4D vector, float scalar) => new Vector4D(vector.XYZ * scalar, vector.w * scalar);

        /// <summary>
        /// Multiplies every component of vector by scalar.
        /// </summary>
        public static Vector4D operator *(float scalar, Vector4D vector) => vector * scalar;

        /// <summary>
        /// Takes the dot product of this and other.
        /// </summary>
        public float Dot(Vector4D other) => other.XYZ.Dot(XYZ) + other.w * w;
        #endregion

        #region Division.
        /// <summary>
        /// Multiplies this and other component-wise.
        /// </summary>
        public Vector4D Divide(Vector4D other) => this / other;

        /// <summary>
        /// Multiplies a and b component-wise.
        /// </summary>
        public static Vector4D operator /(Vector4D a, Vector4D b) => new Vector4D(a.XYZ / b.XYZ, a.w / b.w);

        /// <summary>
        /// Multiplies every component of this by scalar.
        /// </summary>
        public Vector4D Divide(float scalar) => this / scalar;

        /// <summary>
        /// Multiplies every component of vector by scalar.
        /// </summary>
        public static Vector4D operator /(Vector4D vector, float scalar) => new Vector4D(vector.XYZ / scalar, vector.w / scalar);
        #endregion

        #region Magnitude, SqrMagnitude and normalization.
        /// <summary>
        /// Gets the magnitude of this vector (less efficient than <seealso cref="SqrMagnitude"/>).
        /// </summary>
        public float Magnitude => Vector.Magnitude(this);

        /// <summary>
        /// Gets the squared magnitude of this vector (more efficient than <seealso cref="Magnitude"/>).
        /// </summary>
        public float SqrMagnitude => Vector.SqrMagnitude(this);

        /// <summary>
        /// Makes vector unit length. If the magnitude is zero, value is defaulted to (0, 0, 0, 0).
        /// </summary>
        public Vector4D Normalized => Vector.Normalize(this);
        #endregion

        #region BiggestAxis/SmallestAxis
        /// <summary>
        /// Gets the value of the biggest axis in this vector.
        /// </summary>
        public float BiggestComponent => Math1D.Max(x, y, z, w);

        /// <summary>
        /// Gets the value of the smallest axis in this vector.
        /// </summary>
        public float SmallestComponent => Math1D.Min(x, y, z, w);
        #endregion
        
        #region Comparison methods (Equals, GetHashCode, operators, ContainsAxis, CloseEnough).
        /// <summary>Does this contain an axis with the value of axisValue?</summary>
        public bool ContainsComponent(float componentValue) => XYZ.ContainsComponent(componentValue) || w == componentValue;

        /// <summary>Is this <seealso cref="SMath.CloseEnough(float, float, float)"/> to other?</summary>
        public bool CloseEnough(Vector4D other, float threshold = Math1D.Epsilon) => XYZ.CloseEnough(other.XYZ, threshold) && w.CloseEnough(other.w, threshold);

        /// <summary>Are all of the components of this and other the same?</summary>
        public bool Equals(Vector4D other) => this == other;

        public override bool Equals(object obj)
        {
            if (!(obj is Vector4D))
                return false;

            return Equals((Vector4D)obj);
        }

        /// <summary>Are all of the components of left and right the same?</summary>
        public static bool operator ==(Vector4D left, Vector4D right) => left.XYZ == right.XYZ && left.w == right.w;

        /// <summary>Are any of the components of left and right different?</summary>
        public static bool operator !=(Vector4D left, Vector4D right) => !(left == right);

        public override int GetHashCode()
        {
            return XYZ.GetHashCode() ^ w.GetHashCode();
        }
        #endregion

        #region Clamping (Clamp, ClampAxes, Abs).
        /// <summary>Clamps all of the axes of this so they are never smaller than the corresponding component in min and never bigger than the corresponding component in max.</summary>
        public Vector4D Clamp(Vector4D min, Vector4D max) => new Vector4D(XYZ.Clamp(min.XYZ, max.XYZ), w.Clamp(min.w, max.w));

        /// <summary>Clamps all of the axes of this so they are never smaller than min and never bigger than max.</summary>
        public Vector4D ClampComponents(float min, float max) => new Vector4D(XYZ.ClampComponents(min, max), w.Clamp(min, max));

        /// <summary>Gets the absolute value of all of the components in the vector.</summary>
        public Vector4D Abs => new Vector4D(XYZ.Abs, w.Abs());
        #endregion

        #region Convertions.
        /// <summary>
        /// Constructs a new Vector2D with the x and y components of vector.
        /// </summary>
        public static explicit operator Vector2D(Vector4D vector)
        {
            return new Vector2D(vector.x, vector.y);
        }

        /// <summary>
        /// Constructs a new Vector2D with the x, y and z components of vector.
        /// </summary>
        public static explicit operator Vector3D(Vector4D vector)
        {
            return new Vector3D(vector.x, vector.y, vector.z);
        }

        /// <summary>
        /// Converts a Vector4D to a Color where x = r, y = g, z = b, w = a.
        /// </summary>
        public static implicit operator FColor(Vector4D vector)
        {
            return new FColor(vector.x, vector.y, vector.z, vector.w);
        }

        /// <summary>
        /// Returns a new quaternion with the x, y, z, and w components of the vector.
        /// </summary>
        public static implicit operator Quaternion(Vector4D vector)
        {
            return new Quaternion(vector.w, vector.XYZ);
        }

        /// <summary>
        /// Returns a formatted string of the vector as "(x, y, z, w)".
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"({x}, {y}, {z}, {w})";
        }
        #endregion
    }
}