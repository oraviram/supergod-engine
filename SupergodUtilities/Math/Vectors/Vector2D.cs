﻿using System;

namespace SupergodUtilities.Math
{
    /// <summary>
    /// A 2D vector with an x and a y component.
    /// </summary>
    public struct Vector2D : IVector<Vector2D>
    {
        #region Static memory variables.
        /// <summary>
        /// The size of an instance of this class (measured in bytes).
        /// </summary>
        public static readonly int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(typeof(Vector2D));

        /// <summary>
        /// The size of an instance of this class (measured in floats).
        /// </summary>
        public static readonly int sizeInFloats = sizeInBytes / sizeof(float);
        #endregion

        #region Static readonly presets for commonly used vectors.
        /// <summary>Same as doing new Vector2D(0, 0).</summary>
        public static readonly Vector2D zero = new Vector2D(0, 0);

        /// <summary>Same as doing new Vector2D(1, 1).</summary>
        public static readonly Vector2D one = new Vector2D(1, 1);

        /// <summary>Same as doing new Vector2D(0, -1).</summary>
        public static readonly Vector2D down = new Vector2D(0, -1);

        /// <summary>Same as doing new Vector2D(0, 1).</summary>
        public static readonly Vector2D up = new Vector2D(0, 1);

        /// <summary>Same as doing new Vector2D(1, 0).</summary>
        public static readonly Vector2D right = new Vector2D(1, 0);

        /// <summary>Same as doing new Vector2D(-1, 0).</summary>
        public static readonly Vector2D left = new Vector2D(-1, 0);
        #endregion

        public float x, y;

        private const string INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE = "Component index in a Vector2D indexer must be either 0 or 1.";

        /// <summary>
        /// Gets or sets a component at the index of index.
        /// </summary>
        /// <param name="index">0 = x, 1 = y. Anything else will through an index out of range exception.</param>
        /// <exception cref="IndexOutOfRangeException">The given index wasn't 0 or 1.</exception>
        public float this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return x;

                    case 1:
                        return y;
                }
                throw new IndexOutOfRangeException(INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE);
            }
            set
            {
                switch (index)
                {
                    case 0:
                        x = value;
                        return;

                    case 1:
                        y = value;
                        return;
                }
                throw new IndexOutOfRangeException(INDEXER_OUT_OF_RANGE_EXCEPTION_MESSAGE);
            }
        }

        /// <summary>
        /// Converts the vector to a float array.
        /// </summary>
        public float[] AsFloatArray
        {
            get { return new float[] { x, y }; }
        }

        /// <summary>
        /// Creates a new 2D vector with default x and y values.
        /// </summary>
        public Vector2D(float x = 0, float y = 0)
        {
            this.x = x;
            this.y = y;
        }

        #region Magnitude, squared magnitude and normalization.
        /// <summary>
        /// Gets the magnitude of this vector. Note that if the exact magnitude is not needed <seealso cref="SqrMagnitude"/> should be more performant.
        /// </summary>
        public float Magnitude => Vector.Magnitude(this);

        /// <summary>
        /// Gets the square of the magnitude of this vector. This action is less expensive than <seealso cref="Magnitude"/>.
        /// </summary>
        public float SqrMagnitude => Vector.SqrMagnitude(this);

        /// <summary>
        /// Gets the normalized version of this vector.
        /// </summary>
        public Vector2D Normalized => Vector.Normalize(this);
        #endregion

        #region Multiplication (Dot, Multiply, and the operators).
        /// <summary>Returns the <seealso cref="Dot(Vector2D, Vector2D)"/> product of this and other.</summary>
        public float Dot(Vector2D other) => x * other.x + y * other.y;

        /// <summary>Multiplies this and other component-wise.</summary>
        public Vector2D Multiply(Vector2D other) => this * other;

        /// <summary>Multiplies each element of this by scalar.</summary>
        public Vector2D Multiply(float scalar) => this * scalar;

        /// <summary>Multiplies each element of vector by scalar.</summary>
        public static Vector2D operator *(Vector2D vector, float scalar) => new Vector2D(vector.x * scalar, vector.y * scalar);

        /// <summary>Multiplies each element of vector by scalar.</summary>
        public static Vector2D operator *(float scalar, Vector2D vector) => vector * scalar;

        /// <summary>Multiplies left and right component-wise.</summary>
        public static Vector2D operator *(Vector2D left, Vector2D right) => new Vector2D(left.x * right.x, left.y * right.y);
        #endregion

        #region Division.
        /// <summary>Divides each component of vectors this and other.</summary>
        public Vector2D Divide(Vector2D other) => this / other;

        /// <summary>Divides each component of this by scalar.</summary>
        public Vector2D Divide(float scalar) => this / scalar;

        /// <summary>Divides each component of vector by scalar.</summary>
        public static Vector2D operator /(Vector2D vector, float scalar) => new Vector2D(vector.x / scalar, vector.y / scalar);
        
        /// <summary>Divides each component of vector by scalar.</summary>
        public static Vector2D operator /(Vector2D left, Vector2D right) => new Vector2D(left.x / right.x, left.y / right.y);

        #endregion

        #region Addition and subtraction.
        /// <summary>Adds each component of the vectors.</summary>
        public Vector2D Add(Vector2D other) => this + other;

        /// <summary>Adds each component of the vectors.</summary>
        public static Vector2D operator +(Vector2D left, Vector2D right) => new Vector2D(left.x + right.x, left.y + right.y);

        /// <summary>Subtracts each component of the vectors.</summary>
        public Vector2D Subtract(Vector2D other) => this - other;

        /// <summary>Subtracts each component of the vectors.</summary>
        public static Vector2D operator -(Vector2D left, Vector2D right) => new Vector2D(left.x - right.x, left.y - right.y);
        #endregion

        #region Negating (Negate, oprators).
        /// <summary>Flips (multiplies by -1) every component of this.</summary>
        public Vector2D Negated => -this;

        /// <summary>Flips (multiplies by -1) every component of vector.</summary>
        public static Vector2D operator -(Vector2D vector) => new Vector2D(-vector.x, -vector.y);
        #endregion

        #region BiggestAxis/SmallestAxis.
        /// <summary>Returns the value of the biggest axis.</summary>
        public float BiggestComponent => Math1D.Max(x, y);

        /// <summary>Returns the value of the smallest axis.</summary>
        public float SmallestComponent => Math1D.Min(x, y);
        #endregion

        #region Comparison methods (Equals, operators, ContainsAxis, CloseEnough, GetHashCode)
        /// <summary>Does this contain an axis with the value of axisValue?</summary>
        public bool ContainsComponent(float componentValue) => x == componentValue || y == componentValue;

        /// <summary>Are all of the components of this and other the same?</summary>
        public bool Equals(Vector2D other) => this == other;

        /// <summary>Is this <seealso cref="SMath.CloseEnough(float, float, float)"/> to other?</summary>
        public bool CloseEnough(Vector2D other, float threshold = Math1D.Epsilon) => other.x.CloseEnough(x, threshold) && other.y.CloseEnough(y, threshold);

        /// <summary>Are all of the components of left and right the same?</summary>
        public static bool operator ==(Vector2D left, Vector2D right) => left.x == right.x && left.y == right.y;
        
        /// <summary>Are any of the components of left and right different?</summary>
        public static bool operator !=(Vector2D left, Vector2D right) { return !(left == right); }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector2D))
                return false;

            return Equals((Vector2D)obj);
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ y.GetHashCode();
        }
        #endregion

        #region Clamping (Clamp, ClampAxes, Abs).
        /// <summary>Clamps all of the axes of this so they are never smaller than the corresponding component in min and never bigger than the corresponding component in max.</summary>
        public Vector2D Clamp(Vector2D min, Vector2D max) => new Vector2D(x.Clamp(min.x, max.x), y.Clamp(min.y, max.y));

        /// <summary>Clamps all of the axes of this so they are never smaller than min and never bigger than max.</summary>
        public Vector2D ClampComponents(float min, float max) => new Vector2D(x.Clamp(min, max), y.Clamp(min, max));

        /// <summary>Gets the absolute value of all the components in the vector.</summary>
        public Vector2D Abs => new Vector2D(x.Abs(), y.Abs());
        
        #endregion

        #region Conversions.
        /// <summary>
        /// Returns a new Vector3D with the x and y components of vector and defaults z to zero.
        /// </summary>
        public static implicit operator Vector3D(Vector2D vector)
        {
            return new Vector3D(vector.x, vector.y, 0);
        }

        /// <summary>
        /// Returns a new Vector4D with the x and y components of vector and defaults z and w to zero.
        /// </summary>
        /// <param name="vector"></param>
        public static implicit operator Vector4D(Vector2D vector)
        {
            return new Vector4D(vector.x, vector.y, 0, 0);
        }

        /// <summary>
        /// Returns a string formatted as "(x, y)".
        /// </summary>
        public override string ToString()
        {
            return $"({x}, {y})";
        }
        #endregion
    }
}