﻿namespace SupergodUtilities.Math
{
    /// <summary>
    /// Class with all the shared functionality between all <seealso cref="IVector{T}"/>s.
    /// </summary>
    public static class Vector
    {
        #region Static wrappers for methods implemented by the individual vector structs.
        /// <summary>
        /// Calls the Add method on a and b.
        /// </summary>
        public static T Add<T>(T a, T b)
            where T : struct, IVector<T>
        {
            return a.Add(b);
        }

        /// <summary>
        /// Calls the Subtract method on a and passes b.
        /// </summary>
        public static T Subtract<T>(T a, T b)
            where T : struct, IVector<T>
        {
            return a.Subtract(b);
        }

        /// <summary>
        /// Negates vector.
        /// </summary>
        public static T Negate<T>(T vector)
            where T : struct, IVector<T>
        {
            return vector.Negated;
        }

        /// <summary>
        /// Calls the Multiply method on vector and passes scalar.
        /// </summary>
        public static T Multiply<T>(T vector, float scalar)
            where T : struct, IVector<T>
        {
            return vector.Multiply(scalar);
        }

        /// <summary>
        /// Calls the Multiply method on a and b.
        /// </summary>
        public static T Multiply<T>(T a, T b)
            where T : struct, IVector<T>
        {
            return a.Multiply(b);
        }

        /// <summary>
        /// Calls the Divide method on a and passes b.
        /// </summary>
        public static T Divide<T>(T a, T b)
            where T : struct, IVector<T>
        {
            return a.Divide(b);
        }
        
        /// <summary>
        /// Calls the Divide method on vector and passes scalar.
        /// </summary>
        public static T Divide<T>(T vector, float scalar)
            where T : struct, IVector<T>
        {
            return vector.Divide(scalar);
        }

        /// <summary>
        /// Calls the Dot method on a and b.
        /// </summary>
        public static float Dot<T>(T a, T b)
            where T : struct, IVector<T>
        {
            return a.Dot(b);
        }

        /// <summary>
        /// Calls the Clamp method of vector and passes min as the minimum and max as the maximum.
        /// </summary>
        public static T Clamp<T>(T vector, T min, T max)
            where T : struct, IVector<T>
        {
            return vector.Clamp(min, max);
        }

        /// <summary>
        /// Calls teh ClampAxes method of vector and passes min as the minimum and max as the maximum.
        /// </summary>
        public static T ClampAxes<T>(T vector, float min, float max)
            where T : struct, IVector<T>
        {
            return vector.ClampComponents(min, max);
        }

        /// <summary>
        /// Calls the Abs method of vector.
        /// This WON'T return the magnitude of the vector, instead it will be the same vector except all
        /// of its components will have the absolute value of the old vector.
        /// </summary>
        public static T Abs<T>(T vector)
            where T : struct, IVector<T>
        {
            return vector.Abs;
        }

        /// <summary>
        /// Calls the ContainsAxis method on vector and passes axisValue.
        /// </summary>
        public static bool ContainsAxis<T>(T vector, float axisValue)
            where T : struct, IVector<T>
        {
            return vector.ContainsComponent(axisValue);
        }

        /// <summary>
        /// Gets the biggest axis on vector.
        /// </summary>
        public static float BiggestAxis<T>(T vector)
            where T : struct, IVector<T>
        {
            return vector.BiggestComponent;
        }

        /// <summary>
        /// Gets the smalles axis on vector.
        /// </summary>
        public static float SmallestAxis<T>(T vector)
            where T : struct, IVector<T>
        {
            return vector.SmallestComponent;
        }
        #endregion

        #region Implementations for shared functions.
        /// <summary>
        /// Gets the squared magnitude of vector (the dot product of vector and vector).
        /// </summary>
        public static float SqrMagnitude<T>(T vector)
            where T : struct, IVector<T>
        {
            return vector.Dot(vector);
        }

        /// <summary>
        /// Takes the magnitude of vector. If the squared magnitude is 1,
        /// it will be used, so there is no need to be scared of the square root.
        /// </summary>
        public static float Magnitude<T>(T vector)
            where T : struct, IVector<T>
        {
            float sqrMagnitude = vector.SqrMagnitude;
            if (sqrMagnitude.CloseEnough(1))
                return sqrMagnitude;

            return Math1D.Sqrt(sqrMagnitude);
        }

        /// <summary>
        /// Makes vector a unit length vector.
        /// If the magnitude of vector is 0, the default vector is returned.
        /// </summary>
        public static T Normalize<T>(T vector)
            where T : struct, IVector<T>
        {
            float magnitude = vector.Magnitude;
            if (magnitude == 0)
                return default(T);

            return vector.Divide(magnitude);
        }

        /// <summary>
        /// Projects source onto target.
        /// </summary>
        public static T ProjectOnto<T>(this T source, T target)
            where T : struct, IVector<T>
        {
            return target.Multiply(source.Dot(target) / target.SqrMagnitude);
        }

        /// <summary>
        /// Reflects source through the mirror.
        /// </summary>
        public static T Reflect<T>(this T source, T mirror)
            where T : struct, IVector<T>
        {
            return source.Subtract(source.ProjectOnto(mirror).Multiply(2));
        }
        
        /// <summary>
        /// Gets the smalles angle [0, 180 degrees] between left and right.
        /// </summary>
        /// <param name="normalizeLeft">Should left be normalized?</param>
        /// <param name="normalizeRight">Should right be normalized?</param>
        public static Angle SmallestAngle<T>(this T left, T right, bool normalizeLeft = true, bool normalizeRight = true)
            where T : struct, IVector<T>
        {
            if (normalizeLeft)
                left = left.Normalized;

            if (normalizeRight)
                right = right.Normalized;

            return Math1D.Acos(left.Dot(right));
        }

        /// <summary>
        /// Gets the biggest angle [180 degrees, 360 degrees] between left and right.
        /// </summary>
        /// <param name="normalizeLeft">Should left be normalized?</param>
        /// <param name="normalizeRight">Should right be normalized?</param>
        public static Angle BiggestAngle<T>(this T left, T right, bool normalizeLeft = true, bool normalizeRight = true)
            where T : struct, IVector<T>
        {
            return Angle.fullRotation - left.SmallestAngle(right, normalizeLeft, normalizeRight);
        }

        /// <summary>
        /// Clamps the magnitude of vector so it's never bigger than max and never bigger than min.
        /// </summary>
        public static T ClampMagnitude<T>(this T vector, float min, float max)
            where T : struct, IVector<T>
        {
            float magnitude = vector.Magnitude;
            if (magnitude < min)
                return vector.Divide(magnitude).Multiply(min);
            else if (magnitude > max)
                return vector.Divide(magnitude).Multiply(max);
            else
                return vector;
        }

        /// <summary>
        /// Clamps the magnitude of vector so it's never bigger than max.
        /// </summary>
        public static T ClampMagnitude<T>(this T vector, float max)
            where T : struct, IVector<T>
        {
            return vector.ClampMagnitude(0, max);
        }

        /// <summary>
        /// Gets a vector going in the same direction as vector with the magnitude of magnitude.
        /// </summary>
        public static T SetMagnitude<T>(this T vector, float magnitude)
            where T : struct, IVector<T>
        {
            return vector.Normalized.Multiply(magnitude);
        }

        /// <summary>
        /// Looks at target from origin.
        /// </summary>
        public static T LookAt<T>(this T origin, T target)
            where T : struct, IVector<T>
        {
            return target.Subtract(origin);
        }

        /// <summary>
        /// Points at target from origin (result is unit length).
        /// </summary>
        public static T PointAt<T>(this T origin, T target)
            where T : struct, IVector<T>
        {
            return origin.LookAt(target).Normalized;
        }

        /// <summary>
        /// Gets the squared distance between a and b.
        /// </summary>
        public static float SqrDistance<T>(this T a, T b)
            where T : struct, IVector<T>
        {
            return a.LookAt(b).SqrMagnitude;
        }

        /// <summary>
        /// Gets the distance between a and b.
        /// </summary>
        public static float Distance<T>(this T a, T b)
            where T : struct, IVector<T>
        {
            return a.SqrDistance(b).Sqrt();
        }

        /// <summary>
        /// Normalizes the result from <seealso cref="SMath.Lerp{T}(T, T, float, bool)"/>.
        /// </summary>
        public static T NLerp<T>(this T source, T target, float alpha, bool clampAlpha = true)
            where T : struct, IVector<T>
        {
            return source.Lerp(target, alpha, clampAlpha).Normalized;
        }
        #endregion
    }
}