﻿namespace SupergodUtilities.DataStructures
{
    /// <summary>
    /// Interface for things that can be destroyed.
    /// </summary>
    public interface IDestroyable
    {
        /// <summary>
        /// Implement to destroy the object.
        /// </summary>
        void Destroy();
    }
}