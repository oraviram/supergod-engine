﻿namespace SupergodUtilities.DataStructures
{
    /// <summary>
    /// Interface for things that have a name.
    /// </summary>
    public interface INamed
    {
        string Name { get; set; }
    }
}