﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace SupergodUtilities.DataStructures
{
    /// <summary>
    /// Stores a collection of subobjects.
    /// </summary>
    /// <typeparam name="TSubobject">The base type for the subobjects</typeparam>
    public class SubobjectCollection<TSubobject> : IEnumerable<TSubobject>
        where TSubobject : class, INamed, IDestroyable
    {
        private List<Tuple<TSubobject, bool>> subobjects = new List<Tuple<TSubobject, bool>>();

        public delegate TSubobject AddSubobjectFunctionDel(TSubobject requestedSubobject, Type subobjectType);
        private AddSubobjectFunctionDel addSubobjectFunction;

        public delegate TSubobject RemoveSubobjectFunctionDel(TSubobject requestedSubobject);
        private RemoveSubobjectFunctionDel removeSubobjectFunction;

        /// <summary>
        /// Creates a new subobject collection.
        /// </summary>
        /// <param name="addFunction">The function that is called whenever adding a subobject.</param>
        /// <param name="removeFunction">The function that is called whenever removing a subobject.</param>
        public SubobjectCollection(AddSubobjectFunctionDel addFunction = null, RemoveSubobjectFunctionDel removeFunction = null)
        {
            addSubobjectFunction = addFunction;
            removeSubobjectFunction = removeFunction;
        }

        #region Collection stuff.
        /// <summary>
        /// Gets a subobject at the index of index.
        /// </summary>
        /// <param name="index">The index of the subobject to get.</param>
        /// <returns>The subobject at index index.</returns>
        public TSubobject this[int index] => subobjects[index].Item1;

        /// <summary>
        /// Gets the amount of subobjects in the collection.
        /// </summary>
        public int Count => subobjects.Count;

        /// <summary>
        /// The amount of subobjects in the collection that cannot be removed.
        /// </summary>
        public int RequiredCount { get; private set; }

        /// <summary>
        /// The amount of subobjects in the collection that can be removed.
        /// </summary>
        public int OptionalCount => Count - RequiredCount;
        #endregion

        #region Adding subobjects.
        /// <summary>
        /// Creates and adds a subobject of type T to the collection.
        /// </summary>
        /// <typeparam name="T">The type of subobject to create.</typeparam>
        /// <param name="name">The name of the subobject.</param>
        /// <param name="required">Is the subobject required in this collection?</param>
        /// <returns>A reference to the subobject.</returns>
        public T Add<T>(string name, bool required = true)
            where T : TSubobject
        {
            VerifyNameForNewSubobject(name);
            Type subobjectType = typeof(T);
            TSubobject requestedSubobject = Activator.CreateInstance<T>();
            return (T)InitializeSubobject(requestedSubobject, name, subobjectType, required);
        }

        /// <summary>
        /// Creates and adds a subobject of type subobjectType to the collection.
        /// </summary>
        /// <param name="subobjectType">The type of subobject to create.</typeparam>
        /// <param name="name">The name of the subobject.</param>
        /// <param name="required">Is the subobject required in this collection?</param>
        /// <returns>A reference to the subobject.</returns>
        public TSubobject Add(Type subobjectType, string name, bool required = true)
        {
            VerifyNameForNewSubobject(name);
            if (!typeof(TSubobject).IsAssignableFrom(subobjectType))
            {
                throw new ArgumentException
                    ("Parameter subobjectType in the method SubobjectCollection<TSubobject>.Add must be a type that derives from TSubobject or TSubobject!",
                    "subobjectType");
            }
            TSubobject requestedSubobject = (TSubobject)Activator.CreateInstance(subobjectType);
            return InitializeSubobject(requestedSubobject, name, subobjectType, required);
        }

        TSubobject InitializeSubobject(TSubobject requestedSubobject, string name, Type subobjectType, bool required)
        {
            if (required)
                RequiredCount++;

            requestedSubobject.Name = name;
            TSubobject subobject = addSubobjectFunction?.Invoke(requestedSubobject, subobjectType) ?? requestedSubobject;
            subobjects.Add(Tuple.Create(subobject, required));
            return subobject;
        }

        void VerifyNameForNewSubobject(string name)
        {
            bool isInvalid = subobjects.Any((tuple) => tuple.Item1.Name == name);
            if (isInvalid)
                throw new ArgumentException("A subobject with the name '" + name + "' already exists in the subobject collection! There cannot be two subobjects with the name in a single subobject collection. The subobject will not be added to the collection.");
        }
        #endregion

        #region Getting subobjects (generic version).
        private IEnumerable<T> GetInternal<T>(Predicate<TSubobject> test, bool stopAfterFoundFirst)
            where T : class
        {
            bool foundSubobject = false;
            foreach (Tuple<TSubobject, bool> subobject in subobjects)
            {
                T subobjectAsT = subobject.Item1 as T;
                if (subobjectAsT != null && test(subobject.Item1))
                {
                    yield return subobjectAsT;
                    if (stopAfterFoundFirst)
                        break;

                    foundSubobject = true;
                }
            }
            if (!foundSubobject)
                yield return null;
        }

        /// <summary>
        /// Gets the first subobject of type T that passes test.
        /// </summary>
        /// <param name="test">Test to run for each subobject.</param>
        /// <returns>The first subobject that test returned true with that happens to be of type T.</returns>
        public T GetFirst<T>(Predicate<TSubobject> test)
            where T : class => GetInternal<T>(test, true).First();

        /// <summary>
        /// Gets the first subobject of type T.
        /// </summary>
        /// <typeparam name="T">The type of subobject to search for.</typeparam>
        /// <returns>The first subobject of type T.</returns>
        public T GetFirst<T>()
            where T : class => GetFirst<T>((subobject) => true);

        /// <summary>
        /// Gets the first subobject of type T with the name name.
        /// </summary>
        /// <typeparam name="T">The type of subobject to search for.</typeparam>
        /// <param name="name">The name of the subobject.</param>
        /// <returns>The first subobject of type T with name name.</returns>
        public T GetFirst<T>(string name)
            where T : class => GetFirst<T>((subobject) => subobject.Name == name);

        /// <summary>
        /// Gets all the subobjects of type T that pass test.
        /// </summary>
        /// <typeparam name="T">The type of subobjects to search for.</typeparam>
        /// <param name="test">The test to run on all the subobjects.</param>
        /// <returns>All of the subobjects of type T that passed test.</returns>
        public T[] GetAll<T>(Predicate<TSubobject> test)
            where T : class => GetInternal<T>(test, false).ToArray();

        /// <summary>
        /// Gets all the subobjects of type T.
        /// </summary>
        /// <typeparam name="T">The type of subobjects to search for.</typeparam>
        /// <returns>All of the subobjects of type T.</returns>
        public T[] GetAll<T>()
            where T : class => GetAll<T>((subobject) => true);
        #endregion

        #region Getting subobjects (normal version).
        private IEnumerable<TSubobject> GetInternal(Predicate<TSubobject> test, bool stopAfterFoundFirst)
        {
            bool foundSubobject = false;
            foreach (Tuple<TSubobject, bool> subobject in subobjects)
            {
                if (test(subobject.Item1))
                {
                    yield return subobject.Item1;
                    if (stopAfterFoundFirst)
                        break;

                    foundSubobject = true;
                }
            }
            if (!foundSubobject)
                yield return null;
        }

        /// <summary>
        /// Gets the first subobject that passes test.
        /// </summary>
        /// <param name="test">The test to run for each subobject.</param>
        /// <returns>The first subobject that test returned true with.</returns>
        public TSubobject GetFirst(Predicate<TSubobject> test)
            => GetInternal(test, true).First();

        /// <summary>
        /// Gets the first subobject of type type.
        /// </summary>
        /// <param name="type">The type of subobject to search for.</param>
        /// <returns>The first subobject of type type.</returns>
        public TSubobject GetFirst(Type type)
            => GetFirst((subobject) => type.IsAssignableFrom(subobject.GetType()));

        /// <summary>
        /// Gets the first subobject with the name name.
        /// </summary>
        /// <param name="name">The name of the subobject to search for.</param>
        /// <returns>The first subobject with name as its name.</returns>
        public TSubobject GetFirst(string name)
            => GetFirst((subobject) => subobject.Name == name);

        /// <summary>
        /// Gets the first subobject of type type with name name.
        /// </summary>
        /// <param name="type">The type of subobject to search for.</param>
        /// <param name="name">The name of the subobject to search for.</param>
        /// <returns>The first subobject of type type with name name.</returns>
        public TSubobject GetFirst(Type type, string name)
            => GetFirst((subobject) => subobject.Name == name && type.IsAssignableFrom(subobject.GetType()));

        /// <summary>
        /// Gets all the subobjects that pass test.
        /// </summary>
        /// <param name="test">The test to run for each subobject.</param>
        /// <returns>All of the subobjects that test returned true with.</returns>
        public TSubobject[] GetAll(Predicate<TSubobject> test)
            => GetInternal(test, false).ToArray();

        /// <summary>
        /// Gets all of the subobjects of type type.
        /// </summary>
        /// <param name="type">The type of subobjects to search for.</param>
        /// <returns>All of the subobjects of type type.</returns>
        public TSubobject[] GetAll(Type type)
            => GetAll((subobject) => type.IsAssignableFrom(subobject.GetType()));

        /// <summary>
        /// Gets all of the subobjects in the collection.
        /// </summary>
        public TSubobject[] GetAll()
            => subobjects.Select((subobject) => subobject.Item1).ToArray();
        #endregion

        #region Adding or getting subobjects.
        /// <summary>
        /// Tries to get a subobject of type T. If "searchWithName" is true, it will also search for a subobject with the name of name.<para/>
        /// If the subobject was found, it will be returned. If not, it will be added with the name of name and then returned.<para/>
        /// </summary>
        /// <typeparam name="T">The type of subobject to search for/add.</typeparam>
        /// <param name="name">The name of the added subobject, and depends on searchWithName, might also be the name of the subobject to search for.</param>
        /// <param name="searchWithName">Should name also be used when searching for the subobject?</param>
        /// <param name="required">If the subobject is being added, should be be required?</param>
        /// <returns>The found/created subobject.</returns>
        public T GetOrAdd<T>(string name, bool searchWithName, bool required = true)
            where T : class, TSubobject
        {
            T subobject =
                searchWithName
                ? GetFirst<T>(name)
                : GetFirst<T>();

            if (subobject == null || subobject.Equals(null))
                subobject = Add<T>(name, required);

            return subobject;
        }

        /// <summary>
        /// Tries to get a subobject of type type. If "searchWithName" is true, it will also search for a subobject with the name of name.<para/>
        /// If the subobject was found, it will be returned. If not, it will be added with the name of name and then returned.<para/>
        /// </summary>
        /// <param name="type">The type of subobject to search for/add.</typeparam>
        /// <param name="name">The name of the added subobject, and depends on searchWithName, might also be the name of the subobject to search for.</param>
        /// <param name="searchWithName">Should name also be used when searching for the subobject?</param>
        /// <param name="required">If the subobject is being added, should be be required?</param>
        /// <returns>The found/created subobject.</returns>
        public TSubobject GetOrAdd(Type type, string name, bool searchWithName, bool required = true)
        {
            TSubobject subobject =
                searchWithName
                ? GetFirst(type, name)
                : GetFirst(type);

            if (subobject.Equals(null))
                subobject = Add(type, name, required);

            return subobject;
        }
        #endregion

        #region Removing subobjects.
        /// <summary>
        /// Removes a subobject. Note that is must NOT be required, else an exception will be thrown. It must also be a part of the collection, else another exception will be thrown.
        /// </summary>
        /// <param name="subobject">The subobject to remove.</param>
        public void Remove(TSubobject subobject)
        {
            if ((object)subobject == null)
                return;

            Tuple<TSubobject, bool> subobjectTuple = subobjects.Find((tuple) => tuple.Item1 == subobject);
            if (subobjectTuple == null)
                throw new ArgumentException($"Subobject {subobject.Name} does not exist in the subobject collection. Cannot remove subobject!", "subobject");

            if (subobjectTuple.Item2)
                throw new ArgumentException("Cannot remove a required subobject.", "subobject");

            subobject = removeSubobjectFunction?.Invoke(subobject) ?? subobject;
            subobjects.Remove(new Tuple<TSubobject, bool>(subobject, false));

            if (!subobject.Equals(null))
                subobject.Destroy();
        }
        #endregion

        #region Clearning.
        /// <summary>
        /// Removes all of the subobject that are not required. There is no way to delete required subobjects.
        /// </summary>
        /// <returns>Is the collection empty now?</returns>
        public bool ClearOptional()
        {
            bool isEmpty = true;
            for (int i = subobjects.Count - 1; i >= 0; i--)
            {
                Tuple<TSubobject, bool> subobject = subobjects[i];
                if (subobject.Item2)
                {
                    isEmpty = false;
                    continue;
                }
                Remove(subobject.Item1);
            }
            return isEmpty;
        }
        #endregion

        #region IEnumerable interface.
        /// <summary>
        /// Gets an iterator that iterates through all the subobjects in the collection.
        /// </summary>
        public IEnumerator<TSubobject> GetEnumerator()
        {
            foreach (Tuple<TSubobject, bool> tuple in subobjects)
                yield return tuple.Item1;
        }
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        #endregion
    }
}