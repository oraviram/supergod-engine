﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class Vector2DTests : SupergodTestClass
    {
        [TestMethod]
        public void ConstructorTest()
        {
            Vector2D first = new Vector2D(1, 1);
            Assert.IsTrue(first.x == 1);
            Assert.IsTrue(first.y == 1);

            Vector2D second = new Vector2D(3, 876);
            Assert.IsTrue(second.x == 3);
            Assert.IsTrue(second.y == 876);

            Vector2D third = new Vector2D(5, -2.3f);
            Assert.IsTrue(third.x == 5);
            Assert.IsTrue(third.y == -2.3f);
        }

        [TestMethod]
        public void EqualsTest()
        {
            Assert.IsTrue(new Vector2D(10, 10) == new Vector2D(10, 10));
            Assert.IsFalse(new Vector2D(5, 10) == new Vector2D(10, 10));
            Assert.IsTrue(new Vector2D(10, 10).Equals(new Vector2D(10, 10)));
            Assert.IsFalse(new Vector2D(10, 10).Equals(new Vector2D(10, 4)));
        }

        [TestMethod]
        public void NotEqualTest()
        {
            Assert.IsFalse(new Vector2D(10, 10) != new Vector2D(10, 10));
            Assert.IsTrue(new Vector2D(5, 10) != new Vector2D(10, 10));
        }

        [TestMethod]
        public void BiggestSmallestAxisTest()
        {
            Vector2D vector = new Vector2D(5, 2);
            Assert.AreEqual(Vector.SmallestAxis(vector), 2);
            Assert.AreNotEqual(Vector.SmallestAxis(vector), 5);
            Assert.AreEqual(Vector.BiggestAxis(vector), 5);
            Assert.AreNotEqual(Vector.BiggestAxis(vector), 2);

            Assert.AreEqual(vector.SmallestComponent, 2);
            Assert.AreNotEqual(vector.SmallestComponent, 5);
            Assert.AreEqual(vector.BiggestComponent, 5);
            Assert.AreNotEqual(vector.BiggestComponent, 2);
        }

        [TestMethod]
        public void ContainsAxisTest()
        {
            Vector2D vector = new Vector2D(1, 2);
            Assert.IsTrue(vector.ContainsComponent(1));
            Assert.IsTrue(vector.ContainsComponent(2));
            Assert.IsFalse(vector.ContainsComponent(3));
        }

        [TestMethod]
        public void CloseEnoughTest()
        {
            Vector2D left = new Vector2D(4, 2);
            Vector2D right = new Vector2D(2, 4);

            Assert.IsTrue(!left.CloseEnough(right));
            Assert.IsTrue(!right.CloseEnough(left));

            Assert.IsTrue(left.CloseEnough(right, 2));
            Assert.IsTrue(right.CloseEnough(left, 2));
        }

        [TestMethod]
        public void IndexerTest()
        {
            Vector2D vec = new Vector2D();

            vec[0] = 10;
            vec[1] = 4;

            Assert.AreEqual(vec[0], 10);
            Assert.AreEqual(vec[0], vec.x);

            Assert.AreEqual(vec[1], 4);
            Assert.AreEqual(vec[1], vec.y);
        }

        [TestMethod]
        public void AsFloatArrayTest()
        {
            Vector2D vector = new Vector2D(2, 4);
            float[] floatArray = vector.AsFloatArray;

            Assert.AreEqual(floatArray[0], 2);
            Assert.AreEqual(floatArray[1], 4);
        }

        [TestMethod]
        public void MagnitudeTest()
        {
            Vector2D vec = new Vector2D(5, .6f);
            Assert.IsTrue(vec.Magnitude == Vector.Magnitude(vec) && vec.Magnitude.CloseEnough(5.03587f));
            Assert.IsTrue(vec.SqrMagnitude.CloseEnough(Vector.SqrMagnitude(vec)) && vec.SqrMagnitude.CloseEnough(25.36f));

            vec = new Vector2D(-5, .2f);
            Assert.IsTrue(vec.Magnitude == Vector.Magnitude(vec) && vec.Magnitude.CloseEnough(5.004f));
            Assert.IsTrue(vec.SqrMagnitude.CloseEnough(Vector.SqrMagnitude(vec)) && vec.SqrMagnitude.CloseEnough(25.04f));
        }

        [TestMethod]
        public void NormalizeTest()
        {
            Vector2D vec = new Vector2D(10, 15);
            Assert.IsTrue(vec.Normalized.Magnitude.CloseEnough(1) && Vector.Normalize(vec).Magnitude.CloseEnough(1));

            vec = new Vector2D(1000, -1000);
            Assert.IsTrue(vec.Normalized.Magnitude.CloseEnough(1) && Vector.Normalize(vec).Magnitude.CloseEnough(1));
        }

        [TestMethod]
        public void ProjectOntoTest()
        {
            Vector2D source = new Vector2D(2, 4);
            Vector2D target = new Vector2D(1, 0);
            Vector2D result = source.ProjectOnto(target);

            Assert.AreEqual(result, new Vector2D(2, 0));

            Vector2D[] vectors = new Vector2D[]
            {
                new Vector2D(1, 3),
                new Vector2D(4.8f, 5.6f),
                new Vector2D(0, 5),
                new Vector2D(-3.2f, 4.9f),
                new Vector2D(2.4f, -99.6f),
            };

            void TestProjection(Vector2D _source, Vector2D _target)
            {
                Vector2D bNormalized = _target.Normalized;
                Vector2D oldResult = _source.Dot(bNormalized) * bNormalized;
                Vector2D newResult = Vector.ProjectOnto(_source, _target);

                Assert.IsTrue(newResult.x.CloseEnough(oldResult.x));
                Assert.IsTrue(newResult.y.CloseEnough(oldResult.y));
            }

            for (int i = 0; i < vectors.Length - 1; i++)
            {
                TestProjection(vectors[i], vectors[i + 1]);
                TestProjection(vectors[i + 1], vectors[i]);
            }

            Vector2D a = new Vector2D(0.5f, Math1D.Sqrt(3) / 2);
            Vector2D b = new Vector2D(Math1D.Sqrt(3) / 2, 0.5f);
            float dot = a.Dot(b);
            Assert.AreEqual(dot, Math1D.Cos(Math1D.PI / 6));
        }
        
        [TestMethod]
        public void ReflectionTest()
        {
            Vector2D source = new Vector2D(-1, 2);

            Assert.AreEqual(source.Reflect(Vector2D.right), new Vector2D(1, 2));
            Assert.AreEqual(source.Reflect(Vector2D.up), new Vector2D(-1, -2));
        }

        [TestMethod]
        public void AngleTest()
        {
            Vector2D a = new Vector2D(10, 0);
            Vector2D b = new Vector2D(0, 10);

            AssertUtils.CloseEnough(a.SmallestAngle(b), Math1D.PI / 2);
            AssertUtils.CloseEnough(Vector.SmallestAngle(a, b), Math1D.PI / 2);
            AssertUtils.CloseEnough(a.BiggestAngle(b), Math1D.PI * 1.5f);
            AssertUtils.CloseEnough(Vector.BiggestAngle(a, b), Math1D.PI * 1.5f);

            b = new Vector2D(-10, 0);
            AssertUtils.CloseEnough(a.SmallestAngle(b), Math1D.PI);
            AssertUtils.CloseEnough(Vector.SmallestAngle(a, b), Math1D.PI);
            AssertUtils.CloseEnough(a.BiggestAngle(b), Math1D.PI);
            AssertUtils.CloseEnough(Vector.BiggestAngle(a, b), Math1D.PI);

            b = new Vector2D(5, 5);
            AssertUtils.CloseEnough(a.SmallestAngle(b), Math1D.PI / 4);
            AssertUtils.CloseEnough(Vector.SmallestAngle(a, b), Math1D.PI / 4);
            AssertUtils.CloseEnough(a.BiggestAngle(b), 5.4977871437821382f);
            AssertUtils.CloseEnough(Vector.BiggestAngle(a, b), 5.4977871437821382f);

            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector2D(1, 0), new Vector2D(1, 0), false, false), 0);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector2D(-1, 0), new Vector2D(1, 0), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector2D(0, 1), new Vector2D(1, 0), false, false), Math1D.PI / 2);

            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector2D(1, 0), new Vector2D(1, 0), false, false), Math1D.PI * 2);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector2D(-1, 0), new Vector2D(1, 0), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector2D(0, 1), new Vector2D(1, 0), false, false), Math1D.PI * 1.5f);
        }

        [TestMethod]
        public void DistanceTests()
        {
            Vector2D a = new Vector2D(10, 0);
            Vector2D b = new Vector2D(-10, 0);
            float dist = a.Distance(b);
            float sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, 20);
            Assert.AreEqual(sqrDist, 400);

            a = new Vector2D(0, 0);
            b = new Vector2D(-10, 0);
            dist = a.Distance(b);
            sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, 10);
            Assert.AreEqual(sqrDist, 100);

            a = new Vector2D(0, 0);
            b = new Vector2D(-10, 10);
            dist = a.Distance(b);
            sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, Math1D.Sqrt(2)*10);
            Assert.AreEqual(sqrDist, Math1D.Squared(Math1D.Sqrt(2) * 10));

            a = new Vector2D(0, 5);
            b = new Vector2D(0, -5);
            dist = a.Distance(b);
            sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, 10);
            Assert.AreEqual(sqrDist, 100);
        }

        [TestMethod]
        public void LookPointAtTests()
        {
            Vector2D a = new Vector2D(10, 0);
            Vector2D b = new Vector2D(-10, 0);
            Vector2D lookAt = a.LookAt(b);
            Vector2D pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector2D(-20, 0));
            Assert.AreEqual(pointAt, new Vector2D(-1, 0));

            a = new Vector2D(0, 0);
            b = new Vector2D(-10, 0);
            lookAt = a.LookAt(b);
            pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector2D(-10, 0));
            Assert.AreEqual(pointAt, new Vector2D(-1, 0));

            a = new Vector2D(0, 0);
            b = new Vector2D(-10, 10);
            lookAt = a.LookAt(b);
            pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector2D(-10, 10));
            Assert.AreEqual(pointAt, new Vector2D(-0.70710678f, 0.70710678f));

            a = new Vector2D(0, 5);
            b = new Vector2D(0, -5);
            lookAt = a.LookAt(b);
            pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector2D(0, -10));
            Assert.AreEqual(pointAt, new Vector2D(0, -1));
        }

        [TestMethod]
        public void ClampingTests()
        {
            Vector2D vector = new Vector2D(0, 10).Clamp(new Vector2D(-2, 15), new Vector2D(2, 20));
            Assert.AreEqual(vector, new Vector2D(0, 15));

            vector = new Vector2D(2, 2).ClampComponents(0, 1);
            Assert.AreEqual(vector, new Vector2D(1, 1));

            vector = new Vector2D(2, 2).ClampComponents(3, 4);
            Assert.AreEqual(vector, new Vector2D(3, 3));

            Vector2D lengthVector = new Vector2D(1, 1);
            Vector2D oldLengthVector = lengthVector;
            lengthVector = lengthVector.ClampMagnitude(.5f, .7f);

            Assert.AreEqual(lengthVector.Magnitude, .7f);
            Assert.AreEqual(lengthVector.Normalized, oldLengthVector.Normalized);

            lengthVector = new Vector2D(10, 10);
            oldLengthVector = lengthVector;
            lengthVector = lengthVector.ClampMagnitude(18, 20);

            Assert.AreEqual(lengthVector.Magnitude, 18);
            Assert.AreEqual(lengthVector.Normalized, oldLengthVector.Normalized);

            lengthVector = new Vector2D(5, 5);
            oldLengthVector = lengthVector;
            lengthVector = lengthVector.ClampMagnitude(-10, 10);
            Assert.AreEqual(lengthVector.Magnitude, oldLengthVector.Magnitude);
            Assert.AreEqual(lengthVector.Normalized, oldLengthVector.Normalized);

            Assert.AreEqual(new Vector2D(-1, -2).Abs, new Vector2D(1, 2));
            Assert.AreEqual(Vector.Abs(new Vector2D(1, 2)), new Vector2D(1, 2));
        }

        [TestMethod]
        public void LerpTest()
        {
            Vector2D a = new Vector2D(-10, -5);
            Vector2D b = new Vector2D(2, .5f);
            Vector2D difference = b - a;

            Test01((alpha) =>
            {
                Vector2D result1 = SMath.Lerp(a, b, alpha);
                Vector2D result1NonStatic = a.Lerp(b, alpha);
                Vector2D result2 = a + alpha * difference;

                Assert.IsTrue(result1.x.CloseEnough(result2.x));
                Assert.IsTrue(result1.y.CloseEnough(result2.y));

                Assert.IsTrue(result1NonStatic.x.CloseEnough(result2.x));
                Assert.IsTrue(result1NonStatic.y.CloseEnough(result2.y));
            });
        }

        [TestMethod]
        public void AdditionTest()
        {
            Vector2D left = new Vector2D(1, 1);
            Vector2D right = new Vector2D(3, 5);
            Vector2D expectedResult = new Vector2D(4, 6);

            Assert.IsTrue(left + right == expectedResult);
            Assert.IsTrue(left.Add(right) == expectedResult);
            Assert.IsTrue(Vector.Add(left, right) == expectedResult);

            left = new Vector2D(3, 876);
            right = new Vector2D(-1, -500);
            expectedResult = new Vector2D(2, 376);

            Assert.IsTrue(left + right == expectedResult);
            Assert.IsTrue(left.Add(right) == expectedResult);
            Assert.IsTrue(Vector.Add(left, right) == expectedResult);

            left = new Vector2D(5, -2.3f);
            right = new Vector2D(5, 0.3f);
            expectedResult = new Vector2D(10, -2);

            Assert.IsTrue(left + right == expectedResult);
            Assert.IsTrue(left.Add(right) == expectedResult);
            Assert.IsTrue(Vector.Add(left, right) == expectedResult);
        }

        [TestMethod]
        public void SubtractionTest()
        {
            Vector2D left = new Vector2D(1, 1);
            Vector2D right = new Vector2D(3, 5);
            Vector2D expectedResult = new Vector2D(-2, -4);

            Assert.IsTrue(left - right == expectedResult);
            Assert.IsTrue(left.Subtract(right) == expectedResult);
            Assert.IsTrue(Vector.Subtract(left, right) == expectedResult);

            left = new Vector2D(3, 876);
            right = new Vector2D(-1, -500);
            expectedResult = new Vector2D(4, 1376);

            Assert.IsTrue(left - right == expectedResult);
            Assert.IsTrue(left.Subtract(right) == expectedResult);
            Assert.IsTrue(Vector.Subtract(left, right) == expectedResult);

            left = new Vector2D(5, -2.3f);
            right = new Vector2D(5, 0.3f);
            expectedResult = new Vector2D(0, -2.6f);

            Assert.IsTrue(left - right == expectedResult);
            Assert.IsTrue(left.Subtract(right) == expectedResult);
            Assert.IsTrue(Vector.Subtract(left, right) == expectedResult);
        }

        [TestMethod]
        public void NegatingTest()
        {
            Vector2D vector = new Vector2D(1, 1);
            Vector2D expectedResult = new Vector2D(-1, -1);

            Assert.IsTrue(-vector == expectedResult);
            Assert.IsTrue(vector.Negated == expectedResult);
            Assert.IsTrue(Vector.Negate(vector) == expectedResult);

            vector = new Vector2D(2, 5);
            expectedResult = new Vector2D(-2, -5);

            Assert.IsTrue(-vector == expectedResult);
            Assert.IsTrue(vector.Negated == expectedResult);
            Assert.IsTrue(Vector.Negate(vector) == expectedResult);

            vector = new Vector2D(-2, 6.5f);
            expectedResult = new Vector2D(2, -6.5f);

            Assert.IsTrue(-vector == expectedResult);
            Assert.IsTrue(vector.Negated == expectedResult);
            Assert.IsTrue(Vector.Negate(vector) == expectedResult);
        }

        [TestMethod]
        public void ScalarMultiplicationTest()
        {
            Vector2D vector = new Vector2D(1, 1);
            float scalar = 5;
            Vector2D expectedResult = new Vector2D(5, 5);

            Assert.IsTrue(vector * scalar == expectedResult);
            Assert.IsTrue(scalar * vector == expectedResult);
            Assert.IsTrue(Vector.Multiply(vector, scalar) == expectedResult);
            Assert.IsTrue(vector.Multiply(scalar) == expectedResult);

            vector = new Vector2D(3, 876);
            scalar = 2;
            expectedResult = new Vector2D(6, 1752);

            Assert.IsTrue(vector * scalar == expectedResult);
            Assert.IsTrue(scalar * vector == expectedResult);
            Assert.IsTrue(Vector.Multiply(vector, scalar) == expectedResult);
            Assert.IsTrue(vector.Multiply(scalar) == expectedResult);

            vector = new Vector2D(5, .3f);
            scalar = -1.5f;
            expectedResult = new Vector2D(-7.5f, -.45f);

            Assert.IsTrue((vector * scalar).CloseEnough(expectedResult));
            Assert.IsTrue((scalar * vector).CloseEnough(expectedResult));
            Assert.IsTrue(Vector.Multiply(vector, scalar).CloseEnough(expectedResult));
            Assert.IsTrue(vector.Multiply(scalar).CloseEnough(expectedResult));
        }

        [TestMethod]
        public void DivisionTest()
        {
            Vector2D vector = new Vector2D(5, 2);
            float scalar = 2;
            Vector2D expectedResult = new Vector2D(2.5f, 1);

            Assert.AreEqual(vector / scalar, expectedResult);
            Assert.AreEqual(vector.Divide(scalar), expectedResult);
            Assert.AreEqual(Vector.Divide(vector, scalar), expectedResult);

            vector = new Vector2D(.5f, -5);
            scalar = .5f;
            expectedResult = new Vector2D(1, -10);

            Assert.AreEqual(vector / scalar, expectedResult);
            Assert.AreEqual(vector.Divide(scalar), expectedResult);
            Assert.AreEqual(Vector.Divide(vector, scalar), expectedResult);

            vector = new Vector2D(5, 2);
            Vector2D vector2 = new Vector2D(2, -2);
            expectedResult = new Vector2D(2.5f, -1);

            Assert.AreEqual(vector / vector2, expectedResult);
            Assert.AreEqual(vector.Divide(vector2), expectedResult);
            Assert.AreEqual(Vector.Divide(vector, vector2), expectedResult);

            vector = new Vector2D(1, -5);
            vector2 = new Vector2D(5, 2);
            expectedResult = new Vector2D(.2f, -2.5f);

            Assert.AreEqual(vector / vector2, expectedResult);
            Assert.AreEqual(vector.Divide(vector2), expectedResult);
            Assert.AreEqual(Vector.Divide(vector, vector2), expectedResult);
        }

        [TestMethod]
        public void MultiplicationTest()
        {
            Vector2D left = new Vector2D(1, 1);
            Vector2D right = new Vector2D(2, 0);
            Vector2D expectedResult = new Vector2D(2, 0);

            Assert.IsTrue(left * right == expectedResult);
            Assert.IsTrue(left.Multiply(right) == expectedResult);
            Assert.IsTrue(Vector.Multiply(left, right) == expectedResult);

            left = new Vector2D(3, 10);
            right = new Vector2D(-1, 1.5f);
            expectedResult = new Vector2D(-3, 15);

            Assert.IsTrue(left * right == expectedResult);
            Assert.IsTrue(left.Multiply(right) == expectedResult);
            Assert.IsTrue(Vector.Multiply(left, right) == expectedResult);

            left = new Vector2D(3, 5);
            right = new Vector2D(5, -.5f);
            expectedResult = new Vector2D(15, -2.5f);

            Assert.IsTrue(left * right == expectedResult);
            Assert.IsTrue(left.Multiply(right) == expectedResult);
            Assert.IsTrue(Vector.Multiply(left, right) == expectedResult);
        }

        [TestMethod]
        public void DotTest()
        {
            Vector2D firstLeft = new Vector2D(1, 5);
            Vector2D firstRight = new Vector2D(2, .5f);
            float first = Vector.Dot(firstLeft, firstRight);
            Assert.IsTrue(first == 4.5f);

            Vector2D secondLeft = new Vector2D(-2, -4f);
            Vector2D secondRight = new Vector2D(5, 5);
            float second = Vector.Dot(secondLeft, secondRight);
            Assert.IsTrue(second == -30);

            Vector2D thirdLeft = new Vector2D(-2, -4f);
            Vector2D thirdRight = new Vector2D(5, 5);
            float third = thirdLeft.Dot(thirdRight);
            Assert.IsTrue(third == -30);
        }

        [TestMethod]
        public void ConversionTests()
        {
            Vector3D threeD = new Vector2D(1, 2);
            Assert.IsTrue(threeD == new Vector3D(1, 2, 0));

            Vector4D fourD = new Vector2D(-1, 2.2f);
            Assert.IsTrue(fourD == new Vector4D(-1, 2.2f, 0, 0));
        }
    }
}