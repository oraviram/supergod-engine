﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class Matrix3x3Tests : SupergodTestClass
    {
        [TestMethod]
        public void ConstructorTest()
        {
            Matrix3x3 test = new Matrix3x3(1, 2, 3, 4, 5, 6, 7, 8, 9);
            Assert.AreEqual(test.r0c0, 1);
            Assert.AreEqual(test.r0c1, 2);
            Assert.AreEqual(test.r0c2, 3);

            Assert.AreEqual(test.r1c0, 4);
            Assert.AreEqual(test.r1c1, 5);
            Assert.AreEqual(test.r1c2, 6);

            Assert.AreEqual(test.r2c0, 7);
            Assert.AreEqual(test.r2c1, 8);
            Assert.AreEqual(test.r2c2, 9);
        }

        #region Comparison methods tests.
        [TestMethod]
        public void EqualsTest()
        {
            Assert.IsTrue(new Matrix3x3(10, 10, 5) == new Matrix3x3(10, 10, 5));
            Assert.IsFalse(new Matrix3x3(10, 10, 2) == new Matrix3x3(10, 10, 1));
            Assert.IsTrue(new Matrix3x3(10, 10, 5).Equals(new Matrix3x3(10, 10, 5)));
            Assert.IsFalse(new Matrix3x3(10, 10, 1).Equals(new Matrix3x3(10, 10, 2)));
        }

        [TestMethod]
        public void NotEqualTest()
        {
            Assert.IsFalse(new Matrix3x3(10, 10, 1) != new Matrix3x3(10, 10, 1));
            Assert.IsTrue(new Matrix3x3(10, 10, 1) != new Matrix3x3(10, 10, 2));
        }
        #endregion

        #region Addition subtraction tests.
        [TestMethod]
        public void AdditionTest()
        {
            Test50((i) =>
            {
                Matrix3x3 a = new Matrix3x3(
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100());

                Matrix3x3 b = new Matrix3x3(
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100());

                Matrix3x3 result = a + b;

                Assert.AreEqual(result, b + a);
                Assert.AreEqual(result, a.Add(b));
                Assert.AreEqual(result, b.Add(a));

                AssertUtils.CloseEnough(result.r0c0, a.r0c0 + b.r0c0);
                AssertUtils.CloseEnough(result.r0c1, a.r0c1 + b.r0c1);
                AssertUtils.CloseEnough(result.r0c2, a.r0c2 + b.r0c2);

                AssertUtils.CloseEnough(result.r1c0, a.r1c0 + b.r1c0);
                AssertUtils.CloseEnough(result.r1c1, a.r1c1 + b.r1c1);
                AssertUtils.CloseEnough(result.r1c2, a.r1c2 + b.r1c2);

                AssertUtils.CloseEnough(result.r2c0, a.r2c0 + b.r2c0);
                AssertUtils.CloseEnough(result.r2c1, a.r2c1 + b.r2c1);
                AssertUtils.CloseEnough(result.r2c2, a.r2c2 + b.r2c2);
            });
        }

        [TestMethod]
        public void SubtractionTest()
        {
            Test50((i) =>
            {
                Matrix3x3 a = new Matrix3x3(
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100());

                Matrix3x3 b = new Matrix3x3(
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100());

                Matrix3x3 result = a - b;

                Assert.AreEqual(result, a.Subtract(b));
                Assert.AreNotEqual(result, b - a);
                Assert.AreEqual(b - a, b.Subtract(a));
                Assert.AreNotEqual(result, b.Subtract(a));

                AssertUtils.CloseEnough(result.r0c0, a.r0c0 - b.r0c0);
                AssertUtils.CloseEnough(result.r0c1, a.r0c1 - b.r0c1);
                AssertUtils.CloseEnough(result.r0c2, a.r0c2 - b.r0c2);

                AssertUtils.CloseEnough(result.r1c0, a.r1c0 - b.r1c0);
                AssertUtils.CloseEnough(result.r1c1, a.r1c1 - b.r1c1);
                AssertUtils.CloseEnough(result.r1c2, a.r1c2 - b.r1c2);

                AssertUtils.CloseEnough(result.r2c0, a.r2c0 - b.r2c0);
                AssertUtils.CloseEnough(result.r2c1, a.r2c1 - b.r2c1);
                AssertUtils.CloseEnough(result.r2c2, a.r2c2 - b.r2c2);
            });
        }
        #endregion

        #region Multiplication tests.
        [TestMethod]
        public void Matrix3x3FloatMultiplicationTest()
        {
            Test50((i) =>
            {
                float scalar = RandFloat100();
                Matrix3x3 matrix = new Matrix3x3(
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100());

                Matrix3x3 result = matrix * scalar;

                Assert.AreEqual(result, scalar * matrix);
                Assert.AreEqual(result, matrix.Multiply(scalar));

                AssertUtils.CloseEnough(result.r0c0, matrix.r0c0 * scalar);
                AssertUtils.CloseEnough(result.r0c1, matrix.r0c1 * scalar);
                AssertUtils.CloseEnough(result.r0c2, matrix.r0c2 * scalar);

                AssertUtils.CloseEnough(result.r1c0, matrix.r1c0 * scalar);
                AssertUtils.CloseEnough(result.r1c1, matrix.r1c1 * scalar);
                AssertUtils.CloseEnough(result.r1c2, matrix.r1c2 * scalar);

                AssertUtils.CloseEnough(result.r2c0, matrix.r2c0 * scalar);
                AssertUtils.CloseEnough(result.r2c1, matrix.r2c1 * scalar);
                AssertUtils.CloseEnough(result.r2c2, matrix.r2c2 * scalar);
            });
        }

        [TestMethod]
        public void Matrix3x3VectorMultiplicationTest()
        {
            Vector3D vector = new Vector3D(1, 2, 3);
            Matrix3x3 matrix = new Matrix3x3(1, 2, 3, 4, 5, 6, 7, 8, 9);
            Assert.AreEqual(vector * matrix, new Vector3D(30, 36, 42));
            Assert.AreEqual(matrix * vector, new Vector3D(14, 32, 50));

            vector = new Vector3D(4, -1, .5f);
            matrix = new Matrix3x3(4, .1f, 1, 8, 4, 6, 8, 7, -9);
            AssertUtils.CloseEnough(vector * matrix, new Vector3D(12, -.1f, -6.5f));
            Assert.AreEqual(matrix * vector, new Vector3D(16.4f, 31, 20.5f));
        }

        [TestMethod]
        public void Matrix3x3TimesMatrix3x3Test()
        {
            Matrix3x3 first = new Matrix3x3(
                1, 2, 3,
                4, 5, 6,
                7, 8, 9
                );
            Matrix3x3 second = new Matrix3x3(
                5, 2, 5,
                1, 2, 6,
                10, 3, 0
                );

            Assert.AreEqual(first * first, new Matrix3x3(30, 36, 42, 66, 81, 96, 102, 126, 150));
            Assert.AreEqual(second * second, new Matrix3x3(77, 29, 37, 67, 24, 17, 53, 26, 68));
            Assert.AreEqual(first * second, new Matrix3x3(37, 15, 17, 85, 36, 50, 133, 57, 83));
        }
        #endregion

        #region Transformation tests.
        [TestMethod]
        public void ScaleTest()
        {
            Matrix3x3 scalar = Matrix3x3.Scale(new Vector2D(1, 4));
            Vector3D original = new Vector3D(5, 2, 0);
            Vector3D scaled = scalar * original;
            Assert.AreEqual(scaled, new Vector3D(5, 8));

            scalar = Matrix3x3.Scale(new Vector2D(5f, 5f));
            original = new Vector3D(.5f, -1, 0);
            scaled = scalar * original;
            Assert.AreEqual(scaled, new Vector3D(2.5f, -5));
        }

        [TestMethod]
        public void TranslationTest()
        {
            Vector3D test = Matrix3x3.Translate(new Vector2D(4, 8)) * new Vector3D(-3, 8, 1);
            Assert.AreEqual(test, new Vector3D(1, 16, 1));
        }

        [TestMethod]
        public void RotateXYZTests()
        {
            TestAngle((a) =>
            {
                float cos = Math1D.Cos(a);
                float sin = Math1D.Sin(a);

                Matrix3x3 rotation = Matrix3x3.RotateX(a);
                AssertUtils.CloseEnough(rotation, new Matrix3x3(
                    1, 0, 0,
                    0, cos, -sin,
                    0, sin, cos
                    ));

                rotation = Matrix3x3.RotateY(a);
                AssertUtils.CloseEnough(rotation, new Matrix3x3(
                    cos, 0, sin,
                    0, 1, 0,
                    -sin, 0, cos
                    ));

                rotation = Matrix3x3.RotateZ(a);
                AssertUtils.CloseEnough(rotation, new Matrix3x3(
                    cos, -sin, 0,
                    sin, cos, 0,
                    0, 0, 1 
                    ));
            });
        }

        [TestMethod]
        public void FromEulerAnglesTest()
        {
            Test50((i) =>
            {
                EulerAngles angles = new EulerAngles(RandAngle(), RandAngle(), RandAngle());
                Matrix3x3 matrix = Matrix3x3.FromEulerAngles(angles);
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(angles.RotateVector(vector), matrix * vector);
            });
        }

        [TestMethod]
        public void FromQuaternionTest()
        {
            Test50((i) =>
            {
                Quaternion quaternion = new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100()).Normalized;
                Matrix3x3 matrix = Matrix3x3.FromRotationQuaternion(quaternion);
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(quaternion.RotateVector(vector), matrix * vector, .001f);
            });
        }

        [TestMethod]
        public void FromAxisAngle()
        {
            Test50((i) =>
            {
                AxisAngle axisAngle = new AxisAngle(new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()).Normalized, RandAngle());
                Matrix3x3 matrix = Matrix3x3.FromAxisAngle(axisAngle);
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(axisAngle.RotateVector(vector), matrix * vector, .001f);
            });
        }
        #endregion

        #region Inverse, transpose, cofactor, adjugate, inverse, negated, trace and abs tests.
        [TestMethod]
        public void TransposeTest()
        {
            Test50((i) =>
            {
                Matrix3x3 matrix = new Matrix3x3(
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100());

                Matrix3x3 transpose = matrix.Transposed;

                Assert.AreEqual(transpose.Transposed, matrix);
                Assert.AreEqual(transpose, Matrix.Transpose(matrix));

                for (int r = 0; r < 3; r++)
                {
                    for (int c = 0; c < 3; c++)
                    {
                        Assert.AreEqual(transpose[r, c], matrix[c, r]);
                        Assert.AreEqual(transpose[c, r], matrix[r, c]);
                    }
                }
            });
        }

        [TestMethod]
        public void InverseCofactorAdjugateDeterminantTest()
        {
            // This doesn't directly test the adjugate, cofactor and determinant,
            // but it does directly test the inverse,
            // which uses the adjugate, cofactor and determinant.
            Test50((i) =>
            {
                Matrix3x3 matrix = new Matrix3x3(
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100());

                Assert.AreEqual(matrix.Determinant, Matrix.Det(matrix));
                Assert.AreEqual(matrix.Cofactor, Matrix.Cofactor(matrix));
                Assert.AreEqual(matrix.Adjugate, Matrix.Adjugate(matrix));

                if (matrix.Determinant != 0)
                {
                    Matrix3x3 inverse = matrix.Inverted;
                    Assert.AreEqual(inverse, Matrix.Invert(matrix));

                    AssertUtils.CloseEnough(inverse.Inverted, matrix, .01f);
                    AssertUtils.CloseEnough(inverse * matrix, Matrix3x3.identity);
                }
            });
        }

        [TestMethod]
        public void NegationTest()
        {
            Test50((i) =>
            {
                Matrix3x3 matrix = new Matrix3x3(
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100());

                Matrix3x3 negated = -matrix;

                Assert.AreEqual(negated, matrix.Negated);
                Assert.AreEqual(negated, Matrix.Negate(matrix));
                Assert.AreEqual(negated, matrix * -1);

                for (int r = 0; r < 3; r++)
                {
                    for (int c = 0; c < 3; c++)
                    {
                        Assert.AreEqual(negated[r, c], -matrix[r, c]);
                    }
                }
            });
        }

        [TestMethod]
        public void TraceTest()
        {
            Test50((a) =>
            {
                Matrix3x3 matrix = new Matrix3x3(
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100());

                float trace = matrix.Trace;
                Assert.AreEqual(trace, Matrix.Tr(matrix));

                float sum = 0;
                for (int i = 0; i < 3; i++)
                {
                    sum += matrix[i, i];
                }
                AssertUtils.CloseEnough(trace, sum);
                AssertUtils.CloseEnough(trace, matrix.r0c0 + matrix.r1c1 + matrix.r2c2);
            });
        }

        [TestMethod]
        public void AbsTest()
        {
            Test50((i) =>
            {
                Matrix3x3 matrix = new Matrix3x3(
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100());

                Matrix3x3 abs = matrix.Abs;
                Assert.AreEqual(abs, Matrix.Abs(matrix));

                for (int r = 0; r < 3; r++)
                {
                    for (int c = 0; c < 3; c++)
                    {
                        float element = matrix[r, c];
                        float absElement = abs[r, c];

                        Assert.AreEqual(absElement.Sign(), 1);
                        if (element.Sign() == -1)
                            Assert.AreEqual(absElement, -element);
                        else
                            Assert.AreEqual(abs[r, c], matrix[r, c]);

                        Assert.AreEqual(abs[r, c], matrix[r, c].Abs());
                    }
                }
            });
        }
        #endregion

        [TestMethod]
        public void ConversionTests()
        {
            Test50((i) =>
            {
                Matrix3x3 matrix = new Matrix3x3(
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100());

                Matrix2x2 twoBy2 = (Matrix2x2)matrix;
                Matrix4x4 fourBy4 = matrix;

                Assert.AreEqual(fourBy4.r3c3, 1);
                for (int r = 0; r < 2; r++)
                {
                    for (int c = 0; c < 2; c++)
                    {
                        Assert.AreEqual(twoBy2[r, c], matrix[r, c]);
                    }
                }

                for (int r = 0; r < 3; r++)
                {
                    for (int c = 0; c < 3; c++)
                    {
                        Assert.AreEqual(fourBy4[r, c], matrix[r, c]);
                    }
                }
            });
        }
    }
}