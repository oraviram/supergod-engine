﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class Vector3DTests : SupergodTestClass
    {
        [TestMethod]
        public void ConstructorTest()
        {
            Vector3D first = new Vector3D(1, 1, 2);
            Assert.IsTrue(first.x == 1);
            Assert.IsTrue(first.y == 1);
            Assert.IsTrue(first.z == 2);

            Vector3D second = new Vector3D(3, new Vector2D(876, .5f));
            Assert.IsTrue(second.x == 3);
            Assert.IsTrue(second.y == 876);
            Assert.IsTrue(second.z == .5f);

            Vector3D third = new Vector3D(new Vector2D(5, -2.3f), -3);
            Assert.IsTrue(third.x == 5);
            Assert.IsTrue(third.y == -2.3f);
            Assert.IsTrue(third.z == -3);
        }

        [TestMethod]
        public void AxisCombinationTests()
        {
            Vector3D vector = new Vector3D(2, 4, 6);

            Assert.IsTrue(vector.XZ == new Vector2D(2, 6));
            Assert.IsTrue(vector.ZX == new Vector2D(6, 2));

            Assert.IsTrue(vector.XY == new Vector2D(2, 4));
            Assert.IsTrue(vector.YX == new Vector2D(4, 2));

            Assert.IsTrue(vector.YZ == new Vector2D(4, 6));
            Assert.IsTrue(vector.ZY == new Vector2D(6, 4));
        }

        [TestMethod]
        public void EqualsTest()
        {
            Assert.IsTrue(new Vector3D(10, 10, 5) == new Vector3D(10, 10, 5));
            Assert.IsFalse(new Vector3D(10, 10, 2) == new Vector3D(10, 10, 1));
            Assert.IsTrue(new Vector3D(10, 10, 5).Equals(new Vector3D(10, 10, 5)));
            Assert.IsFalse(new Vector3D(10, 10, 1).Equals(new Vector3D(10, 10, 2)));
        }

        [TestMethod]
        public void NotEqualTest()
        {
            Assert.IsFalse(new Vector3D(10, 10, 1) != new Vector3D(10, 10, 1));
            Assert.IsTrue(new Vector3D(10, 10, 1) != new Vector3D(10, 10, 2));
        }

        [TestMethod]
        public void BiggestSmallestAxisTest()
        {
            Vector3D vector = new Vector3D(5, 2, 3);
            Assert.AreEqual(Vector.SmallestAxis(vector), 2);
            Assert.AreNotEqual(Vector.SmallestAxis(vector), 5);
            Assert.AreNotEqual(Vector.SmallestAxis(vector), 3);
            Assert.AreEqual(Vector.BiggestAxis(vector), 5);
            Assert.AreNotEqual(Vector.BiggestAxis(vector), 2);
            Assert.AreNotEqual(Vector.BiggestAxis(vector), 3);

            Assert.AreEqual(vector.SmallestComponent, 2);
            Assert.AreNotEqual(vector.SmallestComponent, 5);
            Assert.AreNotEqual(vector.SmallestComponent, 3);
            Assert.AreEqual(vector.BiggestComponent, 5);
            Assert.AreNotEqual(vector.BiggestComponent, 2);
            Assert.AreNotEqual(vector.BiggestComponent, 3);
        }

        [TestMethod]
        public void ContainsAxisTest()
        {
            Vector3D vector = new Vector3D(1, 2, 10);
            Assert.IsTrue(vector.ContainsComponent(10));
            Assert.IsTrue(vector.ContainsComponent(2));
            Assert.IsTrue(vector.ContainsComponent(1));
            Assert.IsFalse(vector.ContainsComponent(5));

            vector = new Vector3D(1, 2, 10);
            Assert.IsTrue(Vector.ContainsAxis(vector, 10));
            Assert.IsTrue(Vector.ContainsAxis(vector, 2));
            Assert.IsTrue(Vector.ContainsAxis(vector, 1));
            Assert.IsFalse(Vector.ContainsAxis(vector, 5));
        }

        [TestMethod]
        public void CloseEnoughTest()
        {
            Assert.IsTrue(new Vector3D(1, 1, 1).CloseEnough(new Vector3D(1.000001f, 1.000001f, 1.000001f)));
            Assert.IsTrue(new Vector3D(1, 1, 1).CloseEnough(new Vector3D(2, 2, 2), 1));
        }

        [TestMethod]
        public void IndexerTest()
        {
            Vector3D vec = new Vector3D();

            vec[0] = 10;
            vec[1] = 4;
            vec[2] = -4;

            Assert.AreEqual(vec[0], 10);
            Assert.AreEqual(vec[0], vec.x);

            Assert.AreEqual(vec[1], 4);
            Assert.AreEqual(vec[1], vec.y);

            Assert.AreEqual(vec[2], -4);
            Assert.AreEqual(vec[2], vec.z);
        }

        [TestMethod]
        public void AsFloatArrayTest()
        {
            Vector3D vector = new Vector3D(2, 3, 4);
            float[] asFloatArray = vector.AsFloatArray;

            Assert.AreEqual(asFloatArray[0], 2);
            Assert.AreEqual(asFloatArray[1], 3);
            Assert.AreEqual(asFloatArray[2], 4);
        }

        [TestMethod]
        public void MagnitudeTest()
        {
            Vector3D vec = new Vector3D(5, .6f, 2);
            Assert.IsTrue(vec.Magnitude == Vector.Magnitude(vec) && vec.Magnitude.CloseEnough(5.41849f));
            Assert.IsTrue(vec.SqrMagnitude.CloseEnough(Vector.SqrMagnitude(vec)) && vec.SqrMagnitude.CloseEnough(29.3600338801f));

            vec = new Vector3D(-5, .2f, 5.2f);
            Assert.IsTrue(vec.Magnitude == Vector.Magnitude(vec) && vec.Magnitude.CloseEnough(7.21665f));
            Assert.IsTrue(vec.SqrMagnitude.CloseEnough(Vector.SqrMagnitude(vec)) && vec.SqrMagnitude.CloseEnough(52.0800372226f));
        }

        [TestMethod]
        public void NormalizeTest()
        {
            Vector3D vec = new Vector3D(10, 15);
            Assert.IsTrue(vec.Normalized.Magnitude.CloseEnough(1) && Vector.Normalize(vec).Magnitude.CloseEnough(1));

            vec = new Vector3D(1000, -1000);
            Assert.IsTrue(vec.Normalized.Magnitude.CloseEnough(1) && Vector.Normalize(vec).Magnitude.CloseEnough(1));
        }

        [TestMethod]
        public void ProjectOntoTest()
        {
            Vector3D source = new Vector3D(1, 3, 2);
            Vector3D target = new Vector3D(2, 0, 0);
            Vector3D result = source.ProjectOnto(target);

            Assert.AreEqual(result, new Vector3D(1, 0, 0));

            Vector3D[] vectors = new Vector3D[]
            {
                new Vector3D(1, 3, 4),
                new Vector3D(4.8f, 5.6f, 2),
                new Vector3D(0, 5, -6222),
                new Vector3D(-3.2f, 543),
                new Vector3D(2.4f, -2.4f),
            };

            void TestProjection(Vector3D _source, Vector3D _target)
            {
                Vector3D bNormalized = _target.Normalized;
                Vector3D oldResult = _source.Dot(bNormalized) * bNormalized;
                Vector3D newResult = Vector.ProjectOnto(_source, _target);

                Assert.IsTrue(newResult.x.CloseEnough(oldResult.x));
                Assert.IsTrue(newResult.y.CloseEnough(oldResult.y));
                Assert.IsTrue(newResult.z.CloseEnough(oldResult.z));
            }

            for (int i = 0; i < vectors.Length - 1; i++)
            {
                TestProjection(vectors[i], vectors[i + 1]);
                TestProjection(vectors[i + 1], vectors[i]);
            }
            Vector3D a = new Vector3D(0.5f, Math1D.Sqrt(3) / 2);
            Vector3D b = new Vector3D(Math1D.Sqrt(3) / 2, 0.5f);
            float dot = a.Dot(b);
            Assert.AreEqual(dot, Math1D.Cos(Math1D.PI / 6));
        }

        [TestMethod]
        public void ReflectionTest()
        {
            Vector3D source = new Vector3D(-1, 2, 3);

            Assert.AreEqual(source.Reflect(Vector3D.right), new Vector3D(1, 2, 3));
            Assert.AreEqual(source.Reflect(Vector3D.up), new Vector3D(-1, -2, 3));
            Assert.AreEqual(source.Reflect(Vector3D.forward), new Vector3D(-1, 2, -3));
        }

        [TestMethod]
        public void AngleTest()
        {
            Vector3D a = new Vector3D(0, 0, 10);
            Vector3D b = new Vector3D(0, 10);

            AssertUtils.CloseEnough(a.SmallestAngle(b), Math1D.PI / 2);
            AssertUtils.CloseEnough(Vector.SmallestAngle(a, b), Math1D.PI / 2);
            AssertUtils.CloseEnough(a.BiggestAngle(b), Math1D.PI * 1.5f);
            AssertUtils.CloseEnough(Vector.BiggestAngle(a, b), Math1D.PI * 1.5f);

            b = new Vector3D(0, 0, -10);
            AssertUtils.CloseEnough(a.SmallestAngle(b), Math1D.PI);
            AssertUtils.CloseEnough(Vector.SmallestAngle(a, b), Math1D.PI);
            AssertUtils.CloseEnough(a.BiggestAngle(b), Math1D.PI);
            AssertUtils.CloseEnough(Vector.BiggestAngle(a, b), Math1D.PI);

            b = new Vector3D(5, 0, 5);
            AssertUtils.CloseEnough(a.SmallestAngle(b), Math1D.PI / 4);
            AssertUtils.CloseEnough(Vector.SmallestAngle(a, b), Math1D.PI / 4);
            AssertUtils.CloseEnough(a.BiggestAngle(b), 5.4977871437821382f);
            AssertUtils.CloseEnough(Vector.BiggestAngle(a, b), 5.4977871437821382f);

            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector3D(1, 0, 0), new Vector3D(1, 0, 0), false, false), 0);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector3D(0, 0, 1), new Vector3D(0, 0, 1), false, false), 0);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector3D(-1, 0, 0), new Vector3D(1, 0, 0), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector3D(0, 1, -1), new Vector3D(1, 0, 1), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector3D(0, 1, 0), new Vector3D(1, 0, 0), false, false), Math1D.PI / 2);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector3D(0, 1, 0), new Vector3D(0, 0, 1), false, false), Math1D.PI / 2);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector3D(1, 0, 0), new Vector3D(0, 0, -1), false, false), Math1D.PI / 2);

            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector3D(1, 0, 0), new Vector3D(1, 0, 0), false, false), Math1D.PI * 2);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector3D(0, 0, 1), new Vector3D(0, 0, 1), false, false), Math1D.PI * 2);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector3D(-1, 0, 0), new Vector3D(1, 0, 0), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector3D(0, 1, -1), new Vector3D(1, 0, 1), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector3D(0, 1, 0), new Vector3D(1, 0, 0), false, false), Math1D.PI * 1.5f);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector3D(0, 1, 0), new Vector3D(0, 0, 1), false, false), Math1D.PI * 1.5f);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector3D(1, 0, 0), new Vector3D(0, 0, -1), false, false), Math1D.PI * 1.5f);
        }

        [TestMethod]
        public void DistanceTests()
        {
            Vector3D a = new Vector3D(10, 0, 0);
            Vector3D b = new Vector3D(-10, 0, 0);
            float dist = a.Distance(b);
            float sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, 20);
            Assert.AreEqual(sqrDist, 400);

            a = new Vector3D(0, 0, 0);
            b = new Vector3D(-10, 0, 0);
            dist = a.Distance(b);
            sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, 10);
            Assert.AreEqual(sqrDist, 100);

            a = new Vector3D(0, 0, 0);
            b = new Vector3D(-10, 10, -5);
            dist = a.Distance(b);
            sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, 15);
            Assert.AreEqual(sqrDist, 225);

            a = new Vector3D(0, 5, 0);
            b = new Vector3D(0, -5, 0);
            dist = a.Distance(b);
            sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, 10);
            Assert.AreEqual(sqrDist, 100);

            a = new Vector3D(0, 5, -5);
            b = new Vector3D(0, -5, 5);
            dist = a.Distance(b);
            sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, Math1D.Sqrt(2) * 10);
            Assert.AreEqual(sqrDist, Math1D.Squared(Math1D.Sqrt(2) * 10));
        }

        [TestMethod]
        public void LookPointAtTests()
        {
            Vector3D a = new Vector3D(10, 0, 0);
            Vector3D b = new Vector3D(-10, 0, 0);
            Vector3D lookAt = a.LookAt(b);
            Vector3D pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector3D(-20, 0, 0));
            Assert.AreEqual(pointAt, new Vector3D(-1, 0, 0));

            a = new Vector3D(0, 0, 0);
            b = new Vector3D(-10, 0, 0);
            lookAt = a.LookAt(b);
            pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector3D(-10, 0, 0));
            Assert.AreEqual(pointAt, new Vector3D(-1, 0, 0));

            a = new Vector3D(0, 0, 0);
            b = new Vector3D(-10, 10, -5);
            lookAt = a.LookAt(b);
            pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector3D(-10, 10, -5));
            Assert.AreEqual(pointAt, new Vector3D(-0.6666666666f, 0.6666666666f, -0.33333333333333f));

            a = new Vector3D(0, 5, 0);
            b = new Vector3D(0, -5, 0);
            lookAt = a.LookAt(b);
            pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector3D(0, -10, 0));
            Assert.AreEqual(pointAt, new Vector3D(0, -1, 0));

            a = new Vector3D(0, 5, -5);
            b = new Vector3D(0, -5, 5);
            lookAt = a.LookAt(b);
            pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector3D(0, -10, 10));
            Assert.AreEqual(pointAt, new Vector3D(0, -0.70710678f, 0.70710678f));
        }

        [TestMethod]
        public void ClampingTests()
        {
            Vector3D vector = new Vector3D(0, 10, 3).Clamp(new Vector3D(-2, 15, 6), new Vector3D(2, 20, 10));
            Assert.AreEqual(vector, new Vector3D(0, 15, 6));

            vector = new Vector3D(2, 2, 2).ClampComponents(0, 1);
            Assert.AreEqual(vector, new Vector3D(1, 1, 1));

            vector = new Vector3D(2, 2, 10).ClampComponents(3, 4);
            Assert.AreEqual(vector, new Vector3D(3, 3, 4));

            Vector3D lengthVector = new Vector3D(1, 1, 1);
            Vector3D oldLengthVector = lengthVector;
            lengthVector = lengthVector.ClampMagnitude(.5f, .7f);

            Assert.AreEqual(lengthVector.Magnitude, .7f);
            Assert.AreEqual(lengthVector.Normalized, oldLengthVector.Normalized);

            lengthVector = new Vector3D(10, 10, 6);
            oldLengthVector = lengthVector;
            lengthVector = lengthVector.ClampMagnitude(18, 20);

            Assert.IsTrue(lengthVector.Magnitude.CloseEnough(18));
            Assert.IsTrue(lengthVector.Normalized.CloseEnough(oldLengthVector.Normalized));

            lengthVector = new Vector3D(5, 5, 5);
            oldLengthVector = lengthVector;
            lengthVector = lengthVector.ClampMagnitude(-10, 10);
            Assert.AreEqual(lengthVector.Magnitude, oldLengthVector.Magnitude);
            Assert.AreEqual(lengthVector.Normalized, oldLengthVector.Normalized);

            Assert.AreEqual(new Vector3D(-1, -2, -3).Abs, new Vector3D(1, 2, 3));
            Assert.AreEqual(Vector.Abs(new Vector3D(1, 2, 3)), new Vector3D(1, 2, 3));
        }

        [TestMethod]
        public void LerpTest()
        {
            Vector3D a = new Vector3D(-10, -5, 2);
            Vector3D b = new Vector3D(2, .5f, 344);
            Vector3D difference = b - a;

            Test01((alpha) =>
            {
                Vector3D result1 = SMath.Lerp(a, b, alpha);
                Vector3D result1NonStatic = a.Lerp(b, alpha);
                Vector3D result2 = a + alpha * difference;

                Assert.IsTrue(result1.x.CloseEnough(result2.x));
                Assert.IsTrue(result1.y.CloseEnough(result2.y));
                Assert.IsTrue(result1.z.CloseEnough(result2.z));

                Assert.IsTrue(result1NonStatic.x.CloseEnough(result2.x));
                Assert.IsTrue(result1NonStatic.y.CloseEnough(result2.y));
                Assert.IsTrue(result1NonStatic.z.CloseEnough(result2.z));
            });
        }

        [TestMethod]
        public void AdditionTest()
        {
            Vector3D left = new Vector3D(1, 1, 2);
            Vector3D right = new Vector3D(3, 5, 2);
            Vector3D expectedResult = new Vector3D(4, 6, 4);

            Assert.IsTrue(left + right == expectedResult);
            Assert.IsTrue(left.Add(right) == expectedResult);
            Assert.IsTrue(Vector.Add(left, right) == expectedResult);

            left = new Vector3D(3, 876, 6);
            right = new Vector3D(-1, -500, -1);
            expectedResult = new Vector3D(2, 376, 5);

            Assert.IsTrue(left + right == expectedResult);
            Assert.IsTrue(left.Add(right) == expectedResult);
            Assert.IsTrue(Vector.Add(left, right) == expectedResult);

            left = new Vector3D(5, -2.3f, 3);
            right = new Vector3D(5, 0.3f, -3);
            expectedResult = new Vector3D(10, -2, 0);

            Assert.IsTrue(left + right == expectedResult);
            Assert.IsTrue(left.Add(right) == expectedResult);
            Assert.IsTrue(Vector.Add(left, right) == expectedResult);
        }

        [TestMethod]
        public void SubtractionTest()
        {
            Vector3D left = new Vector3D(1, 1);
            Vector3D right = new Vector3D(3, 5, 1);
            Vector3D expectedResult = new Vector3D(-2, -4, -1);

            Assert.IsTrue(left - right == expectedResult);
            Assert.IsTrue(left.Subtract(right) == expectedResult);
            Assert.IsTrue(Vector.Subtract(left, right) == expectedResult);

            left = new Vector3D(3, 876, 0);
            right = new Vector3D(-1, -500, 0);
            expectedResult = new Vector3D(4, 1376, 0);

            Assert.IsTrue(left - right == expectedResult);
            Assert.IsTrue(left.Subtract(right) == expectedResult);
            Assert.IsTrue(Vector.Subtract(left, right) == expectedResult);

            left = new Vector3D(5, -2.3f, 3.5f);
            right = new Vector3D(5, 0.3f, 3.6f);
            expectedResult = new Vector3D(0, -2.6f, -.1f);

            Assert.IsTrue((left - right).CloseEnough(expectedResult));
            Assert.IsTrue(left.Subtract(right).CloseEnough(expectedResult));
            Assert.IsTrue(Vector.Subtract(left, right).CloseEnough(expectedResult));
        }

        [TestMethod]
        public void NegatingTest()
        {
            Vector3D vector = new Vector3D(1, 1, 1);
            Vector3D expectedResult = new Vector3D(-1, -1, -1);

            Assert.IsTrue(-vector == expectedResult);
            Assert.IsTrue(vector.Negated == expectedResult);
            Assert.IsTrue(Vector.Negate(vector) == expectedResult);

            vector = new Vector3D(2, 5, -2);
            expectedResult = new Vector3D(-2, -5, 2);

            Assert.IsTrue(-vector == expectedResult);
            Assert.IsTrue(vector.Negated == expectedResult);
            Assert.IsTrue(Vector.Negate(vector) == expectedResult);

            vector = new Vector3D(-2, 6.5f, .5f);
            expectedResult = new Vector3D(2, -6.5f, -.5f);

            Assert.IsTrue(-vector == expectedResult);
            Assert.IsTrue(vector.Negated == expectedResult);
            Assert.IsTrue(Vector.Negate(vector) == expectedResult);
        }

        [TestMethod]
        public void ScalarMultiplicationTest()
        {
            Vector3D vector = new Vector3D(1, 1, 1);
            float scalar = 5;
            Vector3D expectedResult = new Vector3D(5, 5, 5);

            Assert.IsTrue(vector * scalar == expectedResult);
            Assert.IsTrue(scalar * vector == expectedResult);
            Assert.IsTrue(Vector.Multiply(vector, scalar) == expectedResult);
            Assert.IsTrue(vector.Multiply(scalar) == expectedResult);

            vector = new Vector3D(3, 876, -62);
            scalar = 2;
            expectedResult = new Vector3D(6, 1752, -124);

            Assert.IsTrue(vector * scalar == expectedResult);
            Assert.IsTrue(scalar * vector == expectedResult);
            Assert.IsTrue(Vector.Multiply(vector, scalar) == expectedResult);
            Assert.IsTrue(vector.Multiply(scalar) == expectedResult);

            vector = new Vector3D(5, .3f, 8);
            scalar = -1.5f;
            expectedResult = new Vector3D(-7.5f, -.45f, -12);

            Assert.IsTrue((vector * scalar).CloseEnough(expectedResult));
            Assert.IsTrue((scalar * vector).CloseEnough(expectedResult));
            Assert.IsTrue(Vector.Multiply(vector, scalar).CloseEnough(expectedResult));
            Assert.IsTrue(vector.Multiply(scalar).CloseEnough(expectedResult));
        }

        [TestMethod]
        public void DivisionTest()
        {
            Vector3D vector = new Vector3D(5, 2, 0);
            float scalar = 2;
            Vector3D expectedResult = new Vector3D(2.5f, 1, 0);

            Assert.AreEqual(vector / scalar, expectedResult);
            Assert.AreEqual(vector.Divide(scalar), expectedResult);
            Assert.AreEqual(Vector.Divide(vector, scalar), expectedResult);

            vector = new Vector3D(.5f, -5, 2);
            scalar = .5f;
            expectedResult = new Vector3D(1, -10, 4);

            Assert.AreEqual(vector / scalar, expectedResult);
            Assert.AreEqual(vector.Divide(scalar), expectedResult);
            Assert.AreEqual(Vector.Divide(vector, scalar), expectedResult);

            vector = new Vector3D(5, 2, 3);
            Vector3D vector2 = new Vector3D(2, -2, 6);
            expectedResult = new Vector3D(2.5f, -1, .5f);

            Assert.AreEqual(vector / vector2, expectedResult);
            Assert.AreEqual(vector.Divide(vector2), expectedResult);
            Assert.AreEqual(Vector.Divide(vector, vector2), expectedResult);

            vector = new Vector3D(1, -5, 1);
            vector2 = new Vector3D(5, 2, 0);
            expectedResult = new Vector3D(.2f, -2.5f, Math1D.Infinity);

            Assert.AreEqual(vector / vector2, expectedResult);
            Assert.AreEqual(vector.Divide(vector2), expectedResult);
            Assert.AreEqual(Vector.Divide(vector, vector2), expectedResult);
        }

        [TestMethod]
        public void MultiplicationTest()
        {
            Vector3D left = new Vector3D(1, 1, 2);
            Vector3D right = new Vector3D(2, 0, 2);
            Vector3D expectedResult = new Vector3D(2, 0, 4);

            Assert.IsTrue(left * right == expectedResult);
            Assert.IsTrue(left.Multiply(right) == expectedResult);
            Assert.IsTrue(Vector.Multiply(left, right) == expectedResult);

            left = new Vector3D(3, 10, 2);
            right = new Vector3D(-1, 1.5f, -3);
            expectedResult = new Vector3D(-3, 15, -6);

            Assert.IsTrue(left * right == expectedResult);
            Assert.IsTrue(left.Multiply(right) == expectedResult);
            Assert.IsTrue(Vector.Multiply(left, right) == expectedResult);

            left = new Vector3D(3, 5, 5);
            right = new Vector3D(5, -.5f, -10.5f);
            expectedResult = new Vector3D(15, -2.5f, -52.5f);

            Assert.IsTrue(left * right == expectedResult);
            Assert.IsTrue(left.Multiply(right) == expectedResult);
            Assert.IsTrue(Vector.Multiply(left, right) == expectedResult);
        }

        [TestMethod]
        public void DotTest()
        {
            Vector3D firstLeft = new Vector3D(1, 5, 3);
            Vector3D firstRight = new Vector3D(2, .5f, 4);
            float first = Vector.Dot(firstLeft, firstRight);
            Assert.IsTrue(first == 16.5f);
            Assert.IsTrue(first.CloseEnough(firstLeft.Magnitude * firstRight.Magnitude * Math1D.Cos(firstLeft.SmallestAngle(firstRight))));

            Vector3D secondLeft = new Vector3D(-2, -4f, 1);
            Vector3D secondRight = new Vector3D(5, 5, 3);
            float second = Vector.Dot(secondLeft, secondRight);
            Assert.IsTrue(second == -27);
            Assert.AreEqual(second, secondLeft.Magnitude * secondRight.Magnitude * Math1D.Cos(secondLeft.SmallestAngle(secondRight)));

            Vector3D thirdLeft = new Vector3D(-2, -4f, 6);
            Vector3D thirdRight = new Vector3D(5, 5, 3.5f);
            float third = thirdLeft.Dot(thirdRight);
            Assert.IsTrue(third == -9);
            Assert.IsTrue(third.CloseEnough(thirdLeft.Magnitude * thirdRight.Magnitude * Math1D.Cos(thirdLeft.SmallestAngle(thirdRight))));
        }

        [TestMethod]
        public void CrossTest()
        {
            Vector3D left = new Vector3D(1, 5, .4f);
            Vector3D right = new Vector3D(2, .5f, 0);
            Vector3D result = Vector3D.Cross(left, right);
            Assert.AreEqual(result, (new Vector3D(-.2f, .8f, -9.5f)));

            left = new Vector3D(3, -5, 8);
            right = new Vector3D(0, -3, 11);
            result = Vector3D.Cross(left, right);
            Assert.AreEqual(result, new Vector3D(-31, -33, -9));

            left = new Vector3D(8, 6, 9);
            right = new Vector3D(.1f, .1f, .1f);
            result = left.Cross(right);
            Assert.AreEqual(result, new Vector3D(-.3f, .1f, .2f));
        }

        [TestMethod]
        public void ConversionTests()
        {
            Vector2D twoD = (Vector2D)new Vector3D(1, 2, 3);
            Assert.IsTrue(twoD == new Vector3D(1, 2));

            Vector4D fourD = new Vector3D(-1, 2.2f, 3);
            Assert.IsTrue(fourD == new Vector4D(-1, 2.2f, 3, 0));

            // Euler angles.
            {
                Vector3D vec = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());
                EulerAngles angles = vec;
                AssertUtils.CloseEnough(angles.Pitch, Angle.WrapRadians(vec.x));
                AssertUtils.CloseEnough(angles.Yaw, Angle.WrapRadians(vec.y));
                AssertUtils.CloseEnough(angles.Roll, Angle.WrapRadians(vec.z));
            }
        }
    }
}