﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class Math1DTests : SupergodTestClass
    {
        [TestMethod]
        public void CloseEnoughTest()
        {
            Assert.IsTrue(SMath.CloseEnough(1, 2, 1.1f));
            Assert.IsTrue(SMath.CloseEnough(1, 1.0000001f, 0.000001f));
            Assert.IsTrue(SMath.CloseEnough(1, 1.0000001f));
            Assert.IsFalse(SMath.CloseEnough(1, 1.0000001f, 0.000000001f));
        }

        [TestMethod]
        public void SignTest()
        {
            float value = 123f.Sign();
            Assert.AreEqual(value, 1);

            value = 0f.Sign();
            Assert.AreEqual(value, 1);

            value = Math1D.Sign(.2f);
            Assert.AreEqual(value, 1);

            value = -123f.Sign();
            Assert.AreEqual(value, -1);

            value = Math1D.Sign(-.2f);
            Assert.AreEqual(value, -1);
        }

        [TestMethod]
        public void FactorialTest()
        {
            int x = Math1D.Factorial(5);
            Assert.AreEqual(x, 120);

            x = Math1D.Factorial(0);
            Assert.AreEqual(x, 1);

            x = Math1D.Factorial(1);
            Assert.AreEqual(x, 1);
        }

        [TestMethod]
        public void IsNaNTest()
        {
            Assert.IsTrue(Math1D.NaN.IsNaN());
            Assert.IsTrue((0f / 0f).IsNaN());
            Assert.IsFalse((1f / 0f).IsNaN());
            Assert.IsFalse((1f / 1f).IsNaN());
            Assert.IsFalse((0f / 1f).IsNaN());
        }

        [TestMethod]
        public void AverageTest()
        {
            float x = Math1D.Average(2, 5, -1, 8, 5.5f, 2.3f, 0);
            Assert.AreEqual(x, 3.1142857142857f);

            x = Math1D.Average(3232, .32131f, .3232f, 321, 10101010, 0, 0, 0, 0, 1000);
            Assert.AreEqual(x, 1010556.364451f);

            x = Math1D.Average(-1, -2, -3, -4, -5, 5, 4, 3, 2, 1, 1.5f, 2.5f, .4f, 10f, 10000, 0, 0, 0, 0, 0, 0, 0, -1001);
            Assert.AreEqual(x, 391.88695652174f);
        }

        [TestMethod]
        public void RootsPowersTests()
        {
            float test = Math1D.Pow(3, 2);
            Assert.IsTrue(test == 9);

            test = Math1D.Pow(.5f, 5);
            Assert.IsTrue(test == 0.03125f);

            test = Math1D.Pow(-5, 10);
            Assert.IsTrue(SMath.CloseEnough(test, 9765625));

            test = Math1D.Pow(10, 2);
            Assert.IsTrue(SMath.CloseEnough(test, 100));

            TestMultiple(1, 50, .1f, (i) =>
            {
                test = Math1D.Pow(i, -1);
                AssertUtils.CloseEnough(test, 1 / i);

                test = Math1D.Pow(i, -2);
                AssertUtils.CloseEnough(test, 1 / (i * i));

                test = Math1D.Pow(i, -i);
                AssertUtils.CloseEnough(test, 1 / (i.Pow(i)));

                test = Math1D.Pow(i, 0);
                Assert.AreEqual(test, 1);

                test = Math1D.Exp(i);
                Assert.AreEqual(test, Math1D.E.Pow(i));
                Assert.AreEqual(test, i.Exp());

                test = i.Squared();
                Assert.AreEqual(test, i * i);
                Assert.AreEqual(test, i.Pow(2));

                test = i.Cubed();
                Assert.AreEqual(test, i * i * i);
                Assert.AreEqual(test, i.Pow(3));

                test = i.Sqrt();
                AssertUtils.CloseEnough(test.Squared(), i);

                TestMultiple(1, 10, .1f, (n) =>
                {
                    test = i.Root(n);
                    AssertUtils.CloseEnough(test.Pow(n), i);
                });

                test = Math1D.Ln(i);
                AssertUtils.CloseEnough(test.Exp(), i);

                test = Math1D.Log10(i);
                AssertUtils.CloseEnough(10f.Pow(test), i);

                TestMultiple(-10, 10, .1f, (logBase) =>
                {
                    test = Math1D.Log(i, logBase);
                    if (!test.IsNaN())
                    {
                        AssertUtils.CloseEnough(logBase.Pow(test), i);
                    }
                });
            });
        }

        [TestMethod]
        public void PiTest()
        {
            float radius = 5f;
            Assert.IsTrue(SMath.CloseEnough(31.41592653589793f, radius * 2 * Math1D.PI));
            Assert.IsTrue(SMath.CloseEnough(78.53981633974483f, radius * radius * Math1D.PI));

            radius = .52f;
            Assert.IsTrue(SMath.CloseEnough(3.267256359733385f, radius * 2 * Math1D.PI));
            Assert.IsTrue(SMath.CloseEnough(0.8494866535306802f, radius * radius * Math1D.PI));
        }

        [TestMethod]
        public void ClampTest()
        {
            float test = SMath.Clamp(5, 6, 10);
            Assert.AreEqual(test, 6);

            test = SMath.Clamp(5, 4, 10);
            Assert.AreEqual(test, 5);

            test = SMath.Clamp(20, 4, 10);
            Assert.AreEqual(test, 10);
        }

        [TestMethod]
        public void AbsTest()
        {
            Assert.AreEqual(5, Math1D.Abs(5));
            Assert.AreEqual(5, Math1D.Abs(-5));
            Assert.AreNotEqual(4, Math1D.Abs(-5));
        }

        #region Wrap tests.
        [TestMethod]
        public void WrapTest()
        {
            float test = Math1D.Wrap(2, 0, 3);
            Assert.AreEqual(2, test);

            test = Math1D.Wrap(4, 3);
            Assert.AreEqual(1, test);

            test = Math1D.Wrap(5, 0, 2);
            Assert.AreEqual(1, test);

            test = Math1D.Wrap(360, 0, 180);
            Assert.AreEqual(180, test);

            test = Math1D.Wrap(540, 360);
            Assert.AreEqual(180, test);

            test = Math1D.Wrap(5, 10, 20);
            Assert.AreEqual(15, test);

            test = Math1D.Wrap(9, 15, 20);
            Assert.AreEqual(19, test);

            test = Math1D.Wrap(15, 15, 20);
            Assert.AreEqual(15, test);

            test = Math1D.Wrap(20, 15, 20);
            Assert.AreEqual(20, test);

            test = Math1D.Wrap(17, 15, 20);
            Assert.AreEqual(17, test);

            test = Math1D.Wrap(0, 15, 20);
            Assert.AreEqual(15, test);

            test = Math1D.Wrap(21, 15, 20);
            Assert.AreEqual(16, test);
        }
        #endregion

        #region Min/max tests.
        [TestMethod]
        public void MaxTest()
        {
            float test = Math1D.Max(5, .1f);
            Assert.AreEqual(test, 5);

            test = Math1D.Max(-1, 0);
            Assert.AreEqual(test, 0);

            test = Math1D.Max(1, 4, 1, .2f, -20);
            Assert.AreEqual(test, 4);
        }

        [TestMethod]
        public void MinTest()
        {
            float test = Math1D.Min(5, .1f);
            Assert.AreEqual(test, .1f);

            test = Math1D.Min(-1, 0);
            Assert.AreEqual(test, -1);

            test = Math1D.Min(1, 4, 1, .2f, -20);
            Assert.AreEqual(test, -20);
        }
        #endregion

        #region Trig tests.
        [TestMethod]
        public void SinTest()
        {
            float test = Math1D.Sin(0);
            Assert.IsTrue(SMath.CloseEnough(test, 0));

            test = Math1D.Sin(Math1D.PI);
            Assert.IsTrue(SMath.CloseEnough(test, 0));

            test = Math1D.Sin(10);
            Assert.IsTrue(SMath.CloseEnough(test, -0.544021110889f));

            test = Math1D.Sin(Math1D.PI / 4);
            Assert.IsTrue(SMath.CloseEnough(test, 0.707106781187f));
        }

        [TestMethod]
        public void CosTest()
        {
            float test = Math1D.Cos(0);
            Assert.IsTrue(SMath.CloseEnough(test, 1));

            test = Math1D.Cos(Math1D.PI);
            Assert.IsTrue(SMath.CloseEnough(test, -1));

            test = Math1D.Cos(10);
            Assert.IsTrue(SMath.CloseEnough(test, -0.839071529076f));

            test = Math1D.Cos(Math1D.PI / 4);
            Assert.IsTrue(SMath.CloseEnough(test, 0.707106781187f));
        }

        [TestMethod]
        public void TanTest()
        {
            float test = Math1D.Tan(0);
            Assert.IsTrue(SMath.CloseEnough(test, 0));

            test = Math1D.Tan(Math1D.PI);
            Assert.IsTrue(SMath.CloseEnough(test, 0));

            test = Math1D.Tan(10);
            Assert.IsTrue(SMath.CloseEnough(test, 0.648360827459f));

            test = Math1D.Tan(Math1D.PI / 4);
            Assert.IsTrue(SMath.CloseEnough(test, 1));
        }

        [TestMethod]
        public void AcosTest()
        {
            float test = Math1D.Acos(1);
            Assert.IsTrue(SMath.CloseEnough(test, 0));

            test = Math1D.Acos(.5f);
            Assert.IsTrue(SMath.CloseEnough(test, 1.04719755f));

            test = Math1D.Acos(0);
            Assert.IsTrue(SMath.CloseEnough(test, 1.57079633f));

            test = Math1D.Acos(-.5f);
            Assert.IsTrue(SMath.CloseEnough(test, 2.0943951f));

            test = Math1D.Acos(-1);
            Assert.IsTrue(SMath.CloseEnough(test, 3.14159265f));
        }

        [TestMethod]
        public void AsinTest()
        {
            float test = Math1D.Asin(-1);
            Assert.IsTrue(SMath.CloseEnough(test, -Math1D.PI / 2));

            test = Math1D.Asin(-.5f);
            Assert.IsTrue(SMath.CloseEnough(test, -30 * Angle.Deg2Rad));

            test = Math1D.Asin(0);
            Assert.IsTrue(SMath.CloseEnough(test, 0));

            test = Math1D.Asin(.5f);
            Assert.IsTrue(SMath.CloseEnough(test, 30 * Angle.Deg2Rad));

            test = Math1D.Asin(1);
            Assert.IsTrue(SMath.CloseEnough(test, Math1D.PI * .5f));
        }

        [TestMethod]
        public void AtanTest()
        {
            float test = Math1D.Atan(-1);
            Assert.IsTrue(SMath.CloseEnough(test, -0.785398163397f));

            test = Math1D.Atan(-.5f);
            Assert.IsTrue(SMath.CloseEnough(test, -0.463647609001f));

            test = Math1D.Atan(0);
            Assert.IsTrue(SMath.CloseEnough(test, 0));

            test = Math1D.Atan(.5f);
            Assert.IsTrue(SMath.CloseEnough(test, 0.463647609001f));

            test = Math1D.Atan(1);
            Assert.IsTrue(SMath.CloseEnough(test, 0.785398163397f));
        }

        [TestMethod]
        public void Atan2Test()
        {
            float test = Math1D.Atan2(0, 1);
            Assert.IsTrue(SMath.CloseEnough(test, 0));

            test = Math1D.Atan2(1, 0);
            Assert.IsTrue(SMath.CloseEnough(test, 1.5708f));

            test = Math1D.Atan2(1, 1);
            Assert.IsTrue(SMath.CloseEnough(test, 0.7854f));

            test = Math1D.Atan2(1, 2);
            Assert.IsTrue(SMath.CloseEnough(test, 0.4636f));

            test = Math1D.Atan2(2, 1);
            Assert.IsTrue(SMath.CloseEnough(test, 1.1071f));
        }
        #endregion
    }
}