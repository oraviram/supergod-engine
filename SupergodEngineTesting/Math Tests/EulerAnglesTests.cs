﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class EulerAnglesTests : SupergodTestClass
    {
        [TestMethod]
        public void ConstructionTest()
        {
            EulerAngles angle = new EulerAngles(Math1D.PI / 2, Math1D.PI, 0);
            Assert.AreEqual(angle.x, angle.Pitch);
            AssertUtils.CloseEnough(angle.x, Math1D.PI / 2);

            Assert.AreEqual(angle.y, angle.Yaw);
            AssertUtils.CloseEnough(angle.y, Math1D.PI);

            Assert.AreEqual(angle.z, angle.Roll);
            AssertUtils.CloseEnough(angle.z, 0);

            angle = new EulerAngles(new Angle(.25f, AngleMeasurement.Revolutions), Math1D.PI * 1.5f, new Angle(60, AngleMeasurement.Degrees));
            Assert.AreEqual(angle.x, angle.Pitch);
            AssertUtils.CloseEnough(angle.x, Math1D.PI / 2);

            Assert.AreEqual(angle.y, angle.Yaw);
            AssertUtils.CloseEnough(angle.y, Math1D.PI * 1.5f);

            Assert.AreEqual(angle.z, angle.Roll);
            AssertUtils.CloseEnough(angle.z.Degrees, 60);

            Test50((i) =>
            {
                float pitch = RandFloat100();
                float yaw = RandFloat100();
                float roll = RandFloat100();

                EulerAngles angles = new EulerAngles(pitch, yaw, roll, AngleMeasurement.Radians);
                AssertUtils.CloseEnough(angles.Pitch.Radians, Angle.WrapRadians(pitch));
                AssertUtils.CloseEnough(angles.Yaw.Radians, Angle.WrapRadians(yaw));
                AssertUtils.CloseEnough(angles.Roll.Radians, Angle.WrapRadians(roll));

                angles = new EulerAngles(pitch, yaw, roll, AngleMeasurement.Degrees);
                AssertUtils.CloseEnough(angles.Pitch.Degrees, Angle.WrapDegrees(pitch));
                AssertUtils.CloseEnough(angles.Yaw.Degrees, Angle.WrapDegrees(yaw));
                AssertUtils.CloseEnough(angles.Roll.Degrees, Angle.WrapDegrees(roll));
                
                angles = new EulerAngles(pitch, yaw, roll, AngleMeasurement.Revolutions);
                AssertUtils.CloseEnough(angles.Pitch.Revolutions, Angle.WrapRevolutions(pitch));
                AssertUtils.CloseEnough(angles.Yaw.Revolutions, Angle.WrapRevolutions(yaw));
                AssertUtils.CloseEnough(angles.Roll.Revolutions, Angle.WrapRevolutions(roll));
            });
        }

        [TestMethod]
        public void RotateTests()
        {
            Vector3D vector = Vector3D.right * 5;
            EulerAngles rotation = new EulerAngles(0, 0, Math1D.PI);
            Vector3D rotated = rotation.RotateVector(vector);

            AssertUtils.CloseEnough(rotated, Vector3D.left * 5);
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(vector, rotation));
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(rotation, vector));
            
            vector = Vector3D.up;
            rotation = new EulerAngles(0, 0, Math1D.PI);
            rotated = rotation.RotateVector(vector);

            AssertUtils.CloseEnough(rotated, Vector3D.down);
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(vector, rotation));
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(rotation, vector));

            vector = Vector3D.forward * 2;
            rotation = new EulerAngles(0, Math1D.PI / 2, 0);
            rotated = rotation.RotateVector(vector);

            AssertUtils.CloseEnough(rotated, Vector3D.right * 2);
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(vector, rotation));
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(rotation, vector));

            vector = Vector3D.forward * 2;
            rotation = new EulerAngles(Math1D.PI / 2, 0, 0);
            rotated = rotation.RotateVector(vector);

            AssertUtils.CloseEnough(rotated, Vector3D.down * 2);
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(vector, rotation));
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(rotation, vector));
        }

        [TestMethod]
        public void CombineRotationsTest()
        {
            Test50((i) =>
            {
                EulerAngles a = new EulerAngles(RandAngle(), RandAngle(), RandAngle());
                EulerAngles b = new EulerAngles(RandAngle(), RandAngle(), RandAngle());
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(a.CombineRotations(b).RotateVector(vector), a.RotateVector(vector).RotateVector(b), .001f);
            });
        }

        [TestMethod]
        public void AdditionTest()
        {
            Test50((i) =>
            {
                EulerAngles a = new EulerAngles(RandAngle(), RandAngle(), RandAngle());
                EulerAngles b = new EulerAngles(RandAngle(), RandAngle(), RandAngle());
                EulerAngles addition = a + b;

                Assert.AreEqual(addition.Pitch, a.Pitch + b.Pitch);
                Assert.AreEqual(addition.Yaw, a.Yaw + b.Yaw);
                Assert.AreEqual(addition.Roll, a.Roll + b.Roll);

                Assert.AreEqual(addition, b + a);
                Assert.AreEqual(addition, a.Add(b));
                Assert.AreEqual(addition, EulerAngles.Add(a, b));
            });
        }

        [TestMethod]
        public void SubtractionTest()
        {
            Test50((i) =>
            {
                EulerAngles a = new EulerAngles(RandAngle(), RandAngle(), RandAngle());
                EulerAngles b = new EulerAngles(RandAngle(), RandAngle(), RandAngle());
                EulerAngles aMinusB = a - b;
                EulerAngles bMinusA = b - a;

                Assert.AreNotEqual(aMinusB, bMinusA);

                Assert.AreEqual(aMinusB.Pitch, a.Pitch - b.Pitch);
                Assert.AreEqual(aMinusB.Yaw, a.Yaw - b.Yaw);
                Assert.AreEqual(aMinusB.Roll, a.Roll - b.Roll);

                Assert.AreEqual(bMinusA.Pitch, b.Pitch - a.Pitch);
                Assert.AreEqual(bMinusA.Yaw, b.Yaw - a.Yaw);
                Assert.AreEqual(bMinusA.Roll, b.Roll - a.Roll);

                Assert.AreEqual(aMinusB, a.Subtract(b));
                Assert.AreEqual(aMinusB, EulerAngles.Subtract(a, b));
                Assert.AreEqual(bMinusA, b.Subtract(a));
                Assert.AreEqual(bMinusA, EulerAngles.Subtract(b, a));
            });
        }

        [TestMethod]
        public void MultiplicationDivisionTests()
        {
            Test50((i) =>
            {
                EulerAngles angles = new EulerAngles(RandAngle(), RandAngle(), RandAngle());
                float scalar = RandFloat100();

                EulerAngles multA = angles * scalar;
                EulerAngles multB = scalar * angles;
                EulerAngles div = angles / scalar;

                Assert.AreEqual(multA, multB);

                Assert.AreEqual(multA.Pitch, angles.Pitch * scalar);
                Assert.AreEqual(multA.Yaw, angles.Yaw * scalar);
                Assert.AreEqual(multA.Roll, angles.Roll * scalar);

                Assert.AreEqual(multB.Pitch, scalar * angles.Pitch);
                Assert.AreEqual(multB.Yaw, scalar * angles.Yaw);
                Assert.AreEqual(multB.Roll, scalar * angles.Roll);

                Assert.AreEqual(div.Pitch, angles.Pitch / scalar);
                Assert.AreEqual(div.Yaw, angles.Yaw / scalar);
                Assert.AreEqual(div.Roll, angles.Roll / scalar);

                Assert.AreEqual(multA, angles.Multiply(scalar));
                Assert.AreEqual(multA, EulerAngles.Multiply(angles, scalar));
                Assert.AreEqual(multB, EulerAngles.Multiply(scalar, angles));

                Assert.AreEqual(div, angles.Divide(scalar));
                Assert.AreEqual(div, EulerAngles.Divide(angles, scalar));
            });
        }

        #region Conversion from other types of rotations tests.
        [TestMethod]
        public void FromRotationQuaternionTest()
        {
            Test50((i) =>
            {
                Quaternion quaternion = new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100()).Normalized;
                EulerAngles angles = EulerAngles.FromRotationQuaternion(quaternion);
                Vector3D vec = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(quaternion.RotateVector(vec), angles.RotateVector(vec), .001f);
            });
        }

        [TestMethod]
        public void FromRotationMatrixTest()
        {
            Test50((i) =>
            {
                Matrix3x3 matrix = Matrix3x3.FromEulerAngles(new EulerAngles(RandAngle(), RandAngle(), RandAngle()));
                EulerAngles angles = EulerAngles.FromRotationMatrix(matrix);
                Vector3D vec = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(matrix * vec, angles.RotateVector(vec), .02f);
            });
        }

        [TestMethod]
        public void FromAxisAngleTest()
        {
            Test50((i) =>
            {
                AxisAngle axisAngle = new AxisAngle(new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()).Normalized, RandAngle());
                EulerAngles angles = EulerAngles.FromAxisAngle(axisAngle);
                Vector3D vec = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(axisAngle.RotateVector(vec), angles.RotateVector(vec), .001f);
            });
        }
        #endregion

        [TestMethod]
        public void IndexerTest()
        {
            EulerAngles q = new EulerAngles(RandAngle(), RandAngle(), RandAngle());

            Assert.AreEqual(q.x, q[0]);
            Assert.AreEqual(q.y, q[1]);
            Assert.AreEqual(q.z, q[2]);

            Angle x = RandAngle();
            Angle y = RandAngle();
            Angle z = RandAngle();

            q[0] = x;
            q[1] = y;
            q[2] = z;

            Assert.AreEqual(q.x, x);
            Assert.AreEqual(q.y, y);
            Assert.AreEqual(q.z, z);
        }

        [TestMethod]
        public void ConversionTests()
        {
            EulerAngles angles = new EulerAngles(RandAngle(), RandAngle(), RandAngle());

            Vector3D vector = angles;
            Assert.AreEqual(vector.x, angles.Pitch.Radians);
            Assert.AreEqual(vector.y, angles.Yaw.Radians);
            Assert.AreEqual(vector.z, angles.Roll.Radians);
        }
    }
}