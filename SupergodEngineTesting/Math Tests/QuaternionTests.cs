﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class QuaternionTests : SupergodTestClass
    {
        [TestMethod]
        public void ConstructionTest()
        {
            Quaternion quaternion = new Quaternion(1, 0, 0, 0);
            Assert.AreEqual(quaternion.x, 0);
            Assert.AreEqual(quaternion.y, 0);
            Assert.AreEqual(quaternion.z, 0);
            Assert.AreEqual(quaternion.w, 1);

            quaternion = new Quaternion(Math1D.Sqrt(2) / 2, Math1D.Sqrt(2) / 2, 0, 0);
            Assert.AreEqual(quaternion.x, Math1D.Sqrt(2) / 2);
            Assert.AreEqual(quaternion.y, 0);
            Assert.AreEqual(quaternion.z, 0);
            Assert.AreEqual(quaternion.w, Math1D.Sqrt(2) / 2);

            quaternion = new Quaternion(5, 3, 23, 10);
            Assert.AreEqual(quaternion.x, 3);
            Assert.AreEqual(quaternion.y, 23);
            Assert.AreEqual(quaternion.z, 10);
            Assert.AreEqual(quaternion.w, 5);

            quaternion = new Quaternion(5, new Vector3D(3, 23, 10));
            Assert.AreEqual(quaternion.x, 3);
            Assert.AreEqual(quaternion.y, 23);
            Assert.AreEqual(quaternion.z, 10);
            Assert.AreEqual(quaternion.w, 5);
        }

        [TestMethod]
        public void EqualityTests()
        {
            Quaternion a = new Quaternion(2, 1, -1, 3);
            Quaternion b = new Quaternion(2, 1, -1, 3);
            bool result = a == b;

            Assert.AreEqual(result, a.Equals(b));
            Assert.AreEqual(result, a.Equals((object)b));
            Assert.AreNotEqual(result, a != b);
            Assert.IsTrue(result);
            Assert.IsFalse(a != b);

            a = new Quaternion(3, 1, -1, 3);
            b = new Quaternion(2, 1, -1, 3);
            result = a == b;

            Assert.AreEqual(result, a.Equals(b));
            Assert.AreEqual(result, a.Equals((object)b));
            Assert.AreNotEqual(result, a != b);
            Assert.IsFalse(result);
            Assert.IsTrue(a != b);

            a = new Quaternion(2, 2, -12, 33);
            b = new Quaternion(22, 1, -1, 3);
            result = a == b;

            Assert.AreEqual(result, a.Equals(b));
            Assert.AreEqual(result, a.Equals((object)b));
            Assert.AreNotEqual(result, a != b);
            Assert.IsFalse(result);
            Assert.IsTrue(a != b);
        }
        
        [TestMethod]
        public void MultiplicationCombinationTests()
        {
            Quaternion a = Quaternion.identity;
            Quaternion b = Quaternion.identity;
            Quaternion result = Quaternion.identity;

            #region Two 90 degrees rotations. Multiplication and combination.
            // Right two 90.
            a = Quaternion.FromAxisAngle(Vector3D.right, Angle.right);
            b = Quaternion.FromAxisAngle(Vector3D.right, Angle.right);
            result = a * b;

            AssertUtils.CloseEnough(Quaternion.Multiply(a, b), result);
            AssertUtils.CloseEnough(a.Multiply(b), result);
            
            AssertUtils.CloseEnough(result, b * a);
            Log((a * b).Magnitude);
            Log(a.Magnitude);
            Log(b.Magnitude);
            AssertUtils.CloseEnough(result, b.CombineRotations(a));
            AssertUtils.CloseEnough(result, Rotator.Combine(b, a));
            
            AssertUtils.CloseEnough(result, a.CombineRotations(b));
            AssertUtils.CloseEnough(result, Rotator.Combine(a, b));
            
            AssertUtils.CloseEnough(result.w, 0);
            AssertUtils.CloseEnough(result.XYZ, new Vector3D(1, 0, 0));

            // Up two 90.
            a = Quaternion.FromAxisAngle(Vector3D.up, Angle.right);
            b = Quaternion.FromAxisAngle(Vector3D.up, Angle.right);
            result = a * b;

            AssertUtils.CloseEnough(Quaternion.Multiply(a, b), result);
            AssertUtils.CloseEnough(a.Multiply(b), result);
            
            AssertUtils.CloseEnough(result, b * a);
            AssertUtils.CloseEnough(result, b.CombineRotations(a));
            AssertUtils.CloseEnough(result, Rotator.Combine(b, a));
            
            AssertUtils.CloseEnough(result, a.CombineRotations(b));
            AssertUtils.CloseEnough(result, Rotator.Combine(a, b));
            
            AssertUtils.CloseEnough(result.w, 0);
            AssertUtils.CloseEnough(result.XYZ, new Vector3D(0, 1, 0));

            // Forward two 90.
            a = Quaternion.FromAxisAngle(Vector3D.forward, Angle.right);
            b = Quaternion.FromAxisAngle(Vector3D.forward, Angle.right);
            result = a * b;

            AssertUtils.CloseEnough(Quaternion.Multiply(a, b), result);
            AssertUtils.CloseEnough(a.Multiply(b), result);

            AssertUtils.CloseEnough(result, b * a);
            AssertUtils.CloseEnough(result, b.CombineRotations(a));
            AssertUtils.CloseEnough(result, Rotator.Combine(b, a));

            AssertUtils.CloseEnough(result, a.CombineRotations(b));
            AssertUtils.CloseEnough(result, Rotator.Combine(a, b));

            AssertUtils.CloseEnough(result.w, 0);
            AssertUtils.CloseEnough(result.XYZ, new Vector3D(0, 0, 1));
            #endregion

            #region Two 45 degrees rotations. Multiplication and combination.
            // Right two 45.
            a = Quaternion.FromAxisAngle(Vector3D.right, Angle.halfRight);
            b = Quaternion.FromAxisAngle(Vector3D.right, Angle.halfRight);
            result = a * b;

            AssertUtils.CloseEnough(result, b * a);

            AssertUtils.CloseEnough(Quaternion.Multiply(a, b), result);
            AssertUtils.CloseEnough(a.Multiply(b), result);

            AssertUtils.CloseEnough(result, b.CombineRotations(a));
            AssertUtils.CloseEnough(result, Rotator.Combine(b, a));

            AssertUtils.CloseEnough(result, a.CombineRotations(b));
            AssertUtils.CloseEnough(result, Rotator.Combine(a, b));

            AssertUtils.CloseEnough(result.w, Math1D.Sqrt2Over2);
            AssertUtils.CloseEnough(result.XYZ, new Vector3D(Math1D.Sqrt2Over2, 0, 0));

            // Up two 45.
            a = Quaternion.FromAxisAngle(Vector3D.up, Angle.halfRight);
            b = Quaternion.FromAxisAngle(Vector3D.up, Angle.halfRight);
            result = a * b;

            AssertUtils.CloseEnough(result, b * a);

            AssertUtils.CloseEnough(Quaternion.Multiply(a, b), result);
            AssertUtils.CloseEnough(a.Multiply(b), result);

            AssertUtils.CloseEnough(result, b.CombineRotations(a));
            AssertUtils.CloseEnough(result, Rotator.Combine(b, a));

            AssertUtils.CloseEnough(result, a.CombineRotations(b));
            AssertUtils.CloseEnough(result, Rotator.Combine(a, b));

            AssertUtils.CloseEnough(result.w, Math1D.Sqrt2Over2);
            AssertUtils.CloseEnough(result.XYZ, new Vector3D(0, Math1D.Sqrt2Over2, 0));

            // Forward two 45.
            a = Quaternion.FromAxisAngle(Vector3D.forward, Angle.halfRight);
            b = Quaternion.FromAxisAngle(Vector3D.forward, Angle.halfRight);
            result = a * b;

            AssertUtils.CloseEnough(result, b * a);

            AssertUtils.CloseEnough(Quaternion.Multiply(a, b), result);
            AssertUtils.CloseEnough(a.Multiply(b), result);

            AssertUtils.CloseEnough(result, b.CombineRotations(a));
            AssertUtils.CloseEnough(result, Rotator.Combine(b, a));

            AssertUtils.CloseEnough(result, a.CombineRotations(b));
            AssertUtils.CloseEnough(result, Rotator.Combine(a, b));

            AssertUtils.CloseEnough(result.w, Math1D.Sqrt2Over2);
            AssertUtils.CloseEnough(result.XYZ, new Vector3D(0, 0, Math1D.Sqrt2Over2));
            #endregion

            #region Non-rotations. Multiplication.
            a = new Quaternion(3, 2, -1, 23);
            b = new Quaternion(.4f, -.2f, .1f, .23f);
            result = a * b;

            AssertUtils.TooFar(result, b * a);
            AssertUtils.TooFar(result, b.Multiply(a));
            AssertUtils.TooFar(result, Quaternion.Multiply(b, a));

            AssertUtils.TooFar(result, a.CombineRotations(b));
            AssertUtils.TooFar(result, a.CombineRotations(b));

            AssertUtils.CloseEnough(Quaternion.Multiply(a, b), result);
            AssertUtils.CloseEnough(a.Multiply(b), result);

            AssertUtils.CloseEnough(result, new Quaternion(-3.59f, -2.33f, -5.16f, 9.89f));

            a = new Quaternion(3, 2.4f, -23, -3.46f);
            b = new Quaternion(34, 56, 25, 64);
            result = a * b;

            AssertUtils.TooFar(result, b * a);
            AssertUtils.TooFar(result, b.Multiply(a));
            AssertUtils.TooFar(result, Quaternion.Multiply(b, a));

            AssertUtils.TooFar(result, a.CombineRotations(b));
            AssertUtils.TooFar(result, a.CombineRotations(b));

            AssertUtils.CloseEnough(Quaternion.Multiply(a, b), result);
            AssertUtils.CloseEnough(a.Multiply(b), result);

            AssertUtils.CloseEnough(result, new Quaternion(764.04f, -1135.9f, -1054.36f, 1422.36f));
            #endregion

            Quaternion combined = Quaternion.identity;
            #region Making sure combination works as intended.
            a = new Quaternion(3, 2, -1, 23);
            b = new Quaternion(.4f, -.2f, .1f, .23f);
            result = b * a;
            combined = a.CombineRotations(b);

            AssertUtils.TooFar(result, combined);
            AssertUtils.CloseEnough(result.Normalized, combined);

            AssertUtils.CloseEnough(b.Normalized * a.Normalized, combined);
            AssertUtils.CloseEnough(combined.Magnitude, 1);

            AssertUtils.CloseEnough(result, new Quaternion(-3.59f, 2.73f, 4.96f, 9.89f));
            AssertUtils.CloseEnough(combined, new Quaternion(-0.300468701899570f, 0.228490127071261f, 0.415132245521412f, 0.827753610525557f));

            a = new Quaternion(3, 2.4f, -23, -3.46f);
            b = new Quaternion(34, 56, 25, 64);
            result = b * a;
            combined = a.CombineRotations(b);

            AssertUtils.TooFar(result, combined);
            AssertUtils.CloseEnough(result.Normalized, combined);

            AssertUtils.CloseEnough(b.Normalized * a.Normalized, combined);
            AssertUtils.CloseEnough(combined.Magnitude, 1);

            AssertUtils.CloseEnough(result, new Quaternion(764.04f, 1635.1f, -359.64f, -1273.64f));
            AssertUtils.CloseEnough(combined, new Quaternion(0.341388481774f, 0.730595658014f, -0.160694405509f, -0.569088039798f));
            #endregion
        }

        [TestMethod]
        public void VectorRotationTest()
        {
            Vector3D vector = new Vector3D(0, 5, 0);
            Quaternion quat = Quaternion.FromAxisAngle(Vector3D.right, Angle.right);
            Vector3D rotated = vector.RotateVector(quat);

            AssertUtils.CloseEnough(rotated, quat.RotateVector(vector));
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(quat, vector));
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(vector, quat));
            AssertUtils.CloseEnough(rotated, new Vector3D(0, 0, 5));

            vector = new Vector3D(0, 3, -3);
            quat = Quaternion.FromAxisAngle(Vector3D.right, Angle.halfRight);
            rotated = vector.RotateVector(quat);

            AssertUtils.CloseEnough(rotated, quat.RotateVector(vector));
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(quat, vector));
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(vector, quat));
            AssertUtils.CloseEnough(rotated, new Vector3D(0, 4.2426406871192851f, 0));

            vector = new Vector3D(5, 5, -5);
            quat = Quaternion.FromAxisAngle(new Vector3D(-5, 0, -5).Normalized, Angle.straight);
            rotated = vector.RotateVector(quat);

            AssertUtils.CloseEnough(rotated, quat.RotateVector(vector));
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(quat, vector));
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(vector, quat));
            AssertUtils.CloseEnough(rotated, new Vector3D(-5, -5, 5));

            vector = new Vector3D(2, 132, -23.3f);
            quat = Quaternion.identity;
            rotated = vector.RotateVector(quat);

            AssertUtils.CloseEnough(rotated, quat.RotateVector(vector));
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(quat, vector));
            AssertUtils.CloseEnough(rotated, Rotator.RotateVector(vector, quat));
            AssertUtils.CloseEnough(rotated, vector);
        }

        [TestMethod]
        public void ScalarDivisionMultiplicationTests()
        {
            Test50((i) =>
            {
                Quaternion q = new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());
                float scalar = RandFloat100();
                Quaternion result = q / scalar;

                Assert.AreEqual(q.Divide(scalar), result);
                Assert.AreEqual(Quaternion.Divide(q, scalar), result);
                AssertUtils.CloseEnough(result.XYZ, q.XYZ / scalar);
                AssertUtils.CloseEnough(result.w, q.w / scalar);

                result = q * scalar;

                Assert.AreEqual(q.Multiply(scalar), result);
                Assert.AreEqual(Quaternion.Multiply(q, scalar), result);
                AssertUtils.CloseEnough(result.XYZ, q.XYZ * scalar);
                AssertUtils.CloseEnough(result.w, q.w * scalar);

                Assert.AreEqual(result, scalar * q);
                Assert.AreEqual(result, Quaternion.Multiply(scalar, q));
            });
        }

        [TestMethod]
        public void QuaternionScalarAndQuaternionQuaternionDivisionTest()
        {
            const float CLOSE_ENOUGH_THRESHOLD = .04f;

            Test50((i) =>
            {
                const int MIN = 1;
                const int MAX = 100000;
                Quaternion a = new Quaternion(RandFloat(MIN, MAX), RandFloat(MIN, MAX), RandFloat(MIN, MAX), RandFloat(MIN, MAX));
                Quaternion b = new Quaternion(RandFloat(MIN, MAX), RandFloat(MIN, MAX), RandFloat(MIN, MAX), RandFloat(MIN, MAX));
                Quaternion result = a / b;

                AssertUtils.TooFar(result, b / a);
                Assert.AreEqual(result, a.Divide(b));
                Assert.AreEqual(result, Quaternion.Divide(a, b));

                AssertUtils.CloseEnough(a, (a / b) * b, CLOSE_ENOUGH_THRESHOLD);
                AssertUtils.CloseEnough(b, (b / a) * a, CLOSE_ENOUGH_THRESHOLD);

                Quaternion aTimesBConjugate = a * b.Conjugate;
                Quaternion bTimesBConjugate = b * b.Conjugate;
                AssertUtils.CloseEnough(bTimesBConjugate.XYZ, Vector3D.zero);
                AssertUtils.CloseEnough(result, aTimesBConjugate / bTimesBConjugate.w);

                float scalar = RandFloat100();

                result = scalar / a;
                Quaternion scalarTimesQuaternionConjugate = scalar * a.Conjugate;
                Quaternion quaternionTimesQuaternionConjugate = a * a.Conjugate;

                Assert.AreEqual(result, Quaternion.Divide(scalar, a));
                AssertUtils.CloseEnough(quaternionTimesQuaternionConjugate.XYZ, Vector3D.zero);
                AssertUtils.CloseEnough(result, scalarTimesQuaternionConjugate / quaternionTimesQuaternionConjugate.w, CLOSE_ENOUGH_THRESHOLD);

                result = scalar / b;
                scalarTimesQuaternionConjugate = scalar * b.Conjugate;
                quaternionTimesQuaternionConjugate = b * b.Conjugate;

                Assert.AreEqual(result, Quaternion.Divide(scalar, b));
                AssertUtils.CloseEnough(quaternionTimesQuaternionConjugate.XYZ, Vector3D.zero);
                AssertUtils.CloseEnough(result, scalarTimesQuaternionConjugate / quaternionTimesQuaternionConjugate.w, CLOSE_ENOUGH_THRESHOLD);

            });
        }

        [TestMethod]
        public void AdditionSubtractionTests()
        {
            Test50((i) =>
            {
                float ax = RandFloat100();
                float ay = RandFloat100();
                float az = RandFloat100();
                float aw = RandFloat100();

                float bx = RandFloat100();
                float by = RandFloat100();
                float bz = RandFloat100();
                float bw = RandFloat100();

                Quaternion a = new Quaternion(aw, ax, ay, az);
                Quaternion b = new Quaternion(bw, bx, by, bz);
                Quaternion result = a + b;

                AssertUtils.CloseEnough(result, Quaternion.Add(a, b));
                AssertUtils.CloseEnough(result, a.Add(b));
                AssertUtils.CloseEnough(result, b.Add(a));
                AssertUtils.CloseEnough(result, b + a);
                AssertUtils.CloseEnough(result, new Quaternion(aw + bw, ax + bx, ay + by, az + bz));

                result = a - b;

                AssertUtils.CloseEnough(result, Quaternion.Subtract(a, b));
                AssertUtils.CloseEnough(result, a.Subtract(b));
                AssertUtils.TooFar(result, b.Subtract(a));
                AssertUtils.TooFar(result, b - a);
                AssertUtils.CloseEnough(result, new Quaternion(aw - bw, ax - bx, ay - by, az - bz));
            });
        }

        [TestMethod]
        public void DotTest()
        {
            Quaternion a = new Quaternion(2, 2, 2, .5f);
            Quaternion b = new Quaternion(3, 2, 1, 6);
            float dot = a.Dot(b);

            Assert.AreEqual(dot, Quaternion.Dot(a, b));
            Assert.AreEqual(dot, 15);

            a = new Quaternion(23.2f, -32, 3, -4.5f);
            b = new Quaternion(10, -3.2f, -1, 10);
            dot = a.Dot(b);

            Assert.AreEqual(dot, Quaternion.Dot(a, b));
            AssertUtils.CloseEnough(dot, 286.4f);
        }

        [TestMethod]
        public void MagnitudeTest()
        {
            Quaternion q = new Quaternion(1, 2, 3, 4);
            float magnitude = q.Magnitude;
            float sqrMagnitude = q.SqrMagnitude;

            Assert.AreEqual(magnitude, Quaternion.GetMagnitude(q));
            Assert.AreEqual(sqrMagnitude, Quaternion.GetSqrMagnitude(q));

            AssertUtils.CloseEnough(magnitude, 5.47722558f);
            AssertUtils.CloseEnough(sqrMagnitude, 5.47722558f.Squared());

            q = new Quaternion(3, -3.4f, -1, 20.3f);
            magnitude = q.Magnitude;
            sqrMagnitude = q.SqrMagnitude;

            Assert.AreEqual(magnitude, Quaternion.GetMagnitude(q));
            Assert.AreEqual(sqrMagnitude, Quaternion.GetSqrMagnitude(q));

            AssertUtils.CloseEnough(magnitude, 20.82426469f);
            AssertUtils.CloseEnough(sqrMagnitude, 20.82426469f.Squared());
        }

        [TestMethod]
        public void NormalizationTests()
        {
            Test50((i) =>
            {
                Quaternion q = new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());
                Quaternion normalized = q.Normalized;

                Assert.AreEqual(normalized, Quaternion.Normalize(q));
                AssertUtils.CloseEnough(normalized.Magnitude, 1);
                AssertUtils.CloseEnough(normalized.SqrMagnitude, 1);

                Assert.AreEqual(normalized, q / q.Magnitude);
            });
        }

        [TestMethod]
        public void ConjugateTest()
        {
            float sqrt2 = Math1D.Sqrt(2);
            float sqrt2Over2 = sqrt2 / 2;
            float sqrt2MinusSqrt2Over2 = Math1D.Sqrt(2 - sqrt2) / 2;
            float sqrt2PlusSqrt2Over2 = Math1D.Sqrt(2 + sqrt2) / 2;

            // Unit length quaternions.
            Quaternion q = new Quaternion(sqrt2Over2, 0, 0, sqrt2Over2);
            Assert.AreEqual(q.Conjugate, new Quaternion(sqrt2Over2, 0, 0, -sqrt2Over2));
            Quaternion qTimesInverse = q * q.Conjugate;

            AssertUtils.CloseEnough(qTimesInverse.XYZ, Quaternion.identity.XYZ);
            AssertUtils.CloseEnough(qTimesInverse.w, Quaternion.identity.w);
            AssertUtils.CloseEnough(q.Conjugate * q, qTimesInverse);

            q = Quaternion.identity;
            Assert.AreEqual(q.Conjugate, Quaternion.identity);
            AssertUtils.CloseEnough(q * q.Conjugate, Quaternion.identity);

            q = Quaternion.FromAxisAngle(Vector3D.up, Angle.halfRight);
            AssertUtils.CloseEnough(q.XYZ, new Vector3D(0, sqrt2MinusSqrt2Over2, 0));
            AssertUtils.CloseEnough(q.w, sqrt2PlusSqrt2Over2);
            AssertUtils.CloseEnough(q.Conjugate.XYZ, new Vector3D(0, -sqrt2MinusSqrt2Over2, 0));
            AssertUtils.CloseEnough(q.Conjugate.w, sqrt2PlusSqrt2Over2);
            AssertUtils.CloseEnough(q * q.Conjugate, Quaternion.identity);

            // Non-unit-length quaternions.
            q = new Quaternion(3, 1, 2, 4);
            AssertUtils.CloseEnough(q.Conjugate, new Quaternion(3, -1, -2, -4));
            AssertUtils.TooFar(q.Conjugate * q, Quaternion.identity);
            AssertUtils.TooFar(q * q.Conjugate, Quaternion.identity);

            q = new Quaternion(56, 4, 1, 10);
            AssertUtils.CloseEnough(q.Conjugate, new Quaternion(56, -4, -1, -10));
            AssertUtils.TooFar(q.Conjugate * q, Quaternion.identity);
            AssertUtils.TooFar(q * q.Conjugate, Quaternion.identity);
        }

        [TestMethod]
        public void InverseTest()
        {
            Quaternion q = new Quaternion(3, -1, 2, 5);
            Quaternion inverse = q.Inverted;

            AssertUtils.CloseEnough(inverse * q, Quaternion.identity);
            AssertUtils.CloseEnough(q * inverse, Quaternion.identity);
            
            Assert.AreEqual(inverse, Quaternion.Invert(q));
            AssertUtils.CloseEnough(inverse, new Quaternion(1/13f, 1/39f, -2/39f, -5/39f));

            q = new Quaternion(32, -43.34f, -12, 432.54f);
            inverse = q.Inverted;

            AssertUtils.CloseEnough(inverse * q, Quaternion.identity);
            AssertUtils.CloseEnough(q * inverse, Quaternion.identity);

            Assert.AreEqual(inverse, Quaternion.Invert(q));
            AssertUtils.CloseEnough(inverse, new Quaternion(0.00017f, 0.00023f, 0.00006f, -0.00227f));

            q = new Quaternion(231, 235, -54.543f, 25);
            inverse = q.Inverted;

            AssertUtils.CloseEnough(inverse * q, Quaternion.identity);
            AssertUtils.CloseEnough(q * inverse, Quaternion.identity);

            Assert.AreEqual(inverse, Quaternion.Invert(q));
            AssertUtils.CloseEnough(inverse, new Quaternion(0.00206f, -0.00209f, 0.00049f, -0.00022f));
        }

        [TestMethod]
        public void NegationTests()
        {
            Test50((i) =>
            {
                float x = RandFloat100();
                float y = RandFloat100();
                float z = RandFloat100();
                float w = RandFloat100();
                Quaternion q = new Quaternion(w, x, y, z);
                Quaternion negated = -q;

                AssertUtils.CloseEnough(negated, Vector.Negate(q));
                AssertUtils.CloseEnough(negated, q.Negated);

                AssertUtils.CloseEnough(negated, new Quaternion(-w, -x, -y, -z));
            });
        }

        [TestMethod]
        public void AngleTests()
        {
            Quaternion a;
            Quaternion b;
            
            #region Vector-like tests:
            a = new Quaternion(0, 0, 0, 10);
            b = new Quaternion(0, 10);

            AssertUtils.CloseEnough(a.SmallestAngle(b), Math1D.PI / 2);
            AssertUtils.CloseEnough(Quaternion.SmallestAngle(a, b), Math1D.PI / 2);

            b = new Quaternion(0, 0, 0, -10);
            AssertUtils.CloseEnough(a.SmallestAngle(b), Math1D.PI);
            AssertUtils.CloseEnough(Quaternion.SmallestAngle(a, b), Math1D.PI);

            b = new Quaternion(5, 0, 0, 5);
            AssertUtils.CloseEnough(a.SmallestAngle(b), Math1D.PI / 4);
            AssertUtils.CloseEnough(Quaternion.SmallestAngle(a, b), Math1D.PI / 4);

            AssertUtils.CloseEnough(Quaternion.SmallestAngle(new Quaternion(1, 0, 0), new Quaternion(1, 0, 0), false, false), 0);
            AssertUtils.CloseEnough(Quaternion.SmallestAngle(new Quaternion(0, 0, 1), new Quaternion(0, 0, 1), false, false), 0);
            AssertUtils.CloseEnough(Quaternion.SmallestAngle(new Quaternion(0, 0, 0, 1), new Quaternion(0, 0, 0, 1), false, false), 0);

            AssertUtils.CloseEnough(Quaternion.SmallestAngle(new Quaternion(-1, 0, 0), new Quaternion(1, 0, 0), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Quaternion.SmallestAngle(new Quaternion(0, 1, -1), new Quaternion(1, 0, 1), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Quaternion.SmallestAngle(new Quaternion(0, 0, 0, -1), new Quaternion(0, 0, 0, 1), false, false), Math1D.PI);

            AssertUtils.CloseEnough(Quaternion.SmallestAngle(new Quaternion(0, 1, 0), new Quaternion(1, 0, 0), false, false), Math1D.PI / 2);
            AssertUtils.CloseEnough(Quaternion.SmallestAngle(new Quaternion(0, 1, 0), new Quaternion(0, 0, 1), false, false), Math1D.PI / 2);
            AssertUtils.CloseEnough(Quaternion.SmallestAngle(new Quaternion(1, 0, 0), new Quaternion(0, 0, -1), false, false), Math1D.PI / 2);
            AssertUtils.CloseEnough(Quaternion.SmallestAngle(new Quaternion(0, 0, 0, 1), new Quaternion(0, 0, -1), false, false), Math1D.PI / 2);

            AssertUtils.CloseEnough(Quaternion.BiggestAngle(new Quaternion(1, 0, 0), new Quaternion(1, 0, 0), false, false), Math1D.PI * 2);
            AssertUtils.CloseEnough(Quaternion.BiggestAngle(new Quaternion(0, 0, 1), new Quaternion(0, 0, 1), false, false), Math1D.PI * 2);
            AssertUtils.CloseEnough(Quaternion.BiggestAngle(new Quaternion(0, 0, 0, 1), new Quaternion(0, 0, 0, 1), false, false), Math1D.PI * 2);

            AssertUtils.CloseEnough(Quaternion.BiggestAngle(new Quaternion(-1, 0, 0), new Quaternion(1, 0, 0), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Quaternion.BiggestAngle(new Quaternion(0, 1, -1), new Quaternion(1, 0, 1), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Quaternion.BiggestAngle(new Quaternion(0, 0, 0, -1), new Quaternion(0, 0, 0, 1), false, false), Math1D.PI);

            AssertUtils.CloseEnough(Quaternion.BiggestAngle(new Quaternion(0, 1, 0), new Quaternion(1, 0, 0), false, false), Math1D.PI * 1.5f);
            AssertUtils.CloseEnough(Quaternion.BiggestAngle(new Quaternion(0, 1, 0), new Quaternion(0, 0, 1), false, false), Math1D.PI * 1.5f);
            AssertUtils.CloseEnough(Quaternion.BiggestAngle(new Quaternion(1, 0, 0), new Quaternion(0, 0, -1), false, false), Math1D.PI * 1.5f);
            AssertUtils.CloseEnough(Quaternion.BiggestAngle(new Quaternion(0, 0, 0, 1), new Quaternion(0, 0, -1), false, false), Math1D.PI * 1.5f);
            #endregion
            
            a = Quaternion.FromAxisAngle(Vector3D.up, Angle.straight);
            b = Quaternion.FromAxisAngle(Vector3D.down, Angle.straight);
            Angle biggestAngle = a.BiggestAngle(b);
            Angle smallestAngle = a.SmallestAngle(b);

            AssertUtils.CloseEnough(biggestAngle, smallestAngle);
            Assert.AreEqual(biggestAngle, Quaternion.BiggestAngle(a, b, false, false));
            Assert.AreEqual(smallestAngle, Quaternion.SmallestAngle(a, b, false, false));

            Assert.AreEqual(smallestAngle, Angle.straight);

            a = Quaternion.FromAxisAngle(Vector3D.right, Angle.right);
            b = Quaternion.FromAxisAngle(Vector3D.up, Angle.straight);
            biggestAngle = a.BiggestAngle(b);
            smallestAngle = a.SmallestAngle(b);

            AssertUtils.TooFar(biggestAngle, smallestAngle);
            Assert.AreEqual(biggestAngle, Quaternion.BiggestAngle(a, b, false, false));
            Assert.AreEqual(smallestAngle, Quaternion.SmallestAngle(a, b, false, false));

            Assert.AreEqual(smallestAngle, Angle.right);
            Assert.AreEqual(biggestAngle, Angle.straightAndHalf);
        }

        [TestMethod]
        public void ExpLogPowTests()
        {
            #region Exponentials for non unit length quaternions.
            const float sin1 = 0.841470984808f;
            const float sin1TimesSqrt2Over2 = sin1 * Math1D.Sqrt2Over2;
            const float cos1 = 0.540302305868f;
            const float eToPower10 = 22026.465794806716517f;
            const float twoThreeFiveVectorMagnitude = 6.1644140029689765f;
            const float sinTwoThreeFiveVectorMagnitude = -0.118492257441f;
            const float cosTwoThreeFiveVectorMagnitude = 0.992954976284f;
            const float sinOverMagnitude235Vector = sinTwoThreeFiveVectorMagnitude / twoThreeFiveVectorMagnitude;
            const float closeEnoughThreshold = .01f;

            Quaternion quaternion = new Quaternion(0, Math1D.Sqrt2Over2, 0, Math1D.Sqrt2Over2);
            Quaternion exp = quaternion.Exp();

            AssertUtils.CloseEnough(exp, Quaternion.Exp(quaternion));
            AssertUtils.CloseEnough(exp, new Quaternion(cos1, sin1TimesSqrt2Over2, 0, sin1TimesSqrt2Over2));

            quaternion = Quaternion.identity;
            exp = quaternion.Exp();

            AssertUtils.CloseEnough(exp.w, Math1D.E);
            Assert.IsTrue(float.IsNaN(exp.x));
            Assert.IsTrue(float.IsNaN(exp.y));
            Assert.IsTrue(float.IsNaN(exp.z));

            quaternion = new Quaternion(10, 2, 3, 5);
            exp = quaternion.Exp();

            AssertUtils.CloseEnough(exp, Quaternion.Exp(quaternion));
            AssertUtils.CloseEnough(exp, new Quaternion(eToPower10 * cosTwoThreeFiveVectorMagnitude, sinOverMagnitude235Vector * 2 * eToPower10, sinOverMagnitude235Vector * 3 * eToPower10, sinOverMagnitude235Vector * 5 * eToPower10), closeEnoughThreshold);
            #endregion

            AssertUtils.CloseEnough(Quaternion.i ^ 2, -1);
            AssertUtils.CloseEnough(Quaternion.j ^ 2, -1);
            AssertUtils.CloseEnough(Quaternion.k ^ 2, -1);

            Test50((i) =>
            {
                Quaternion q;

                // Pow tests: NOPE. It's way too complicated, and I am tired of unit testing...
                //            But I know it's used in Slerp, which easily passes the unit tests, so I don't see why it wouldn't work.
                //Vector3D axis = Vector3D.up;
                //Angle angle = RandAngle();

                //Quaternion q = Quaternion.FromAxisAngle(axis, angle);
                //Quaternion pow = q ^ i;
                ////SMath.Acos(baseQuaternion.w) * 2
                //if (!pow.HasNaN)
                //{
                //    const float THRESHOLD = .001f;
                //    AssertUtils.CloseEnough(pow, q.Pow(i));
                //    AssertUtils.CloseEnough(pow, Quaternion.Pow(q, i));

                //    Log("Real: " + axis + ", Complicated thing: " + q.XYZ / SMath.Sin(SMath.Acos(q.w)));

                //    AssertUtils.CloseEnough(pow.w, SMath.Cos(angle / 2 * i), THRESHOLD);
                //    AssertUtils.CloseEnough(pow.XYZ, SMath.Sin(angle / 2 * i) * axis, THRESHOLD);
                //    //AssertUtils.CloseEnough(pow.XYZ, SMath.Sin(angle / 2 * i) * (q.XYZ / SMath.Sin(SMath.Acos(q.w))), THRESHOLD);
                //}

                // Exp and Ln tests:
                const int MAX = 10000;
                const int MIN = -10000;
                q = new Quaternion(RandFloat(MIN, MAX), RandFloat(MIN, MAX), RandFloat(MIN, MAX), RandFloat(MIN, MAX));
                q = q.Normalized;

                exp = q.Exp();
                Assert.AreEqual(Quaternion.Exp(q), exp);

                Quaternion ln = q.Ln();
                if (!ln.HasNaN) // Only continue if the logarithms exists.
                {
                    Assert.AreEqual(ln, Quaternion.Ln(q));
                    AssertUtils.CloseEnough(ln.Exp(), q);
                }
            });
        }

        #region From other rotation representations.
        [TestMethod]
        public void FromAxisAngleTest()
        {
            // Rotate with an angle of 0.
            Vector3D axis = Vector3D.right;
            Angle angle = Angle.zero;
            Quaternion quaternion = Quaternion.FromAxisAngle(axis, angle);

            Assert.AreEqual(quaternion, Quaternion.FromAxisAngle(new AxisAngle(axis, angle)));
            Assert.AreEqual(quaternion, Quaternion.identity);

            axis = Vector3D.up;
            angle = Angle.zero;
            quaternion = Quaternion.FromAxisAngle(axis, angle);

            Assert.AreEqual(quaternion, Quaternion.FromAxisAngle(new AxisAngle(axis, angle)));
            Assert.AreEqual(quaternion, Quaternion.identity);

            axis = Vector3D.forward;
            angle = Angle.zero;
            quaternion = Quaternion.FromAxisAngle(axis, angle);

            Assert.AreEqual(quaternion, Quaternion.FromAxisAngle(new AxisAngle(axis, angle)));
            Assert.AreEqual(quaternion, Quaternion.identity);

            // Rotate around right.
            axis = Vector3D.right;
            angle = Angle.right;
            quaternion = Quaternion.FromAxisAngle(axis, angle);

            Assert.AreEqual(quaternion, Quaternion.FromAxisAngle(new AxisAngle(axis, angle)));
            Assert.AreEqual(quaternion, new Quaternion(Math1D.Sqrt(2) / 2, Math1D.Sqrt(2) / 2, 0, 0));

            angle = Angle.straight;
            quaternion = Quaternion.FromAxisAngle(axis, angle);

            Assert.AreEqual(quaternion, Quaternion.FromAxisAngle(new AxisAngle(axis, angle)));
            Assert.AreEqual(quaternion.XYZ, new Vector3D(1, 0, 0));
            AssertUtils.CloseEnough(quaternion.w, 0);

            // Rotate around up.
            axis = Vector3D.up;
            angle = Angle.right;
            quaternion = Quaternion.FromAxisAngle(axis, angle);

            Assert.AreEqual(quaternion, Quaternion.FromAxisAngle(new AxisAngle(axis, angle)));
            Assert.AreEqual(quaternion, new Quaternion(Math1D.Sqrt(2) / 2, 0, Math1D.Sqrt(2) / 2, 0));

            angle = Angle.straight;
            quaternion = Quaternion.FromAxisAngle(axis, angle);

            Assert.AreEqual(quaternion, Quaternion.FromAxisAngle(new AxisAngle(axis, angle)));
            Assert.AreEqual(quaternion.XYZ, new Vector3D(0, 1, 0));
            AssertUtils.CloseEnough(quaternion.w, 0);

            // Rotate around forward.
            axis = Vector3D.forward;
            angle = Angle.right;
            quaternion = Quaternion.FromAxisAngle(axis, angle);

            Assert.AreEqual(quaternion, Quaternion.FromAxisAngle(new AxisAngle(axis, angle)));
            Assert.AreEqual(quaternion, new Quaternion(Math1D.Sqrt(2) / 2, 0, 0, Math1D.Sqrt(2) / 2));

            angle = Angle.straight;
            quaternion = Quaternion.FromAxisAngle(axis, angle);

            Assert.AreEqual(quaternion, Quaternion.FromAxisAngle(new AxisAngle(axis, angle)));
            Assert.AreEqual(quaternion.XYZ, new Vector3D(0, 0, 1));
            AssertUtils.CloseEnough(quaternion.w, 0);
        }

        [TestMethod]
        public void FromEulerAnglesTest()
        {
            /*
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(0, 0, 0), Quaternion.identity);
            TestAngle((a) =>
            {
                float sinA = SMath.Sin(a / 2);
                float cosA = SMath.Cos(a / 2);
                float sinACosA = sinA * cosA;
                float cosASquared = cosA.Squared();
                float sinASquared = sinA.Squared();

                // Single axis:
                EulerAngles angles = new EulerAngles(a, 0, 0);
                Quaternion q = Quaternion.FromEulerAngles(angles);

                Assert.AreEqual(q, Quaternion.FromEulerAngles(angles.x, angles.y, angles.z));
                AssertUtils.CloseEnough(q, new Quaternion(cosA, sinA, 0, 0));

                angles = new EulerAngles(0, a, 0);
                q = Quaternion.FromEulerAngles(angles);

                Assert.AreEqual(q, Quaternion.FromEulerAngles(angles.x, angles.y, angles.z));
                AssertUtils.CloseEnough(q, new Quaternion(cosA, 0, sinA, 0));

                angles = new EulerAngles(0, 0, a);
                q = Quaternion.FromEulerAngles(angles);

                Assert.AreEqual(q, Quaternion.FromEulerAngles(angles.x, angles.y, angles.z));
                AssertUtils.CloseEnough(q, new Quaternion(cosA, 0, 0, sinA));

                // Two axes:
                angles = new EulerAngles(a, a, 0);
                q = Quaternion.FromEulerAngles(angles);

                Assert.AreEqual(q, Quaternion.FromEulerAngles(angles.x, angles.y, angles.z));
                AssertUtils.CloseEnough(q, new Quaternion(cosASquared, sinACosA, sinACosA, -sinASquared));

                angles = new EulerAngles(a, 0, a);
                q = Quaternion.FromEulerAngles(angles);

                Assert.AreEqual(q, Quaternion.FromEulerAngles(angles.x, angles.y, angles.z));
                AssertUtils.CloseEnough(q, new Quaternion(cosASquared, sinACosA, -sinASquared, sinACosA));

                angles = new EulerAngles(0, a, a);
                q = Quaternion.FromEulerAngles(angles);

                Assert.AreEqual(q, Quaternion.FromEulerAngles(angles.x, angles.y, angles.z));
                AssertUtils.CloseEnough(q, new Quaternion(cosASquared, sinASquared, sinACosA, sinACosA));
            });
            */
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(Angle.right, 0, 0).RotateVector(Vector3D.forward), Vector3D.down);
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(Angle.halfRight, 0, 0).RotateVector(Vector3D.forward), new Vector3D(0, -Math1D.Sqrt2Over2, Math1D.Sqrt2Over2));
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(-Angle.right, 0, 0).RotateVector(Vector3D.forward), Vector3D.up);
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(-Angle.halfRight, 0, 0).RotateVector(Vector3D.forward), new Vector3D(0, Math1D.Sqrt2Over2, Math1D.Sqrt2Over2));
            
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(0, Angle.right, 0).RotateVector(Vector3D.right), Vector3D.back);
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(0, Angle.halfRight, 0).RotateVector(Vector3D.right), new Vector3D(Math1D.Sqrt2Over2, 0, -Math1D.Sqrt2Over2));
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(0, -Angle.right, 0).RotateVector(Vector3D.right), Vector3D.forward);
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(0, -Angle.halfRight, 0).RotateVector(Vector3D.right), new Vector3D(Math1D.Sqrt2Over2, 0, Math1D.Sqrt2Over2));
            
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(0, 0, Angle.right).RotateVector(Vector3D.up), Vector3D.left);
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(0, 0, Angle.halfRight).RotateVector(Vector3D.up), new Vector3D(-Math1D.Sqrt2Over2, Math1D.Sqrt2Over2, 0));
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(0, 0, -Angle.right).RotateVector(Vector3D.up), Vector3D.right);
            AssertUtils.CloseEnough(Quaternion.FromEulerAngles(0, 0, -Angle.halfRight).RotateVector(Vector3D.up), new Vector3D(Math1D.Sqrt2Over2, Math1D.Sqrt2Over2, 0));
        }

        [TestMethod]
        public void FromMatricesTest()
        {
            Test50((i) =>
            {
                // Yes, I know I am using Matrix4x4.Rotate...I should really improve the matrix maths library. :/
                Matrix3x3 mat = Matrix3x3.FromEulerAngles(
                    new EulerAngles(RandFloat100(), RandFloat100(), RandFloat100()));

                Vector3D vec = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());
                Quaternion quat = Quaternion.FromRotationMatrix(mat);

                AssertUtils.CloseEnough(mat * vec, quat.RotateVector(vec));
            });
        }
        #endregion

        #region Utility Construction Methods Tests.
        [TestMethod]
        public void FromToRotationTest()
        {
            Vector3D
                from = Vector3D.forward,
                to = Vector3D.up;
            Quaternion rotation = Quaternion.FromToRotation(from, to);

            AssertUtils.CloseEnough(from.RotateVector(rotation), to);
            AssertUtils.CloseEnough(to.RotateVector(rotation.Inverted), from);

            from = new Vector3D(Math1D.Sqrt2Over2, Math1D.Sqrt2Over2);
            to = new Vector3D(Math1D.Sqrt2Over2, 0, Math1D.Sqrt2Over2);
            rotation = Quaternion.FromToRotation(from, to);

            AssertUtils.CloseEnough(from.RotateVector(rotation), to);
            AssertUtils.CloseEnough(to.RotateVector(rotation.Inverted), from);

            from = Vector3D.forward * 2;
            to = Vector3D.down * 5;
            rotation = Quaternion.FromToRotation(from, to);

            AssertUtils.CloseEnough(from.RotateVector(rotation), to.SetMagnitude(from.Magnitude));
            AssertUtils.CloseEnough(to.RotateVector(rotation.Inverted), from.SetMagnitude(to.Magnitude));

            from = Vector3D.down;
            to = Vector3D.up;
            rotation = Quaternion.FromToRotation(from, to);
            
            AssertUtils.CloseEnough(from.RotateVector(rotation), to);
            AssertUtils.CloseEnough(to.RotateVector(rotation.Inverted), from);
            
            from = Vector3D.left;
            to = Vector3D.right;
            rotation = Quaternion.FromToRotation(from, to);
            
            AssertUtils.CloseEnough(from.RotateVector(rotation), to);
            AssertUtils.CloseEnough(to.RotateVector(rotation.Inverted), from);

            from = Vector3D.forward;
            to = Vector3D.back;
            rotation = Quaternion.FromToRotation(from, to);
            
            AssertUtils.CloseEnough(from.RotateVector(rotation), to);
            AssertUtils.CloseEnough(to.RotateVector(rotation.Inverted), from);

            from = Vector3D.forward;
            to = from;
            rotation = Quaternion.FromToRotation(from, to);

            AssertUtils.CloseEnough(from.RotateVector(rotation), from);
            AssertUtils.CloseEnough(to.RotateVector(rotation.Inverted), to);
        }

        [TestMethod]
        public void LookAtLookForwardTests()
        {
            Test50((i) =>
            {
                Vector3D forward = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()).Normalized;
                Vector3D up = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                Quaternion q = Quaternion.Look(forward, up);

                AssertUtils.CloseEnough(q.RotateVector(Vector3D.forward), forward);
                //AssertUtils.CloseEnough(q.RotateVector(Vector3D.up), up);

                Vector3D lookAtPos = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());
                Vector3D lookFromPos = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());
                q = Quaternion.LookAt(lookFromPos, lookAtPos, up);

                AssertUtils.CloseEnough(q.RotateVector(Vector3D.forward), lookFromPos.PointAt(lookAtPos));
                //AssertUtils.CloseEnough(q.RotateVector(Vector3D.up), lookFromPos.PointAt(lookAtPos));

                // TODO: Add good unit tests! Those unit tests just do what FromToRotation can do, which is not good. I will have to test the up vector, too.
                Log("READ THE COMMENT HERE!");
            });
        }
#endregion

        [TestMethod]
        public void InterpolationsTests()
        {
            Test50((i) =>
            {
                const int MIN = 1;
                const int MAX = 750;
                Quaternion a = new Quaternion(RandFloat(MIN, MAX), RandFloat(MIN, MAX), RandFloat(MIN, MAX), RandFloat(MIN, MAX));
                Quaternion b = new Quaternion(RandFloat(MIN, MAX), RandFloat(MIN, MAX), RandFloat(MIN, MAX), RandFloat(MIN, MAX));
                Quaternion difference = b - a;

                Quaternion aNorm = a.Normalized;
                Quaternion bNorm = b.Normalized;
                float normDot = aNorm.Dot(bNorm).Clamp(0, 1);

                Test01((alpha) =>
                {
                    // Test linear interpolation.
                    Quaternion result1 = SMath.Lerp(a, b, alpha);
                    Quaternion result1NonStatic = a.Lerp(b, alpha);
                    Quaternion result2 = a + alpha * difference;

                    AssertUtils.CloseEnough(result1, result2, .001f);
                    AssertUtils.CloseEnough(result1NonStatic, result2, .001f);

                    Quaternion nLerp = Vector.NLerp(a, b, alpha);
                    AssertUtils.CloseEnough(nLerp, a.NLerp(b, alpha));
                    AssertUtils.CloseEnough(nLerp, result1.Normalized);
                    Assert.IsTrue(nLerp.IsUnitLength);
                    
                    // Test spherical interpolation.
                    Quaternion slerp = aNorm.Slerp(bNorm, alpha);
                    if (!slerp.HasNaN)
                    {
                        Assert.AreEqual(slerp, Quaternion.Slerp(aNorm, bNorm, alpha));

                        float theta = Math1D.Acos(normDot) * alpha;
                        AssertUtils.CloseEnough(slerp, aNorm * Math1D.Cos(theta) + (bNorm - aNorm * normDot).Normalized * Math1D.Sin(theta));
                    }
                });
            });
        }

        [TestMethod]
        public void NaNTests()
        {
            Assert.IsTrue(Quaternion.NaN.HasNaN);
            Assert.IsTrue(Quaternion.NaN.IsNaN);

            Quaternion q = new Quaternion(Math1D.NaN, 0, 0, 0);
            Assert.IsFalse(q.IsNaN);
            Assert.IsTrue(q.HasNaN);
            Assert.IsFalse(Quaternion.EqualsNaN(q));
            Assert.IsTrue(Quaternion.ContainsNaN(q));

            q = new Quaternion(0, Math1D.NaN, 0, 0);
            Assert.IsFalse(q.IsNaN);
            Assert.IsTrue(q.HasNaN);
            Assert.IsFalse(Quaternion.EqualsNaN(q));
            Assert.IsTrue(Quaternion.ContainsNaN(q));

            q = new Quaternion(0, 0, Math1D.NaN, 0);
            Assert.IsFalse(q.IsNaN);
            Assert.IsTrue(q.HasNaN);
            Assert.IsFalse(Quaternion.EqualsNaN(q));
            Assert.IsTrue(Quaternion.ContainsNaN(q));

            q = new Quaternion(0, 0, 0, Math1D.NaN);
            Assert.IsFalse(q.IsNaN);
            Assert.IsTrue(q.HasNaN);
            Assert.IsFalse(Quaternion.EqualsNaN(q));
            Assert.IsTrue(Quaternion.ContainsNaN(q));
        }

        [TestMethod]
        public void IndexerTest()
        {
            Quaternion q = new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());

            Assert.AreEqual(q.w, q[0]);
            Assert.AreEqual(q.x, q[1]);
            Assert.AreEqual(q.y, q[2]);
            Assert.AreEqual(q.z, q[3]);

            float w = RandFloat100();
            float x = RandFloat100();
            float y = RandFloat100();
            float z = RandFloat100();

            q[0] = w;
            q[1] = x;
            q[2] = y;
            q[3] = z;

            Assert.AreEqual(q.w, w);
            Assert.AreEqual(q.x, x);
            Assert.AreEqual(q.y, y);
            Assert.AreEqual(q.z, z);
        }

        [TestMethod]
        public void ConversionTests()
        {
            Vector4D vector = new Quaternion(5, 3, 2, 1);
            Assert.AreEqual(vector, new Vector4D(3, 2, 1, 5));

            Quaternion fromFloat = 4;
            Assert.AreEqual(fromFloat, new Quaternion(4, Vector3D.zero));

            fromFloat = -32.423f;
            Assert.AreEqual(fromFloat, new Quaternion(-32.423f, Vector3D.zero));
        }
    }
}