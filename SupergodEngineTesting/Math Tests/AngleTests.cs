﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class AngleTests : SupergodTestClass
    {
        #region Angle conversion tests.
        [TestMethod]
        public void Deg2RadTest()
        {
            float test = 45 * Angle.Deg2Rad;
            Assert.AreEqual(test, 0.25f * Math1D.PI);

            test = 90 * Angle.Deg2Rad;
            Assert.AreEqual(test, 0.5f * Math1D.PI);

            test = 180 * Angle.Deg2Rad;
            Assert.AreEqual(test, Math1D.PI);

            test = 270 * Angle.Deg2Rad;
            Assert.AreEqual(test, 1.5f * Math1D.PI);

            test = 360 * Angle.Deg2Rad;
            Assert.AreEqual(test, 2 * Math1D.PI);
        }

        [TestMethod]
        public void Deg2RevTest()
        {
            float test = 45 * Angle.Deg2Rev;
            Assert.AreEqual(test, .125f);

            test = 90 * Angle.Deg2Rev;
            Assert.AreEqual(test, .25f);

            test = 180 * Angle.Deg2Rev;
            Assert.AreEqual(test, .5f);

            test = 270 * Angle.Deg2Rev;
            Assert.AreEqual(test, .75f);

            test = 360 * Angle.Deg2Rev;
            Assert.AreEqual(test, 1);
        }

        [TestMethod]
        public void Rad2DegTest()
        {
            float test = 0.25f * Math1D.PI * Angle.Rad2Deg;
            Assert.AreEqual(test, 45);

            test = 0.5f * Math1D.PI * Angle.Rad2Deg;
            Assert.AreEqual(test, 90);

            test = Math1D.PI * Angle.Rad2Deg;
            Assert.AreEqual(test, 180);

            test = 1.5f * Math1D.PI * Angle.Rad2Deg;
            Assert.AreEqual(test, 270);

            test = 2 * Math1D.PI * Angle.Rad2Deg;
            Assert.AreEqual(test, 360);
        }

        [TestMethod]
        public void Rad2RevTest()
        {
            float test = 0.25f * Math1D.PI * Angle.Rad2Rev;
            Assert.AreEqual(test, .125f);

            test = 0.5f * Math1D.PI * Angle.Rad2Rev;
            Assert.AreEqual(test, .25f);

            test = Math1D.PI * Angle.Rad2Rev;
            Assert.AreEqual(test, .5f);

            test = 1.5f * Math1D.PI * Angle.Rad2Rev;
            Assert.AreEqual(test, .75f);

            test = 2 * Math1D.PI * Angle.Rad2Rev;
            Assert.AreEqual(test, 1);
        }

        [TestMethod]
        public void Rev2DegTest()
        {
            float test = .125f * Angle.Rev2Deg;
            Assert.AreEqual(test, 45);

            test = .25f * Angle.Rev2Deg;
            Assert.AreEqual(test, 90);

            test = .5f * Angle.Rev2Deg;
            Assert.AreEqual(test, 180);

            test = .75f * Angle.Rev2Deg;
            Assert.AreEqual(test, 270);

            test = Angle.Rev2Deg;
            Assert.AreEqual(test, 360);
        }

        [TestMethod]
        public void Rev2RadTest()
        {
            float test = .125f * Angle.Rev2Rad;
            Assert.AreEqual(test, Math1D.PI / 4);

            test = .25f * Angle.Rev2Rad;
            Assert.AreEqual(test, Math1D.PI / 2);

            test = .5f * Angle.Rev2Rad;
            Assert.AreEqual(test, Math1D.PI);

            test = .75f * Angle.Rev2Rad;
            Assert.AreEqual(test, Math1D.PI * 1.5f);

            test = Angle.Rev2Rad;
            Assert.AreEqual(test, Math1D.PI * 2);
        }
        #endregion

        [TestMethod]
        public void FloatWrappingTests()
        {
            AssertUtils.CloseEnough(Angle.WrapDegrees(1080), 360);
            AssertUtils.CloseEnough(Angle.WrapDegrees(900), 180);
            AssertUtils.CloseEnough(Angle.WrapDegrees(360), 360);
            AssertUtils.CloseEnough(Angle.WrapDegrees(180), 180);

            AssertUtils.CloseEnough(Angle.WrapRadians(Math1D.PI * 6), Math1D.PI * 2);
            AssertUtils.CloseEnough(Angle.WrapRadians(Math1D.PI * 5), Math1D.PI);
            AssertUtils.CloseEnough(Angle.WrapRadians(Math1D.PI * 2), Math1D.PI * 2);
            AssertUtils.CloseEnough(Angle.WrapRadians(Math1D.PI), Math1D.PI);

            AssertUtils.CloseEnough(Angle.WrapRevolutions(3), 1);
            AssertUtils.CloseEnough(Angle.WrapRevolutions(2.5f), .5f);
            AssertUtils.CloseEnough(Angle.WrapRevolutions(1), 1);
            AssertUtils.CloseEnough(Angle.WrapRevolutions(.5f), .5f);
        }

        [TestMethod]
        public void ConstructorTest()
        {
            Angle angle = new Angle(1080, AngleMeasurement.Degrees);
            AssertUtils.CloseEnough(angle.Radians, Math1D.PI * 2);

            angle = new Angle(180, AngleMeasurement.Degrees);
            Assert.AreEqual(angle.Radians, Math1D.PI);

            angle = new Angle(2, AngleMeasurement.Revolutions);
            Assert.AreEqual(angle.Radians, Math1D.PI * 2);

            angle = new Angle(.5f, AngleMeasurement.Revolutions);
            Assert.AreEqual(angle.Radians, Math1D.PI);
            
            angle = new Angle(Math1D.PI * 2, AngleMeasurement.Radians);
            Assert.AreEqual(angle.Radians, Math1D.PI * 2);

            angle = new Angle(Math1D.PI * 3, AngleMeasurement.Radians);
            AssertUtils.CloseEnough(angle.Radians, Math1D.PI);
        }

        [TestMethod]
        public void SettersTest()
        {
            Angle angle = new Angle();
            angle.Radians = Math1D.PI * 8;
            AssertUtils.CloseEnough(angle.Radians, Math1D.PI * 2);
            AssertUtils.CloseEnough(angle.Degrees, 360);
            AssertUtils.CloseEnough(angle.Revolutions, 1);

            angle.Degrees = 540;
            AssertUtils.CloseEnough(angle.Radians, Math1D.PI);
            AssertUtils.CloseEnough(angle.Degrees, 180);
            AssertUtils.CloseEnough(angle.Revolutions, .5f);

            angle.Revolutions = 2.25f;
            AssertUtils.CloseEnough(angle.Radians, Math1D.PI / 2);
            AssertUtils.CloseEnough(angle.Degrees, 90);
            AssertUtils.CloseEnough(angle.Revolutions, .25f);
        }

        [TestMethod]
        public void NegativeSettersTest()
        {
            Angle angle = new Angle();

            // Setting negative radians.
            angle.NegativeRadians = Math1D.PI * 3;
            Assert.AreEqual(angle.NegativeRadians, Math1D.PI);
            Assert.AreEqual(angle.NegativeDegrees, 180);
            Assert.AreEqual(angle.NegativeRevolutions, .5f);
            Assert.AreEqual(angle.Radians, Math1D.PI * 2);
            Assert.AreEqual(angle.Degrees, 360);
            Assert.AreEqual(angle.Revolutions, 1);

            angle.NegativeRadians = 0;
            Assert.AreEqual(angle.NegativeRadians, 0);
            Assert.AreEqual(angle.NegativeDegrees, 0);
            Assert.AreEqual(angle.NegativeRevolutions, 0);
            Assert.AreEqual(angle.Radians, Math1D.PI);
            Assert.AreEqual(angle.Degrees, 180);
            Assert.AreEqual(angle.Revolutions, .5f);

            angle.NegativeRadians = -Math1D.PI;
            Assert.AreEqual(angle.NegativeRadians, -Math1D.PI);
            Assert.AreEqual(angle.NegativeDegrees, -180);
            Assert.AreEqual(angle.NegativeRevolutions, -.5f);
            Assert.AreEqual(angle.Radians, 0);
            Assert.AreEqual(angle.Degrees, 0);
            Assert.AreEqual(angle.Revolutions, 0);

            // Setting degrees.
            angle.NegativeDegrees = 540;
            Assert.AreEqual(angle.NegativeRadians, Math1D.PI);
            Assert.AreEqual(angle.NegativeDegrees, 180);
            Assert.AreEqual(angle.NegativeRevolutions, .5f);
            Assert.AreEqual(angle.Radians, Math1D.PI * 2);
            Assert.AreEqual(angle.Degrees, 360);
            Assert.AreEqual(angle.Revolutions, 1);

            angle.NegativeDegrees = 0;
            Assert.AreEqual(angle.NegativeRadians, 0);
            Assert.AreEqual(angle.NegativeDegrees, 0);
            Assert.AreEqual(angle.NegativeRevolutions, 0);
            Assert.AreEqual(angle.Radians, Math1D.PI);
            Assert.AreEqual(angle.Degrees, 180);
            Assert.AreEqual(angle.Revolutions, .5f);

            angle.NegativeDegrees = -180;
            Assert.AreEqual(angle.NegativeRadians, -Math1D.PI);
            Assert.AreEqual(angle.NegativeDegrees, -180);
            Assert.AreEqual(angle.NegativeRevolutions, -.5f);
            Assert.AreEqual(angle.Radians, 0);
            Assert.AreEqual(angle.Degrees, 0);
            Assert.AreEqual(angle.Revolutions, 0);

            // Setting revolutions.
            angle.NegativeRevolutions = 1.5f;
            Assert.AreEqual(angle.NegativeRadians, Math1D.PI);
            Assert.AreEqual(angle.NegativeDegrees, 180);
            Assert.AreEqual(angle.NegativeRevolutions, .5f);
            Assert.AreEqual(angle.Radians, Math1D.PI * 2);
            Assert.AreEqual(angle.Degrees, 360);
            Assert.AreEqual(angle.Revolutions, 1);

            angle.NegativeRevolutions = 0;
            Assert.AreEqual(angle.NegativeRadians, 0);
            Assert.AreEqual(angle.NegativeDegrees, 0);
            Assert.AreEqual(angle.NegativeRevolutions, 0);
            Assert.AreEqual(angle.Radians, Math1D.PI);
            Assert.AreEqual(angle.Degrees, 180);
            Assert.AreEqual(angle.Revolutions, .5f);

            angle.NegativeRevolutions = -.5f;
            Assert.AreEqual(angle.NegativeRadians, -Math1D.PI);
            Assert.AreEqual(angle.NegativeDegrees, -180);
            Assert.AreEqual(angle.NegativeRevolutions, -.5f);
            Assert.AreEqual(angle.Radians, 0);
            Assert.AreEqual(angle.Degrees, 0);
            Assert.AreEqual(angle.Revolutions, 0);
        }

        [TestMethod]
        public void ConversionTests()
        {
            Angle angle = Math1D.PI * 3;
            AssertUtils.CloseEnough(angle, Math1D.PI);
            AssertUtils.CloseEnough(angle.Radians, Math1D.PI);
            AssertUtils.CloseEnough(angle.Degrees, 180);
            AssertUtils.CloseEnough(angle.Revolutions, .5f);
            
            angle = Math1D.PI / 2;
            AssertUtils.CloseEnough(angle, Math1D.PI / 2);
            AssertUtils.CloseEnough(angle.Radians, Math1D.PI / 2);
            AssertUtils.CloseEnough(angle.Degrees, 90);
            AssertUtils.CloseEnough(angle.Revolutions, .25f);
        }

        [TestMethod]
        public void EqualsTests()
        {
            Angle a = new Angle(180, AngleMeasurement.Degrees);
            Angle b = new Angle(Math1D.PI, AngleMeasurement.Radians);

            Assert.IsTrue(a == b);
            Assert.IsFalse(a != b);
            Assert.AreEqual(a, b);
            Assert.IsTrue(a.Equals(b));
            Assert.IsTrue(a.Equals((object)b));
            Assert.IsTrue(a.Equals((float)b));

            b = new Angle(Math1D.PI * 2, AngleMeasurement.Radians);

            Assert.IsFalse(a == b);
            Assert.IsTrue(a != b);
            Assert.AreNotEqual(a, b);
            Assert.IsFalse(a.Equals(b));
            Assert.IsFalse(a.Equals((object)b));
            Assert.IsFalse(a.Equals((float)b));
        }

        [TestMethod]
        public void BiggerSmallerThanTests()
        {
            Angle a = new Angle(180, AngleMeasurement.Degrees);
            Angle b = new Angle(90, AngleMeasurement.Degrees);

            Assert.IsTrue(a > b);
            Assert.IsFalse(b > a);
            Assert.IsTrue(Angle.BiggerThan(a, b));
            Assert.IsFalse(Angle.BiggerThan(b, a));

            Assert.IsTrue(b < a);
            Assert.IsFalse(a < b);
            Assert.IsTrue(b.SmallerThan(a));
            Assert.IsFalse(a.SmallerThan(b));

            Assert.IsFalse(a == b);
            Assert.IsTrue(a >= b);
            Assert.IsFalse(a <= b);

            Assert.IsFalse(b >= a);
            Assert.IsTrue(b <= a);

            a = new Angle(180, AngleMeasurement.Degrees);
            b = new Angle(180, AngleMeasurement.Degrees);

            Assert.IsFalse(a > b);
            Assert.IsFalse(b > a);
            Assert.IsFalse(a.BiggerThan(b));
            Assert.IsFalse(b.BiggerThan(a));

            Assert.IsFalse(b < a);
            Assert.IsFalse(a < b);
            Assert.IsFalse(Angle.SmallerThan(b, a));
            Assert.IsFalse(Angle.SmallerThan(a, b));

            Assert.IsTrue(a == b);
            Assert.IsTrue(a >= b);
            Assert.IsTrue(a <= b);

            Assert.IsTrue(b >= a);
            Assert.IsTrue(b <= a);
        }

        [TestMethod]
        public void CloseEnoughTests()
        {
            Angle a = new Angle(180, AngleMeasurement.Degrees);
            Angle b = new Angle(Math1D.PI, AngleMeasurement.Radians);

            Assert.IsTrue(a.CloseEnough(b));
            Assert.IsTrue(SMath.CloseEnough(a, b));
            AssertUtils.CloseEnough(a, b);

            a = new Angle(0, AngleMeasurement.Degrees);

            Assert.IsFalse(a.CloseEnough(b));
            Assert.IsFalse(SMath.CloseEnough(a, b));
            Assert.IsTrue(a.CloseEnough(b, Math1D.PI));
            Assert.IsTrue(SMath.CloseEnough(a, b, Math1D.PI));
        }

        [TestMethod]
        public void ScalingTests()
        {
            Angle angle = new Angle(180, AngleMeasurement.Degrees);
            float scalar = 2;
            Angle result1 = angle * scalar;
            Angle result2 = scalar * angle;

            Assert.AreEqual(result1, result2);
            Assert.AreEqual(result1, angle.Multiply(scalar));
            Assert.AreEqual(result1, Angle.Multiply(angle, scalar));
            Assert.AreEqual(result1, Angle.Multiply(scalar, angle));
            Assert.AreEqual(result1.Degrees, 360);
            Assert.AreEqual(result1.Radians, Math1D.PI * 2);
            Assert.AreEqual(result1.Revolutions, 1);

            result1 = angle / scalar;

            Assert.AreEqual(result1, Angle.Divide(angle, scalar));
            Assert.AreEqual(result1, angle.Divide(scalar));
            Assert.AreEqual(result1.Degrees, 90);
            Assert.AreEqual(result1.Radians, Math1D.PI / 2);
            Assert.AreEqual(result1.Revolutions, .25f);
        }

        [TestMethod]
        public void AdditionSubtractionTests()
        {
            Angle a = new Angle(180, AngleMeasurement.Degrees);
            Angle b = new Angle(270, AngleMeasurement.Degrees);
            Angle add = a + b;
            Angle sub1 = a - b;
            Angle sub2 = b - a;

            AssertUtils.CloseEnough(add, new Angle(90, AngleMeasurement.Degrees));
            AssertUtils.CloseEnough(add, a.Add(b));
            AssertUtils.CloseEnough(add, Angle.Add(a, b));

            Assert.AreEqual(sub1, new Angle(270, AngleMeasurement.Degrees));
            Assert.AreEqual(sub1, a.Subtract(b));
            Assert.AreEqual(sub1, Angle.Subtract(a, b));

            AssertUtils.CloseEnough(sub2, new Angle(90, AngleMeasurement.Degrees));
            AssertUtils.CloseEnough(sub2, new Angle(90, AngleMeasurement.Degrees));
            AssertUtils.CloseEnough(sub2, b.Subtract(a));
            AssertUtils.CloseEnough(sub2, Angle.Subtract(b, a));
        }

        [TestMethod]
        public void FlippingReflectingTests()
        {
            Angle angle = 0;
            Assert.AreEqual(angle.Flipped, new Angle(180, AngleMeasurement.Degrees));
            Assert.AreEqual(angle.Flipped, Angle.Flip(angle));
            Assert.AreEqual(-angle, Angle.fullRotation);
            Assert.AreEqual(-angle, angle.Reflection);
            Assert.AreEqual(-angle, Angle.Reflect(angle));

            angle = Math1D.PI / 2;
            Assert.AreEqual(angle.Flipped, new Angle(270, AngleMeasurement.Degrees));
            Assert.AreEqual(angle.Flipped, Angle.Flip(angle));
            Assert.AreEqual(-angle, Angle.straightAndHalf);
            Assert.AreEqual(-angle, angle.Reflection);
            Assert.AreEqual(-angle, Angle.Reflect(angle));

            angle = Math1D.PI;
            Assert.AreEqual(angle.Flipped, 0);
            Assert.AreEqual(angle.Flipped, Angle.Flip(angle));
            Assert.AreEqual(-angle, Angle.straight);
            Assert.AreEqual(-angle, angle.Reflection);
            Assert.AreEqual(-angle, Angle.Reflect(angle));
            
            angle = Math1D.PI * 1.5f;
            AssertUtils.CloseEnough(angle.Flipped, new Angle(90, AngleMeasurement.Degrees));
            Assert.AreEqual(angle.Flipped, Angle.Flip(angle));
            AssertUtils.CloseEnough(-angle, Angle.right);
            Assert.AreEqual(-angle, angle.Reflection);
            Assert.AreEqual(-angle, Angle.Reflect(angle));

            angle = Math1D.PI * 2;
            Assert.AreEqual(angle.Flipped, new Angle(180, AngleMeasurement.Degrees));
            Assert.AreEqual(angle.Flipped, Angle.Flip(angle));
            Assert.AreEqual(-angle, Angle.zero);
            Assert.AreEqual(-angle, angle.Reflection);
            Assert.AreEqual(-angle, Angle.Reflect(angle));
        }

        [TestMethod]
        public void ClampTest()
        {
            Angle angle = new Angle(45, AngleMeasurement.Degrees);
            Angle min = new Angle(20, AngleMeasurement.Degrees);
            Angle max = new Angle(90, AngleMeasurement.Degrees);

            Assert.AreEqual(angle.Clamp(min, max), new Angle(45, AngleMeasurement.Degrees));
            Assert.AreEqual(SMath.Clamp(angle, min, max), new Angle(45, AngleMeasurement.Degrees));

            min = new Angle(60, AngleMeasurement.Degrees);
            Assert.AreEqual(angle.Clamp(min, max), new Angle(60, AngleMeasurement.Degrees));
            Assert.AreEqual(SMath.Clamp(angle, min, max), new Angle(60, AngleMeasurement.Degrees));

            angle = new Angle(180, AngleMeasurement.Degrees);
            Assert.AreEqual(angle.Clamp(min, max), new Angle(90, AngleMeasurement.Degrees));
            Assert.AreEqual(SMath.Clamp(angle, min, max), new Angle(90, AngleMeasurement.Degrees));
        }

        [TestMethod]
        public void LerpTests()
        {
            Angle source = new Angle(180, AngleMeasurement.Degrees);
            Angle target = new Angle(360, AngleMeasurement.Degrees);

            AssertUtils.TooFar(source.Lerp(target, -1, false), Math1D.PI);
            AssertUtils.CloseEnough(source.Lerp(target, -1), Math1D.PI);
            AssertUtils.CloseEnough(source.Lerp(target, 0), Math1D.PI);
            AssertUtils.CloseEnough(source.Lerp(target, .5f), Math1D.PI * 1.5f);
            AssertUtils.CloseEnough(source.Lerp(target, 1), Math1D.PI * 2);
            AssertUtils.CloseEnough(source.Lerp(target, 2), Math1D.PI * 2);
            AssertUtils.TooFar(source.Lerp(target, 2, false), Math1D.PI * 2);

            Angle difference = source - target;
            Test01((alpha) =>
            {
                Angle result1 = SMath.Lerp(source, target, alpha);
                Angle result2 = source + alpha * difference;

                Assert.IsTrue(result1.CloseEnough(result2));
            });
        }
    }
}