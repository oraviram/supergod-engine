﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class BColorTests : SupergodTestClass
    {
        [TestMethod]
        public void ConstructorTest()
        {
            Test50((i) =>
            {
                byte r = RandByte();
                byte g = RandByte();
                byte b = RandByte();
                byte a = RandByte();

                BColor color = new BColor(r, g, b, a);
                Assert.AreEqual(r, color.r);
                Assert.AreEqual(g, color.g);
                Assert.AreEqual(b, color.b);
                Assert.AreEqual(a, color.a);

                uint uintValue = RandUInt();

                color = uintValue;
                Assert.AreEqual(color, new BColor(uintValue));
                Assert.AreEqual(color.UIntValue, uintValue);
                Assert.AreEqual((uint)color, uintValue);
            });
        }
    }
}