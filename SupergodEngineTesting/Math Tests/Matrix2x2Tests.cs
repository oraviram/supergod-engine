﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class Matrix2x2Tests : SupergodTestClass
    {
        [TestMethod]
        public void ConstructorTest()
        {
            Matrix2x2 matrix = new Matrix2x2(1, 3, 2, -1);
            Assert.IsTrue(matrix.r0c0 == 1 && matrix.r0c1 == 3 && matrix.r1c0 == 2 && matrix.r1c1 == -1);

            matrix = new Matrix2x2(.5f, 1);
            Assert.IsTrue(matrix.r0c0 == .5f && matrix.r0c1 == 1 && matrix.r1c0 == 0 && matrix.r1c1 == 1);
            
            matrix = new Matrix2x2(1);
            Assert.IsTrue(
                matrix.r0c0 == Matrix2x2.identity.r0c0 && matrix.r0c1 == Matrix2x2.identity.r0c1
                && matrix.r1c0 == Matrix2x2.identity.r1c0 && matrix.r1c1 == Matrix2x2.identity.r1c1);

            matrix = new Matrix2x2();
            Assert.IsTrue(
                matrix.r0c0 == Matrix2x2.zero.r0c0 && matrix.r0c1 == Matrix2x2.zero.r0c1
                && matrix.r1c0 == Matrix2x2.zero.r1c0 && matrix.r1c1 == Matrix2x2.zero.r1c1);

            matrix = Matrix2x2.FromRows(new Vector2D(1, 3), new Vector2D(2, -1));
            Assert.IsTrue(matrix.r0c0 == 1 && matrix.r0c1 == 3 && matrix.r1c0 == 2 && matrix.r1c1 == -1);

            matrix = Matrix2x2.FromColumns(new Vector2D(1, 3), new Vector2D(2, -1));
            Assert.IsTrue(matrix.r0c0 == 1 && matrix.r0c1 == 2 && matrix.r1c0 == 3 && matrix.r1c1 == -1);
        }

        #region Equals tests.
        [TestMethod]
        public void EqualsTest()
        {
            Assert.IsTrue(new Matrix2x2(0, 1, 0, 1) == new Matrix2x2(0, 1, 0, 1));
            Assert.IsFalse(new Matrix2x2(0, 1, 0, 1) == new Matrix2x2(1, 0, 1, 0));
            Assert.IsTrue(new Matrix2x2(0, 4, 2, 1).Equals(new Matrix2x2(0, 4, 2, 1)));
            Assert.IsFalse(new Matrix2x2(0, 0, 1, 1).Equals(new Matrix2x2(0, 1, 0, 0)));
            Assert.IsTrue(new Matrix2x2(0, 4, 2, 1).Equals((object)new Matrix2x2(0, 4, 2, 1)));
            Assert.IsFalse(new Matrix2x2(0, 0, 1, 1).Equals((object)new Matrix2x2(0, 1, 0, 0)));
            Assert.IsFalse(new Matrix2x2(0, 1, 1, 1).Equals(1));
        }

        [TestMethod]
        public void NotEqualsTest()
        {
            Assert.IsTrue(new Matrix2x2(1, 1, 1, 1) != new Matrix2x2());
            Assert.IsFalse(new Matrix2x2(1, 1, 1, 1) != new Matrix2x2(1, 1, 1, 1));
        }

        [TestMethod]
        public void CloseEnoughTest()
        {
            Matrix2x2 a = new Matrix2x2(1, 2, 3, 4);
            Matrix2x2 b = new Matrix2x2(4, 3, .2f, -.1f);
            Assert.IsFalse(a.CloseEnough(b));
            Assert.IsFalse(SMath.CloseEnough(a, b));

            a = new Matrix2x2(1, 2, 3, 4);
            b = new Matrix2x2(1, 2, 3, 4);
            Assert.IsTrue(a.CloseEnough(b));
            Assert.IsTrue(SMath.CloseEnough(a, b));

            a = new Matrix2x2(1, 2, 3, 4);
            b = new Matrix2x2(2, 3, 4, 5);
            Assert.IsFalse(a.CloseEnough(b));
            Assert.IsFalse(SMath.CloseEnough(a, b));
            Assert.IsTrue(a.CloseEnough(b, 1));
            Assert.IsTrue(SMath.CloseEnough(a, b, 1));
        }
        #endregion

        #region Special getters/setters and arrays.
        [TestMethod]
        public void IndexerTest()
        {
            Matrix2x2 getTest = new Matrix2x2(3, 1, .4f, 2);
            Assert.IsTrue(getTest[0, 0] == 3);
            Assert.IsTrue(getTest[0, 1] == 1);
            Assert.IsTrue(getTest[1, 0] == .4f);
            Assert.IsTrue(getTest[1, 1] == 2);

            Matrix2x2 setTest = new Matrix2x2();
            for (int row = 0; row < 2; row++)
            {
                for (int column = 0; column < 2; column++)
                {
                    setTest[row, column] = row * column;
                }
            }
            Assert.IsTrue(setTest == new Matrix2x2(0, 0, 0, 1));

            for (int row = 0; row < 2; row++)
            {
                for (int column = 0; column < 2; column++)
                {
                    setTest[row, column] = 5;
                }
            }
            Assert.IsTrue(setTest == new Matrix2x2(5, 5, 5, 5));
        }

        [TestMethod]
        public void GettersAndSettersTests()
        {
            Matrix2x2 matrix = new Matrix2x2();

            matrix.SetRow(0, new Vector2D(2, 3));
            Assert.AreEqual(matrix.GetRow(0), new Vector2D(2, 3));
            Assert.AreEqual(matrix.r0c0, 2);
            Assert.AreEqual(matrix.r0c1, 3);
            Assert.AreEqual(matrix.r1c0, 0);
            Assert.AreEqual(matrix.r1c1, 0);

            matrix.SetRow(1, new Vector2D(4, 5));
            Assert.AreEqual(matrix.GetRow(1), new Vector2D(4, 5));
            Assert.AreEqual(matrix.r0c0, 2);
            Assert.AreEqual(matrix.r0c1, 3);
            Assert.AreEqual(matrix.r1c0, 4);
            Assert.AreEqual(matrix.r1c1, 5);

            matrix.SetColumn(0, new Vector2D(6, 7));
            Assert.AreEqual(matrix.GetColumn(0), new Vector2D(6, 7));
            Assert.AreEqual(matrix.r0c0, 6);
            Assert.AreEqual(matrix.r1c0, 7);
            Assert.AreEqual(matrix.r0c1, 3);
            Assert.AreEqual(matrix.r1c1, 5);

            matrix.SetColumn(1, new Vector2D(8, 9));
            Assert.AreEqual(matrix.GetColumn(1), new Vector2D(8, 9));
            Assert.AreEqual(matrix.r0c0, 6);
            Assert.AreEqual(matrix.r1c0, 7);
            Assert.AreEqual(matrix.r0c1, 8);
            Assert.AreEqual(matrix.r1c1, 9);
        }

        [TestMethod]
        public void AsArraysTests()
        {
            Matrix2x2 matrix = new Matrix2x2(2, 43, 3, -1);

            float[,] asFloatArray = matrix.AsFloatArray;
            Assert.AreEqual(asFloatArray[0, 0], 2);
            Assert.AreEqual(asFloatArray[0, 1], 43);
            Assert.AreEqual(asFloatArray[1, 0], 3);
            Assert.AreEqual(asFloatArray[1, 1], -1);

            Vector2D[] rows = matrix.Rows;
            Assert.AreEqual(rows[0], new Vector2D(2, 43));
            Assert.AreEqual(rows[1], new Vector2D(3, -1));

            Vector2D[] columns = matrix.Columns;
            Assert.AreEqual(columns[0], new Vector2D(2, 3));
            Assert.AreEqual(columns[1], new Vector2D(43, -1));
        }
        #endregion

        #region Basic math operation tests.
        [TestMethod]
        public void MultiplicationTest()
        {
            Matrix2x2 a = new Matrix2x2(
                2, 34,
                -1, 10);
            Matrix2x2 b = new Matrix2x2(
                2, -2,
                .3f, 32.3f);
            Matrix2x2 result1 = a * b;
            Matrix2x2 result2 = b * a;

            Assert.AreNotEqual(result1, result2);
            AssertUtils.CloseEnough(result1, new Matrix2x2(14.2f, 1094.2f, 1, 325));
            AssertUtils.CloseEnough(result2, new Matrix2x2(6, 48, -31.7f, 333.2f));
            
            Assert.AreEqual(result1, a.Multiply(b));
            Assert.AreEqual(result1, Matrix.Multiply(a, b));
            
            Assert.AreEqual(result2, b.Multiply(a));
            Assert.AreEqual(result2, Matrix.Multiply(b, a));
        }

        [TestMethod]
        public void ScalarMultiplicationTest()
        {
            Matrix2x2 matrix = new Matrix2x2(3, -1, 2, 10);
            float scalar = 3.5f;
            Matrix2x2 result = matrix * scalar;

            Assert.AreEqual(result, new Matrix2x2(10.5f, -3.5f, 7, 35));
            Assert.AreEqual(result, scalar * matrix);
            Assert.AreEqual(result, Matrix.Multiply(scalar, matrix));
            Assert.AreEqual(result, Matrix.Multiply(matrix, scalar));
            Assert.AreEqual(result, matrix.Multiply(scalar));

            matrix = new Matrix2x2(4, -322, 1, -1.5f);
            scalar = -2;
            result = matrix * scalar;

            Assert.AreEqual(result, new Matrix2x2(-8, 644, -2, 3));
            Assert.AreEqual(result, scalar * matrix);
            Assert.AreEqual(result, Matrix.Multiply(scalar, matrix));
            Assert.AreEqual(result, Matrix.Multiply(matrix, scalar));
            Assert.AreEqual(result, matrix.Multiply(scalar));
        }

        [TestMethod]
        public void VectorMatrixMultiplicationTest()
        {
            Vector2D vector = new Vector2D(3, 2);
            Matrix2x2 matrix = new Matrix2x2(0, 1, -4, .5f);
            Assert.AreEqual(vector * matrix, new Vector2D(-8, 4));

            Assert.AreEqual(matrix * vector, new Vector2D(2, -11));
            Assert.AreEqual(matrix.Multiply(vector), new Vector2D(2, -11));

            vector = new Vector2D(0, .5f);
            matrix = new Matrix2x2(2, 1, 5, -1);
            Assert.AreEqual(vector * matrix, new Vector2D(2.5f, -.5f));
            Assert.AreEqual(matrix * vector, new Vector2D(.5f, -.5f));
        }

        [TestMethod]
        public void AdditionSubtractionTests()
        {
            Matrix2x2 a = new Matrix2x2(2, 3, 4, 10);
            Matrix2x2 b = new Matrix2x2(-1.5f, -5, 3.5f, 3);
            Matrix2x2 result = a + b;

            Assert.AreEqual(result, new Matrix2x2(.5f, -2, 7.5f, 13));
            Assert.AreEqual(result, b + a);
            Assert.AreEqual(result, a.Add(b));
            Assert.AreEqual(result, b.Add(a));
            Assert.AreEqual(result, Matrix.Add(a, b));
            Assert.AreEqual(result, Matrix.Add(b, a));

            result = a - b;
            Matrix2x2 result2 = b - a;

            Assert.AreEqual(result, new Matrix2x2(3.5f, 8, .5f, 7));
            Assert.AreEqual(result2, new Matrix2x2(-3.5f, -8, -.5f, -7));
            Assert.AreNotEqual(result, result2);

            Assert.AreEqual(result, a.Subtract(b));
            Assert.AreEqual(result2, b.Subtract(a));
            Assert.AreEqual(result, Matrix.Subtract(a, b));
            Assert.AreEqual(result2, Matrix.Subtract(b, a));
        }
        #endregion

        #region Negate, transposing, inversing and determinant tests.
        [TestMethod]
        public void NegatingTest()
        {
            Matrix2x2 matrix = new Matrix2x2(2, -1, .2f, -32.1f);
            Matrix2x2 result = -matrix;

            Assert.AreEqual(result, new Matrix2x2(-2, 1, -.2f, 32.1f));
            Assert.AreNotEqual(result, matrix);

            Assert.AreEqual(result, matrix.Negated);
            Assert.AreEqual(result, Matrix.Negate(matrix));
            Assert.AreEqual(result, matrix * -1);
        }

        [TestMethod]
        public void TransposingTest()
        {
            Matrix2x2 matrix = new Matrix2x2(
                2, -1,
                3, 8);

            Matrix2x2 transposed = matrix.Transposed;

            Assert.AreNotEqual(transposed, matrix);
            Assert.AreEqual(transposed, Matrix.Transpose(matrix));
            Assert.AreEqual(transposed, new Matrix2x2(
                2, 3,
                -1, 8));

            Vector2D vectorTest = new Vector2D(2, -1);
            Assert.AreEqual(matrix * vectorTest, vectorTest * matrix.Transposed);
            Assert.AreEqual(vectorTest * matrix, matrix.Transposed * vectorTest);
        }

        [TestMethod]
        public void DeterminantTest()
        {
            Matrix2x2 matrix = new Matrix2x2(2, -1, 2.5f, -10.12f);
            float det = matrix.Determinant;

            Assert.AreEqual(det, -17.74f);
            Assert.AreEqual(det, Matrix.Det(matrix));

            matrix = Matrix2x2.Rotate(90 * Angle.Deg2Rad);
            Assert.AreEqual(matrix.Determinant, 1);

            matrix = Matrix2x2.Shear(1, 1);
            Assert.AreEqual(matrix.Determinant, 0);

            matrix = Matrix2x2.Scale(Vector2D.one * 2);
            Assert.AreEqual(matrix.Determinant, 4);

            matrix = Matrix2x2.Scale(2, 1);
            Assert.AreEqual(matrix.Determinant, 2);

            matrix = Matrix2x2.Scale(.5f, 10);
            Assert.AreEqual(matrix.Determinant, 5);
        }

        [TestMethod]
        public void InverseTest()
        {
            Matrix2x2 matrix = new Matrix2x2(20, -15, 5, 6);
            Matrix2x2 inverse = matrix.Inverted;
            Assert.AreEqual(inverse, Matrix.Invert(matrix));

            AssertUtils.CloseEnough(matrix * inverse, Matrix2x2.identity);
            AssertUtils.CloseEnough(inverse * matrix, Matrix2x2.identity);

            matrix = new Matrix2x2(432, -321.321f, 421.132f, -32132);
            inverse = matrix.Inverted;
            Assert.AreEqual(inverse, Matrix.Invert(matrix));

            AssertUtils.CloseEnough(matrix * inverse, Matrix2x2.identity);
            AssertUtils.CloseEnough(inverse * matrix, Matrix2x2.identity);

            matrix = Matrix2x2.Rotate(Math1D.PI / 2);
            Vector2D vector = new Vector2D(1, 0);
            Vector2D transformed = matrix * vector;

            AssertUtils.CloseEnough(transformed, new Vector2D(0, 1));
            AssertUtils.CloseEnough(matrix.Inverted * transformed, new Vector2D(1, 0));
        }
        #endregion

        #region Clamping tests.
        [TestMethod]
        public void ClampTest()
        {
            Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);
            Matrix2x2 min = new Matrix2x2(-1, 2, 3, 5);
            Matrix2x2 max = new Matrix2x2(0, 3, 4, 8);

            Matrix2x2 clamped = matrix.Clamp(min, max);

            Assert.AreEqual(clamped, new Matrix2x2(0, 2, 3, 5));
            Assert.AreEqual(clamped, SMath.Clamp(matrix, min, max));

            matrix = new Matrix2x2(1, 2, 3, 4);
            min = new Matrix2x2(5, 3, 0, 0);
            max = new Matrix2x2(5, 4, 2, 2);
            
            clamped = matrix.Clamp(min, max);

            Assert.AreEqual(clamped, new Matrix2x2(5, 3, 2, 2));
            Assert.AreEqual(clamped, SMath.Clamp(matrix, min, max));
        }

        [TestMethod]
        public void ClampElementsTest()
        {
            Matrix2x2 matrix = new Matrix2x2(3213, 2, 10, -1);
            float min = 15;
            float max = 3000;

            Assert.AreEqual(matrix.ClampElements(min, max), Matrix.ClampElements(matrix, min, max));
            Assert.AreEqual(matrix.ClampElements(min, max), new Matrix2x2(3000, 15, 15, 15));

            min = 0;
            max = 5;
            Assert.AreEqual(matrix.ClampElements(min, max), Matrix.ClampElements(matrix, min, max));
            Assert.AreEqual(matrix.ClampElements(min, max), new Matrix2x2(5, 2, 5, 0));
        }

        [TestMethod]
        public void ClampRowsTest()
        {
            Matrix2x2 matrix = new Matrix2x2(
                2423, 2,
                10, -1);
            Vector2D min = new Vector2D(3000, 1);
            Vector2D max = new Vector2D(3500, 5);

            Assert.AreEqual(matrix.ClampRows(min, max), Matrix.ClampRows(matrix, min, max));
            Assert.AreEqual(matrix.ClampRows(min, max), new Matrix2x2(
                3000, 2,
                3000, 1));

            min = new Vector2D(5, -2);
            max = new Vector2D(7, 0);
            Assert.AreEqual(matrix.ClampRows(min, max), Matrix.ClampRows(matrix, min, max));
            Assert.AreEqual(matrix.ClampRows(min, max), new Matrix2x2(
                7, 0,
                7, -1));
        }

        [TestMethod]
        public void ClampColumnsTest()
        {
            Matrix2x2 matrix = new Matrix2x2(
                3356, 2,
                10, -1);
            Vector2D min = new Vector2D(
                3000,
                1);
            Vector2D max = new Vector2D(
                3500,
                5);

            Assert.AreEqual(matrix.ClampColumns(min, max), Matrix.ClampColumns(matrix, min, max));
            Assert.AreEqual(matrix.ClampColumns(min, max), new Matrix2x2(
                3356, 3000,
                5, 1));

            min = new Vector2D(
                5,
                -2);
            max = new Vector2D(
                7,
                0);
            Assert.AreEqual(matrix.ClampColumns(min, max), Matrix.ClampColumns(matrix, min, max));
            Assert.AreEqual(matrix.ClampColumns(min, max), new Matrix2x2(
                7, 5,
                0, -1));
        }
        #endregion

        #region Transformation tests.
        [TestMethod]
        public void ShearTest()
        {
            Matrix2x2 matrix = Matrix2x2.Shear(new Vector2D(2, 0));
            Vector2D vector = new Vector2D(1, 1);
            Assert.AreEqual(matrix * vector, new Vector2D(3, 1));

            matrix = Matrix2x2.Shear(-1, 5);
            vector = new Vector2D(2, 2);
            Assert.AreEqual(matrix * vector, new Vector2D(0, 12));
        }

        [TestMethod]
        public void ScaleTests()
        {
            Matrix2x2 matrix = Matrix2x2.Scale(Vector2D.one);
            Assert.AreEqual(matrix, Matrix2x2.identity);

            matrix = Matrix2x2.Scale(Vector2D.zero);
            Assert.AreEqual(matrix, Matrix2x2.zero);

            matrix = Matrix2x2.Scale(2, 3);
            Assert.AreEqual(matrix * new Vector2D(3, 2), new Vector2D(6, 6));

            matrix = Matrix2x2.Scale(3, -1);
            Assert.AreEqual(matrix * new Vector2D(0, 10), new Vector2D(0, -10));
        }

        [TestMethod]
        public void RotationTest()
        {
            Matrix2x2 op = Matrix2x2.Rotate(0);
            Assert.IsTrue(op.r0c0 == 1);
            Assert.IsTrue(op.r0c1 == 0);
            Assert.IsTrue(op.r1c0 == 0);
            Assert.IsTrue(op.r1c1 == 1);

            op = Matrix2x2.Rotate(Math1D.PI);
            Assert.IsTrue(op.r0c0 == -1);
            Assert.IsTrue(SMath.CloseEnough(op.r0c1, 0));
            Assert.IsTrue(SMath.CloseEnough(op.r1c0, 0));
            Assert.IsTrue(op.r1c1 == -1);

            op = Matrix2x2.Rotate(Math1D.PI / 2);
            Assert.IsTrue(SMath.CloseEnough(op.r0c0, 0));
            Assert.IsTrue(op.r0c1 == -1);
            Assert.IsTrue(op.r1c0 == 1);
            Assert.IsTrue(SMath.CloseEnough(op.r1c1, 0));

            const float sqrt2Over2 = 0.70710678118f;
            op = Matrix2x2.Rotate(Math1D.PI / 4);
            Assert.IsTrue(SMath.CloseEnough(op.r0c0, sqrt2Over2));
            Assert.IsTrue(SMath.CloseEnough(op.r0c1, -sqrt2Over2));
            Assert.IsTrue(SMath.CloseEnough(op.r1c0, sqrt2Over2));
            Assert.IsTrue(SMath.CloseEnough(op.r1c1, sqrt2Over2));
            
            op = Matrix2x2.Rotate(-Math1D.PI / 4);
            Assert.IsTrue(SMath.CloseEnough(op.r0c0, sqrt2Over2));
            Assert.IsTrue(SMath.CloseEnough(op.r0c1, sqrt2Over2));
            Assert.IsTrue(SMath.CloseEnough(op.r1c0, -sqrt2Over2));
            Assert.IsTrue(SMath.CloseEnough(op.r1c1, sqrt2Over2));

            const float sqrt3Over2 = 0.86602540378f;
            op = Matrix2x2.Rotate(Math1D.PI / 3);
            Assert.IsTrue(SMath.CloseEnough(op.r0c0, .5f));
            Assert.IsTrue(SMath.CloseEnough(op.r0c1, -sqrt3Over2));
            Assert.IsTrue(SMath.CloseEnough(op.r1c0, sqrt3Over2));
            Assert.IsTrue(SMath.CloseEnough(op.r1c1, .5f));

            op = Matrix2x2.Rotate(-Math1D.PI / 3);
            Assert.IsTrue(SMath.CloseEnough(op.r0c0, .5f));
            Assert.IsTrue(SMath.CloseEnough(op.r0c1, sqrt3Over2));
            Assert.IsTrue(SMath.CloseEnough(op.r1c0, -sqrt3Over2));
            Assert.IsTrue(SMath.CloseEnough(op.r1c1, .5f));
        }
        #endregion

        [TestMethod]
        public void AbsTest()
        {
            Matrix2x2 matrix = new Matrix2x2(2, -1, 2, .1f);
            Assert.AreEqual(matrix.Abs, Matrix.Abs(matrix));
            Assert.AreEqual(matrix.Abs, new Matrix2x2(2, 1, 2, .1f));

            matrix = new Matrix2x2(-3, 1, -.2f, -102.2f);
            Assert.AreEqual(matrix.Abs, Matrix.Abs(matrix));
            Assert.AreEqual(matrix.Abs, new Matrix2x2(3, 1, .2f, 102.2f));
        }

        [TestMethod]
        public void LerpTest()
        {
            Matrix2x2 a = new Matrix2x2(-10, -5, 2, 3);
            Matrix2x2 b = new Matrix2x2(2, .5f, 344, 324.432f);
            Matrix2x2 difference = b - a;

            Test01((alpha) =>
            {
                Matrix2x2 result1 = SMath.Lerp(a, b, alpha);
                Matrix2x2 result1NonStatic = a.Lerp(b, alpha);
                Matrix2x2 result2 = a + alpha * difference;

                Assert.IsTrue(result1.r0c0.CloseEnough(result2.r0c0));
                Assert.IsTrue(result1.r0c1.CloseEnough(result2.r0c1));
                Assert.IsTrue(result1.r1c0.CloseEnough(result2.r1c0));
                Assert.IsTrue(result1.r1c1.CloseEnough(result2.r1c1));

                Assert.IsTrue(result1NonStatic.r0c0.CloseEnough(result2.r0c0));
                Assert.IsTrue(result1NonStatic.r0c1.CloseEnough(result2.r0c1));
                Assert.IsTrue(result1NonStatic.r1c0.CloseEnough(result2.r1c0));
                Assert.IsTrue(result1NonStatic.r1c1.CloseEnough(result2.r1c1));
            });
        }

        [TestMethod]
        public void ConversionTests()
        {
            Test50((i) =>
            {
                Matrix2x2 matrix = new Matrix2x2(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());

                Matrix3x3 threeBy3 = matrix;
                Matrix4x4 fourBy4 = matrix;

                Assert.AreEqual(threeBy3.r2c2, 1);
                Assert.AreEqual(fourBy4.r2c2, 1);
                Assert.AreEqual(fourBy4.r3c3, 1);

                for (int r = 0; r < 2; r++)
                {
                    for (int c = 0; c < 2; c++)
                    {
                        Assert.AreEqual(threeBy3[r, c], matrix[r, c]);
                        Assert.AreEqual(fourBy4[r, c], matrix[r, c]);
                    }
                }
            });
        }
    }
}