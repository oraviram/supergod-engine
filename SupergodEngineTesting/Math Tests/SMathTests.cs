﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class SMathTests : SupergodTestClass
    {
        [TestMethod]
        public void LerpTest()
        {
            float test = SMath.Lerp(-10, 10, -1);
            Assert.AreEqual(test, -10);

            test = SMath.Lerp(-10, 10, 0);
            Assert.AreEqual(test, -10);

            test = SMath.Lerp(-10, 10, .25f);
            Assert.AreEqual(test, -5);

            test = SMath.Lerp(-10, 10, .5f);
            Assert.AreEqual(test, 0);

            test = SMath.Lerp(-10, 10, .75f);
            Assert.AreEqual(test, 5);

            test = SMath.Lerp(-10, 10, 1);
            Assert.AreEqual(test, 10);

            test = SMath.Lerp(-10, 10, 2);
            Assert.AreEqual(test, 10);
        }
    }
}