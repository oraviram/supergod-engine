﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class TransformTests : SupergodTestClass
    {
        [TestMethod]
        public void ConstructionTest()
        {
            Test50((i) =>
            {
                Vector3D position = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());
                Quaternion rotation = new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100()).Normalized;
                Vector3D scale = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                Transform transform = new Transform(position, rotation, scale);
                Assert.AreEqual(transform.position, position);
                Assert.AreEqual(transform.rotation, rotation);
                Assert.AreEqual(transform.scale, scale);

                // TODO: The commented out check here usually works, but there are a few cases where it doesn't.
                // I should look into it. What I assume the problem is is that EulerAngles.FromRotationQuaternion doesn't do checks for speial cases, but I won't worry about it for now.
                // I am just putting this here so I remember to check it later.

                EulerAngles eulerAngles = new EulerAngles(RandAngle(), RandAngle(), RandAngle());
                transform = new Transform(position, eulerAngles, scale);
                Assert.AreEqual(transform.position, position);
                //AssertUtils.CloseEnough(transform.rotation.EulerAngles, eulerAngles);
                Assert.AreEqual(transform.scale, scale);

                AxisAngle axisAngle = new AxisAngle(new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()).Normalized, RandAngle());
                transform = new Transform(position, axisAngle, scale);
                Assert.AreEqual(transform.position, position);
                AssertUtils.CloseEnough(transform.rotation.AxisAngle, axisAngle, .03f);
                Assert.AreEqual(transform.scale, scale);
            });
        }

        [TestMethod]
        public void SpecificDirectionTests()
        {
            #region Getter tests.
            // Rotation around x axis.
            Transform transform = Transform.identity;
            Assert.IsTrue(transform.Up == Vector3D.up);
            Assert.IsTrue(transform.Forward == Vector3D.forward);
            Assert.IsTrue(transform.Right == Vector3D.right);

            transform.rotation = new EulerAngles(Math1D.PI / 2, 0, 0).Quaternion;
            Assert.IsTrue(Vector3D.forward.CloseEnough(transform.Up));
            Assert.IsTrue(Vector3D.down.CloseEnough(transform.Forward));
            Assert.IsTrue(Vector3D.right.CloseEnough(transform.Right));

            transform.rotation = new EulerAngles(Math1D.PI, 0, 0).Quaternion;
            Assert.IsTrue(Vector3D.down.CloseEnough(transform.Up));
            Assert.IsTrue(Vector3D.back.CloseEnough(transform.Forward));
            Assert.IsTrue(Vector3D.right.CloseEnough(transform.Right));

            transform.rotation = new EulerAngles(Math1D.PI * 1.5f, 0, 0).Quaternion;
            Assert.IsTrue(Vector3D.back.CloseEnough(transform.Up));
            Assert.IsTrue(Vector3D.up.CloseEnough(transform.Forward));
            Assert.IsTrue(Vector3D.right.CloseEnough(transform.Right));

            // Rotation around y axis.
            transform = Transform.identity;

            transform.rotation = new EulerAngles(0, Math1D.PI / 2, 0).Quaternion;
            AssertUtils.CloseEnough(Vector3D.up, transform.Up);
            AssertUtils.CloseEnough(Vector3D.right, transform.Forward);
            AssertUtils.CloseEnough(Vector3D.back, transform.Right);

            transform.rotation = new EulerAngles(0, Math1D.PI, 0).Quaternion;
            Assert.IsTrue(Vector3D.up.CloseEnough(transform.Up));
            Assert.IsTrue(Vector3D.back.CloseEnough(transform.Forward));
            Assert.IsTrue(Vector3D.left.CloseEnough(transform.Right));

            transform.rotation = new EulerAngles(0, Math1D.PI * 1.5f, 0).Quaternion;
            Assert.IsTrue(Vector3D.up.CloseEnough(transform.Up));
            Assert.IsTrue(Vector3D.left.CloseEnough(transform.Forward));
            Assert.IsTrue(Vector3D.forward.CloseEnough(transform.Right));

            // Rotating around z axis.
            transform = Transform.identity;

            transform.rotation = new EulerAngles(0, 0, Math1D.PI / 2).Quaternion;
            Assert.IsTrue(Vector3D.left.CloseEnough(transform.Up));
            Assert.IsTrue(Vector3D.forward.CloseEnough(transform.Forward));
            Assert.IsTrue(Vector3D.up.CloseEnough(transform.Right));

            transform.rotation = new EulerAngles(0, 0, Math1D.PI).Quaternion;
            Assert.IsTrue(Vector3D.down.CloseEnough(transform.Up));
            Assert.IsTrue(Vector3D.forward.CloseEnough(transform.Forward));
            Assert.IsTrue(Vector3D.left.CloseEnough(transform.Right));

            transform.rotation = new EulerAngles(0, 0, Math1D.PI * 1.5f).Quaternion;
            Assert.IsTrue(Vector3D.right.CloseEnough(transform.Up));
            Assert.IsTrue(Vector3D.forward.CloseEnough(transform.Forward));
            Assert.IsTrue(Vector3D.down.CloseEnough(transform.Right));
            #endregion

            Test50((i) =>
            {
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()).Normalized;
                transform.Forward = vector;
                AssertUtils.CloseEnough(vector, transform.Forward, .001f);

                transform.Back = vector;
                AssertUtils.CloseEnough(vector, transform.Back, .001f);

                transform.Up = vector;
                AssertUtils.CloseEnough(vector, transform.Up, .001f);

                transform.Down = vector;
                AssertUtils.CloseEnough(vector, transform.Down, .001f);

                transform.Right = vector;
                AssertUtils.CloseEnough(vector, transform.Right);

                transform.Left = vector;
                AssertUtils.CloseEnough(vector, transform.Left, .001f);
            });
        }
        
        #region Comparison tests.
        [TestMethod]
        public void EqualsTest()
        {
            Assert.IsTrue(new Transform(Vector3D.zero) == Transform.identity);
            Assert.IsFalse(new Transform(new Vector3D(1, 2, 3), new EulerAngles(5, 6, 7), new Vector3D(-1, -2, -3))
                == new Transform(new Vector3D(1, 2, 3), new EulerAngles(5, 6, 7), new Vector3D(-1, -2, -4)));

            Assert.IsTrue(new Transform(Vector3D.zero).Equals(Transform.identity));
            Assert.IsFalse(new Transform(new Vector3D(1, 2, 3), new EulerAngles(5, 6, 7), new Vector3D(-1, -2, -3))
                .Equals(new Transform(new Vector3D(1, 2, 3), new EulerAngles(5, 6, 7), new Vector3D(-1, -2, -4))));
        }

        [TestMethod]
        public void NotEqualTest()
        {
            Assert.IsFalse(new Transform(Vector3D.zero) != Transform.identity);
            Assert.IsTrue(Transform.identity != new Transform(new Vector3D(1, 2, 3), new EulerAngles(5, 6, 7), new Vector3D(-1, -2, -4)));
        }

        [TestMethod]
        public void CloseEnoughTest()
        {
            Transform a = new Transform(Vector3D.up, EulerAngles.zero, Vector3D.one * 2);
            Transform b = new Transform(Vector3D.up, EulerAngles.zero, Vector3D.one * 2);
            float threshold = Math1D.Epsilon;
            AssertUtils.CloseEnough(a, b, threshold);
            
            a = new Transform(Vector3D.up, EulerAngles.zero, Vector3D.one * 2);
            b = new Transform(Vector3D.up, EulerAngles.zero, Vector3D.one * 3);
            threshold = 1;
            AssertUtils.CloseEnough(a, b, threshold);

            a = new Transform(Vector3D.up, EulerAngles.zero, Vector3D.one * 2);
            b = new Transform(Vector3D.up, (EulerAngles)Vector3D.one, Vector3D.one * 2);
            threshold = 1;
            AssertUtils.CloseEnough(a, b, threshold);
            
            a = new Transform(Vector3D.up, EulerAngles.zero, Vector3D.one * 2);
            b = new Transform(Vector3D.down, EulerAngles.zero, Vector3D.one * 2);
            threshold = 2;
            AssertUtils.CloseEnough(a, b, threshold);
        }
        #endregion

        #region Transformations tests.
        [TestMethod]
        public void TransformVectorTest()
        {
            Test50((i) =>
            {
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());
                Transform transform = new Transform(
                    new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()),
                    new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100()).Normalized,
                    new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()));

                AssertUtils.CloseEnough(transform.ModelMatrix.TransformVector(vector), transform.TransformVector(vector), 0.005f);
            });
        }

        [TestMethod]
        public void CombinationTests()
        {
            Transform a = new Transform(
                    new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()),
                    new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100()).Normalized,
                    new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()));

            Transform b = new Transform(
                    new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()),
                    new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100()).Normalized,
                    new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()));

            Transform defaultCombination;
            AssertUtils.CloseEnough(defaultCombination = a.CombineTransformations(b), new Transform(
                b.TransformVector(a.position),
                a.rotation.CombineRotations(b.rotation),
                a.scale * b.scale
                ));

            AssertUtils.CloseEnough(defaultCombination, a.CombineTransformations(b, Transform.CombinationRules.Default));
            AssertUtils.CloseEnough(defaultCombination, a.CombineTransformations(b, Transform.CombinationRules.CombineAll));

            AssertUtils.CloseEnough(a.CombineTransformations(b, Transform.CombinationRules.Translate), new Transform(
                a.position + b.position,
                a.rotation,
                a.scale
                ));

            AssertUtils.CloseEnough(a.CombineTransformations(b, Transform.CombinationRules.FullPosition), new Transform(
                b.TransformVector(a.position),
                a.rotation,
                a.scale
                ));

            AssertUtils.CloseEnough(a.CombineTransformations(b, Transform.CombinationRules.RotatePosition), new Transform(
                b.rotation.RotateVector(a.position),
                a.rotation,
                a.scale
                ));

            AssertUtils.CloseEnough(a.CombineTransformations(b, Transform.CombinationRules.ScalePosition), new Transform(
                a.position * b.scale,
                a.rotation,
                a.scale
                ));

            AssertUtils.CloseEnough(a.CombineTransformations(b, Transform.CombinationRules.Rotate), new Transform(
                a.position,
                a.rotation.CombineRotations(b.rotation),
                a.scale
                ));

            AssertUtils.CloseEnough(a.CombineTransformations(b, Transform.CombinationRules.Scale), new Transform(
                a.position,
                a.rotation,
                a.scale * b.scale
                ));

            AssertUtils.CloseEnough(a.CombineTransformations(b, Transform.CombinationRules.DontCombine), new Transform(
                a.position,
                a.rotation,
                a.scale
                ));

            AssertUtils.CloseEnough(a.CombineTransformations(b, Transform.CombinationRules.Translate | Transform.CombinationRules.Rotate), new Transform(
                a.position + b.position,
                a.rotation.CombineRotations(b.rotation),
                a.scale
                ));

            AssertUtils.CloseEnough(a.CombineTransformations(b, Transform.CombinationRules.Translate | Transform.CombinationRules.Scale), new Transform(
                a.position + b.position,
                a.rotation,
                a.scale * b.scale
                ));

            AssertUtils.CloseEnough(a.CombineTransformations(b, Transform.CombinationRules.Rotate | Transform.CombinationRules.Scale), new Transform(
                a.position,
                a.rotation.CombineRotations(b.rotation),
                a.scale * b.scale
                ));
        }

        [TestMethod]
        public void DiscombinationTests()
        {
            Test50((i) =>
            {
                Transform a = new Transform(
                        new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()),
                        new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100()).Normalized,
                        new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()));

                Transform b = new Transform(
                        new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()),
                        new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100()).Normalized,
                        new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()));

                if (!b.scale.ContainsComponent(0))
                {
                    AssertUtils.CloseEnough(a.CombineTransformations(b).DiscombineTransformations(b), a);

                    Transform defaultDiscombination = a.DiscombineTransformations(b);
                    AssertUtils.CloseEnough(defaultDiscombination, a.DiscombineTransformations(b, Transform.CombinationRules.Default));
                    AssertUtils.CloseEnough(defaultDiscombination, a.DiscombineTransformations(b, Transform.CombinationRules.CombineAll));

                    AssertUtils.CloseEnough(a.DiscombineTransformations(b, Transform.CombinationRules.Translate), new Transform(
                        a.position - b.position,
                        a.rotation,
                        a.scale
                        ));

                    AssertUtils.CloseEnough(a.DiscombineTransformations(b, Transform.CombinationRules.FullPosition), new Transform(
                        b.ModelMatrix.Inverted.TransformVector(a.position),
                        a.rotation,
                        a.scale
                        ));

                    AssertUtils.CloseEnough(a.DiscombineTransformations(b, Transform.CombinationRules.RotatePosition), new Transform(
                        b.rotation.Inverted.RotateVector(a.position),
                        a.rotation,
                        a.scale
                        ));

                    AssertUtils.CloseEnough(a.DiscombineTransformations(b, Transform.CombinationRules.ScalePosition), new Transform(
                        a.position / b.scale,
                        a.rotation,
                        a.scale
                        ));

                    AssertUtils.CloseEnough(a.DiscombineTransformations(b, Transform.CombinationRules.Rotate), new Transform(
                        a.position,
                        a.rotation.CombineRotations(b.rotation.Inverted),
                        a.scale
                        ));

                    AssertUtils.CloseEnough(a.DiscombineTransformations(b, Transform.CombinationRules.Scale), new Transform(
                        a.position,
                        a.rotation,
                        a.scale / b.scale
                        ));

                    AssertUtils.CloseEnough(a.DiscombineTransformations(b, Transform.CombinationRules.DontCombine), new Transform(
                        a.position,
                        a.rotation,
                        a.scale
                        ));

                    AssertUtils.CloseEnough(a.DiscombineTransformations(b, Transform.CombinationRules.Translate | Transform.CombinationRules.Rotate), new Transform(
                        a.position - b.position,
                        a.rotation.CombineRotations(b.rotation.Inverted),
                        a.scale
                        ));

                    AssertUtils.CloseEnough(a.DiscombineTransformations(b, Transform.CombinationRules.Translate | Transform.CombinationRules.Scale), new Transform(
                        a.position - b.position,
                        a.rotation,
                        a.scale / b.scale
                        ));

                    AssertUtils.CloseEnough(a.DiscombineTransformations(b, Transform.CombinationRules.Rotate | Transform.CombinationRules.Scale), new Transform(
                        a.position,
                        a.rotation.CombineRotations(b.rotation.Inverted),
                        a.scale / b.scale
                        ));
                }
            });
        }
        #endregion
    }
}