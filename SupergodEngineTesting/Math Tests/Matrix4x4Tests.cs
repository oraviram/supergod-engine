﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class Matrix4x4Tests : SupergodTestClass
    {
        [TestMethod]
        public void ConstructorTest()
        {
            Matrix4x4 test1 = new Matrix4x4(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12,
                13, 14, 15, 16);

            Matrix4x4 test2 = Matrix4x4.FromRows(
                new Vector4D(1, 2, 3, 4),
                new Vector4D(5, 6, 7, 8),
                new Vector4D(9, 10, 11, 12),
                new Vector4D(13, 14, 15, 16));

            Assert.IsTrue(test1.CloseEnough(test2));

            Assert.AreEqual(test1.r0c0, 1);
            Assert.AreEqual(test1.r0c1, 2);
            Assert.AreEqual(test1.r0c2, 3);
            Assert.AreEqual(test1.r0c3, 4);

            Assert.AreEqual(test1.r1c0, 5);
            Assert.AreEqual(test1.r1c1, 6);
            Assert.AreEqual(test1.r1c2, 7);
            Assert.AreEqual(test1.r1c3, 8);

            Assert.AreEqual(test1.r2c0, 9);
            Assert.AreEqual(test1.r2c1, 10);
            Assert.AreEqual(test1.r2c2, 11);
            Assert.AreEqual(test1.r2c3, 12);

            Assert.AreEqual(test1.r3c0, 13);
            Assert.AreEqual(test1.r3c1, 14);
            Assert.AreEqual(test1.r3c2, 15);
            Assert.AreEqual(test1.r3c3, 16);
        }

        [TestMethod]
        public void IndexersTests()
        {
            Matrix4x4 matrix = new Matrix4x4(
                0, 1, 2, 3,
                4, 5, 6, 7,
                8, 9, 10, 11,
                12, 13, 14, 15
                );

            Assert.AreEqual(matrix.GetColumn(0), new Vector4D(0, 4, 8, 12));
            Assert.AreEqual(matrix.GetColumn(1), new Vector4D(1, 5, 9, 13));
            Assert.AreEqual(matrix.GetColumn(2), new Vector4D(2, 6, 10, 14));
            Assert.AreEqual(matrix.GetColumn(3), new Vector4D(3, 7, 11, 15));

            Assert.AreEqual(matrix.GetRow(0), new Vector4D(0, 1, 2, 3));
            Assert.AreEqual(matrix.GetRow(1), new Vector4D(4, 5, 6, 7));
            Assert.AreEqual(matrix.GetRow(2), new Vector4D(8, 9, 10, 11));
            Assert.AreEqual(matrix.GetRow(3), new Vector4D(12, 13, 14, 15));

            matrix = new Matrix4x4(
                5, 1, 7, 10,
                5, 10, -5, 3,
                6, 1, 1, 4,
                0, 0, 1, 0
                );

            Assert.AreEqual(matrix.GetColumn(0), new Vector4D(5, 5, 6, 0));
            Assert.AreEqual(matrix.GetColumn(1), new Vector4D(1, 10, 1, 0));
            Assert.AreEqual(matrix.GetColumn(2), new Vector4D(7, -5, 1, 1));
            Assert.AreEqual(matrix.GetColumn(3), new Vector4D(10, 3, 4, 0));

            Assert.AreEqual(matrix.GetRow(0), new Vector4D(5, 1, 7, 10));
            Assert.AreEqual(matrix.GetRow(1), new Vector4D(5, 10, -5, 3));
            Assert.AreEqual(matrix.GetRow(2), new Vector4D(6, 1, 1, 4));
            Assert.AreEqual(matrix.GetRow(3), new Vector4D(0, 0, 1, 0));
        }

        #region Comparison methods tests.
        [TestMethod]
        public void EqualsTest()
        {
            Assert.IsTrue(new Matrix4x4(10, 10, 5, 2) == new Matrix4x4(10, 10, 5, 2));
            Assert.IsFalse(new Matrix4x4(10, 10, 2, 5) == new Matrix4x4(10, 10, 2, 3));
            Assert.IsTrue(new Matrix4x4(10, 10, 5, 2).Equals(new Matrix4x4(10, 10, 5, 2)));
            Assert.IsFalse(new Matrix4x4(10, 10, 2, 4).Equals(new Matrix4x4(10, 10, 2, 442)));
        }

        [TestMethod]
        public void CloseEnoughTest()
        {
            Matrix4x4 first = new Matrix4x4(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12,
                13, 14, 15, 16
                );

            Matrix4x4 second = new Matrix4x4(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12,
                13, 14, 15, 17
                );

            Assert.IsTrue(first.CloseEnough(second, 1f));
            Assert.IsFalse(first.CloseEnough(second, .5f));
        }

        [TestMethod]
        public void NotEqualTest()
        {
            Assert.IsFalse(new Matrix4x4(10, 10, 1, 6) != new Matrix4x4(10, 10, 1, 6));
            Assert.IsTrue(new Matrix4x4(10, 10, 1, 5) != new Matrix4x4(10, 10, 1, 4));
        }
        #endregion

        #region Addition and subtraction tests.
        [TestMethod]
        public void AdditionTest()
        {
            Test50((i) =>
            {
                Matrix4x4 a = new Matrix4x4(
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());

                Matrix4x4 b = new Matrix4x4(
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());

                Matrix4x4 result = a + b;

                Assert.AreEqual(result, b + a);
                Assert.AreEqual(result, a.Add(b));
                Assert.AreEqual(result, b.Add(a));

                AssertUtils.CloseEnough(result.r0c0, a.r0c0 + b.r0c0);
                AssertUtils.CloseEnough(result.r0c1, a.r0c1 + b.r0c1);
                AssertUtils.CloseEnough(result.r0c2, a.r0c2 + b.r0c2);
                AssertUtils.CloseEnough(result.r0c3, a.r0c3 + b.r0c3);

                AssertUtils.CloseEnough(result.r1c0, a.r1c0 + b.r1c0);
                AssertUtils.CloseEnough(result.r1c1, a.r1c1 + b.r1c1);
                AssertUtils.CloseEnough(result.r1c2, a.r1c2 + b.r1c2);
                AssertUtils.CloseEnough(result.r1c3, a.r1c3 + b.r1c3);

                AssertUtils.CloseEnough(result.r2c0, a.r2c0 + b.r2c0);
                AssertUtils.CloseEnough(result.r2c1, a.r2c1 + b.r2c1);
                AssertUtils.CloseEnough(result.r2c2, a.r2c2 + b.r2c2);
                AssertUtils.CloseEnough(result.r2c3, a.r2c3 + b.r2c3);

                AssertUtils.CloseEnough(result.r3c0, a.r3c0 + b.r3c0);
                AssertUtils.CloseEnough(result.r3c1, a.r3c1 + b.r3c1);
                AssertUtils.CloseEnough(result.r3c2, a.r3c2 + b.r3c2);
                AssertUtils.CloseEnough(result.r3c3, a.r3c3 + b.r3c3);
            });
        }

        [TestMethod]
        public void SubtractionTest()
        {
            Test50((i) =>
            {
                Matrix4x4 a = new Matrix4x4(
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());

                Matrix4x4 b = new Matrix4x4(
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());

                Matrix4x4 result = a - b;

                Assert.AreEqual(result, a.Subtract(b));
                Assert.AreNotEqual(result, b - a);
                Assert.AreEqual(b - a, b.Subtract(a));
                Assert.AreNotEqual(result, b.Subtract(a));

                AssertUtils.CloseEnough(result.r0c0, a.r0c0 - b.r0c0);
                AssertUtils.CloseEnough(result.r0c1, a.r0c1 - b.r0c1);
                AssertUtils.CloseEnough(result.r0c2, a.r0c2 - b.r0c2);
                AssertUtils.CloseEnough(result.r0c3, a.r0c3 - b.r0c3);

                AssertUtils.CloseEnough(result.r1c0, a.r1c0 - b.r1c0);
                AssertUtils.CloseEnough(result.r1c1, a.r1c1 - b.r1c1);
                AssertUtils.CloseEnough(result.r1c2, a.r1c2 - b.r1c2);
                AssertUtils.CloseEnough(result.r1c3, a.r1c3 - b.r1c3);

                AssertUtils.CloseEnough(result.r2c0, a.r2c0 - b.r2c0);
                AssertUtils.CloseEnough(result.r2c1, a.r2c1 - b.r2c1);
                AssertUtils.CloseEnough(result.r2c2, a.r2c2 - b.r2c2);
                AssertUtils.CloseEnough(result.r2c3, a.r2c3 - b.r2c3);

                AssertUtils.CloseEnough(result.r3c0, a.r3c0 - b.r3c0);
                AssertUtils.CloseEnough(result.r3c1, a.r3c1 - b.r3c1);
                AssertUtils.CloseEnough(result.r3c2, a.r3c2 - b.r3c2);
                AssertUtils.CloseEnough(result.r3c3, a.r3c3 - b.r3c3);
            });
        }
        #endregion

        #region Multiplication tests.
        [TestMethod]
        public void Matrix4x4TimesFloatTest()
        {
            Test50((i) =>
            {
                float scalar = RandFloat100();
                Matrix4x4 matrix = new Matrix4x4(
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100());

                Matrix4x4 result = matrix * scalar;

                Assert.AreEqual(result, scalar * matrix);
                Assert.AreEqual(result, matrix.Multiply(scalar));

                AssertUtils.CloseEnough(result.r0c0, matrix.r0c0 * scalar);
                AssertUtils.CloseEnough(result.r0c1, matrix.r0c1 * scalar);
                AssertUtils.CloseEnough(result.r0c2, matrix.r0c2 * scalar);
                AssertUtils.CloseEnough(result.r0c3, matrix.r0c3 * scalar);

                AssertUtils.CloseEnough(result.r1c0, matrix.r1c0 * scalar);
                AssertUtils.CloseEnough(result.r1c1, matrix.r1c1 * scalar);
                AssertUtils.CloseEnough(result.r1c2, matrix.r1c2 * scalar);
                AssertUtils.CloseEnough(result.r1c3, matrix.r1c3 * scalar);

                AssertUtils.CloseEnough(result.r2c0, matrix.r2c0 * scalar);
                AssertUtils.CloseEnough(result.r2c1, matrix.r2c1 * scalar);
                AssertUtils.CloseEnough(result.r2c2, matrix.r2c2 * scalar);
                AssertUtils.CloseEnough(result.r2c3, matrix.r2c3 * scalar);

                AssertUtils.CloseEnough(result.r3c0, matrix.r3c0 * scalar);
                AssertUtils.CloseEnough(result.r3c1, matrix.r3c1 * scalar);
                AssertUtils.CloseEnough(result.r3c2, matrix.r3c2 * scalar);
                AssertUtils.CloseEnough(result.r3c3, matrix.r3c3 * scalar);
            });
        }

        [TestMethod]
        public void Matrix4x4TimesVector()
        {
            Vector4D vector = new Vector4D(1, 2, 3, 4);
            Matrix4x4 matrix = new Matrix4x4(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12,
                13, 14, 15, 16
                );
            Assert.AreEqual(matrix * vector, new Vector4D(30, 70, 110, 150));
            Assert.AreEqual(vector * matrix, new Vector4D(90, 100, 110, 120));
        }

        [TestMethod]
        public void Matrix4x4TimesMatrix4x4Test()
        {
            Matrix4x4 first = new Matrix4x4(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12,
                13, 14, 15, 16
                );
            Matrix4x4 second = new Matrix4x4(
                5, 2, 5, 2,
                1, 2, 6, 3,
                10, 3, 0, 1,
                3, 5, 2, -1
                );
            
            Matrix4x4 test = first * first;
            Assert.IsTrue(test.CloseEnough(new Matrix4x4(
                90, 100, 110, 120,
                202, 228, 254, 280,
                314, 356, 398, 440,
                426, 484, 542, 600)), test.ToString());

            test = second * second;
            Assert.IsTrue(test.CloseEnough(new Matrix4x4(
                83, 39, 41, 19,
                76, 39, 23, 11,
                56, 31, 70, 28,
                37, 17, 43, 24)), test.ToString());

            test = first * second;
            Assert.IsTrue(test.CloseEnough(new Matrix4x4(
                49, 35, 25, 7,
                125, 83, 77, 27,
                201, 131, 129, 47,
                277, 179, 181, 67)), test.ToString());

            test = second * first;
            Assert.IsTrue(test.CloseEnough(new Matrix4x4(
                86, 100, 114, 128,
                104, 116, 128, 140,
                38, 52, 66, 80,
                33, 42, 51, 60)), test.ToString());
        }
        #endregion

        #region Transformation tests.
        [TestMethod]
        public void ScaleTest()
        {
            Matrix4x4 scalar = Matrix4x4.Scale(new Vector3D(1, 4, .5f));
            Vector4D original = new Vector4D(5, 2, 1, 0);
            Vector4D scaled = scalar * original;
            Assert.AreEqual(scaled, new Vector4D(5, 8, .5f, 0));

            scalar = Matrix4x4.Scale(new Vector3D(5f, 5f, 5f));
            original = new Vector4D(.5f, -1, 2, 0);
            scaled = scalar * original;
            Assert.AreEqual(scaled, new Vector4D(2.5f, -5, 10, 0));
        }

        [TestMethod]
        public void TranslationTest()
        {
            Vector4D test = Matrix4x4.Translate(new Vector3D(4, 8, 5)) * new Vector4D(-3, 8, 1, 1);
            Assert.AreEqual(test, new Vector4D(1, 16, 6, 1));

            test = Matrix4x4.Translate(-3, 2.5f, 6) * new Vector4D(-3, 8, 1, 1);
            Assert.AreEqual(test, new Vector4D(-6, 10.5f, 7, 1));

            Assert.AreEqual(Matrix4x4.Translate(0, 0, 0), Matrix4x4.identity);
        }

        [TestMethod]
        public void RotateXYZTests()
        {
            TestAngle((a) =>
            {
                float cos = Math1D.Cos(a);
                float sin = Math1D.Sin(a);

                Matrix4x4 rotation = Matrix4x4.RotateX(a);
                AssertUtils.CloseEnough(rotation, new Matrix4x4(
                    1, 0, 0, 0,
                    0, cos, -sin, 0,
                    0, sin, cos, 0,
                    0, 0, 0, 1));

                rotation = Matrix4x4.RotateY(a);
                AssertUtils.CloseEnough(rotation, new Matrix4x4(
                    cos, 0, sin, 0,
                    0, 1, 0, 0,
                    -sin, 0, cos, 0,
                    0, 0, 0, 1));

                rotation = Matrix4x4.RotateZ(a);
                AssertUtils.CloseEnough(rotation, new Matrix4x4(
                    cos, -sin, 0, 0,
                    sin, cos, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1));
            });
        }

        [TestMethod]
        public void FromEulerAnglesTest()
        {
            Test50((i) =>
            {
                EulerAngles angles = new EulerAngles(RandAngle(), RandAngle(), RandAngle());
                Matrix4x4 matrix = Matrix4x4.FromEulerAngles(angles);
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(angles.RotateVector(vector), (Vector3D)(matrix * vector));
            });
        }

        [TestMethod]
        public void FromQuaternionTest()
        {
            Test50((i) =>
            {
                Quaternion quaternion = new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100()).Normalized;
                Matrix4x4 matrix = Matrix4x4.FromRotationQuaternion(quaternion);
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(quaternion.RotateVector(vector), (Vector3D)(matrix * vector), .001f);
            });
        }

        [TestMethod]
        public void FromAxisAngle()
        {
            Test50((i) =>
            {
                AxisAngle axisAngle = new AxisAngle(new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()).Normalized, RandAngle());
                Matrix4x4 matrix = Matrix4x4.FromAxisAngle(axisAngle);
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(axisAngle.RotateVector(vector), (Vector3D)(matrix * vector), .001f);
            });
        }

        [TestMethod]
        public void TRSTest()
        {
            Matrix4x4 transformation = Matrix4x4.TRS(new Vector3D(0, 0, 0), Quaternion.identity, new Vector3D(1, 1, 1));
            Vector4D original = new Vector4D(8, -5, 1);
            Vector4D transformed = transformation * original;

            Assert.AreEqual(transformed, new Vector4D(8, -5, 1, 0));

            transformation = Matrix4x4.TRS(new Vector3D(5, 2, 5), Quaternion.identity, new Vector3D(2, 2, 2));
            original = new Vector4D(5, 0, -5, 1);
            transformed = transformation * original;

            AssertUtils.CloseEnough(transformed, new Vector4D(20, 4, 0, 1));

            transformation = Matrix4x4.TRS(new Vector3D(-3, 15, 1), Quaternion.FromEulerAngles(0, 0, -Angle.right), new Vector3D(-3, 1, 2));
            original = new Vector4D(3, -10, -1, 1);
            transformed = transformation * original;

            AssertUtils.CloseEnough(transformed, new Vector4D(-15, 0, 0, 1));
        }
        #endregion

        #region Inverse, transpose, cofactor, adjugate, inverse, negated, trace and abs tests.
        [TestMethod]
        public void TransposeTest()
        {
            Test50((i) =>
            {
                Matrix4x4 matrix = new Matrix4x4(
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());

                Matrix4x4 transpose = matrix.Transposed;

                Assert.AreEqual(transpose.Transposed, matrix);
                Assert.AreEqual(transpose, Matrix.Transpose(matrix));

                for (int r = 0; r < 4; r++)
                {
                    for (int c = 0; c < 4; c++)
                    {
                        Assert.AreEqual(transpose[r, c], matrix[c, r]);
                        Assert.AreEqual(transpose[c, r], matrix[r, c]);
                    }
                }
            });
        }

        [TestMethod]
        public void InverseCofactorAdjugateDeterminantTest()
        {
            // This doesn't directly test the adjugate, cofactor and determinant,
            // but it does directly test the inverse,
            // which uses the adjugate, cofactor and determinant.
            Test50((i) =>
            {
                Matrix4x4 matrix = new Matrix4x4(
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());

                Assert.AreEqual(matrix.Determinant, Matrix.Det(matrix));
                Assert.AreEqual(matrix.Cofactor, Matrix.Cofactor(matrix));
                Assert.AreEqual(matrix.Adjugate, Matrix.Adjugate(matrix));

                if (matrix.Determinant != 0)
                {
                    Matrix4x4 inverse = matrix.Inverted;
                    Assert.AreEqual(inverse, Matrix.Invert(matrix));

                    AssertUtils.CloseEnough(inverse.Inverted, matrix, .03f);
                    AssertUtils.CloseEnough(inverse * matrix, Matrix4x4.identity);
                }
            });
        }

        [TestMethod]
        public void NegationTest()
        {
            Test50((i) =>
            {
                Matrix4x4 matrix = new Matrix4x4(
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());

                Matrix4x4 negated = -matrix;

                Assert.AreEqual(negated, matrix.Negated);
                Assert.AreEqual(negated, Matrix.Negate(matrix));
                Assert.AreEqual(negated, matrix * -1);

                for (int r = 0; r < 4; r++)
                {
                    for (int c = 0; c < 4; c++)
                    {
                        Assert.AreEqual(negated[r, c], -matrix[r, c]);
                    }
                }
            });
        }

        [TestMethod]
        public void TraceTest()
        {
            Test50((a) =>
            {
                Matrix4x4 matrix = new Matrix4x4(
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());

                float trace = matrix.Trace;
                Assert.AreEqual(trace, Matrix.Tr(matrix));

                float sum = 0;
                for (int i = 0; i < 4; i++)
                {
                    sum += matrix[i, i];
                }
                AssertUtils.CloseEnough(trace, sum);
                AssertUtils.CloseEnough(trace, matrix.r0c0 + matrix.r1c1 + matrix.r2c2 + matrix.r3c3);
            });
        }

        [TestMethod]
        public void AbsTest()
        {
            Test50((i) =>
            {
                Matrix4x4 matrix = new Matrix4x4(
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());

                Matrix4x4 abs = matrix.Abs;
                Assert.AreEqual(abs, Matrix.Abs(matrix));

                for (int r = 0; r < 4; r++)
                {
                    for (int c = 0; c < 4; c++)
                    {
                        float element = matrix[r, c];
                        float absElement = abs[r, c];

                        Assert.AreEqual(absElement.Sign(), 1);
                        if (element.Sign() == -1)
                            Assert.AreEqual(absElement, -element);
                        else
                            Assert.AreEqual(abs[r, c], matrix[r, c]);

                        Assert.AreEqual(abs[r, c], matrix[r, c].Abs());
                    }
                }
            });
        }
        #endregion

        [TestMethod]
        public void ConversionTests()
        {
            Test50((i) =>
            {
                Matrix4x4 matrix = new Matrix4x4(
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100(),
                    RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100());

                Matrix2x2 twoBy2 = (Matrix2x2)matrix;
                Matrix3x3 threeBy3 = (Matrix3x3)matrix;

                for (int r = 0; r < 2; r++)
                {
                    for (int c = 0; c < 2; c++)
                        Assert.AreEqual(twoBy2[r, c], matrix[r, c]);
                }

                for (int r = 0; r < 3; r++)
                {
                    for (int c = 0; c < 3; c++)
                        Assert.AreEqual(threeBy3[r, c], matrix[r, c]);
                }
            });
        }
    }
}