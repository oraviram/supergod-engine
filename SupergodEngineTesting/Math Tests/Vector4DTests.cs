﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class Vector4DTests : SupergodTestClass
    {
        [TestMethod]
        public void ConstructorTest()
        {
            Vector4D test = new Vector4D(1, 1, 2, 5);
            Assert.IsTrue(test.x == 1);
            Assert.IsTrue(test.y == 1);
            Assert.IsTrue(test.z == 2);
            Assert.IsTrue(test.w == 5);

            test = new Vector4D(3, new Vector3D(876, .5f, 2));
            Assert.IsTrue(test.x == 3);
            Assert.IsTrue(test.y == 876);
            Assert.IsTrue(test.z == .5f);
            Assert.IsTrue(test.w == 2);

            test = new Vector4D(new Vector3D(5, -2.3f, -3), 8);
            Assert.IsTrue(test.x == 5);
            Assert.IsTrue(test.y == -2.3f);
            Assert.IsTrue(test.z == -3);
            Assert.IsTrue(test.w == 8);

            test = new Vector4D(new Vector2D(5, -2.3f), -3, 8);
            Assert.IsTrue(test.x == 5);
            Assert.IsTrue(test.y == -2.3f);
            Assert.IsTrue(test.z == -3);
            Assert.IsTrue(test.w == 8);

            test = new Vector4D(5, -2.3f, new Vector2D(-3, 8));
            Assert.IsTrue(test.x == 5);
            Assert.IsTrue(test.y == -2.3f);
            Assert.IsTrue(test.z == -3);
            Assert.IsTrue(test.w == 8);

            test = new Vector4D(5, new Vector2D(-2.3f, -3), 8);
            Assert.IsTrue(test.x == 5);
            Assert.IsTrue(test.y == -2.3f);
            Assert.IsTrue(test.z == -3);
            Assert.IsTrue(test.w == 8);

            test = new Vector4D(new Vector2D(5, -2.3f), new Vector2D(-3, 8));
            Assert.IsTrue(test.x == 5);
            Assert.IsTrue(test.y == -2.3f);
            Assert.IsTrue(test.z == -3);
            Assert.IsTrue(test.w == 8);
        }

        [TestMethod]
        public void EqualsTest()
        {
            Assert.IsTrue(new Vector4D(10, 10, 5, 2) == new Vector4D(10, 10, 5, 2));
            Assert.IsFalse(new Vector4D(10, 10, 2, 1) == new Vector4D(10, 10, 2));
            Assert.IsTrue(new Vector4D(10, 10, 5).Equals(new Vector4D(10, 10, 5)));
            Assert.IsFalse(new Vector4D(10, 10, 2, 5).Equals(new Vector4D(10, 10, 2, 4)));
        }

        [TestMethod]
        public void NotEqualTest()
        {
            Assert.IsFalse(new Vector4D(10, 10, 1, 2) != new Vector4D(10, 10, 1, 2));
            Assert.IsTrue(new Vector4D(10, 10, 1, 5) != new Vector4D(10, 10, 1, 4));
        }

        [TestMethod]
        public void ContainsAxisTest()
        {
            Vector4D vector = new Vector4D(1, 2, 10, 6);
            Assert.IsTrue(vector.ContainsComponent(10));
            Assert.IsTrue(vector.ContainsComponent(2));
            Assert.IsTrue(vector.ContainsComponent(1));
            Assert.IsTrue(vector.ContainsComponent(6));
            Assert.IsFalse(vector.ContainsComponent(5));

            vector = new Vector4D(1, 2, 10, 2);
            Assert.IsTrue(Vector.ContainsAxis(vector, 10));
            Assert.IsTrue(Vector.ContainsAxis(vector, 2));
            Assert.IsTrue(Vector.ContainsAxis(vector, 1));
            Assert.IsFalse(Vector.ContainsAxis(vector, 5));
        }

        [TestMethod]
        public void CloseEnoughTest()
        {
            Assert.IsTrue(new Vector4D(1, 1, 1, 1).CloseEnough(new Vector4D(1.000001f, 1.000001f, 1.000001f, 1.000001f)));
            Assert.IsTrue(new Vector4D(1, 1, 1, 1).CloseEnough(new Vector4D(2, 2, 2, 2), 1));
        }

        [TestMethod]
        public void IndexerTest()
        {
            Vector4D vector = new Vector4D();
            vector[0] = 2;
            vector[1] = 4;
            vector[2] = 6;
            vector[3] = 8;

            Assert.AreEqual(vector[0], vector.x);
            Assert.AreEqual(vector[0], 2);

            Assert.AreEqual(vector[1], vector.y);
            Assert.AreEqual(vector[1], 4);

            Assert.AreEqual(vector[2], vector.z);
            Assert.AreEqual(vector[2], 6);

            Assert.AreEqual(vector[3], vector.w);
            Assert.AreEqual(vector[3], 8);
        }

        [TestMethod]
        public void AsFloatArrayTest()
        {
            float[] floatArray = new Vector4D(2, 3, -1, 5544).AsFloatArray;
            Assert.IsTrue(floatArray[0] == 2);
            Assert.IsTrue(floatArray[1] == 3);
            Assert.IsTrue(floatArray[2] == -1);
            Assert.IsTrue(floatArray[3] == 5544);
        }

        [TestMethod]
        public void SpecificAxesTests()
        {
            Vector4D vector = new Vector4D(1, 2, 3, 4);

            // X y and z combinations:
            Assert.AreEqual(vector.YXZ, new Vector3D(2, 1, 3));
            Assert.AreEqual(vector.YZX, new Vector3D(2, 3, 1));

            Assert.AreEqual(vector.ZXY, new Vector3D(3, 1, 2));
            Assert.AreEqual(vector.XZY, new Vector3D(1, 3, 2));

            Assert.AreEqual(vector.ZYX, new Vector3D(3, 2, 1));
            Assert.AreEqual(vector.XYZ, new Vector3D(1, 2, 3));

            // Y z and w combinations:
            Assert.AreEqual(vector.YZW, new Vector3D(2, 3, 4));
            Assert.AreEqual(vector.YWZ, new Vector3D(2, 4, 3));

            Assert.AreEqual(vector.ZYW, new Vector3D(3, 2, 4));
            Assert.AreEqual(vector.WYZ, new Vector3D(4, 2, 3));

            Assert.AreEqual(vector.ZWY, new Vector3D(3, 4, 2));
            Assert.AreEqual(vector.WZY, new Vector3D(4, 3, 2));

            // X z and w combinations:
            Assert.AreEqual(vector.XZW, new Vector3D(1, 3, 4));
            Assert.AreEqual(vector.XWZ, new Vector3D(1, 4, 3));

            Assert.AreEqual(vector.ZXW, new Vector3D(3, 1, 4));
            Assert.AreEqual(vector.WXZ, new Vector3D(4, 1, 3));

            Assert.AreEqual(vector.ZWX, new Vector3D(3, 4, 1));
            Assert.AreEqual(vector.WZX, new Vector3D(4, 3, 1));

            // Y w and x combinations:
            Assert.AreEqual(vector.YWX, new Vector3D(2, 4, 1));
            Assert.AreEqual(vector.YXW, new Vector3D(2, 1, 4));

            Assert.AreEqual(vector.XYW, new Vector3D(1, 2, 4));
            Assert.AreEqual(vector.WYX, new Vector3D(4, 2, 1));

            Assert.AreEqual(vector.XWY, new Vector3D(1, 4, 2));
            Assert.AreEqual(vector.WXY, new Vector3D(4, 1, 2));

            // Vector2D axes:
            Assert.AreEqual(vector.XY, new Vector2D(1, 2));
            Assert.AreEqual(vector.YX, new Vector2D(2, 1));

            Assert.AreEqual(vector.XZ, new Vector2D(1, 3));
            Assert.AreEqual(vector.ZX, new Vector2D(3, 1));

            Assert.AreEqual(vector.XW, new Vector2D(1, 4));
            Assert.AreEqual(vector.WX, new Vector2D(4, 1));

            Assert.AreEqual(vector.YZ, new Vector2D(2, 3));
            Assert.AreEqual(vector.ZY, new Vector2D(3, 2));

            Assert.AreEqual(vector.YW, new Vector2D(2, 4));
            Assert.AreEqual(vector.WY, new Vector2D(4, 2));

            Assert.AreEqual(vector.ZW, new Vector2D(3, 4));
            Assert.AreEqual(vector.WZ, new Vector2D(4, 3));
        }

        [TestMethod]
        public void BiggestSmallestAxisTest()
        {
            Vector4D vector = new Vector4D(5, 2, 3, 4);
            Assert.AreEqual(Vector.SmallestAxis(vector), 2);
            Assert.AreNotEqual(Vector.SmallestAxis(vector), 5);
            Assert.AreNotEqual(Vector.SmallestAxis(vector), 3);
            Assert.AreNotEqual(Vector.SmallestAxis(vector), 4);
            Assert.AreEqual(Vector.BiggestAxis(vector), 5);
            Assert.AreNotEqual(Vector.BiggestAxis(vector), 2);
            Assert.AreNotEqual(Vector.BiggestAxis(vector), 3);
            Assert.AreNotEqual(Vector.BiggestAxis(vector), 4);

            Assert.AreEqual(vector.SmallestComponent, 2);
            Assert.AreNotEqual(vector.SmallestComponent, 5);
            Assert.AreNotEqual(vector.SmallestComponent, 3);
            Assert.AreNotEqual(vector.SmallestComponent, 4);
            Assert.AreEqual(vector.BiggestComponent, 5);
            Assert.AreNotEqual(vector.BiggestComponent, 2);
            Assert.AreNotEqual(vector.BiggestComponent, 3);
            Assert.AreNotEqual(vector.BiggestComponent, 4);
        }

        [TestMethod]
        public void AdditionTest()
        {
            Vector4D a = new Vector4D(2, 4, -1, 3);
            Vector4D b = new Vector4D(8, -1, -1.5f, 0);
            Vector4D expectedResult = new Vector4D(10, 3, -2.5f, 3);

            Assert.AreEqual(a + b, expectedResult);
            Assert.AreEqual(a.Add(b), expectedResult);
            Assert.AreEqual(Vector.Add(a, b), expectedResult);

            a = new Vector4D(8.2f, 9, 3, 5);
            b = new Vector4D(-1.4f, -1, -1.5f, 5);
            expectedResult = new Vector4D(6.8f, 8, 1.5f, 10);

            Assert.IsTrue((a + b).CloseEnough(expectedResult));
            Assert.IsTrue(a.Add(b).CloseEnough(expectedResult));
            Assert.IsTrue(Vector.Add(a, b).CloseEnough(expectedResult));

            a = new Vector4D(2, 3, 4, 5);
            b = new Vector4D(5, 4, 3, 2);
            expectedResult = new Vector4D(7, 7, 7, 7);

            Assert.AreEqual(a + b, expectedResult);
            Assert.AreEqual(a.Add(b), expectedResult);
            Assert.AreEqual(Vector.Add(a, b), expectedResult);
        }

        [TestMethod]
        public void SubtractionTest()
        {
            Vector4D a = new Vector4D(2, 4, -1, 3);
            Vector4D b = new Vector4D(8, -1, -1.5f, 0);
            Vector4D expectedResult = new Vector4D(-6, 5, .5f, 3);

            Assert.AreEqual(a - b, expectedResult);
            Assert.AreEqual(a.Subtract(b), expectedResult);
            Assert.AreEqual(Vector.Subtract(a, b), expectedResult);

            a = new Vector4D(8.2f, 9, 3, 5);
            b = new Vector4D(-1.4f, -1, -1.5f, 5);
            expectedResult = new Vector4D(9.6f, 10, 4.5f, 0);

            Assert.IsTrue((a - b).CloseEnough(expectedResult));
            Assert.IsTrue(a.Subtract(b).CloseEnough(expectedResult));
            Assert.IsTrue(Vector.Subtract(a, b).CloseEnough(expectedResult));

            a = new Vector4D(2, 3, 4, 5);
            b = new Vector4D(5, 4, 3, 2);
            expectedResult = new Vector4D(-3, -1, 1, 3);

            Assert.AreEqual(a - b, expectedResult);
            Assert.AreEqual(a.Subtract(b), expectedResult);
            Assert.AreEqual(Vector.Subtract(a, b), expectedResult);
        }

        [TestMethod]
        public void NegatingTest()
        {
            Assert.AreEqual(new Vector4D(1, 2.5f, -1, -2.2f).Negated, new Vector4D(-1, -2.5f, 1, 2.2f));
            Assert.AreEqual(Vector.Negate(new Vector4D(1, 2.5f, -1, -2.2f)), new Vector4D(-1, -2.5f, 1, 2.2f));
            Assert.AreEqual(-new Vector4D(1, 2.5f, -1, -2.2f), new Vector4D(-1, -2.5f, 1, 2.2f));
        }

        [TestMethod]
        public void ScalarMultiplicationTest()
        {
            Vector4D vector = new Vector4D(2, 4, -1, 3);
            float scalar = 2;
            Vector4D expectedResult = new Vector4D(4, 8, -2, 6);

            Assert.AreEqual(vector * scalar, expectedResult);
            Assert.AreEqual(vector.Multiply(scalar), expectedResult);
            Assert.AreEqual(Vector.Multiply(vector, scalar), expectedResult);

            vector = new Vector4D(-2, 6, .2f, 5);
            scalar = 3;
            expectedResult = new Vector4D(-6, 18, .6f, 15);

            Assert.AreEqual(vector * scalar, expectedResult);
            Assert.AreEqual(vector.Multiply(scalar), expectedResult);
            Assert.AreEqual(Vector.Multiply(vector, scalar), expectedResult);

            vector = new Vector4D(-2, 6, .2f, 5);
            scalar = -.5f;
            expectedResult = new Vector4D(1, -3, -.1f, -2.5f);

            Assert.AreEqual(vector * scalar, expectedResult);
            Assert.AreEqual(vector.Multiply(scalar), expectedResult);
            Assert.AreEqual(Vector.Multiply(vector, scalar), expectedResult);
        }

        [TestMethod]
        public void MultiplicationTest()
        {
            Vector4D vector = new Vector4D(-2, 6, 1, 5);
            Vector4D scalar = new Vector4D(-2, 6, .2f, 5);
            Vector4D expectedResult = new Vector4D(4, 36, .2f, 25);
            
            Assert.AreEqual(vector * scalar, expectedResult);
            Assert.AreEqual(vector.Multiply(scalar), expectedResult);
            Assert.AreEqual(Vector.Multiply(vector, scalar), expectedResult);
        }

        [TestMethod]
        public void DivisionTest()
        {
            Vector4D a = new Vector4D(2, 4, -1.5f, 3);
            Vector4D b = new Vector4D(8, -1, -1.5f, 0);
            Vector4D expectedResult = new Vector4D(0.25f, -4, 1, Math1D.Infinity);

            Assert.AreEqual(a / b, expectedResult);
            Assert.AreEqual(a.Divide(b), expectedResult);
            Assert.AreEqual(Vector.Divide(a, b), expectedResult);

            a = new Vector4D(2, 3, 3, 5);
            b = new Vector4D(5, 4, 3, 2);
            expectedResult = new Vector4D(.4f, .75f, 1, 2.5f);

            Assert.AreEqual(a / b, expectedResult);
            Assert.AreEqual(a.Divide(b), expectedResult);
            Assert.AreEqual(Vector.Divide(a, b), expectedResult);

            a = new Vector4D(8.2f, 9, 3, 5);
            float scalar = 2;
            expectedResult = new Vector4D(4.1f, 4.5f, 1.5f, 2.5f);

            Assert.IsTrue((a / scalar).CloseEnough(expectedResult));
            Assert.IsTrue(a.Divide(scalar).CloseEnough(expectedResult));
            Assert.IsTrue(Vector.Divide(a, scalar).CloseEnough(expectedResult));
        }

        [TestMethod]
        public void DotTest()
        {
            Vector4D left = new Vector4D(1, 5, 10021, 0);
            Vector4D right = new Vector4D(2, .5f, 0, 1);
            float expectedResult = 4.5f;

            Assert.AreEqual(Vector.Dot(left, right), expectedResult);
            Assert.AreEqual(left.Dot(right), expectedResult);

            left = new Vector4D(-2, -4f, 2, 5);
            right = new Vector4D(5, 5, 5, 2);
            expectedResult = -10;

            Assert.AreEqual(Vector.Dot(left, right), expectedResult);
            Assert.AreEqual(left.Dot(right), expectedResult);

            left = new Vector4D(-2, -4f, 2, -20);
            right = new Vector4D(5, 5, 5, 2);
            expectedResult = -60;

            Assert.AreEqual(Vector.Dot(left, right), expectedResult);
            Assert.AreEqual(left.Dot(right), expectedResult);
        }

        [TestMethod]
        public void MagnitudeTest()
        {
            Vector4D vec = new Vector4D(5, .6f, 2, 3);

            Assert.IsTrue(vec.Magnitude == Vector.Magnitude(vec) && vec.Magnitude.CloseEnough(6.19355f));
            Assert.IsTrue(vec.SqrMagnitude.CloseEnough(Vector.SqrMagnitude(vec)) && vec.SqrMagnitude.CloseEnough(38.36006f));

            vec = new Vector4D(-5, .2f, 5.2f, 4);
            Assert.IsTrue(vec.Magnitude == Vector.Magnitude(vec) && vec.Magnitude.CloseEnough(8.25106f));
            Assert.IsTrue(vec.SqrMagnitude.CloseEnough(Vector.SqrMagnitude(vec)) && vec.SqrMagnitude.CloseEnough(68.08f));
        }

        [TestMethod]
        public void NormalizeTest()
        {
            Vector4D vec = new Vector4D(10, 15);
            Assert.IsTrue(vec.Normalized.Magnitude.CloseEnough(1) && Vector.Normalize(vec).Magnitude.CloseEnough(1));

            vec = new Vector4D(1000, -1000);
            Assert.IsTrue(vec.Normalized.Magnitude.CloseEnough(1) && Vector.Normalize(vec).Magnitude.CloseEnough(1));
        }
        
        [TestMethod]
        public void ProjectOntoTest()
        {
            Vector4D source = new Vector4D(2, 4, 2, 6);
            Vector4D target = new Vector4D(1, 0, 2, 2.325f);
            Vector4D result = source.ProjectOnto(target);

            Assert.IsTrue(result.CloseEnough(new Vector4D(1.91723f, 0, 3.83446f, 4.45757f)));

            Vector4D[] vectors = new Vector4D[]
            {
                new Vector4D(1, 3, 4, 2),
                new Vector4D(4.8f, 5.6f, 2, 4),
                new Vector4D(0, 5, -6222, 2.5f),
                new Vector4D(-3.2f, 543, 3.62f),
                new Vector4D(2.4f, -2.4f, 8),
            };

            void TestProjection(Vector4D _source, Vector4D _target)
            {
                Vector4D bNormalized = _target.Normalized;
                Vector4D oldResult = _source.Dot(bNormalized) * bNormalized;
                Vector4D newResult = Vector.ProjectOnto(_source, _target);

                Assert.IsTrue(newResult.x.CloseEnough(oldResult.x));
                Assert.IsTrue(newResult.y.CloseEnough(oldResult.y));
                Assert.IsTrue(newResult.z.CloseEnough(oldResult.z));
                Assert.IsTrue(newResult.w.CloseEnough(oldResult.w));
            }

            for (int i = 0; i < vectors.Length - 1; i++)
            {
                TestProjection(vectors[i], vectors[i + 1]);
                TestProjection(vectors[i + 1], vectors[i]);
            }
            Vector4D a = new Vector4D(0.5f, Math1D.Sqrt(3) / 2);
            Vector4D b = new Vector4D(Math1D.Sqrt(3) / 2, 0.5f);
            float dot = a.Dot(b);
            Assert.AreEqual(dot, Math1D.Cos(Math1D.PI / 6));
        }

        [TestMethod]
        public void ReflectionTest()
        {
            Vector4D source = new Vector4D(-1, 2, 3, 4);

            Assert.AreEqual(source.Reflect(Vector4D.unitX), new Vector4D(1, 2, 3, 4));
            Assert.AreEqual(source.Reflect(Vector4D.unitY), new Vector4D(-1, -2, 3, 4));
            Assert.AreEqual(source.Reflect(Vector4D.unitZ), new Vector4D(-1, 2, -3, 4));
            Assert.AreEqual(source.Reflect(Vector4D.unitW), new Vector4D(-1, 2, 3, -4));
        }
        
        [TestMethod]
        public void LerpTest()
        {
            Vector4D a = new Vector4D(-10, -5, 2, 3);
            Vector4D b = new Vector4D(2, .5f, 344, 324.432f);
            Vector4D difference = b - a;

            Test01((alpha) =>
            {
                Vector4D result1 = SMath.Lerp(a, b, alpha);
                Vector4D result1NonStatic = a.Lerp(b, alpha);
                Vector4D result2 = a + alpha * difference;

                Assert.IsTrue(result1.x.CloseEnough(result2.x));
                Assert.IsTrue(result1.y.CloseEnough(result2.y));
                Assert.IsTrue(result1.z.CloseEnough(result2.z));
                Assert.IsTrue(result1.w.CloseEnough(result2.w));

                Assert.IsTrue(result1NonStatic.x.CloseEnough(result2.x));
                Assert.IsTrue(result1NonStatic.y.CloseEnough(result2.y));
                Assert.IsTrue(result1NonStatic.z.CloseEnough(result2.z));
                Assert.IsTrue(result1NonStatic.w.CloseEnough(result2.w));
            });
        }

        [TestMethod]
        public void ClampingTests()
        {
            Vector4D vector = new Vector4D(0, 10, 3, 10).Clamp(new Vector4D(-2, 15, 6, 7), new Vector4D(2, 20, 10, 10));
            Assert.AreEqual(vector, new Vector4D(0, 15, 6, 10));

            vector = new Vector4D(2, 2, 2, -1).ClampComponents(0, 1);
            Assert.AreEqual(vector, new Vector4D(1, 1, 1, 0));

            vector = new Vector4D(2, 2, 10, 3).ClampComponents(3, 4);
            Assert.AreEqual(vector, new Vector4D(3, 3, 4, 3));

            Vector4D lengthVector = new Vector4D(1, 1, 1, 1);
            Vector4D oldLengthVector = lengthVector;
            lengthVector = lengthVector.ClampMagnitude(.5f, .7f);

            Assert.AreEqual(lengthVector.Magnitude, .7f);
            Assert.AreEqual(lengthVector.Normalized, oldLengthVector.Normalized);

            lengthVector = new Vector4D(10, 10, 6, 6);
            oldLengthVector = lengthVector;
            lengthVector = lengthVector.ClampMagnitude(18, 20);

            Assert.IsTrue(lengthVector.Magnitude.CloseEnough(18));
            Assert.IsTrue(lengthVector.Normalized.CloseEnough(oldLengthVector.Normalized));

            lengthVector = new Vector4D(5, 5, 5, 5);
            oldLengthVector = lengthVector;
            lengthVector = lengthVector.ClampMagnitude(-10, 10);
            Assert.AreEqual(lengthVector.Magnitude, oldLengthVector.Magnitude);
            Assert.AreEqual(lengthVector.Normalized, oldLengthVector.Normalized);

            Assert.AreEqual(new Vector4D(-1, -2, -3).Abs, new Vector4D(1, 2, 3));
            Assert.AreEqual(Vector.Abs(new Vector4D(1, 2, 3)), new Vector4D(1, 2, 3));
        }

        [TestMethod]
        public void AngleTest()
        {
            Vector4D a = new Vector4D(0, 0, 0, 10);
            Vector4D b = new Vector4D(0, 10);

            AssertUtils.CloseEnough(a.SmallestAngle(b), Math1D.PI / 2);
            AssertUtils.CloseEnough(Vector.SmallestAngle(a, b), Math1D.PI / 2);

            b = new Vector4D(0, 0, 0, -10);
            AssertUtils.CloseEnough(a.SmallestAngle(b), Math1D.PI);
            AssertUtils.CloseEnough(Vector.SmallestAngle(a, b), Math1D.PI);

            b = new Vector4D(5, 0, 0, 5);
            AssertUtils.CloseEnough(a.SmallestAngle(b), Math1D.PI / 4);
            AssertUtils.CloseEnough(Vector.SmallestAngle(a, b), Math1D.PI / 4);

            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector4D(1, 0, 0), new Vector4D(1, 0, 0), false, false), 0);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector4D(0, 0, 1), new Vector4D(0, 0, 1), false, false), 0);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector4D(0, 0, 0, 1), new Vector4D(0, 0, 0, 1), false, false), 0);

            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector4D(-1, 0, 0), new Vector4D(1, 0, 0), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector4D(0, 1, -1), new Vector4D(1, 0, 1), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector4D(0, 0, 0, -1), new Vector4D(0, 0, 0, 1), false, false), Math1D.PI);

            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector4D(0, 1, 0), new Vector4D(1, 0, 0), false, false), Math1D.PI / 2);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector4D(0, 1, 0), new Vector4D(0, 0, 1), false, false), Math1D.PI / 2);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector4D(1, 0, 0), new Vector4D(0, 0, -1), false, false), Math1D.PI / 2);
            AssertUtils.CloseEnough(Vector.SmallestAngle(new Vector4D(0, 0, 0, 1), new Vector4D(0, 0, -1), false, false), Math1D.PI / 2);

            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector4D(1, 0, 0), new Vector4D(1, 0, 0), false, false), Math1D.PI * 2);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector4D(0, 0, 1), new Vector4D(0, 0, 1), false, false), Math1D.PI * 2);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector4D(0, 0, 0, 1), new Vector4D(0, 0, 0, 1), false, false), Math1D.PI * 2);

            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector4D(-1, 0, 0), new Vector4D(1, 0, 0), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector4D(0, 1, -1), new Vector4D(1, 0, 1), false, false), Math1D.PI);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector4D(0, 0, 0, -1), new Vector4D(0, 0, 0, 1), false, false), Math1D.PI);

            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector4D(0, 1, 0), new Vector4D(1, 0, 0), false, false), Math1D.PI * 1.5f);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector4D(0, 1, 0), new Vector4D(0, 0, 1), false, false), Math1D.PI * 1.5f);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector4D(1, 0, 0), new Vector4D(0, 0, -1), false, false), Math1D.PI * 1.5f);
            AssertUtils.CloseEnough(Vector.BiggestAngle(new Vector4D(0, 0, 0, 1), new Vector4D(0, 0, -1), false, false), Math1D.PI * 1.5f);
        }

        [TestMethod]
        public void DistanceTests()
        {
            Vector4D a = new Vector4D(10, 0, 0, 0);
            Vector4D b = new Vector4D(-10, 0, 0, 0);
            float dist = a.Distance(b);
            float sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, 20);
            Assert.AreEqual(sqrDist, 400);

            a = new Vector4D(0, 0, 0, 0);
            b = new Vector4D(-10, 0, 0, 0);
            dist = a.Distance(b);
            sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, 10);
            Assert.AreEqual(sqrDist, 100);

            a = new Vector4D(0, 0, 0, 0);
            b = new Vector4D(-10, 10, -5, 0);
            dist = a.Distance(b);
            sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, 15);
            Assert.AreEqual(sqrDist, 225);

            a = new Vector4D(0, 5, 0, 0);
            b = new Vector4D(0, -5, 0, 0);
            dist = a.Distance(b);
            sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, 10);
            Assert.AreEqual(sqrDist, 100);

            a = new Vector4D(0, 5, -5, 5);
            b = new Vector4D(0, -5, 5, -5);
            dist = a.Distance(b);
            sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            AssertUtils.CloseEnough(dist, Math1D.Sqrt(3) * 10);
            AssertUtils.CloseEnough(sqrDist, Math1D.Squared(Math1D.Sqrt(3) * 10));

            a = new Vector4D(0, 3, 0, 3);
            b = new Vector4D(3, 0, 3, 0);
            dist = a.Distance(b);
            sqrDist = a.SqrDistance(b);

            Assert.AreEqual(dist, Vector.Distance(a, b));
            Assert.AreEqual(sqrDist, Vector.SqrDistance(a, b));
            Assert.AreEqual(dist, 6);
            Assert.AreEqual(sqrDist, 36);
        }

        [TestMethod]
        public void LookPointAtTests()
        {
            Vector4D a = new Vector4D(10, 0, 0, 0);
            Vector4D b = new Vector4D(-10, 0, 0, 0);
            Vector4D lookAt = a.LookAt(b);
            Vector4D pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector4D(-20, 0, 0, 0));
            Assert.AreEqual(pointAt, new Vector4D(-1, 0, 0, 0));

            a = new Vector4D(0, 0, 0, 0);
            b = new Vector4D(-10, 0, 0, 0);
            lookAt = a.LookAt(b);
            pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector4D(-10, 0, 0, 0));
            Assert.AreEqual(pointAt, new Vector4D(-1, 0, 0, 0));

            a = new Vector4D(0, 0, 0, 0);
            b = new Vector4D(-10, 10, -5, 0);
            lookAt = a.LookAt(b);
            pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector4D(-10, 10, -5));
            Assert.AreEqual(pointAt, new Vector4D(-0.6666666666f, 0.6666666666f, -0.33333333333333f, 0));

            a = new Vector4D(0, 5, 0, 0);
            b = new Vector4D(0, -5, 0, 0);
            lookAt = a.LookAt(b);
            pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector4D(0, -10, 0, 0));
            Assert.AreEqual(pointAt, new Vector4D(0, -1, 0, 0));

            a = new Vector4D(0, 5, -5, 5);
            b = new Vector4D(0, -5, 5, -5);
            lookAt = a.LookAt(b);
            pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector4D(0, -10, 10, -10));
            AssertUtils.CloseEnough(pointAt, new Vector4D(0, -0.5773503f, 0.5773503f, -0.5773503f));

            a = new Vector4D(0, 3, 0, 3);
            b = new Vector4D(3, 0, 3, 0);
            lookAt = a.LookAt(b);
            pointAt = a.PointAt(b);

            Assert.AreEqual(lookAt, Vector.LookAt(a, b));
            Assert.AreEqual(pointAt, Vector.PointAt(a, b));
            Assert.AreEqual(lookAt, new Vector4D(3, -3, 3, -3));
            Assert.AreEqual(pointAt, new Vector4D(.5f, -.5f, .5f, -.5f));
        }

        [TestMethod]
        public void ConversionTests()
        {
            Vector2D first = (Vector2D)new Vector4D(1, 2, 3, 4);
            Assert.IsTrue(first == new Vector2D(1, 2));

            Vector3D second = (Vector3D)new Vector4D(4.2f, -1, 3, 4);
            Assert.IsTrue(second == new Vector3D(4.2f, -1, 3));

            FColor color = new Vector4D(2, -1, 3, 10);
            Assert.AreEqual(color, new FColor(2, -1, 3, 10));

            Quaternion quaternion = new Vector4D(2, -1, 3, 5);
            Assert.AreEqual(quaternion, new Quaternion(5, 2, -1, 3));
        }
    }
}