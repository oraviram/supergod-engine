﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class AxisAngleTests : SupergodTestClass
    {
        [TestMethod]
        public void ConstructionTest()
        {
            AxisAngle axisAngle = new AxisAngle(Math1D.PI, Vector3D.up);
            Assert.AreEqual(axisAngle.axis, Vector3D.up);
            Assert.AreEqual(axisAngle.angle.Degrees, 180);
            Assert.AreEqual(axisAngle.angle.Revolutions, .5f);

            axisAngle = new AxisAngle(Vector3D.right, new Angle(90, AngleMeasurement.Degrees));
            Assert.AreEqual(axisAngle.axis, Vector3D.right);
            Assert.AreEqual(axisAngle.angle.Radians, Math1D.PI / 2);
            Assert.AreEqual(axisAngle.angle.Revolutions, .25f);
        }

        [TestMethod]
        public void RotateVectorTest()
        {
            Vector3D vector = Vector3D.right;
            AxisAngle rotation = new AxisAngle(Vector3D.forward, Angle.right);
            Vector3D rotated = rotation.RotateVector(vector);

            AssertUtils.CloseEnough(rotated, Vector3D.up);
            Assert.AreEqual(rotated, Rotator.RotateVector(rotation, vector));
            Assert.AreEqual(rotated, Rotator.RotateVector(vector, rotation));

            vector = Vector3D.forward;
            rotation = new AxisAngle(Vector3D.up, Angle.straight);
            rotated = rotation.RotateVector(vector);

            AssertUtils.CloseEnough(rotated, Vector3D.back);
            Assert.AreEqual(rotated, Rotator.RotateVector(rotation, vector));
            Assert.AreEqual(rotated, Rotator.RotateVector(vector, rotation));

            vector = Vector3D.forward * -3;
            rotation = new AxisAngle(Vector3D.right, Angle.straight);
            rotated = rotation.RotateVector(vector);

            AssertUtils.CloseEnough(rotated, Vector3D.back * -3);
            Assert.AreEqual(rotated, Rotator.RotateVector(rotation, vector));
            Assert.AreEqual(rotated, Rotator.RotateVector(vector, rotation));

            vector = Vector3D.forward;
            rotation = new AxisAngle(Vector3D.right, Angle.right);
            rotated = rotation.RotateVector(vector);

            AssertUtils.CloseEnough(rotated, Vector3D.down);
            Assert.AreEqual(rotated, Rotator.RotateVector(rotation, vector));
            Assert.AreEqual(rotated, Rotator.RotateVector(vector, rotation));

            vector = Vector3D.up;
            rotation = new AxisAngle(Vector3D.right, Angle.right);
            rotated = rotation.RotateVector(vector);

            AssertUtils.CloseEnough(rotated, Vector3D.forward);
            Assert.AreEqual(rotated, Rotator.RotateVector(rotation, vector));
            Assert.AreEqual(rotated, Rotator.RotateVector(vector, rotation));

            vector = Vector3D.up * 2;
            rotation = new AxisAngle(Vector3D.forward, Angle.right);
            rotated = rotation.RotateVector(vector);

            AssertUtils.CloseEnough(rotated, Vector3D.left * 2);
            Assert.AreEqual(rotated, Rotator.RotateVector(rotation, vector));
            Assert.AreEqual(rotated, Rotator.RotateVector(vector, rotation));
        }

        [TestMethod]
        public void CombineRotationsTest()
        {
            Test50((i) =>
            {
                AxisAngle a = new AxisAngle(new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()).Normalized, RandAngle());
                AxisAngle b = new AxisAngle(new Vector3D(RandFloat100(), RandFloat100(), RandFloat100()).Normalized, RandAngle());
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                // Go to the implementation in CombineRotations to see why the threshold is so high...
                // Hopefully I will be able to make it smaller sometime!
                AssertUtils.CloseEnough(a.CombineRotations(b).RotateVector(vector), a.RotateVector(vector).RotateVector(b), .01f);
            });
        }

        [TestMethod]
        public void FromRotationQuaternionTest()
        {
            Test50((i) =>
            {
                Quaternion quaternion = new Quaternion(RandFloat100(), RandFloat100(), RandFloat100(), RandFloat100()).Normalized;
                AxisAngle axisAngle = AxisAngle.FromRotationQuaternion(quaternion);
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(quaternion.RotateVector(vector), axisAngle.RotateVector(vector));
            });
        }

        [TestMethod]
        public void FromRotationMatrixTest()
        {
            Test50((i) =>
            {
                Matrix3x3 matrix = Matrix3x3.FromEulerAngles(new EulerAngles(RandAngle(), RandAngle(), RandAngle()));
                AxisAngle axisAngle = AxisAngle.FromRotationMatrix(matrix);
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(matrix * vector, axisAngle.RotateVector(vector));
            });
        }

        [TestMethod]
        public void FromEulerAnglesTest()
        {
            Test50((i) =>
            {
                EulerAngles angles = new EulerAngles(RandAngle(), RandAngle(), RandAngle());
                AxisAngle axisAngle = AxisAngle.FromEulerAngles(angles);
                Vector3D vector = new Vector3D(RandFloat100(), RandFloat100(), RandFloat100());

                AssertUtils.CloseEnough(angles.RotateVector(vector), axisAngle.RotateVector(vector));
            });
        }
    }
}