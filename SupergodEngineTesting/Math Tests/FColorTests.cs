﻿using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class FColorTests : SupergodTestClass
    {
        [TestMethod]
        public void ConstructorTest()
        {
            Test50((i) =>
            {
                float r = RandFloat();
                float g = RandFloat();
                float b = RandFloat();
                float a = RandFloat();

                FColor color = new FColor(r, g, b, a);
                Assert.AreEqual(r, color.r);
                Assert.AreEqual(g, color.g);
                Assert.AreEqual(b, color.b);
                Assert.AreEqual(a, color.a);
            });
        }

        [TestMethod]
        public void ScalarMultiplicationTests()
        {
            FColor color = new FColor(1, 1, 1, 1);
            float scalar = .5f;

            FColor result1 = color * scalar;
            FColor result2 = scalar * color;

            Assert.IsTrue(result1.r == .5f);
            Assert.IsTrue(result1.r == result2.r);

            Assert.IsTrue(result1.g == .5f);
            Assert.IsTrue(result1.g == result2.g);

            Assert.IsTrue(result1.b == .5f);
            Assert.IsTrue(result1.b == result2.b);

            Assert.IsTrue(result1.a == .5f);
            Assert.IsTrue(result1.a == result2.a);

            result1 /= scalar;
            Assert.AreEqual(result1, result2 / scalar);
            Assert.AreEqual(result1, color);
        }

        [TestMethod]
        public void MultiplicationDivisionTests()
        {
            FColor a = new FColor(1, 1, 1, 1);
            FColor b = new FColor(2, 3, 4, -1);

            FColor result1 = a * b;
            FColor result2 = b * a;

            Assert.IsTrue(result1.r == 2);
            Assert.IsTrue(result1.r == result2.r);

            Assert.IsTrue(result1.g == 3);
            Assert.IsTrue(result1.g == result2.g);

            Assert.IsTrue(result1.b == 4);
            Assert.IsTrue(result1.b == result2.b);

            Assert.IsTrue(result1.a == -1);
            Assert.IsTrue(result1.a == result2.a);

            result1 /= b;
            Assert.AreEqual(result1, a);

            result2 /= a;
            Assert.AreEqual(result2, b);
        }

        [TestMethod]
        public void AdditionSubtractionTests()
        {
            Test50((i) =>
            {
                float aRed = RandFloat();
                float aGreen = RandFloat();
                float aBlue = RandFloat();
                float aAlpha = RandFloat();

                float bRed = RandFloat();
                float bGreen = RandFloat();
                float bBlue = RandFloat();
                float bAlpha = RandFloat();

                FColor a = new FColor(aRed, aGreen, aBlue, aAlpha);
                FColor b = new FColor(bRed, bGreen, bBlue, bAlpha);

                FColor result1 = a + b;
                FColor result2 = b + a;

                AssertUtils.CloseEnough(result1, result2);
                AssertUtils.CloseEnough(result1 - b, a);
                AssertUtils.CloseEnough(result2 - a, b);

                AssertUtils.CloseEnough(result1, new FColor(aRed + bRed, aGreen + bGreen, aBlue + bBlue, aAlpha + bAlpha));
                AssertUtils.CloseEnough(result1, new FColor(aRed + bRed, aGreen + bGreen, aBlue + bBlue, aAlpha + bAlpha));
            });
        }
    }
}