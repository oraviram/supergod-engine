﻿using System.Linq;
using SupergodEngine;
using SupergodUtilities.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    [TestClass]
    public class SceneObjectTests : SupergodTestClass
    {
        class MySceneObject : SceneObject
        {
        }

        MySceneObject testObj;

        // Constructor test:
        public SceneObjectTests()
        {
            testObj = new MySceneObject();
            Assert.IsTrue(testObj.RootComponent.GetType() == typeof(SceneComponent));
            Assert.IsTrue(testObj.Transform == Transform.identity);
            Assert.IsTrue(!string.IsNullOrEmpty(testObj.Name));
        }
        
        [TestMethod]
        public void SceneComponentsTest()
        {
            SceneComponent testSceneComponent1 = testObj.Components.Add<SceneComponent>("Test01");
            Assert.AreEqual(testSceneComponent1.Parent, testObj.RootComponent);

            SceneComponent testSceneComponent2 = testObj.Components.Add<SceneComponent>("Test02");
            Assert.AreEqual(testSceneComponent2.Parent, testObj.RootComponent);
            testSceneComponent2.SetParent(testSceneComponent1);
            Assert.AreEqual(testSceneComponent2.Parent, testSceneComponent1);
            Assert.AreNotEqual(testSceneComponent2.Parent, testObj.RootComponent);
        }
    }
}