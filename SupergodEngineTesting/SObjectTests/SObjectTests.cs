﻿using SupergodEngine;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting.SObjectTests
{
    [TestClass]
    public class SObjectTests : SupergodTestClass
    {
        class EmptyObject : SObject
        {
            private bool destroyed;
            public bool isDestroyed
            {
                get { return destroyed; }
            }

            protected override void OnDestroy()
            {
                destroyed = true;

                base.OnDestroy();
            }
        }

        EmptyObject testObj;

        public SObjectTests()
        {
            testObj = new EmptyObject();
        }

        [TestMethod]
        public void DestroyTest()
        {
            Assert.IsFalse(testObj.isDestroyed);
            SObject.Destroy(testObj);
            Assert.IsTrue(testObj.isDestroyed);

            testObj = new EmptyObject();
            Assert.IsFalse(testObj.isDestroyed);
            testObj.Destroy();
            Assert.IsTrue(testObj.isDestroyed);
        }

        [TestMethod]
        public void EqualityTests()
        {
            Assert.IsTrue(testObj);
            testObj.Destroy();
            Assert.IsFalse(testObj);

            EmptyObject testObj2 = new EmptyObject();
            Assert.IsTrue(testObj != testObj2);
            testObj2.Destroy();
            Assert.IsTrue(testObj == testObj2);

            testObj = new EmptyObject();
            Assert.IsTrue(!testObj.Equals(testObj2));
            testObj2 = testObj;
            Assert.IsTrue(testObj.Equals(testObj2));
        }
    }
}