﻿using SupergodEngine;
using SupergodUtilities.DataStructures;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace SupergodEngineTesting
{
    [TestClass]
    public class SubobjectCollectionTests : SupergodTestClass
    {
        class TestType : SObject
        {
            public bool added = false;
            public bool removed = false;
        }

        [TestMethod]
        public void AddGetRemoveAndClear1Tests()
        {
            SubobjectCollection<TestType> collection = new SubobjectCollection<TestType>(
                (subobj, subobjectType) =>
                {
                    subobj.added = true;
                    return subobj;
                },

                (subobj) =>
                {
                    subobj.removed = true;
                    return subobj;
                });

            TestType subobject = new TestType();
            Assert.IsFalse(subobject.added);
            
            subobject = collection.Add<TestType>("Test");
            Assert.IsTrue(subobject.added);
            Assert.AreEqual(collection.GetFirst("Test"), subobject);
            Assert.AreEqual(collection.GetFirst(typeof(TestType)), subobject);
            Assert.AreEqual(collection.GetFirst<TestType>(), subobject);
            Assert.AreEqual(collection.GetFirst<TestType>("Test"), subobject);
            Assert.AreEqual(collection.GetFirst(typeof(TestType), "Test"), subobject);

            Assert.AreEqual(collection.GetAll<TestType>()[0], subobject);
            Assert.AreEqual(collection.GetAll(typeof(TestType))[0], subobject);
            Assert.AreEqual(collection.GetAll()[0], subobject);

            subobject = collection.Add(typeof(TestType), "Test2");
            Assert.IsTrue(subobject.added);
            Assert.AreNotEqual(collection.GetFirst<TestType>(), subobject);
            Assert.AreNotEqual(collection.GetFirst("Test"), subobject);
            Assert.AreEqual(collection.GetFirst<TestType>("Test2"), subobject);
            Assert.AreEqual(collection.GetFirst(typeof(TestType), "Test2"), subobject);
            Assert.AreEqual(collection.GetFirst("Test2"), subobject);

            Assert.AreEqual(collection.GetAll<TestType>()[1], subobject);
            Assert.AreEqual(collection.GetAll(typeof(TestType))[1], subobject);
            Assert.AreEqual(collection.GetAll()[1], subobject);

            Assert.IsNull(collection.GetFirst(typeof(IComparable)));
            Assert.IsNull(collection.GetFirst<IComparable>());
            Assert.IsNull(collection.GetFirst("BlaBla"));

            bool thingHappened = false;
            try
            {
                subobject = collection.Add<TestType>("Test2");
            }
            catch (ArgumentException)
            {
                thingHappened = true;
            }
            Assert.IsTrue(thingHappened);

            thingHappened = false;
            try
            {
                collection.Remove(subobject);
            }
            catch (ArgumentException)
            {
                thingHappened = true;
            }
            Assert.IsTrue(thingHappened);
            Assert.IsNotNull(thingHappened);

            subobject = collection.Add<TestType>("Removeable", false);
            AssertUtils.IsNotNull(subobject);
            Assert.IsNotNull(collection.GetFirst<TestType>("Removeable"));
            Assert.IsFalse(subobject.removed);

            collection.Remove(subobject);
            AssertUtils.IsNull(subobject);
            Assert.IsNull(collection.GetFirst<TestType>("Removeable"));
            Assert.IsTrue(subobject.removed);

            int oldCount = collection.Count;
            subobject = collection.GetOrAdd<TestType>("Test3", true);
            Assert.AreEqual(collection.Count, oldCount + 1);
            oldCount = collection.Count;

            subobject = collection.GetOrAdd<TestType>("Test4", false);
            Assert.AreEqual(oldCount, collection.Count);
            AssertUtils.IsNotNull(subobject);

            subobject = collection.GetOrAdd<TestType>("Test", true);
            Assert.AreEqual(oldCount, collection.Count);
            AssertUtils.IsNotNull(subobject);
            Assert.AreEqual(subobject, collection.GetOrAdd<TestType>("Test", false));

            bool isEmpty = collection.ClearOptional();
            Assert.IsFalse(isEmpty);
            Assert.IsTrue(collection.Count == 3);
            Assert.IsTrue(collection.RequiredCount == collection.Count);
            Assert.IsTrue(collection.OptionalCount == 0);
        }

        [TestMethod]
        public void IndexerAndClear2Tests()
        {
            const int COUNT = 10;

            var collection = new SubobjectCollection<TestType>();
            var array = new TestType[COUNT];

            for (int i = 0; i < COUNT; i++)
                array[i] = collection.Add<TestType>("Test" + i, false);

            Assert.AreEqual(collection.RequiredCount, 0);
            Assert.AreEqual(collection.OptionalCount, COUNT);
            Assert.AreEqual(collection.Count, collection.OptionalCount);

            for (int i = 0; i < COUNT; i++)
                Assert.AreEqual(array[i], collection[i]);

            Assert.IsTrue(collection.ClearOptional());
            Assert.AreEqual(collection.Count, 0);
        }
    }
}