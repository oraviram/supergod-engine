﻿using SupergodUtilities.Math;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SupergodEngineTesting
{
    public class SupergodTestClass
    {
        private Random rand;
        
        public SupergodTestClass()
        {
            rand = new Random();
        }

        private TestContext _testContext;
        public TestContext TestContext
        {
            get { return _testContext; }
            set { _testContext = value; }
        }
        
        protected void Log(object line)
        {
            TestContext.WriteLine(line.ToString());
        }

        protected int RandInt(int min, int max) { return rand.Next(min, max); }
        protected int RandInt(int max) { return rand.Next(max); }
        protected int RandInt() { return rand.Next(); }
        protected byte RandByte() { return (byte)RandInt(0, byte.MaxValue); }
        protected uint RandUInt() { return (uint)(RandInt(int.MinValue, int.MaxValue) + int.MaxValue); }

        /// <summary>
        /// Generates a random float between 0 and 1.
        /// </summary>
        protected float RandFloat()
        {
            return (float)rand.NextDouble();
        }

        protected float RandFloat(float min, float max)
        {
            return RandFloat() * (max - min) + min;
        }

        /// <summary>
        /// Generates a random float between -100 and 100.
        /// </summary>
        protected float RandFloat100()
        {
            return RandFloat(-100, 100);
        }

        protected Angle RandAngle()
        {
            return RandFloat(0, Math1D.PI * 2);
        }
        
        /// <summary>
        /// Runs a test multiple times with customization to how it's ran.
        /// </summary>
        /// <param name="start">The start index of the loop.</param>
        /// <param name="end">The end index of the loop.</param>
        /// <param name="speed">How much should the current counter in the loop be increased by each iteration?</param>
        /// <param name="test">The test to run.</param>
        protected void TestMultiple(float start, float end, float speed, Action<float> test)
        {
            for (float i = start; i < end; i += speed)
            {
                test(i);
            }
        }

        /// <summary>
        /// Runs a test length amount of times.
        /// </summary>
        /// <param name="length">The amount of times to run the test.</param>
        /// <param name="test">The test to run.</param>
        protected void TestMultiple(int length, Action<int> test)
        {
            TestMultiple(0, length, 1, (f) => test((int)f));
        }

        /// <summary>
        /// Runs test 30 times.
        /// </summary>
        /// <param name="test">The test to run.</param>
        protected void Test50(Action<int> test)
        {
            TestMultiple(30, test);
        }

        /// <summary>
        /// Runs a test multiple times that gives you an angle (should cover enough angles to test with).
        /// </summary>
        /// <param name="test">The test to run.</param>
        protected void TestAngle(Action<Angle> test)
        {
            TestMultiple(0, Math1D.PI * 2, Math1D.PI / 128, (f) => test(f));
        }

        /// <summary>
        /// Runs a test counting from 0 to 1 with a speed of speed.
        /// </summary>
        /// <param name="speed">How much should the counter increase by each iteration?</param>
        /// <param name="test">The test to run.</param>
        protected void Test01(float speed, Action<float> test)
        {
            TestMultiple(0, 1, speed, test);
        }

        /// <summary>
        /// Runs a test counting from 0 to 1 with a speed of 0.01.
        /// </summary>
        /// <param name="test">The test to run each iteration.</param>
        protected void Test01(Action<float> test)
        {
            Test01(.01f, test);
        }
    }

    public static class AssertUtils
    {
        public static void IsNull(object obj)
        {
            Assert.IsTrue(obj.Equals(null), "IsNull failed.");
        }

        public static void IsNotNull(object obj)
        {
            Assert.IsTrue(!obj.Equals(null), "IsNotNull failed.");
        }

        /// <summary>
        /// This is a temporary method until rotators become supergod mathematics.
        /// </summary>
        public static void TooFar(Quaternion a, Quaternion b, float threshold = Math1D.Epsilon)
        {
            Assert.IsFalse(a.CloseEnough(b, threshold), $"TooFar failed! a = {a}, b = {b}, threshold = {threshold}");
        }

        public static void CloseEnough<T>(T a, T b, float threshold = Math1D.Epsilon)
            where T : struct, ISupergodEquatable<T>
        {
            Assert.IsTrue(a.CloseEnough(b, threshold), $"CloseEnough failed! a = {a}, b = {b}, threshold = {threshold}");
        }

        public static void CloseEnough(float a, float b, float threshold = Math1D.Epsilon)
        {
            Assert.IsTrue(a.CloseEnough(b, threshold), $"CloseEnough failed! a = {a}, b = {b}, threshold = {threshold}");
        }

        public static void TooFar(float a, float b, float threshold = Math1D.Epsilon)
        {
            Assert.IsFalse(a.CloseEnough(b, threshold), $"TooFar failed! a = {a}, b = {b}, threshold = {threshold}");
        }
    }
}