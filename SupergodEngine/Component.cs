﻿namespace SupergodEngine
{
    /// <summary>
    /// Base class for all components.
    /// </summary>
    public abstract class Component : SObject, ISupergodEventReciever
    {
        /// <summary>Scene object this component is a part of.</summary>
        public SceneObject SceneObject
        {
            get { return _sceneObject; }
            internal set { _sceneObject = value; }
        }
        private SceneObject _sceneObject;
        
        /// <summary>
        /// Called when the game starts or when the object is spawned at runtime.
        /// </summary>
        public virtual void OnSpawned() { }

        /// <summary>
        /// Called every update frame.
        /// </summary>
        /// <param name="deltaTime">Time since the last frame.</param>
        public virtual void OnUpdate(float deltaTime) { }
    }
}