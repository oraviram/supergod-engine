﻿namespace SupergodEngine
{
    /// <summary>
    /// An interface classes that need to respond to supergod events can implement.
    /// Note that it won't give you with any callbacks, and it's only here to organize things.
    /// </summary>
    public interface ISupergodEventReciever
    {
        void OnSpawned();
        void OnUpdate(float deltaTime);
    }
}