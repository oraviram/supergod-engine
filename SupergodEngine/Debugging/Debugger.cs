﻿using System;

namespace SupergodEngine.Debugging
{
    /// <summary>
    /// Class containing information to do debug-related stuff at runtime.
    /// </summary>
    public static class Debugger
    {
        /// <summary>
        /// Logs an error to the console with a message of errorMessage.
        /// </summary>
        public static void LogError(object errorMessage)
        {
            LogWithColor(ConsoleColor.Red, errorMessage);
        }

        /// <summary>
        /// Logs a warning to the console with a message of warningMessage.
        /// </summary>
        public static void LogWarning(object warningMessage)
        {
            LogWithColor(ConsoleColor.Yellow, warningMessage);
        }

        /// <summary>
        /// Sends a log to the console with with the log of message.
        /// </summary>
        public static void Log(object message)
        {
            LogWithColor(ConsoleColor.Cyan, message);
        }

        // Private method just used to log something to the console with a color of ConsoleColor and message of message.
        private static void LogWithColor(ConsoleColor color, object message)
        {
            ConsoleColor oldColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(message + "\n");
            Console.ForegroundColor = oldColor;
        }
    }
}