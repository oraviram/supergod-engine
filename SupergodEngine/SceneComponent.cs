﻿using SupergodUtilities.Math;
using SupergodUtilities.DataStructures;

namespace SupergodEngine
{
    /// <summary>
    /// Represents a component that has a transform of its own.
    /// </summary>
    public class SceneComponent : Component
    {
        /// <summary>
        /// All of the modifiers (sub-components) on this component.
        /// </summary>
        public SubobjectCollection<Modifier> Modifiers
        {
            get
            {
                if (_modifiers == null)
                {
                    _modifiers = new SubobjectCollection<Modifier>(
                        (modifier, type) =>
                        {
                            modifier.component = this;
                            return modifier;
                        });
                }
                return _modifiers;
            }
        }
        private SubobjectCollection<Modifier> _modifiers;

        #region Transform information.
        /// <summary>
        /// How should the transform of this and the transform of the parent be combined?
        /// </summary>
        public Transform.CombinationRules transformInheritance = Transform.CombinationRules.Default;

        /// <summary>
        /// Parent-relative transform of the component. Same as the world transform if there is no parent.
        /// </summary>
        public virtual Transform LocalTransform { get; set; } = Transform.identity;

        /// <summary>Shortcut for the localTransform's position.</summary>
        public Vector3D LocalPosition
        {
            get { return LocalTransform.position; }
            set { LocalTransform = new Transform(value, LocalTransform.rotation, LocalTransform.scale); }
        }

        /// <summary>Shortcut for the localTransform's rotation.</summary>
        public Quaternion LocalRotation
        {
            get { return LocalTransform.rotation; }
            set { LocalTransform = new Transform(LocalTransform.position, value, LocalTransform.scale); }
        }

        /// <summary>Shortcut for the localTransform's scale.</summary>
        public Vector3D LocalScale
        {
            get { return LocalTransform.scale; }
            set { LocalTransform = new Transform(LocalTransform.position, LocalTransform.rotation, value); }
        }

        /// <summary>World-relative transform of the component. Note that the setter only attempts to set the world transform, and that it might not be 100% accurate.</summary>
        public Transform WorldTransform
        {
            get
            {
                if (Parent)
                    return LocalTransform.CombineTransformations(Parent.WorldTransform, transformInheritance);

                return LocalTransform;
            }

            set
            {
                if (Parent)
                    LocalTransform = value.DiscombineTransformations(Parent.WorldTransform, transformInheritance);

                LocalTransform = value;
            }
        }

        /// <summary>Shortcut for the worldTransform's position.</summary>
        public Vector3D WorldPosition
        {
            get { return WorldTransform.position; }
            set { WorldTransform = new Transform(value, WorldTransform.rotation, WorldTransform.scale); }
        }

        /// <summary>Shortcut for the worldTransform's rotation.</summary>
        public Quaternion WorldRotation
        {
            get { return WorldTransform.rotation; }
            set { WorldTransform = new Transform(WorldTransform.position, value, WorldTransform.scale); }
        }

        /// <summary>Shortcut for the worldTransform's scale.</summary>
        public Vector3D WorldScale
        {
            get { return WorldTransform.scale; }
            set { WorldTransform = new Transform(WorldTransform.position, WorldTransform.rotation, value); }
        }
        #endregion

        #region Parenting system.
        /// <summary>Parent of this component (null if there this component is the root component of the scene component).</summary>
        public SceneComponent Parent
        {
            get { return _parent; }
        }
        private SceneComponent _parent;

        /// <summary>
        /// Sets the parent of this scene component to newParent.
        /// </summary>
        /// <param name="newParent">The new parent of this scene component.</param>
        /// <param name="transformSnapRules">
        /// Optinally you can make the transform of this be combined with the transform of parent immediately.<para/>
        /// This will NOT modify <seealso cref="transformInheritance"/>, but is just an option to combine it when adding (to make the offset from the new parent the same as the offset from the old parent).
        /// </param>
        public void SetParent(SceneComponent newParent, Transform.CombinationRules transformSnapRules = Transform.CombinationRules.DontCombine)
        {
            if (!newParent)
            {
                _parent = null;
                return;
            }
            _parent = newParent;
            LocalTransform = LocalTransform.CombineTransformations(Parent.WorldTransform, transformSnapRules);
        }
        #endregion
        
        public override void OnSpawned()
        {
            base.OnSpawned();

            foreach (Modifier modifier in Modifiers)
            {
                modifier.OnSpawned();
            }
        }

        public override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);

            for (int i = Modifiers.Count - 1; i >= 0; i--)
            {
                Modifier modifier = Modifiers[i];
                modifier.OnUpdate(deltaTime);

                if (!modifier)
                    Modifiers.Remove(modifier);
            }
        }

        protected override void OnDestroy()
        {
            foreach (Modifier modifier in Modifiers)
                Destroy(modifier);

            base.OnDestroy();
        }
    }
}