﻿using SupergodUtilities.DataStructures;

namespace SupergodEngine
{
    /// <summary>
    /// Base class for all supergod objects.
    /// </summary>
    public abstract class SObject : INamed, IDestroyable
    {
        public virtual string Name { get; set; }
        private bool isDestroyed;

        public SObject()
        {
            Name = "New " + GetType().Name;
        }

        public static void Destroy(SObject obj)
        {
            if (obj.isDestroyed)
                return;

            obj.OnDestroy();
        }

        public void Destroy()
        {
            Destroy(this);
        }

        /// <summary>
        /// Called whenever the object is destroyed.
        /// Note that the base implementation must ALWAYS be called AFTER the custom implementation.
        /// </summary>
        protected virtual void OnDestroy()
        {
            isDestroyed = true;
        }

        /// <summary>
        /// Returns the name of the object.
        /// </summary>
        public override string ToString()
        {
            if (!this)
                return "Null";

            return Name;
        }

        public override bool Equals(object obj)
        {
            if (obj is SObject || obj == null)
                return this == (SObject)obj;

            return false;
        }

        /// <summary>
        /// I am a very questionable method that tries to do stuff, but I should probably get a better implementation later...
        /// Why did Microsoft put GetHashCode in System.Object and not some interface...?
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Is the object valid?
        /// </summary>
        /// <param name="obj">False if obj has either been destroyed or is null.</param>
        public static implicit operator bool(SObject obj)
        {
            return obj != null;
        }

        public static bool operator ==(SObject left, SObject right)
        {
            if ((object)left == null || left.isDestroyed)
                return (object)right == null || right.isDestroyed;

            return (object)left == right;
        }

        public static bool operator !=(SObject left, SObject right) { return !(left == right); }
    }
}