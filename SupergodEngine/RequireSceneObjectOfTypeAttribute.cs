﻿using System;

namespace SupergodEngine
{
    /// <summary>
    /// An attribute that can be put on a <seealso cref="Component"/> to limit the types of scene objects the component can be put on.
    /// The component can optionally be put on a class deriving from the desired type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class RequireSceneObjectOfTypeAttribute : Attribute
    {
        /// <summary>
        /// The type of the scene object that is required.
        /// </summary>
        public readonly Type sceneObjectType;

        /// <summary>
        /// Can the component be put on scene objects that inherit from <seealso cref="sceneObjectType"/>?
        /// </summary>
        public readonly bool allowInheritedTypes;

        /// <summary>
        /// Creates a new RequireSceneObjectOfType.
        /// </summary>
        /// <param name="sceneObjectType">The type of the scene object that is required.</param>
        /// <param name="allowInheritedTypes">Can the component be put on types that aren't the desired type but do inherit from it?</param>
        public RequireSceneObjectOfTypeAttribute(Type sceneObjectType, bool allowInheritedTypes = true)
        {
            this.sceneObjectType = sceneObjectType;
            this.allowInheritedTypes = allowInheritedTypes;
        }
    }
}