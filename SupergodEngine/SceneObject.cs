﻿using SupergodUtilities.Math;
using SupergodUtilities.DataStructures;

namespace SupergodEngine
{
    /// <summary>
    /// A scene object is an object that can be placed in a scene.
    /// </summary>
    public class SceneObject : SObject, ISupergodEventReciever
    {
        /// <summary>
        /// A short for accessing the world transform of the root component.
        /// </summary>
        public Transform Transform
        {
            get { return RootComponent.WorldTransform; }
            set { RootComponent.WorldTransform = value; }
        }

        /// <summary>Shortcut for changing the root component's position.</summary>
        public Vector3D Position
        {
            get { return Transform.position; }
            set { Transform = new Transform(value, Transform.rotation, Transform.scale); }
        }

        /// <summary>Shortcut for changing the root component's rotation.</summary>
        public Quaternion Rotation
        {
            get { return Transform.rotation; }
            set { Transform = new Transform(Transform.position, value, Transform.scale); }
        }

        /// <summary>Shortcut for changing the root component's scale.</summary>
        public Vector3D Scale
        {
            get { return Transform.scale; }
            set { Transform = new Transform(Transform.position, Transform.rotation, value); }
        }

        /// <summary>The root component of a scene object is the scene component that's a parent of all the other scene components.
        /// This is what contains the transform of the SceneObject.<para></para>
        /// Note that to set it you need to call <seealso cref="SetRootComponent(SceneComponent, bool)"/>.</summary>
        public SceneComponent RootComponent
        {
            get { return _rootComponent; }
        }
        private SceneComponent _rootComponent;

        /// <summary>Sets the root component of the SceneObject to newRootComponent.</summary>
        /// <param name="removeOldRootComponent">Should the old root component be removed from the components list or just become one of the children?</param>
        /// <param name="saveTransformFromOldRootComponent">Should the transform of the old root component be copied to the transform of the new root component?</param>
        public void SetRootComponent(SceneComponent newRootComponent, bool removeOldRootComponent = true, bool saveTransformFromOldRootComponent = true)
        {
            Transform transform = newRootComponent.WorldTransform;
            if (RootComponent)
            {
                if (saveTransformFromOldRootComponent)
                    transform = RootComponent.WorldTransform;

                if (removeOldRootComponent)
                    Destroy(RootComponent);
                else
                    RootComponent.SetParent(newRootComponent);
            }
            _rootComponent = newRootComponent;
            RootComponent.WorldTransform = transform;

            foreach (Component component in Components)
            {
                SceneComponent sceneComponent = component as SceneComponent;
                if (!sceneComponent || sceneComponent == RootComponent || sceneComponent.Parent)
                    continue;

                sceneComponent.SetParent(RootComponent);
            }
        }

        public SceneObject()
        {
            _rootComponent = Components.Add<SceneComponent>("Default Scene Root", false);
        }

        #region Components collection.
        /// <summary>
        /// The subobject collection of the components on this scene object.
        /// </summary>
        public SubobjectCollection<Component> Components
        {
            get
            {
                if (_components == null)
                {
                    _components = new SubobjectCollection<Component>(
                        (component, type) =>
                        {
                            RequireSceneObjectOfTypeAttribute[] requireSceneObjectAttribs =
                                (RequireSceneObjectOfTypeAttribute[])type.GetCustomAttributes(typeof(RequireSceneObjectOfTypeAttribute), true);

                            for (int i = 0; i < requireSceneObjectAttribs.Length; i++)
                            {
                                RequireSceneObjectOfTypeAttribute attribute = requireSceneObjectAttribs[i];
                                if ((GetType() != attribute.sceneObjectType && !attribute.allowInheritedTypes) || 
                                    (!GetType().IsSubclassOf(attribute.sceneObjectType) && attribute.allowInheritedTypes))
                                {
                                    Destroy(component);
                                    return null;
                                }
                            }

                            if (!IsValidComponent(component))
                            {
                                Destroy(component);
                                return null;
                            }
                            component.SceneObject = this;

                            if (component is SceneComponent sceneComponent)
                            {
                                if (RootComponent)
                                    sceneComponent.SetParent(RootComponent);
                            }
                            return component;
                        });
                }
                return _components;
            }
        }
        private SubobjectCollection<Component> _components;

        /// <summary>
        /// Called whenever a component is added and used to verify the component is valid for the current object.
        /// Default implementation just returns true, but you can override this to verify that a component is fine before adding it.
        /// </summary>
        /// <param name="component">The component being added.</param>
        protected virtual bool IsValidComponent(Component component) { return true; }
        #endregion

        /// <summary>
        /// Called when the game starts or when the object is spawned at runtime.
        /// ALWAYS call the base implementation BEFORE your implementation.
        /// </summary>
        public virtual void OnSpawned()
        {
            foreach (Component component in Components)
            {
                component.OnSpawned();
            }
        }

        /// <summary>
        /// Called before every frame is rendered. Use to make updates to the game state.
        /// ALWAYS call the base implementation BEFORE your implementation.
        /// </summary>
        /// <param name="deltaTime">Time since the last frame.</param>
        public virtual void OnUpdate(float deltaTime)
        {
            for (int i = Components.Count - 1; i >= 0; i--)
            {
                Component component = Components[i];
                component.OnUpdate(deltaTime);

                if (!component)
                    Components.Remove(component);
            }
        }

        /// <summary>Makes sure to destroy all the components in the SceneObject before calling <seealso cref="SObject.OnDestroy"/>.
        /// Base implementation should ALWAYS be called AFTER your implementation.</summary>
        protected override void OnDestroy()
        {
            foreach (Component component in Components)
                Destroy(component);

            _components = null;
            base.OnDestroy();
        }
    }
}