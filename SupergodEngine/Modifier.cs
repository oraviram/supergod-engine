﻿namespace SupergodEngine
{
    /// <summary>
    /// A modifier is like a sub-component that is attached to a <seealso cref="SceneComponent"/> and modifies it.
    /// </summary>
    public abstract class Modifier : SObject, ISupergodEventReciever
    {
        public SceneComponent component { get; internal set; }

        public virtual void OnSpawned() { }
        public virtual void OnUpdate(float deltaTime) { }
    }
}