﻿using SupergodEngine;
using SupergodEngine.Debugging;

namespace SupergodEngineSandboxGame
{
    class TestModifier : Modifier
    {
        public override void OnSpawned()
        {
            base.OnSpawned();
            Debugger.Log("Spawned");
        }

        public override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);
            Debugger.Log("Update");
        }
    }

    class Program
    {
        static void Main()
        {
            SceneObject obj = new SceneObject();
            obj.RootComponent.Modifiers.Add<TestModifier>("Mahmodifier");
            obj.OnSpawned();
            while (true)
            {
                obj.OnUpdate(0);
                System.Threading.Thread.Sleep(5);
            }
        }
    }
}